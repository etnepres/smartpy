variables:
  GIT_SUBMODULE_STRATEGY: recursive

default:
  tags: [shell]

all:
  stage: build
  artifacts:
    when: always
    paths:
      - _build/git_status.txt
      - _build/test/
      - _build/**/*.ok
      - _build/**/*.log.txt
      - _mockup.*/
      - _sandbox.*/
      - test_baselines/

  script:
    - export NIX_PATH=nixpkgs=$HOME/.nix-defexpr/channels/nixpkgs

    # Avoid being spammed with "warning: setlocale: LC_ALL: cannot change locale (en_US.UTF-8)":
    - glibcLocales=$(nix-build --no-out-link "<nixpkgs>" -A glibcLocales)
    - export LOCALE_ARCHIVE_2_27="${glibcLocales}/lib/locale/locale-archive"

    # Opam switch:
    - env/nix/cache.sh with env/nix/switches /cache/switches.$(sha256sum env/switch.export | awk '{print $1}')
        env/nix/init

    # Zcash params:
    - env/nix/cache.sh with ~/.zcash-params/ /cache/zcash-params/
        ./with_env make _build/zcash.ok

    # Tezos binaries:
    - cache=/cache/tezos-binaries.$(sha256sum scripts/fetch_tezos_binaries | awk '{print $1}')
    - env/nix/cache.sh get ext/tezos-binaries $cache && touch _build/local-tezos.ok _build/local-tezos-binaries.ok
    - ./with_env make _build/local-tezos-binaries.ok
    - env/nix/cache.sh put ext/tezos-binaries $cache

    # We do sandboxes before common in the CI because:
    # - common has already been tested usually
    # - it should be faster because sandbox FA2 is so slow that we want to start it as early as possible.

    - ./with_env make -j8 test-mockup test-common _build/cli-test.ok packages-ci-test

    - git status > _build/git_status.txt
    - git diff --exit-code test_baselines
    - git diff --exit-code packages/packages/ts-syntax/tests
    - (! git status --porcelain | grep '^??')
