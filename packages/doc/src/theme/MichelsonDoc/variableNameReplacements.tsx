import React from 'react';

const variablename_replacements = [
    [
        /(\$[a-zA-Z0-9_]+)/g,
        (content) => {
            return <span className="variableName">{content}</span>;
        },
    ],
];

export default variablename_replacements;
