import React from 'react';

import useSyntaxContext, { SYNTAX } from '../hooks/useSyntaxContext';
import styles from './styles.module.css';

const SyntaxSelector: React.FC = () => {
    const { syntax, setSyntax } = useSyntaxContext();
    return (
        <div className={styles.dropdown}>
            {Object.keys(SYNTAX)
                .filter((s) => (process.env.NODE_ENV !== 'development' && SYNTAX[s] === SYNTAX.ML ? false : true))
                .map((key) => (
                    <button
                        key={key}
                        className={`button button--block button--primary margin--xs ${
                            syntax === SYNTAX[key] ? '' : 'button--outline'
                        }`}
                        onClick={() => setSyntax(SYNTAX[key])}
                    >
                        {key}
                    </button>
                ))}
        </div>
    );
};

export default SyntaxSelector;
