import React from 'react';

import useSyntaxContext, { SYNTAX } from '../hooks/useSyntaxContext';

const SelectPython: React.FC = () => {
    const { setSyntax } = useSyntaxContext();
    return (
        <button className={`button button--primary`} onClick={() => setSyntax(SYNTAX.PY)}>
            {SYNTAX.PY}
        </button>
    );
};

export default SelectPython;
