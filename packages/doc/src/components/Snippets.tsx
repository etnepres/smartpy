import React from 'react';
import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';
import Code from './CodeBlock';

const TYPESCRIPT = `
interface TStorage {
    storedValue: TNat;
}

@Contract
class StoreValue {
    storage: TStorage = {
        storedValue: 1,
    };

    @EntryPoint
    replace(newValue: TInt) {
        this.storage.storedValue = newValue;
    }

    @EntryPoint
    double() {
        this.storage.storedValue *= 2;
    }

    @EntryPoint
    divide(divisor: TInt) {
        this.storage.storedValue /= divisor;
    }
}
`;

const PYTHON = `
import smartpy as sp

class StoreValue(sp.Contract):
  def __init__(self, value):
      self.init(storedValue = value)

  @sp.entry_point
  def replace(self, value):
      self.data.storedValue = value

  @sp.entry_point
  def double(self):
      self.data.storedValue *= 2

@sp.add_test(name = "StoreValue")
def test():
  scenario = sp.test_scenario()
  scenario.h1("Store Value")
  contract = StoreValue(1)
  scenario += contract
  scenario += contract.replace(2)
  scenario += contract.double()
`;

const OCAML = `
open Smartml

module Contract = struct

  let%entry_point divide self params =
    verify (params.divisor > 5);
    self.data.storedValue //= params.divisor

  let%entry_point double self () =
    self.data.storedValue *= 2

  let%entry_point replace self params =
    self.data.storedValue <- params.value

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(storedValue = nat).layout("storedValue")
      ~storage:[%expr {storedValue = 12}]
      [divide; double; replace]
end
`;

const MICHELSON = `
parameter (or (unit %double) (int %replace));
storage   int;
code
  {
    UNPAIR;     # @parameter : @storage
    IF_LEFT
      {
        DROP;       # @storage
        PUSH int 2; # int : @storage
        MUL;        # int
      }
      {
        SWAP;       # @storage : @parameter%replace
        DROP;       # @parameter%replace
      }; # int
    NIL operation; # list operation : int
    PAIR;       # pair (list operation) int
  };
`;

const numberOfLines = (code: string) => code.split('\n').length - 1;
const adjustLines = (max: number, code: string) => {
    const lines = numberOfLines(code);
    return `${code}${'\n'.repeat(max > lines ? MAX_LENGTH - lines : 0)}`;
};
const MAX_LENGTH = [MICHELSON, TYPESCRIPT, PYTHON, OCAML].reduce((acc, code) => {
    const lines = numberOfLines(code);
    return acc > lines ? acc : lines;
}, 0);
const SYNTAX = {
    MICHELSON: adjustLines(MAX_LENGTH, MICHELSON),
    TYPESCRIPT: adjustLines(MAX_LENGTH, TYPESCRIPT),
    PYTHON: adjustLines(MAX_LENGTH, PYTHON),
    OCAML: adjustLines(MAX_LENGTH, OCAML),
};

const Snippets: React.FC = () => {
    const MichelsonComponent = () => (
        <div className="col col--6">
            <Code language="lisp" title="storeValue.tz">
                {SYNTAX.MICHELSON}
            </Code>
        </div>
    );

    return (
        <Tabs
            defaultValue="py"
            values={[
                { label: 'Python', value: 'py' },
                { label: 'Typescript', value: 'ts' },
                { label: 'Ocaml', value: 'ml' },
            ]}
        >
            <TabItem value="py">
                <div className="row">
                    <div className="col col--6">
                        <Code language="python" title="storeValue.py">
                            {SYNTAX.PYTHON}
                        </Code>
                    </div>
                    <MichelsonComponent />
                </div>
            </TabItem>

            <TabItem value="ts">
                <div className="row">
                    <div className="col col--6">
                        <Code language="typescript" title="storeValue.ts">
                            {SYNTAX.TYPESCRIPT}
                        </Code>
                    </div>
                    <MichelsonComponent />
                </div>
            </TabItem>

            <TabItem value="ml">
                <div className="row">
                    <div className="col col--6">
                        <Code language="ocaml" title="storeValue.ml">
                            {SYNTAX.OCAML}
                        </Code>
                    </div>
                    <MichelsonComponent />
                </div>
            </TabItem>
        </Tabs>
    );
};

export default Snippets;
