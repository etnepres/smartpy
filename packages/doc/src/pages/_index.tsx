import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import styles from './index.module.css';
import Mesh from '../components/animated/Mesh';
import Snippets from '../components/Snippets';

function HomepageHeader() {
    return (
        <header className={clsx('hero hero--primary', styles.heroBanner)}>
            <div className="container">
                <Snippets />
                <div className={styles.buttons}>
                    <Link className="button button--secondary button--lg" to="/docs">
                        Check the documentation
                    </Link>
                </div>
            </div>
        </header>
    );
}

export default function Home() {
    const { siteConfig } = useDocusaurusContext();
    return (
        <Layout title={siteConfig.title} description="SmartPy Documentation">
            <Mesh className={styles.mesh} />
            <HomepageHeader />
            {/* <main>
                <HomepageFeatures />
            </main> */}
        </Layout>
    );
}
