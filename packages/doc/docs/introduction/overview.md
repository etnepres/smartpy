---
sidebar_position: 1
slug: /
---
# Overview

### What is SmartPy?

SmartPy is a complete system to develop smart contracts for the Tezos blockchain.

<br/>

![SmartPy Overview](/img/tech-overview.svg)

<br/>

:::note About
`Python` and `Typescript` are used to generate programs in an imperative, type inferred, and intermediate language called SmartML. SmartML is the name of the `OCaml` library that provides an interpreter, a compiler to Michelson, and a scenario *"on-chain"* interpreter. The platform uses a mix of OCaml translated to pure javascript through js_of_ocaml, Python, and Typescript to glue everything together. The command-line interface is also built using js_of_ocaml and runs on Node.js.
:::
### What does SmartPy offer?
- An embedded Domain Specific Language (DSL) in **Python** that gives users the ability to customize their smart-contracts with as many configurations as they want by leveraging meta-programming;
- A **Typescript** syntax, which offers a great experience and power for javascript and typescript programmers;
- An online playground where users can write and test their smart contracts, including the ability to quickly share their code;
  - ([Python IDE](https://smartpy.io/ide))
  - ([Typescript IDE](https://smartpy.io/ts-ide))
  - ([Ocaml IDE](https://smartpy.io/ml-ide))
  - ([Michelson IDE](https://smartpy.io/michelson))
- A competent origination page to deploy smart contracts in various Tezos networks ([Origination Page](https://smartpy.io/origination));
- An explorer that allows users to interact with their deployed contracts ([Explorer Page](https://smartpy.io/explorer.html));
- A command-line interface ([CLI](/cli));
