# Meta-Programming

import Snippet, {SYNTAX} from '@theme/Syntax/Snippet';
import SelectPython from '@theme/Syntax/SelectPython';

<Snippet syntax={SYNTAX.PY}>

The functions described here are used to _construct_ a smart contract. Smart contracts are executed once they are deployed in the Tezos blockchain (although they can be simulated). This is indeed _meta-programming_: we can write a program that writes a program, i.e., constructs a contract.

Note that in the example `self.data.x + 2`, the actual addition isn't carried out until the contract has been deployed and the entry point is called.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

SmartTS doesn't support meta-programming.

Switch to <SelectPython /> syntax if you prefer meta-programming support.

</Snippet>

<Snippet syntax={SYNTAX.ML}>

In progress...

</Snippet>
