# Command Line Interface

The command line interface, **SmartPy CLI**, also known as
**smartpy-cli**,  has been introduced in the following outdated
**[Medium Post](https://smartpy-io.medium.com/introducing-smartpybasic-a-simple-cli-to-build-tezos-smart-contract-in-python-f5bd8772b74a)**.

It now provides a CLI that works seamlessly on various syntaxes:
SmartPy, SmartTS and SmartML.

## Installation

The SmartPy CLI can be installed in two ways: the direct installation
or through a project.

### Direct Installation

```shell
# To install the last CLI
bash <(curl -s https://smartpy.io/cli/install.sh)

# To install the last CLI and compile SmartML (opam is required)
bash <(curl -s https://smartpy.io/cli/install.sh) --with-smartml

# To install the current version
bash <(curl -s RELEASE_ID/cli/install.sh)

# To install the current version and compile SmartML
bash <(curl -s RELEASE_ID/cli/install.sh) --with-smartml
```

### Project Management

See [Getting Started/Project Management](/introduction/project_management).

## Check version

This command tells which CLI version is installed.

```shell
~/smartpy-cli/SmartPy.sh --version
```

## Dependencies

The **SmartPy CLI** depends on
**[python3](https://www.python.org/downloads/)** and
**[Node.js](https://nodejs.org/)**.

It also depends on **[opam](https://opam.ocaml.org)** for the optional
and experimental support of the SmartML syntax.

## Execution

### Executing a SmartPy Script with its tests

#### `SmartPy.sh test`
Perform tests defined in a `script` (see **[Tests and Scenarios](/scenarios/framework)**).

```shell
~/smartpy-cli/SmartPy.sh test <script> <output-directory>
```

This includes many outputs: **types**, **generated michelson code**, **pretty-printed scenario**, etc.

### Compiling Contracts and Expressions

#### `SmartPy.sh compile`

Compute the Compilation Targets defined in a `script`.

```shell
# For a SmartPy, SmartTS or SmartML script:
~/smartpy-cli/SmartPy.sh compile <script> <output-directory>
```

**Example:**
```shell
# For a SmartPy, SmartTS or SmartML script:
~/smartpy-cli/SmartPy.sh compile welcome.py /tmp/welcome

~/smartpy-cli/SmartPy.sh compile welcome.ts /tmp/welcome

~/smartpy-cli/SmartPy.sh compile welcome.ml /tmp/welcome
```

### Custom Targets

#### `SmartPy.sh kind <kind>`

Similar to tests. Perform scenarios defined in a `script` introduced by the custom target.

```shell
# For a SmartPy, SmartTS or SmartML script:
~/smartpy-cli/SmartPy.sh kind <kind> <script> <output-directory>
```

### Deploying a contract
```shell
# Using Micheline format (.tz)

~/smartpy-cli/SmartPy.sh originate-contract --code code.tz --storage storage.tz --rpc https://hangzhounet.smartpy.io

# Using Michelson format (.json)

~/smartpy-cli/SmartPy.sh originate-contract --code code.json --storage storage.json --rpc https://hangzhounet.smartpy.io

# By default, the originator will use a faucet account.
# But you can provide your own private key as an argument

~/smartpy-cli/SmartPy.sh originate-contract --code code.json --storage storage.json --rpc https://hangzhounet.smartpy.io --private-key edsk...
```

### CLI optional arguments

`SmartPy.sh` takes several optional arguments.

See [Flag](general/flags) for list of flags and protocols.

| Argument                   | Description                                                                                      |
|:--------------------------:|:------------------------------------------------------------------------------------------------:|
| **`--purge`**              | Empty the output directory before writting to it.                                                |
| **`--html`**               | Adds `.html` outputs such as a `log.html` which is identical to the output panel in the Web IDE. |
| **`--<flag>`** `arguments` | Set some [flag with arguments](general/flags#flags-with-arguments).                              |
| **`--<flag>`**             | Activate some boolean [flag](general/flags).                                                     |
| **`--no-<flag>`**          | Deactivate some boolean [flag](general/flags).                                                   |
| **`--mockup`**             | Run in mockup (experimental, needs installed source).                                            |
| **`--sandbox`**            | Run in sandbox (experimental, needs installed source).                                           |
