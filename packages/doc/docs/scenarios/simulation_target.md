# Simulation Targets

As a convenience, one can call `sp.add_simulation_target(contract, ...)` instead of `@sp.add_test ...`.

### Example

```python
sp.add_simulation_target(contract, name="Simulation", shortname=None, profile=False, is_default=True)
```

**`sp.add_simulation_target`** optional parameters are:

- `name="Simulation"`

    Optional parameter with default value = `"Simulation"`.

- `shortname=None`

    Short names need to be unique. It is an optional parameter and is used in smartpy-cli outputs.

- `profile=False`

    Computes and pretty-prints profiling data.

- `is_default=True`

    Determines if the test is performed by default when evaluating all tests. Can be typically used in conjunction with `sp.in_browser` in templates to improve speed in browser.
