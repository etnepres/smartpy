# Bounded Types

import Snippet, {SYNTAX} from '@theme/Syntax/Snippet';
import MichelsonDocLink from '@theme/MichelsonDocLink';

<Snippet syntax={SYNTAX.PY}>

SmartPy has the ability to limit any simple type's values to a finite subset.
The type of bounded values is `sp.TBounded`.

**`sp.TBounded(cases, t=None, final=True)`**

Introduce a finite subset of a simple type whose cases are `cases` (they must be of the same simple type), whose optional type is `t` (otherwise, it is inferred). The optional boolean parameter `final` determines whether the type acts as a constraint that can be extended (`final=False`) or not.

**`sp.bounded(x)`**

Introduce a bounded value where `x` is supposed to be a literal of a simple type.

**`sp.unbounded(x)`**

Forget the bounded structure of a `sp.TBounded(_, t)` and return an expression of type `t`.

For example, the type `sp.TBounded(["red", "green", "blue"])` has exactly three values: `sp.bounded("red")`, `sp.bounded("green")`, `sp.bounded("blue")`.

`sp.unbounded` expects a bounded value and returns its unbounded equivalent, e.g. it goes from `sp.bounded("red")` to just "red" (of type [sp.TString](/general/types#string)).

Note that bounded types can be based on any simple type, e.g. `sp.TBounded([1,2,3])` possible as well.

There is no cost associated to bounded types and values when compiling to Michelson.

</Snippet>


<Snippet syntax={SYNTAX.TS}>

```typescript
Work in progress
```

</Snippet>


<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>
