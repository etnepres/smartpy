import React from 'react';
import { Provider } from 'react-redux';
import { render } from '@testing-library/react';
import { createMemoryHistory } from 'history';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import { createStore } from '../../store';
import { Router } from 'react-router-dom';

const renderWithStore = (Component: React.ReactNode) => {
    const history = createMemoryHistory();
    const { store } = createStore(history);

    const Wrapper: React.FC = () => (
        <Provider store={store}>
            <ThemeProvider theme={createTheme()}>
                <Router history={history}>{Component}</Router>
            </ThemeProvider>
        </Provider>
    );

    return {
        ...render(<Wrapper />),
        // Added `history` to the returned utilities to allow us
        // to reference it in our tests.
        history,
    };
};

export default renderWithStore;
