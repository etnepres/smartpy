import time from '../../utils/time';

const RealDate = Date.now;

beforeEach(() => {
    global.Date.now = jest.fn(() => new Date('2020-08-17T19:40:14Z').getTime());
});

afterEach(() => {
    global.Date.now = RealDate;
});

describe('Time utils (Get Elapsed Time)', () => {
    describe('Get Elapsed Time (one unit)', () => {
        it('Seconds Only', () => {
            expect(time.getElapsedTime('2020-08-17T19:40:00Z')).toBe('14s');
        });
        it('Minutes Only', () => {
            expect(time.getElapsedTime('2020-08-17T19:38:14Z')).toBe('2m');
        });
        it('Hours Only', () => {
            expect(time.getElapsedTime('2020-08-17T18:40:14Z')).toBe('1h');
        });
        it('Days Only', () => {
            expect(time.getElapsedTime('2020-08-16T19:40:14Z')).toBe('1d');
        });
    });

    describe('Get Elapsed Time (two units)', () => {
        it('Seconds & Minutes', () => {
            expect(time.getElapsedTime('2020-08-17T19:39:10Z')).toBe('1m 4s');
        });
        it('Seconds & Hours', () => {
            expect(time.getElapsedTime('2020-08-17T18:40:10Z')).toBe('1h 4s');
        });
        it('Seconds & Days', () => {
            expect(time.getElapsedTime('2020-08-16T19:40:10Z')).toBe('1d 4s');
        });
        it('Minutes & Hours', () => {
            expect(time.getElapsedTime('2020-08-17T18:39:14Z')).toBe('1h 1m');
        });
        it('Minutes & Days', () => {
            expect(time.getElapsedTime('2020-08-16T19:39:14Z')).toBe('1d 1m');
        });
        it('Hours & Days', () => {
            expect(time.getElapsedTime('2020-08-16T18:40:14Z')).toBe('1d 1h');
        });
    });

    describe('Get Elapsed Time (Three units)', () => {
        it('Seconds & Minutes & Hours', () => {
            expect(time.getElapsedTime('2020-08-17T18:39:10Z')).toBe('1h 1m 4s');
        });
        it('Seconds & Minutes & Days', () => {
            expect(time.getElapsedTime('2020-08-16T19:39:10Z')).toBe('1d 1m 4s');
        });
        it('Seconds & Hours & Days', () => {
            expect(time.getElapsedTime('2020-08-16T18:40:10Z')).toBe('1d 1h 4s');
        });
        it('Minutes & Hours & Days', () => {
            expect(time.getElapsedTime('2020-08-16T18:39:14Z')).toBe('1d 1h 1m');
        });
    });

    describe('Get Elapsed Time (All units)', () => {
        it('Seconds & Minutes & Hours & Days', () => {
            expect(time.getElapsedTime('2020-08-16T18:39:10Z')).toBe('1d 1h 1m 4s');
        });
    });
});
