/* eslint-disable @typescript-eslint/no-unused-vars */

/**
 * Copy Michelson Code
 *
 * @param el HTML element
 */
export const copyMichelson = (el: HTMLElement): void => {
    const range = document.createRange();
    const children = el.parentNode?.parentNode?.childNodes;
    if (Array.isArray(children)) {
        children.forEach((child: HTMLElement) => {
            if (child.nodeType !== Node.TEXT_NODE) {
                if (child.className.includes('michelson')) {
                    range.selectNode(child);
                    window.getSelection()?.removeAllRanges();
                    window.getSelection()?.addRange(range);
                    document.execCommand('copy');
                }
            }
        });
    }
};
