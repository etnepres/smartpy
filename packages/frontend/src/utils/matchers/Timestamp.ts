/**
 * @description Simple compare
 * @param {string} v1
 * @param {string} v2
 * @returns {number}
 */
export const compare = (v1 = '', v2 = ''): number => {
    const timestamp1 = new Date(v1).getTime();
    const timestamp2 = new Date(v2).getTime();
    return timestamp1 > timestamp2 ? 1 : timestamp1 === timestamp2 ? 0 : -1;
};

const Timestamp = {
    compare,
};

export default Timestamp;
