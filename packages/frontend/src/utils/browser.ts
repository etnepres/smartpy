/**
 * @description Identifies if the application is running on safari.
 */
export const usingSafari = () => !!navigator.vendor.match(/[Aa]pple/);
