/**
 * @description Append multiple class names with space delimiter.
 * @param classes Array with class names.
 *
 * @returns A string with all classes appended by order with a space (' ') delimiter.
 */
export const appendClasses = (...classes: string[]) => classes.sort().join(' ');
