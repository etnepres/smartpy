export enum Protocol {
    HANGZHOU = 'Hangzhou',
    ITHACA = 'Ithaca',
}

export const ProtocolHash: Record<string, string> = {
    [Protocol.HANGZHOU]: 'PtHangz2aRngywmSRGGvrcTyMbbdpWdpFKuS4uMWxg2RaH9i1qx',
    [Protocol.ITHACA]: '',
};

export const DefaultProtocol = Protocol.HANGZHOU;
