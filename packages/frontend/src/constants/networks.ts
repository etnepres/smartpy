import { Protocol, ProtocolHash, DefaultProtocol } from './protocol';

export enum Network {
    MAINNET = 'Mainnet',
    HANGZHOU = 'Hangzhounet',
    ITHACA = 'Ithacanet',
    CUSTOM = 'CUSTOM',
}

export const NetworkProtocolHash: Record<string, string> = {
    [Network.MAINNET]: ProtocolHash[DefaultProtocol],
    [Network.HANGZHOU]: ProtocolHash[Protocol.HANGZHOU],
    [Network.ITHACA]: ProtocolHash[Protocol.ITHACA],
};

export const DeprecatedNetworks: Network[] = [];
