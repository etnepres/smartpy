const info = {
    name: 'SmartPy.io',
    description: 'SmartPy is an intuitive and powerful smart contract development platform for Tezos',
    copyright: `Copyright © 2019-${new Date().getUTCFullYear()} Smart Chain Arena LLC. All rights reserved.`,
};

export default info;
