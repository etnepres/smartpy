import { Network } from './networks';

export const tzktAPI: Record<string, string> = {
    [Network.MAINNET]: 'https://api.tzkt.io/v1',
    [Network.HANGZHOU]: 'https://api.hangzhounet.tzkt.io/v1',
    [Network.ITHACA]: 'https://api.ithacanet.tzkt.io/v1',
};

export const tzkt: Record<string, string> = {
    [Network.MAINNET]: 'https://tzkt.io',
    [Network.HANGZHOU]: 'https://hangzhounet.tzkt.io',
    [Network.ITHACA]: 'https://ithacanet.tzkt.io',
};

export const tzstats: Record<string, string> = {
    [Network.MAINNET]: 'https://tzstats.com',
    [Network.ITHACA]: 'https://ithaca.tzstats.com',
};

export enum Endpoint {
    CONTRACT_OPERATIONS = '/accounts/:contract_id/operations',
}

const Explorer = {
    tzkt,
    tzktAPI,
    Endpoint,
};

export default Explorer;
