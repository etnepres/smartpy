import { Network } from './networks';

export const smartpy: Record<string, string> = {
    [Network.MAINNET]: 'https://mainnet.smartpy.io',
    [Network.HANGZHOU]: 'https://hangzhounet.smartpy.io',
    [Network.ITHACA]: 'https://ithacanet.smartpy.io',
};

export const Nodes = {
    // SMARTPY
    [smartpy.Mainnet]: Network.MAINNET,
    [smartpy.Hangzhounet]: Network.HANGZHOU,
    [smartpy.Ithacanet]: Network.ITHACA,
};

export enum Endpoint {
    HEADER = '/chains/main/blocks/head/header',
    ERRORS = '/chains/main/blocks/head/context/constants/errors',
    VERSION = '/version',
    CHECKPOINT = '/chains/main/checkpoint',
    CONSTANTS = '/chains/main/blocks/head/context/constants',
    METADATA = '/chains/main/blocks/head/metadata',
    CONTRACT_BALANCE = '/chains/main/blocks/head/context/contracts/:contract_id/balance',
}

export default Nodes;
