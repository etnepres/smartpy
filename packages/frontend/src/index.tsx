import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { HelmetProvider } from 'react-helmet-async';
import { Router as ReactRouter } from 'react-router-dom';

import { createStore } from 'src/store';
import CircularProgress from 'src/features/loader/components/CircularProgressWithText';
import reportWebVitals from 'src/reportWebVitals';

import 'src/index.css';
import 'src/features/i18n';

const WalletProvider = React.lazy(() => import('src/features/wallet/context/WalletProvider'));
const ThemeProvider = React.lazy(() => import('src/features/theme/context/ThemeProvider'));
const Router = React.lazy(() => import('src/features/navigation/containers/Router'));

const { store, persistor, history } = createStore();

ReactDOM.render(
    <React.StrictMode>
        <Suspense fallback={<CircularProgress loading={true} />}>
            <Provider store={store}>
                <PersistGate loading={'Loading...'} persistor={persistor}>
                    <ThemeProvider>
                        <HelmetProvider>
                            <ReactRouter history={history}>
                                <WalletProvider>
                                    <Router />
                                </WalletProvider>
                            </ReactRouter>
                        </HelmetProvider>
                    </ThemeProvider>
                </PersistGate>
            </Provider>
        </Suspense>
    </React.StrictMode>,
    document.getElementById('root'),
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
