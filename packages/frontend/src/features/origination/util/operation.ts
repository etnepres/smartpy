/**
 * @description Get originated contract address from origination operation result.
 *
 * @param originationResults Origination operation results
 */
export const getContractFromOriginationResult = (originationResults: any[] /* conseiljs types are broken */) => {
    return originationResults?.[0].metadata?.operation_result?.originated_contracts?.[0];
};
