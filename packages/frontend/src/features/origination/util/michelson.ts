import { emitMicheline, Parser } from '@taquito/michel-codec';
import { calculateSize } from 'tezize';
import { DefaultProtocol, ProtocolHash } from '../../../constants/protocol';

import logger from '../../../services/logger';

const INDENT = '    ';

/**
 * @description Convert Michelson JSON representation to Micheline
 * @param json Michelson JSON
 * @param protocol Tezos protocol
 */
export const jsonToMicheline = (json = '', protocol: string = ProtocolHash[DefaultProtocol]) => {
    try {
        const michelsonCode = new Parser({ protocol } as any).parseJSON(JSON.parse(json));
        return emitMicheline(michelsonCode, { indent: INDENT, newline: '\n' });
    } catch (e) {
        logger.error(e);
    }
};

/**
 * @description Calculate the contract (code and storage) sizes
 * @param code      Contract code
 * @param storage   Contract storage
 */
export const getContractSizes = (code = '', storage = '') => {
    if (code && storage) {
        return {
            code: calculateSize(code),
            storage: calculateSize(storage),
        };
    }
};
