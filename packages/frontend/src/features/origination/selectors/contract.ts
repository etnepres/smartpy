import { RootState } from 'SmartPyTypes';
import { useSelector } from 'react-redux';
import { OriginationContract } from 'SmartPyModels';

export const useOriginationContract = () =>
    useSelector<RootState, OriginationContract>(({ origination }: RootState) => origination.contract);
