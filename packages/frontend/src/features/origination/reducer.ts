import { combineReducers } from 'redux';
import { createReducer } from 'typesafe-actions';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import { OriginationContract } from 'SmartPyModels';
import { updateOriginationContract, clearOriginationContract, showError, hideError } from './actions';

const originationContractReducer = createReducer<OriginationContract>({
    codeJson: '',
    storageJson: '',
})
    .handleAction(updateOriginationContract, (state, { payload }) => ({
        ...state,
        ...payload,
    }))
    .handleAction(clearOriginationContract, () => ({
        codeJson: '',
        storageJson: '',
    }));

// ERRORS REDUCER

const errorReducer = createReducer('')
    .handleAction(hideError, () => '')
    .handleAction(showError, (_, action) => action.payload);

const persistConfig = {
    key: 'origination',
    storage: storage,
};

const originationReducer = combineReducers({
    contract: originationContractReducer,
    error: errorReducer,
});

export default persistReducer(persistConfig, originationReducer);
