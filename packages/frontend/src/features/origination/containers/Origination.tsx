import React from 'react';
import { useDispatch } from 'react-redux';

import { SelectChangeEvent } from '@mui/material/Select';

// Local Constants
import Nodes from '../../../constants/rpc';
// Local Hooks
import { useSearchParams } from '../../../hooks/useSearchParams';

import OriginationView from '../views/Origination';
import WalletActions, { updateAccountInfo } from '../../wallet/actions';
import { useNetworkInfo } from '../../wallet/selectors/network';
import { useAccountInfo } from '../../wallet/selectors/account';
import WalletServices from '../../wallet/services';
import logger from '../../../services/logger';
import ipfs from '../../../services/ipfs';
import { updateOriginationContract } from '../actions';
import { AES } from '../../../services/crypto';
import { OriginationContract } from 'SmartPyModels';
import debounce from '../../../utils/debounce';
import OriginationErrorDialog from '../components/ErrorDialog';
import { getRpcNetwork } from '../../../utils/tezosRpc';

// Debounce helpers
const updateBalanceDebouncer = debounce(1000);

const Origination = () => {
    const networkInfo = useNetworkInfo();
    const accountInfo = useAccountInfo();
    const dispatch = useDispatch();
    const searchParams = useSearchParams();

    const onNetworkSelection = (event: SelectChangeEvent<string>) => {
        const rpc = event.target.value;
        if (Nodes[rpc]) {
            dispatch(
                WalletActions.updateNetworkInfo({
                    network: Nodes[rpc],
                    rpc,
                }),
            );
        }
    };

    const onRpcChange = async (event: React.ChangeEvent<{ value: string }>) => {
        const rpc = event.target.value;
        dispatch(WalletActions.updateNetworkInfo({ rpc: rpc }));

        const network = Nodes[rpc] || (await getRpcNetwork(rpc));

        dispatch(WalletActions.updateNetworkInfo({ network, rpc }));
    };

    const updateAccountBalance = React.useCallback(async () => {
        try {
            if (accountInfo?.source && WalletServices[accountInfo.source]) {
                WalletServices[accountInfo.source].setRPC(networkInfo.rpc);
                const accountInformation = await WalletServices[accountInfo.source].getInformation();
                dispatch(updateAccountInfo(accountInformation));
            }
        } catch (e) {
            logger.error('Could not update account balance', e);
        }
    }, [accountInfo.source, dispatch, networkInfo]);

    React.useEffect(() => {
        updateBalanceDebouncer(updateAccountBalance);
    }, [updateAccountBalance]);

    React.useEffect(() => {
        const CID = searchParams.cid as string;
        const passPhrase = searchParams.k as string;
        // If a CID was passed as Path Param, then the link was shared
        if (CID && passPhrase) {
            ipfs.getStringFromCID(CID)
                .then((encryptedContract) => {
                    dispatch(
                        updateOriginationContract(
                            JSON.parse(AES.decrypt(encryptedContract, passPhrase)) as OriginationContract,
                        ),
                    );
                })
                .catch((e) => logger.error(e));
        }
    }, [dispatch, searchParams.cid, searchParams.k]);

    React.useEffect(() => {
        const onResize = () => {
            if (window.screen.width < 512) {
                document.getElementById('viewport-meta')?.setAttribute('content', 'width=512');
            }
        };
        onResize();
        window.addEventListener('resize', onResize);

        return () => {
            window.removeEventListener('resize', onResize);
            document.getElementById('viewport-meta')?.setAttribute('content', 'width=device-width, initial-scale=1');
        };
    }, []);

    return (
        <React.Fragment>
            <OriginationView
                {...{
                    onRpcChange,
                    onNetworkSelection,
                }}
            />
            <OriginationErrorDialog />
        </React.Fragment>
    );
};

export default Origination;
