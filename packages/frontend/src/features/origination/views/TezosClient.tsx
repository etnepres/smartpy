import React from 'react';
import { downloadFile } from '../../../utils/file';

// Material UI
import { Theme } from '@mui/material/styles';
import createStyles from '@mui/styles/createStyles';
import makeStyles from '@mui/styles/makeStyles';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Divider from '@mui/material/Divider';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';

import useTranslation from '../../i18n/hooks/useTranslation';
import CodeBlock from '../../common/components/CodeBlock';
import { OriginationParameters as ParametersType } from 'OriginationTypes';
import { FIELDS } from '../components/OriginationParameters';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        spacer: {
            margin: theme.spacing(2),
        },
        section: {
            padding: 10,
            boxShadow: '0 2px 4px rgba(0,0,0,0.15), 0 4px 2px rgba(0,0,0,0.22)',
        },
        divider: {
            margin: theme.spacing(1, 0, 1, 0),
        },
        fullHeight: {
            height: '100%',
        },
    }),
);

const TEZOS_CLIENT_ORIGINATE_TEMPLATE =
    'tezos-client --endpoint {endpoint} \\\n originate contract {name} \\\n transferring {amount} \\\n from {source} running {name}.tz';

type OwnProps = {
    storage?: string;
    code?: string;
    parameters: ParametersType;
    handleParameterInput: (event: React.ChangeEvent<HTMLInputElement>) => void;
    onRpcChange: (event: React.ChangeEvent<{ value: string }>) => void;
    rpc: string;
    estimateOriginationCosts: () => Promise<void>;
};

const TezosClient: React.FC<OwnProps> = ({
    storage,
    code,
    parameters,
    handleParameterInput,
    onRpcChange,
    rpc,
    estimateOriginationCosts,
    children,
}) => {
    const classes = useStyles();
    const t = useTranslation();

    const command = React.useMemo(() => {
        let cmd = TEZOS_CLIENT_ORIGINATE_TEMPLATE.replace('{endpoint}', rpc)
            .replaceAll('{name}', parameters[FIELDS.NAME])
            .replace('{amount}', String(parameters[FIELDS.BALANCE]))
            .replace('{source}', parameters[FIELDS.SOURCE] || '$SOURCE');

        // Add Initial Storage
        cmd = `${cmd} \\\n --init '${storage}'`;
        // Add Delegate
        if (parameters[FIELDS.DELEGATE]) {
            cmd = `${cmd} \\\n --delegate ${parameters[FIELDS.DELEGATE]}`;
        }
        // Add Fee
        if (Number(parameters[FIELDS.FEE])) {
            cmd = `${cmd} \\\n --fee ${parameters[FIELDS.FEE]}`;
        }
        // Add Gas Limit
        if (Number(parameters[FIELDS.GAS_LIMIT])) {
            cmd = `${cmd} \\\n --gas-limit ${parameters[FIELDS.GAS_LIMIT]}`;
        }
        // Add Storage Limit
        if (Number(parameters[FIELDS.STORAGE_LIMIT])) {
            cmd = `${cmd} \\\n --storage-limit ${parameters[FIELDS.STORAGE_LIMIT]}`;
        }
        return cmd;
    }, [parameters, storage, rpc]);

    const downloadContract = () => {
        downloadFile(`${parameters[FIELDS.NAME]}.tz`, code || '');
    };

    return (
        <>
            {code ? (
                <Paper className={classes.section}>
                    <Divider className={classes.divider} />
                    <Grid container spacing={2} justifyContent="center">
                        <Grid item xs={12} sm={6}>
                            <TextField
                                fullWidth
                                variant="filled"
                                name={FIELDS.ENDPOINT}
                                onChange={onRpcChange}
                                id={FIELDS.ENDPOINT}
                                label={t('wallet.parameterLabels.endpoint')}
                                value={rpc}
                            />
                        </Grid>
                        <Grid item xs={12} sm={4}>
                            <Button
                                fullWidth
                                onClick={downloadContract}
                                variant="outlined"
                                className={classes.fullHeight}
                            >
                                {t('origination.downloadContract')}
                            </Button>
                        </Grid>
                    </Grid>
                    <Divider className={classes.divider} />
                </Paper>
            ) : null}

            <div className={classes.spacer} />

            {children}

            <div className={classes.spacer} />

            <Paper className={classes.section}>
                <Typography variant="h6">{t('origination.tezosClientCommand')}</Typography>
                <Divider className={classes.divider} />
                <CodeBlock showLineNumbers={false} language="bash" text={command} wrapLongLines />
                <Divider className={classes.divider} />
            </Paper>
        </>
    );
};

export default TezosClient;
