import { OriginationContract } from 'SmartPyModels';
import { createAction } from 'typesafe-actions';

export enum Actions {
    UPDATE_ORIGINATION_CONTRACT = 'UPDATE_ORIGINATION_CONTRACT',
    CLEAR_ORIGINATION_CONTRACT = 'CLEAR_ORIGINATION_CONTRACT',
    // ERROR DIALOG
    SHOW_ORIGINATION_ERROR = 'SHOW_ORIGINATION_ERROR',
    HIDE_ORIGINATION_ERROR = 'HIDE_ORIGINATION_ERROR',
}

export const updateOriginationContract = createAction(Actions.UPDATE_ORIGINATION_CONTRACT)<OriginationContract>();
export const clearOriginationContract = createAction(Actions.CLEAR_ORIGINATION_CONTRACT)();

// ERROR
export const showError = createAction(Actions.SHOW_ORIGINATION_ERROR, (error: string) => error)<string>();
export const hideError = createAction(Actions.HIDE_ORIGINATION_ERROR)<void>();

const actions = {
    updateOriginationContract,
    clearOriginationContract,
    showError,
    hideError,
};

export default actions;
