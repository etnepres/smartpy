import React from 'react';
import { useDispatch } from 'react-redux';
import { KeyCode, KeyMod } from 'monaco-editor/esm/vs/editor/editor.api';
import type { editor } from 'monaco-editor/esm/vs/editor/editor.api';
import type { OnMount, BeforeMount } from '@monaco-editor/react';

// Material UI
import { Theme, useTheme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import createStyles from '@mui/styles/createStyles';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';

// State Management
import { IDESettings } from 'SmartPyModels';
import actions, { setVolatileContract, updateNewcomerGuideStep } from '../actions';
import { useNewcomerGuideStep, useSelectedContract } from '../selectors';

// Local Components
import ContractManagement from '../containers/ContractManagement';
import TemplatesMenuItem from '../components/TemplatesMenuItem';
import SettingsMenuItem from '../components/SettingsMenuItem';

// Local Utils
import { evalRun } from '../utils/EditorUtils';
import { SmartTsTypings } from '../utils/EditorUtils';
import toast from '../../../services/toast';
import useTranslation from '../../i18n/hooks/useTranslation';
import { downloadOutputPanel } from '../../common/utils/IDEUtils';
import EditorToolBar from '../../common/components/EditorToolBar';
import { SYNTAX } from '../../common/enums/ide';
import Logger from '../../../services/logger';
import Editor from '../../editor/components/Editor';
import { ST_Scenario } from '@smartpy/ts-syntax/dist/types/@types/scenario';
import { ST_Error } from '@smartpy/ts-syntax/dist/types';
import EditorWithOutput from 'src/features/editor/components/EditorWithOutput';
import { Box } from '@mui/material';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
            flexDirection: 'column',
            flexGrow: 1,
        },
        editorSection: {
            display: 'flex',
            flexGrow: 1,
            borderTopWidth: 2,
            borderTopStyle: 'solid',
            borderTopColor: theme.palette.primary.light,
            // screen height - navBar - toolBar (percentage doesn't work here)
            height: 'calc(100vh - 80px - 60px)',
        },
        editor: {
            display: 'flex',
            flexDirection: 'column',
            height: '100%',
            flexGrow: 1,
        },
        noBorderRadius: {
            borderRadius: 0,
        },
    }),
);

interface OwnProps {
    clearOutputs: () => void;
    editorRef: React.MutableRefObject<editor.IStandaloneCodeEditor>;
    htmlOutput: { __html: string };
    settings: IDESettings;
    setTargets: React.Dispatch<React.SetStateAction<ST_Scenario[]>>;
    targets: ST_Scenario[];
    showError: (error: string | ST_Error) => void;
}

const EditorView: React.FC<OwnProps> = (props) => {
    const classes = useStyles();
    const newcomerGuideStep = useNewcomerGuideStep();
    const outputPanelRef = React.useRef(null as unknown as HTMLDivElement);
    const theme = useTheme();
    const dispatch = useDispatch();
    const contract = useSelectedContract();
    const t = useTranslation();
    const { targets, clearOutputs, editorRef, htmlOutput, settings, showError, setTargets } = props;
    const [compiling, setCompiling] = React.useState(false);

    const downloadOutput = () => {
        if (contract && htmlOutput.__html) {
            downloadOutputPanel(contract.name, theme.palette.mode === 'dark', outputPanelRef.current);
        } else {
            toast.error(t('ide.errors.outputEmpty'));
        }
    };

    const onInput = React.useCallback(
        (code = '') => {
            if (editorRef.current) {
                if (contract?.id && code !== contract?.code) {
                    dispatch(
                        actions.updateContract({
                            ...contract,
                            code,
                        }),
                    );
                } else if (!contract?.id) {
                    dispatch(
                        actions.addContract(
                            {
                                code,
                                shared: false,
                            },
                            false,
                        ),
                    );
                }
            }
        },
        [contract, dispatch, editorRef],
    );

    const compileContract = React.useCallback(
        async (withTests = true) => {
            setCompiling(true);
            try {
                await new Promise((r) => setTimeout(r, 500));
                const code = editorRef?.current?.getValue();
                if (code) {
                    const targets = await evalRun(code, withTests);
                    setTargets(targets);
                }
            } catch (error: any) {
                if (error?.name === 'ST_Error') {
                    showError(error);
                } else if (error) {
                    showError(error);
                }
            }
            setCompiling(false);
        },
        [editorRef, setTargets, showError],
    );

    const runScenario = React.useCallback(
        async (scenarioName: string) => {
            setCompiling(true);
            try {
                const scenario = targets.find(({ name }) => name === scenarioName);
                scenario && window.smartmlCtx.call('runScenarioInBrowser', JSON.stringify(scenario));
            } catch (error: any) {
                showError(error);
            }
            setCompiling(false);
        },
        [showError, targets],
    );

    const handleEditorDidMount: OnMount = React.useCallback(
        (editor, _monaco) => {
            if (!editor) {
                return Logger.error('Monaco Editor could not load, please notify the maintainer.');
            }
            editorRef.current = editor;
            editor.focus();
        },
        [editorRef],
    );

    const handleEditorWillMount: BeforeMount = (monaco) => {
        const compilerOptions = monaco.languages.typescript.typescriptDefaults.getCompilerOptions();

        monaco.languages.typescript.typescriptDefaults.setCompilerOptions({
            ...compilerOptions,
            lib: ['es2015.symbol'],
            allowNonTsExtensions: true,
            experimentalDecorators: true,
            noFallthroughCasesInSwitch: true,
            target: 7, // ES2020
        });
        monaco.languages.typescript.typescriptDefaults.setDiagnosticsOptions({
            diagnosticCodesToIgnore: [2792, 2691],
        });

        monaco.languages.typescript.typescriptDefaults.setExtraLibs([
            { content: SmartTsTypings, filePath: 'global.d.ts' },
        ]);
    };

    /**
     * This method returns the editor
     */
    const IDE = React.useMemo(
        () => (
            <Box className={classes.editor} style={{ display: settings.layout === 'output-only' ? 'none' : 'flex' }}>
                <Alert variant="filled" severity="warning" classes={{ root: classes.noBorderRadius }}>
                    <AlertTitle>{t('common.warning')}</AlertTitle>
                    {t('ide.typescript.disclaimer')}
                </Alert>
                <ContractManagement />
                <Editor
                    onMount={handleEditorDidMount}
                    beforeMount={handleEditorWillMount}
                    language="typescript"
                    value={contract?.code || ''}
                    options={{
                        automaticLayout: true,
                        selectOnLineNumbers: true,
                        fontSize: settings.fontSize,
                    }}
                    actions={[
                        {
                            // Unique identifier
                            id: 'run-code',
                            // A label of the action that will be presented to the user.
                            label: 'Run Code',
                            // An optional array of keybindings for the action.
                            keybindings: [KeyMod.CtrlCmd | KeyCode.Enter],
                            // Custom section
                            contextMenuGroupId: 'utils',
                            contextMenuOrder: 1.5,
                            run: () => compileContract(),
                        },
                    ]}
                    onChange={onInput}
                />
            </Box>
        ),
        [
            classes.editor,
            classes.noBorderRadius,
            compileContract,
            contract?.code,
            handleEditorDidMount,
            onInput,
            settings.fontSize,
            settings.layout,
            t,
        ],
    );

    return (
        <div className={classes.root}>
            <EditorToolBar
                targets={targets}
                clearOutputs={clearOutputs}
                downloadOutputPanel={downloadOutput}
                compileContract={compileContract}
                runScenario={runScenario}
                selectedContract={contract}
                baseUrl={`${window.location.origin}${process.env.PUBLIC_URL}/ts-ide`}
                settingsMenu={<SettingsMenuItem />}
                templatesMenu={<TemplatesMenuItem />}
                updateNewcomerGuideStep={updateNewcomerGuideStep}
                setVolatileContract={setVolatileContract}
                newcomerGuideStep={newcomerGuideStep}
                syntax={SYNTAX.TYPESCRIPT}
            />
            <div className={classes.editorSection}>
                <EditorWithOutput
                    editor={IDE}
                    settings={settings}
                    htmlOutput={htmlOutput}
                    outputPanelRef={outputPanelRef}
                    compiling={compiling}
                />
            </div>
        </div>
    );
};

export default EditorView;
