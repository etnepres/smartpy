import pako from 'pako';

import Logger from '../../../services/logger';
import { copyToClipboard } from '../../../utils/clipboard';
import toast from '../../../services/toast';
import { ST_Scenario } from '@smartpy/ts-syntax/dist/types/@types/scenario';
import SmartTS from '@smartpy/ts-syntax';

export const SmartTsTypings = SmartTS.typings;

/**
 * Evaluate the code in the editor
 *
 * @param {string} code source code
 * @param {boolean} withTests - When true, the evaluation will run with tests.
 */
export const evalRun = async (code: string, withTests = true): Promise<ST_Scenario[]> => {
    const sourceCode = {
        name: 'main.ts',
        code,
    };

    const transpiled = await SmartTS.transpile(sourceCode);
    transpiled
        .filter(({ show }) => show)
        .forEach((scenario) => {
            Logger.debug(JSON.stringify(scenario, null, 4));
            window.smartmlCtx.call('runScenarioInBrowser', JSON.stringify(scenario));
        });

    return transpiled;
};

export const getEmbeddedLink = (contract: string) => {
    const encoded = btoa(pako.deflate(contract, { to: 'string' }))
        .replace(/\+/g, '@')
        .replace(/\//g, '_')
        .replace(/=/g, '-');
    return `${window.location.origin}/ts-ide?code=${encoded}`;
};

export const handleTemplateShare = (fileName: string) => {
    copyToClipboard(`${window.location.origin}/ts-ide/?template=${fileName}`);
    toast.info('The template url was copied!');
};
