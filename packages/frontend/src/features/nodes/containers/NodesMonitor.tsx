import React from 'react';
import httpRequester from '../../../services/httpRequester';

import Skeleton from '@mui/material/Skeleton';

import nodes, { Nodes } from '../constants/nodes';
import NodesMonitorComponent, { NodeStatus } from '../components/NodesMonitor';

const NodesMonitor = () => {
    const [ready, setReady] = React.useState(false);
    const intervalId = React.useRef(null as unknown as ReturnType<typeof setInterval>);
    const [nodesStatus, setNodesStatus] = React.useState<NodeStatus[]>([]);

    const updateNodeStatus = React.useCallback(async () => {
        setNodesStatus(
            await Promise.all(
                Object.keys(Nodes).map(async (node: string) => {
                    let status: NodeStatus = await httpRequester
                        .get(`${node}/chains/main/blocks/head/header`, { timeout: 1000 })
                        .then(({ data: { timestamp, level } }) => ({
                            network: Nodes[node].toUpperCase(),
                            synchronized: Date.now() - new Date(timestamp).getTime() < nodes.allowedDelay,
                            timestamp,
                            blockLevel: level,
                        }))
                        .catch(() => ({
                            network: Nodes[node].toUpperCase(),
                            synchronized: false,
                        }));

                    status = await httpRequester
                        .get(`${node}/version`, { timeout: 1000 })
                        .then(({ data }) => ({
                            ...status,
                            ...data,
                        }))
                        .catch(() => ({
                            ...status,
                        }));

                    status = await httpRequester
                        .get(`${node}/chains/main/checkpoint`, { timeout: 1000 })
                        .then(({ data: { history_mode } }) => ({
                            ...status,
                            historyMode: typeof history_mode === 'string' ? history_mode : Object.keys(history_mode)[0],
                        }))
                        .catch(() => ({
                            ...status,
                        }));

                    return status;
                }),
            ),
        );
        setReady(true);
    }, []);

    React.useEffect(() => {
        updateNodeStatus();
        intervalId.current = setInterval(updateNodeStatus, nodes.healthCheckInterval);
        return () => {
            clearInterval(intervalId.current);
        };
    }, [updateNodeStatus]);

    return ready ? (
        <NodesMonitorComponent nodes={nodesStatus} />
    ) : (
        <div style={{ width: '100%' }}>
            <Skeleton animation="wave" />
            <Skeleton animation="wave" />
            <Skeleton animation="wave" />
            <Skeleton animation="wave" />
        </div>
    );
};

export default NodesMonitor;
