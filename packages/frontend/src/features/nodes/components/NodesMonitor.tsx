import React from 'react';

import makeStyles from '@mui/styles/makeStyles';
import createStyles from '@mui/styles/createStyles';
import Accordion from '@mui/material/Accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import AccordionSummary from '@mui/material/AccordionSummary';
import Typography from '@mui/material/Typography';
import Chip from '@mui/material/Chip';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import { Theme } from '@mui/material';

// Local Components
import ElapsedTime from '../../common/elements/ElapsedTime';

export interface NodeStatus {
    network: string;
    synchronized: boolean;
    timestamp?: string;
    blockLevel?: number;
    historyMode?: string;
    version?: {
        additional_info: string;
        major: number;
        minor: number;
    };
}

interface Props {
    nodes: NodeStatus[];
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        heading: {
            fontWeight: 'bold',
            marginRight: 5,
        },
        summary: {
            display: 'flex',
            alignContent: 'space-between',
            justifyContent: 'space-between',
            alignItems: 'center',
        },
        red: {
            backgroundColor: theme.palette.error.light,
        },
        green: {
            backgroundColor: theme.palette.success.light,
        },
        list: {
            width: '100%',
        },
        link: {
            textDecoration: 'none',
            fontVariant: 'small-caps',
            fontFamily: 'Verdana, sans-serif',
            color: theme.palette.mode === 'dark' ? '#66aacc' : '#01608c',
        },
    }),
);

const NodesMonitorWidget: React.FC<Props> = ({ nodes }) => {
    const classes = useStyles();
    const [expanded, setExpanded] = React.useState<string | false>(false);

    const handleChange = (panel: string) => {
        setExpanded(expanded === panel ? false : panel);
    };

    return (
        <div>
            {nodes.map((node, index) => (
                <Accordion key={index} expanded={expanded === node.network} onChange={() => handleChange(node.network)}>
                    <AccordionSummary expandIcon={<ExpandMoreIcon />} classes={{ content: classes.summary }}>
                        <Chip size="small" label={`Level: ${node.blockLevel}`} className={classes.heading} />
                        <Chip
                            label={node.network.replace('_', ' ')}
                            className={`${classes.heading} ${node.synchronized ? classes.green : classes.red}`}
                        />
                    </AccordionSummary>
                    <AccordionDetails>
                        <List className={classes.list}>
                            {node.version ? (
                                <ListItem className={classes.summary}>
                                    <Typography className={classes.heading}>{`Version: `}</Typography>
                                    <Typography>
                                        {node.version.major}.{node.version.minor}
                                    </Typography>
                                </ListItem>
                            ) : undefined}
                            <ListItem className={classes.summary}>
                                <Typography className={classes.heading}>{`History Mode: `}</Typography>
                                <Typography>{node.historyMode}</Typography>
                            </ListItem>
                            {node.timestamp ? (
                                <ListItem className={classes.summary}>
                                    <Typography className={classes.heading}>{`Last Block: `}</Typography>
                                    <Typography>
                                        <ElapsedTime timestamp={node.timestamp}></ElapsedTime>
                                        {` ago`}
                                    </Typography>
                                </ListItem>
                            ) : undefined}
                            <ListItem className={classes.summary}>
                                <Typography className={classes.heading}>{`Block Level: `}</Typography>
                                <Typography>{node.blockLevel}</Typography>
                            </ListItem>
                            <ListItem className={classes.summary}>
                                <Typography className={classes.heading}>{'RPC Link: '}</Typography>
                                <a
                                    href={`https://${
                                        node.network.split(' ')[0]
                                    }.smartpy.io/chains/main/blocks/head/header`}
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    className={classes.link}
                                >
                                    {node.network.split(' ')[0]}
                                </a>
                            </ListItem>
                        </List>
                    </AccordionDetails>
                </Accordion>
            ))}
        </div>
    );
};

export default NodesMonitorWidget;
