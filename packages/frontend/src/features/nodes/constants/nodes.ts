import { Network } from 'src/constants/networks';

const constants = {
    healthCheckInterval: 10000,
    allowedDelay: 5 * 60000,
};

export const Nodes: Record<string, string> = {
    'https://mainnet-archive.smartpy.io': `${Network.MAINNET} (archive 1)`,
    'https://mainnet-rolling.smartpy.io': `${Network.MAINNET} (rolling 1)`,
    'https://mainnet-rolling2.smartpy.io': `${Network.MAINNET} (rolling 2)`,
    'https://hangzhounet.smartpy.io': Network.HANGZHOU,
    'https://ithacanet.smartpy.io': Network.ITHACA,
};

export default constants;
