import React from 'react';
import { Link } from 'react-router-dom';
import { LocationDescriptor } from 'history';
import { FabProps } from '@mui/material';
import Fab from 'src/features/common/elements/Fab';

interface OwnProps extends FabProps {
    to: LocationDescriptor;
}

const RouterFab: React.FC<OwnProps> = ({ to, ...props }) => (
    <Fab
        variant="extended"
        {...props}
        component={React.forwardRef<HTMLAnchorElement, any>((linkProps, ref) => (
            <Link {...linkProps} to={to} ref={ref} />
        ))}
    />
);

export default RouterFab;
