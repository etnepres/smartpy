import React from 'react';

// Material UI
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import createStyles from '@mui/styles/createStyles';
import useScrollTrigger from '@mui/material/useScrollTrigger';
import Fab from '@mui/material/Fab';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import Zoom from '@mui/material/Zoom';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import TwitterIcon from '@mui/icons-material/Twitter';
import TelegramIcon from '@mui/icons-material/Telegram';
import Divider from '@mui/material/Divider';

// Local UI Components
import MediumIcon from '../../common/elements/icons/Medium';
import GitLabIcon from '../../common/elements/icons/GitLab';
// Constants
import info from '../../../constants/info';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
            background: theme.palette.mode === 'light' ? theme.palette.primary.dark : undefined,
            borderTopWidth: 2,
            borderTopStyle: 'solid',
        },
        zoomRoot: {
            position: 'fixed',
            bottom: theme.spacing(2),
            right: theme.spacing(2),
        },
        fabToTop: {
            borderStyle: 'solid',
            borderWidth: 2,
        },
        copyrightDiv: {
            paddingTop: 30,
            paddingBottom: 10,
            textAlign: 'center',
        },
        followUsText: {
            textAlign: 'center',
            color: theme.palette.common.white,
            fontSize: '1.5rem',
            fontWeight: 'bold',
            textTransform: 'capitalize',
            margin: 5,
        },
        followUsGrid: {
            margin: '10px 0 10px 0',
        },
        extendedIcon: {
            marginRight: theme.spacing(1),
        },
        fabLabel: {
            paddingRight: 5,
            textTransform: 'none',
        },
        divider: {
            background: theme.palette.common.white,
        },
    }),
);

const ScrollTop = () => {
    const classes = useStyles();

    const trigger = useScrollTrigger({
        disableHysteresis: true,
        threshold: 100,
    });

    const handleClick = (event: React.MouseEvent<HTMLDivElement>) => {
        const anchor = ((event.target as HTMLDivElement).ownerDocument || document).querySelector(
            '#back-to-top-anchor',
        );

        if (anchor) {
            anchor.scrollIntoView({ behavior: 'smooth', block: 'center' });
        }
    };

    return (
        <div onClick={handleClick} role="presentation" className={classes.zoomRoot}>
            <Zoom in={trigger}>
                <Fab color="primary" size="small" aria-label="scroll back to top" className={classes.fabToTop}>
                    <KeyboardArrowUpIcon />
                </Fab>
            </Zoom>
        </div>
    );
};

const Footer: React.FC = () => {
    const classes = useStyles();
    return (
        <div className={classes.root} id="contact">
            <Grid container alignItems="center" justifyContent="center">
                <Grid item xs={11}>
                    <Typography className={classes.followUsText}>Follow Us</Typography>
                    <Divider className={classes.divider} />
                    <Grid container alignItems="center" justifyContent="center" className={classes.followUsGrid}>
                        <Grid item xs={11} sm={2} display="flex" alignItems="center" justifyContent="center">
                            <Fab
                                variant="extended"
                                size="small"
                                color="secondary"
                                aria-label="Twitter"
                                href="https://twitter.com/SmartPy_io"
                                classes={{ extended: classes.fabLabel }}
                            >
                                <TwitterIcon className={classes.extendedIcon} color="primary" />
                                Twitter
                            </Fab>
                        </Grid>
                        <Grid item xs={11} sm={2} display="flex" alignItems="center" justifyContent="center">
                            <Fab
                                variant="extended"
                                size="small"
                                color="secondary"
                                aria-label="Telegram"
                                href="https://t.me/SmartPy_io"
                                classes={{ extended: classes.fabLabel }}
                            >
                                <TelegramIcon className={classes.extendedIcon} color="primary" />
                                Telegram
                            </Fab>
                        </Grid>
                        <Grid item xs={11} sm={2} display="flex" alignItems="center" justifyContent="center">
                            <Fab
                                variant="extended"
                                size="small"
                                color="secondary"
                                aria-label="Medium"
                                href="https://smartpy-io.medium.com"
                                classes={{ extended: classes.fabLabel }}
                            >
                                <MediumIcon className={classes.extendedIcon} fontSize="small" />
                                Medium
                            </Fab>
                        </Grid>
                        <Grid item xs={11} sm={2} display="flex" alignItems="center" justifyContent="center">
                            <Fab
                                variant="extended"
                                size="small"
                                color="secondary"
                                aria-label="GitLab"
                                href="https://gitlab.com/SmartPy/smartpy"
                                classes={{ extended: classes.fabLabel }}
                            >
                                <GitLabIcon className={classes.extendedIcon} fontSize="small" />
                                GitLab
                            </Fab>
                        </Grid>
                    </Grid>
                    <Divider className={classes.divider} />
                </Grid>
                <Grid item xs={11} className={classes.copyrightDiv}>
                    <Typography variant="caption" align="justify" color="secondary">
                        {info.copyright}
                    </Typography>
                </Grid>
            </Grid>
            <ScrollTop />
        </div>
    );
};

export default Footer;
