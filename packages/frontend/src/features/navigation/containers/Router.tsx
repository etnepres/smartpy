import React from 'react';
import { Switch } from 'react-router';
import { Helmet } from 'react-helmet-async';

import routes from '../constants/routes';

const Router = () => {
    return (
        <Switch>
            {routes.map(({ Route, routeProps, Component, title }, index) => (
                <Route key={index} {...routeProps}>
                    <Helmet>
                        <title>{title}</title>
                    </Helmet>
                    <Component />
                </Route>
            ))}
        </Switch>
    );
};

export default Router;
