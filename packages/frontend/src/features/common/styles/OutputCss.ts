import { Theme } from '@mui/material/styles';
import withStyles from '@mui/styles/withStyles';

const colors = {
    primary: {
        dark: '#006dcc',
        light: '#006dcc',
    },
    'primary-text-color': {
        light: '#212529',
        dark: '#fafafa',
    },
    'background-color': {
        light: '#fafafa',
        dark: '#212529',
    },
    'link-color': {
        light: '#01608c',
        dark: '#66AACC',
    },
    'button-hover-color': {
        light: '#e8e8e8',
        dark: 'slategray',
    },
    'menu-bg-color': {
        light: '#f1f1f1',
        dark: '#05385C',
    },
    'border-color': {
        light: '#d3d3d3',
        dark: '#D3D3D3',
    },
    'output-bgok': {
        light: '#f1f1fa',
        dark: '#05364a',
    },
    'output-bgko': {
        light: '#faf1f1',
        dark: '#4a3605',
    },
    'danger-color': {
        light: '#dc3545',
        dark: '#dc3545',
    },
    'dataColumn-color': {
        light: '#290075',
        dark: 'blanchedalmond',
    },
    'simulationBuilder-bg-color': {
        light: '#fffdeb',
        dark: '#444488',
    },
    'tabcontentshow-bg-color': {
        light: '#ccffcc',
        dark: 'darkgreen',
    },
    'key-address-color': {
        light: '#006600',
        dark: 'chocolate',
    },
    'timestamp-color': {
        light: '#6600aa',
        dark: 'aqua',
    },
    'code-type-color': {
        light: '#331188',
        dark: 'magenta',
    },
    'code-variable-color': {
        light: '#aa0000',
        dark: 'khaki',
    },
    'code-store-color': {
        light: '#0000aa',
        dark: 'greenyellow',
    },
    'code-constant-color': {
        light: '#000088',
        dark: 'red',
    },
    'code-cons-color': {
        light: '#0000aa',
        dark: 'hotpink',
    },
    'code-comment-color': {
        light: '#006600',
        dark: 'chocolate',
    },
    'button-bg-color': {
        light: '#006dcc',
        dark: '#FFF',
    },
    'button-font-color': {
        light: '#FFF',
        dark: '#000',
    },
};

export default withStyles(
    ({ palette: { mode } }: Theme) => ({
        '@global': {
            select: {
                fontSize: 16,
                fontFamily: 'sans-serif',
                color: '#444',
                lineHeight: 1.3,
                padding: '0.3em 2em 0.2em 0.8em',
                boxSizing: 'border-box',
                margin: '5px',
                border: '1px solid #aaa',
                boxShadow: '0 1px 0 1px rgba(0, 0, 0, 0.04)',
                borderRadius: '0.5em',
                appearance: 'none',
                backgroundColor: '#fff',
                backgroundImage:
                    "url('data:image/svg+xml;charset=US-ASCII,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22292.4%22%20height%3D%22292.4%22%3E%3Cpath%20fill%3D%22%23007CB2%22%20d%3D%22M287%2069.4a17.6%2017.6%200%200%200-13-5.4H18.4c-5%200-9.3%201.8-12.9%205.4A17.6%2017.6%200%200%200%200%2082.2c0%205%201.8%209.3%205.4%2012.9l128%20127.9c3.6%203.6%207.8%205.4%2012.8%205.4s9.2-1.8%2012.8-5.4L287%2095c3.5-3.5%205.4-7.8%205.4-12.8%200-5-1.9-9.2-5.5-12.8z%22%2F%3E%3C%2Fsvg%3E'), linear-gradient(to bottom, #ffffff 0%, #e5e5e5 100%)",
                backgroundRepeat: 'no-repeat, repeat',
                /* arrow icon position (1em from the right, 50% vertical) , then gradient position*/
                backgroundPosition: 'right 0.7em top 50%, 0 0',
                /* icon size, then gradient */
                backgroundSize: '0.65em auto, 100%',
            },
            '.hidden': {
                display: 'none',
            },
            h1: {
                fontSize: '2rem',
                lineHeight: '2.4rem',
            },
            '.execMessage h3, .tabcontent h3': {
                fontSize: '1.1rem',
                fontWeight: 'normal',
                marginBottom: '0.2em',
            },
            '.execMessage *, .tabcontent *, #types': {
                fontSize: '0.9375rem',
                lineHeight: '1.1em',
            },
            '.type, .python, .michelson, .on': {
                fontFamily: 'source-code-pro, Menlo, Monaco, Consolas, "Courier New", monospace',
                fontSize: '0.9375rem',
            },
            'h1, h2, h3, h4, h5': {
                margin: 0,
                marginBottom: '.5rem',
                padding: 0,
            },
            '.tabs': {
                marginBottom: '1rem',
                border: `1px solid ${colors['border-color'][mode]}`,
            },
            '.tab': {
                overflow: 'hidden',
                border: `1px solid ${colors['border-color'][mode]}`,
                backgroundColor: colors['menu-bg-color'][mode],
                lineHeight: 1.5,
            },
            '.tab button': {
                height: 36,
                backgroundColor: 'inherit',
                float: 'left',
                border: 'none',
                outline: 'none',
                cursor: 'pointer',
                padding: '7px 10px',
                transition: '0.3s',
                opacity: 0.6,
                margin: 3,
                color: 'inherit',
                fontFamily: 'inherit',
                fontSize: 'inherit',
            },
            '.tab .title': {
                float: 'left',
                padding: 10,
            },
            '.active': {
                borderBottom: `2px solid ${colors['primary-text-color'][mode]}`,
                opacity: 1,
            },
            '.tabcontent': {
                padding: '6px 12px',
                borderTop: 'none',
                fontSize: 16,
            },
            'button.tabLinks': {
                backgroundColor: colors['button-bg-color'][mode],
                border: 'none',
                outline: 'none',
                cursor: 'pointer',
                padding: '5px 10px',
                transition: '0.3s',
                opacity: 0.7,
                color: colors['button-font-color'][mode],
                margin: 2,
                height: 30,
            },
            'button.tabLinks:hover': {
                backgroundColor: colors['button-bg-color'][mode],
                opacity: 1,
            },
            '.tab button:hover, .tab button.active': {
                backgroundColor: colors['button-hover-color'][mode],
                opacity: 1,
                borderRadius: 3,
            },
            '.contract': {
                padding: '0.5rem',
                overflowX: 'auto',
            },
            '.contract, .execMessageOK': {
                backgroundColor: colors['output-bgok'][mode],
            },
            '.recordList': {
                borderCollapse: 'collapse',
                margin: 2,
            },
            '.recordList th': {
                color: '#880000',
                borderCollapse: 'collapse',
            },
            'td.dataColumn': {
                marginLeft: 20,
                paddingLeft: 20,
                color: colors['dataColumn-color'][mode],
            },
            '.recordList td': {
                border: `1px solid ${colors['primary-text-color'][mode]}`,
                borderStyle: 'solid',
                verticalAlign: 'top',
                padding: 5,
            },
            'div.indent5': {
                paddingLeft: '1ex',
            },
            'div.on': {
                marginLeft: 20,
                whiteSpace: 'nowrap',
            },
            button: {
                margin: 0,
                fontFamily: 'inherit',
                fontSize: 'inherit',
                lineHeight: 'inherit',
            },
            'button.text-button': {
                border: 'none',
                backgroundColor: 'inherit',
                color: colors['link-color'][mode],
                textAlign: 'left',
                paddingLeft: 0,
                paddingRight: 0,
            },
            'button.text-button:hover': {
                textDecoration: 'underline',
                boxShadow: 'none',
                fontWeight: 'bold',
            },
            'button.text-button:active, button.text-button:focus': {
                outline: 'none',
                fontWeight: 'bold',
            },
            'div.simulationBuilder': {
                overflow: 'auto',
                border: `2px solid ${colors['border-color'][mode]}`,
                paddingLeft: 5,
                marginBottom: 20,
            },
            'div.subtype': {
                overflowX: 'auto',
                padding: 5,
                margin: 1,
                border: '1px solid #eeeeee',
                borderRadius: 5,
            },
            '.explorer_button, .centertextbutton': {
                textDecoration: 'none',
                color: colors['link-color'][mode],
                backgroundColor: 'inherit',
                border: `1px solid ${colors['link-color'][mode]}`,
                margin: 5,
                borderRadius: 5,
                lineHeight: '1.1rem',
                height: 30,
                verticalAlign: 'middle',
                '&:hover': {
                    cursor: 'pointer',
                    backgroundColor: '#f1f1fa',
                    opacity: 1,
                },
            },
            '.menu': {
                gridArea: 'menu',
                overflow: 'hidden',
                border: 'none',
                backgroundColor: colors['menu-bg-color'][mode],
                padding: '10px 10px',
                margin: 0,
            },
            'div.michelsonLine': {
                paddingLeft: '2em',
                textIndent: '-2em',
            },
            '.white-space-pre, .white-space-pre-wrap': {
                fontFamily: 'source-code-pro, Menlo, Monaco, Consolas, "Courier New", monospace',
                whiteSpace: 'pre',
                overflowX: 'scroll',
            },
            a: {
                color: colors['link-color'][mode],
                backgroundColor: 'inherit',
                textDecoration: 'none',
                border: 'none',
                outline: 'none',
                cursor: 'pointer',
                transition: '0.3s',
                margin: 0,
            },
            body: {
                color: colors['primary-text-color'][mode],
            },
            ul: {
                marginTop: 0,
                marginBottom: '1rem',
            },
            li: {
                marginBottom: '0.5rem',
            },
            '.execMessage': {
                padding: '0.5rem',
                overflowX: 'auto',
            },
            // Code Highlighting
            'span.timestamp': {
                margin: 2,
                display: 'inline-block',
                overflow: 'hidden',
                whiteSpace: 'nowrap',
                color: colors['timestamp-color'][mode],
                textOverflow: 'ellipsis',
                fontFamily: 'source-code-pro, Menlo, Monaco, Consolas, "Courier New", monospace',
                verticalAlign: 'middle',
            },
            '.execMessageKO': {
                backgroundColor: colors['output-bgko'][mode],
            },
            'span.address, span.key': {
                margin: 2,
                maxWidth: 120,
                display: 'inline-block',
                overflow: 'hidden',
                whiteSpace: 'nowrap',
                color: colors['key-address-color'][mode],
                textOverflow: 'ellipsis',
                fontFamily: 'source-code-pro, Menlo, Monaco, Consolas, "Courier New", monospace',
                verticalAlign: 'middle',
            },
            'span.bytes': {
                margin: 2,
                maxWidth: 120,
                display: 'inline-block',
                overflow: 'hidden',
                whiteSpace: 'nowrap',
                color: colors['code-store-color'][mode],
                textOverflow: 'ellipsis',
                fontFamily: 'source-code-pro, Menlo, Monaco, Consolas, "Courier New", monospace',
                verticalAlign: 'middle',
            },
            'span.channel': {
                color: '#8888ff',
            },
            'span.type': {
                color: colors['code-type-color'][mode],
            },
            'span.partialType': {
                color: '#ff4444',
            },
            'span.variable': {
                color: colors['code-variable-color'][mode],
            },
            'span.store': {
                color: colors['code-store-color'][mode],
            },
            'span.param': {
                color: '#888800',
            },
            'span.iter': {
                color: '#008800',
            },
            'span.keyword': {
                color: '#bb4488',
            },
            'span.conditions': {
                color: '#55aa55',
            },
            'span.constant': {
                color: colors['code-constant-color'][mode],
            },
            'span.cons': {
                color: '#0000aa',
            },
            'span.record': {
                color: '#4288aa',
            },
            'span.conditions span': {
                color: '55aa55',
            },
            'span.gas': {
                color: 'orange',
            },
            'span.analysis': {
                color: 'orange',
            },
            'span.self': {
                color: '#0099aa',
            },
            'span.comment': {
                color: colors['code-comment-color'][mode],
            },
            'span.stack': {
                color: '#0088aa',
            },
            'span.ctt': {
                color: 'lightblue',
            },
            'span.command': {
                backgroundColor: 'hsla(0, 0%, 0%, 0.04)',
                color: '#444488',
            },
            // Progress bar
            'td.pad10': {
                paddingLeft: 10,
                paddingRight: 10,
            },
            '.progress': {
                display: 'flex',
                height: '1rem',
                overflow: 'hidden',
                fontSize: '.75rem',
                backgroundColor: '#e9ecef',
                borderRadius: '.25rem',
            },
            '.progress-bar': {
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                color: '#fff',
                textAlign: 'center',
                backgroundColor: '#007bff',
                transition: 'width .6s ease',
            },
        },
    }),
    { name: 'outputPanel' },
)(({ children }: any) => children);
