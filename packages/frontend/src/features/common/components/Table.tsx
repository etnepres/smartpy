import * as React from 'react';
import { Table as MuiTable, TableContainer, Paper } from '@mui/material';
import { TableContainerProps } from '@mui/material';

interface OwnProps extends TableContainerProps {
    header?: React.ReactElement;
    body: React.ReactElement;
    pagination?: React.ReactElement;
}

const Table: React.FC<OwnProps> = ({ header, body, pagination, ...props }) => {
    return (
        <>
            <TableContainer component={Paper} {...props}>
                <MuiTable aria-label="table">
                    {header}
                    {body}
                </MuiTable>
            </TableContainer>
            {pagination}
        </>
    );
};

export default Table;
