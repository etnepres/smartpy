import React from 'react';

import ipfs from '../../../../services/ipfs';
import { AES } from '../../../../services/crypto';
import { useSearchParams } from '../../../../hooks/useSearchParams';
import { decompressString } from '../../../../utils/encoder';
import logger from '../../../../services/logger';
import { getTemplateCode } from '../../utils/IDEUtils';
import toast from '../../../../services/toast';

interface OwnProps {
    setVolatileContract: (code: string) => void;
}

/**
 * This hook will propagate the shared contract to the Editor if an IPFS CID is provided.
 */
const SharedContractListener = ({ setVolatileContract }: OwnProps) => {
    const mounted = React.useRef(false);
    const searchParams = useSearchParams();

    /**
     * Get shared contract from IPFS.
     * @summary The contract comes encrypted from IPFS, this method fetches the encrypted content and decrypts it.
     */
    const getIpfsPermaLink = React.useCallback(() => {
        const CID = searchParams.cid as string;
        const passPhrase = searchParams.k as string;
        // If a CID was passed as Path Param, then the link was shared
        if (CID && passPhrase) {
            ipfs.getStringFromCID(CID)
                .then((encryptedContract) => {
                    setVolatileContract(AES.decrypt(encryptedContract, passPhrase));
                })
                .catch((e) => logger.error(e));
        }
    }, [searchParams.cid, searchParams.k, setVolatileContract]);

    /**
     * Get shared contract from the URL.
     */
    const getEmbeddedLink = React.useCallback(() => {
        if (searchParams.code) {
            setVolatileContract(decompressString(searchParams.code as string));
        }
    }, [searchParams.code, setVolatileContract]);

    React.useEffect(() => {
        if (!mounted.current) {
            mounted.current = true;

            if (searchParams.cid && searchParams.k) {
                getIpfsPermaLink();
            } else if (searchParams.code) {
                getEmbeddedLink();
            } else if (searchParams.template) {
                getTemplateCode(searchParams.template)
                    .then((code) => setVolatileContract(code))
                    .catch((e) => {
                        toast.error(`Template ${searchParams.template} could not be found.`);
                    });
            }
        }
    }, [
        getEmbeddedLink,
        getIpfsPermaLink,
        searchParams.cid,
        searchParams.code,
        searchParams.k,
        searchParams.template,
        setVolatileContract,
    ]);

    return null;
};

export default SharedContractListener;
