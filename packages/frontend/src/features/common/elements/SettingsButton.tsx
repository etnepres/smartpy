import React from 'react';
// Material UI
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import SettingsIcon from '@mui/icons-material/Settings';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';

interface OwnProps {
    onClick: (event: React.MouseEvent<HTMLButtonElement>) => void;
    onlyIcon: boolean;
}

export default React.forwardRef<HTMLButtonElement, OwnProps>(({ onlyIcon, ...props }, ref) => (
    <Tooltip title="Settings">
        <Button
            startIcon={onlyIcon ? null : <SettingsIcon />}
            variant="contained"
            aria-label="settings"
            endIcon={onlyIcon ? null : <ArrowDropDownIcon />}
            {...{ ...props, ref }}
        >
            {onlyIcon ? <SettingsIcon /> : 'Settings'}
        </Button>
    </Tooltip>
));
