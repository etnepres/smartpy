import React from 'react';

// Material UI
import { Theme } from '@mui/material/styles';
import createStyles from '@mui/styles/createStyles';
import makeStyles from '@mui/styles/makeStyles';
import ErrorIcon from '@mui/icons-material/Error';
import Avatar from '@mui/material/Avatar';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        failureIcon: {
            width: 48,
            height: 48,
            backgroundColor: 'rgb(237, 102, 99)',
        },
    }),
);

const FailureIndicator = () => {
    const classes = useStyles();

    return (
        <Avatar className={classes.failureIcon}>
            <ErrorIcon fontSize="large" />
        </Avatar>
    );
};

export default FailureIndicator;
