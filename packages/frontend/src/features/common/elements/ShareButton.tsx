import React from 'react';
// Material UI
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import ShareOutlinedIcon from '@mui/icons-material/ShareOutlined';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';

interface OwnProps {
    onClick: (event: React.MouseEvent<HTMLButtonElement>) => void;
    onlyIcon: boolean;
}

export default React.forwardRef<HTMLButtonElement, OwnProps>(({ onlyIcon, ...props }, ref) => (
    <Tooltip title="Share">
        <Button
            startIcon={onlyIcon ? null : <ShareOutlinedIcon />}
            color="primary"
            variant="contained"
            aria-label="share"
            endIcon={onlyIcon ? null : <ArrowDropDownIcon />}
            {...{ ...props, ref }}
        >
            {onlyIcon ? <ShareOutlinedIcon /> : 'Share'}
        </Button>
    </Tooltip>
));
