import React from 'react';
// Material UI
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import ToolsIcon from '@mui/icons-material/Build';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';

interface OwnProps {
    onClick: (event: React.MouseEvent<HTMLButtonElement>) => void;
    onlyIcon: boolean;
}

export default React.forwardRef<HTMLButtonElement, OwnProps>(({ onlyIcon, ...props }, ref) => (
    <Tooltip title="Tools">
        <Button
            startIcon={onlyIcon ? null : <ToolsIcon />}
            color="primary"
            variant="contained"
            aria-label="tools"
            endIcon={onlyIcon ? null : <ArrowDropDownIcon />}
            {...{ ...props, ref }}
        >
            {onlyIcon ? <ToolsIcon /> : 'Tools'}
        </Button>
    </Tooltip>
));
