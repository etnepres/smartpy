export interface TemplateProps {
    fileName: string;
    name: string;
    description?: string;
}

export type Templates = {
    [section: string]: TemplateProps[];
};

export type TemplatesObj = {
    [template: string]: TemplateProps;
};

export const syntaxTemplates: Templates = {
    'Syntax Examples': [
        {
            fileName: 'welcome.ml',
            name: 'Welcome',
            description: 'Welcome contract.',
        },
        {
            fileName: 'test_primitives.ml',
            name: 'Primitives',
            description: 'List of primitives.',
        },
    ],
    'Generated Contracts from SmartPy': [
        {
            fileName: 'py_FA2.ml',
            name: 'FA2',
            description: 'Simple token contract generated from the Python version.',
        },
        {
            fileName: 'py_FA2.ml',
            name: 'FA2',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_atomicSwap.ml',
            name: 'atomicSwap',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_bakingSwap.ml',
            name: 'bakingSwap',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_calculator.ml',
            name: 'calculator',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_check_dfs.ml',
            name: 'check_dfs',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_chess.ml',
            name: 'chess',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_collatz.ml',
            name: 'collatz',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_escrow.ml',
            name: 'escrow',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_fibonacci.ml',
            name: 'fibonacci',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_fifo.ml',
            name: 'fifo',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_jingleBells.ml',
            name: 'jingleBells',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_layout.ml',
            name: 'layout',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_metadata.ml',
            name: 'metadata',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_minikitties.ml',
            name: 'minikitties',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_multisig.ml',
            name: 'multisig',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_nim.ml',
            name: 'nim',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_nimLift.ml',
            name: 'nimLift',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_onchain_views.ml',
            name: 'onchain_views',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_self_entry_point.ml',
            name: 'self_entry_point',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_send_back.ml',
            name: 'send_back',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_shuffle.ml',
            name: 'shuffle',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_storeValue.ml',
            name: 'storeValue',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_syntax.ml',
            name: 'syntax',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_testCheckSignature.ml',
            name: 'testCheckSignature',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_testEmpty.ml',
            name: 'testEmpty',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_testHashFunctions.ml',
            name: 'testHashFunctions',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_testLocalRecord.ml',
            name: 'testLocalRecord',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_testMin.ml',
            name: 'testMin',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_testSend.ml',
            name: 'testSend',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_testVoid.ml',
            name: 'testVoid',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_test_import.ml',
            name: 'test_import',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_tictactoe.ml',
            name: 'tictactoe',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_tictactoeFactory.ml',
            name: 'tictactoeFactory',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_voting.ml',
            name: 'voting',
            description: 'generated from the Python version.',
        },
        {
            fileName: 'py_welcome.ml',
            name: 'welcome',
            description: 'generated from the Python version.',
        },
    ],
};

export const contractTemplates: Templates = {
    'Simple Examples': [
        {
            fileName: 'calculator.ml',
            name: 'Calculator',
            description: 'A small calculator.',
        },
        {
            fileName: 'collatz.ml',
            name: 'Collatz',
            description: 'Contract calls.',
        },
    ],
};

export const getAllTemplates = (): TemplatesObj => {
    const availableContractTemplates = Object.keys(contractTemplates).reduce(
        (state, key) => [...state, ...contractTemplates[key]],
        [] as TemplateProps[],
    );
    const availableSyntaxTemplates = Object.keys(syntaxTemplates).reduce(
        (state, key) => [...state, ...syntaxTemplates[key]],
        [] as TemplateProps[],
    );

    return [...availableContractTemplates, ...availableSyntaxTemplates].reduce(
        (state, template) => ({ ...state, [template.fileName]: template }),
        {},
    );
};
