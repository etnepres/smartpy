import { createAction } from 'typesafe-actions';
import { IDEContract, IDESettings } from 'SmartPyModels';
import { IDENewcomerGuideSteps } from '../common/enums/ide';

export enum Actions {
    ML_IDE_UPDATE_CONTRACT = 'ML_IDE_UPDATE_CONTRACT',
    ML_IDE_ADD_CONTRACT = 'ML_IDE_ADD_CONTRACT',
    ML_IDE_REMOVE_CONTRACT = 'ML_IDE_REMOVE_CONTRACT',
    ML_IDE_SELECT_CONTRACT = 'ML_IDE_SELECT_CONTRACT',
    // VOLATILE CONTRACT
    ML_IDE_SET_VOLATILE_CONTRACT = 'ML_IDE_SET_VOLATILE_CONTRACT',
    // SETTINGS
    ML_IDE_UPDATE_SETTINGS = 'ML_IDE_UPDATE_SETTINGS',
    // ERROR DIALOG
    ML_IDE_SHOW_ERROR = 'ML_IDE_SHOW_ERROR',
    ML_IDE_HIDE_ERROR = 'ML_IDE_HIDE_ERROR',
    // SHORTCUTS DIALOG
    ML_IDE_SHOW_SHORTCUTS = 'ML_IDE_SHOW_SHORTCUTS',
    ML_IDE_HIDE_SHORTCUTS = 'ML_IDE_HIDE_SHORTCUTS',
    // TEMPLATES
    ML_IDE_TOGGLE_FAVORITE_TEMPLATE = 'ML_IDE_TOGGLE_FAVORITE_TEMPLATE',
    ML_IDE_LOAD_TEMPLATE = 'ML_IDE_LOAD_TEMPLATE',
    // NEWCOMER
    ML_IDE_UPDATE_NEWCOMER_GUIDE_STEP = 'ML_IDE_UPDATE_NEWCOMER_GUIDE_STEP',
    ML_IDE_TOGGLE_NEWCOMER_DIALOG = 'ML_IDE_TOGGLE_NEWCOMER_DIALOG',
}

// CONTRACTS

export const updateContract = createAction(Actions.ML_IDE_UPDATE_CONTRACT)<IDEContract>();
export const addContract = createAction(Actions.ML_IDE_ADD_CONTRACT)<IDEContract, boolean>();
export const removeContract = createAction(Actions.ML_IDE_REMOVE_CONTRACT)<string>();
export const selectContract = createAction(Actions.ML_IDE_SELECT_CONTRACT)<string>();

// VOLATILE CONTRACT

export const setVolatileContract = createAction(Actions.ML_IDE_SET_VOLATILE_CONTRACT)<string>();

// ERROR

export const showError = createAction(Actions.ML_IDE_SHOW_ERROR, (error: string) => error)<string>();
export const hideError = createAction(Actions.ML_IDE_HIDE_ERROR)<void>();

// Shortcuts

export const showShortcuts = createAction(Actions.ML_IDE_SHOW_SHORTCUTS, (platform: string) => platform)<string>();
export const hideShortcuts = createAction(Actions.ML_IDE_HIDE_SHORTCUTS)<void>();

// Templates

export const toggleFavoriteTemplate = createAction(Actions.ML_IDE_TOGGLE_FAVORITE_TEMPLATE)<string>();
export const loadTemplate = createAction(Actions.ML_IDE_LOAD_TEMPLATE)<string>();

// SETTINGS

export const updateSettings = createAction(
    Actions.ML_IDE_UPDATE_SETTINGS,
    (settings: IDESettings) => settings,
)<IDESettings>();

// NEWCOMER
export const toggleNewcomerDialog = createAction(Actions.ML_IDE_TOGGLE_NEWCOMER_DIALOG)<boolean | void>();
export const updateNewcomerGuideStep = createAction(Actions.ML_IDE_UPDATE_NEWCOMER_GUIDE_STEP)<IDENewcomerGuideSteps>();

const actions = {
    updateContract,
    addContract,
    removeContract,
    updateSettings,
    selectContract,
    showError,
    hideError,
    showShortcuts,
    hideShortcuts,
    loadTemplate,
    updateNewcomerGuideStep,
    toggleNewcomerDialog,
    toggleFavoriteTemplate,
    setVolatileContract,
};

export default actions;
