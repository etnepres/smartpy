import React from 'react';

// Material UI
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import createStyles from '@mui/styles/createStyles';
import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';

// State Management
import useTranslation from '../../i18n/hooks/useTranslation';
import { getBase } from '../../../utils/url';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
            margin: 20,
        },
        card: {
            borderWidth: 1,
            borderStyle: 'solid',
            borderColor: theme.palette.primary.main,
            boxShadow: `5px 5px 0 0 ${theme.palette.primary.main}`,
            marginBottom: 20,
        },
        title: {
            color: theme.palette.primary.main,
            fontWeight: 'bold',
            marginBottom: 20,
        },
        description: {
            color: theme.palette.primary.light,
            marginBottom: 20,
        },
        exampleGrid: {
            marginBottom: 20,
        },
        arrow: {
            display: 'flex',
            justifyContent: 'center',
        },
        description2: {
            fontWeight: 'bold',
            marginBottom: 20,
        },
        innerDescription: {
            fontWeight: 'bold',
            margin: 10,
        },
        list: {
            width: '100%',
            backgroundColor: theme.palette.background.default,
            padding: 0,
        },
        link: {
            color: theme.palette.text.primary,
            textDecoration: 'none',
            border: 'none',
            outline: 'none',
            cursor: 'pointer',
            transition: '0.3s',
            margin: 0,
        },
    }),
);

const Faq: React.FC = () => {
    const classes = useStyles();
    const t = useTranslation();

    return (
        <div className={classes.root} id="faq">
            <Typography variant="h4" className={classes.title}>
                {t('help.faqAcron')}
            </Typography>
            <Typography gutterBottom variant="body1" className={classes.description}>
                Please see our answers to <Link href={`${getBase()}/faq.html`}>{t('help.faq')}</Link>
            </Typography>
        </div>
    );
};

export default Faq;
