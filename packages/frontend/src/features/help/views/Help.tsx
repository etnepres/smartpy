import React from 'react';

// Material UI
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import createStyles from '@mui/styles/createStyles';
import Divider from '@mui/material/Divider';
import Fab from '@mui/material/Fab';
import TelegramIcon from '@mui/icons-material/Telegram';

// Local Components
import Footer from '../../navigation/components/Footer';
import ReferenceManual from '../components/ReferenceManual';
import Faq from '../components/Faq';
import ArticlesGuides from '../../common/components/ArticlesGuides';

// Local Elements
import StackExchangeIcon from '../../common/elements/icons/StackExchange';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            backgroundColor: theme.palette.background.paper,
        },
        extendedIcon: {
            marginRight: theme.spacing(1),
        },
        fabLabel: {
            paddingRight: 5,
            textTransform: 'none',
        },
        topButtons: {
            display: 'flex',
            justifyContent: 'center',
            margin: 20,
            [theme.breakpoints.down('sm')]: {
                display: 'flex',
                flexWrap: 'wrap',
            },
        },
        fabButton: {
            margin: 5,
            borderWidth: 1,
            borderStyle: 'solid',
            borderColor: theme.palette.primary.main,
            [theme.breakpoints.down('sm')]: {
                flexGrow: 1,
            },
        },
    }),
);

const Help = () => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <div className={classes.topButtons}>
                <Fab
                    variant="extended"
                    color="secondary"
                    aria-label="Telegram"
                    href="https://t.me/SmartPy_io"
                    target="_blank"
                    className={classes.fabButton}
                    classes={{ root: classes.fabLabel }}
                >
                    <TelegramIcon className={classes.extendedIcon} color="primary" />
                    Telegram
                </Fab>
                <Fab
                    variant="extended"
                    color="secondary"
                    aria-label="Tezos Stack Exchange"
                    href="https://tezos.stackexchange.com/questions/ask?tags=smartpy"
                    target="_blank"
                    className={classes.fabButton}
                    classes={{ root: classes.fabLabel }}
                >
                    <StackExchangeIcon className={classes.extendedIcon} color="primary" />
                    Tezos Stack Exchange
                </Fab>
            </div>
            <Divider />
            <ReferenceManual />
            <Divider />
            <Faq />
            <Divider />
            <ArticlesGuides />
            <Footer />
        </div>
    );
};

export default Help;
