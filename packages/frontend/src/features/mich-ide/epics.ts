import { RootAction, RootState, Services } from 'SmartPyTypes';
import { Epic } from 'redux-observable';
import { of } from 'rxjs';
import { filter, map, catchError } from 'rxjs/operators';
import { isActionOf } from 'typesafe-actions';

import { addContract, selectContract, showError } from './actions';
import { getLatestModifiedContract } from './selectors';

export const addContractEpic: Epic<RootAction, RootAction, RootState, Services> = (action$, state$) =>
    action$.pipe(
        filter(isActionOf([addContract])),
        map(() => selectContract(getLatestModifiedContract(state$.value)?.id || '')),
        catchError((message: string) => of(showError(message))),
    );
