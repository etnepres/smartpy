import { createAction } from 'typesafe-actions';
import { IDEContract, MichelsonIDESettings } from 'SmartPyModels';

export enum Actions {
    UPDATE_MICHELSON_IDE_CONTRACT = 'UPDATE_MICHELSON_IDE_CONTRACT',
    ADD_MICHELSON_IDE_CONTRACT = 'ADD_MICHELSON_IDE_CONTRACT',
    REMOVE_MICHELSON_IDE_CONTRACT = 'REMOVE_MICHELSON_IDE_CONTRACT',
    SELECT_MICHELSON_IDE_CONTRACT = 'SELECT_MICHELSON_IDE_CONTRACT',
    // VOLATILE CONTRACT
    SET_VOLATILE_MICHELSON_CONTRACT = 'SET_VOLATILE_MICHELSON_CONTRACT',
    // SETTINGS
    UPDATE_MICHELSON_IDE_SETTINGS = 'UPDATE_MICHELSON_IDE_SETTINGS',
    // ERROR DIALOG
    SHOW_MICHELSON_ERROR = 'SHOW_MICHELSON_ERROR',
    HIDE_MICHELSON_ERROR = 'HIDE_MICHELSON_ERROR',
    // SHORTCUTS DIALOG
    SHOW_MICHELSON_IDE_SHORTCUTS = 'SHOW_MICHELSON_IDE_SHORTCUTS',
    HIDE_MICHELSON_IDE_SHORTCUTS = 'HIDE_MICHELSON_IDE_SHORTCUTS',
}

// CONTRACTS

export const updateContract = createAction(Actions.UPDATE_MICHELSON_IDE_CONTRACT)<IDEContract>();
export const addContract = createAction(Actions.ADD_MICHELSON_IDE_CONTRACT)<IDEContract, boolean>();
export const removeContract = createAction(Actions.REMOVE_MICHELSON_IDE_CONTRACT)<string>();
export const selectContract = createAction(Actions.SELECT_MICHELSON_IDE_CONTRACT)<string>();

// VOLATILE CONTRACT

export const setVolatileContract = createAction(Actions.SET_VOLATILE_MICHELSON_CONTRACT)<string>();

// ERROR

export const showError = createAction(Actions.SHOW_MICHELSON_ERROR, (error: string) => error)<string>();
export const hideError = createAction(Actions.HIDE_MICHELSON_ERROR)<void>();

// Shortcuts

export const showShortcuts = createAction(
    Actions.SHOW_MICHELSON_IDE_SHORTCUTS,
    (platform: string) => platform,
)<string>();
export const hideShortcuts = createAction(Actions.HIDE_MICHELSON_IDE_SHORTCUTS)<void>();

// SETTINGS

export const updateSettings = createAction(
    Actions.UPDATE_MICHELSON_IDE_SETTINGS,
    (settings: MichelsonIDESettings) => settings,
)<MichelsonIDESettings>();

const actions = {
    setVolatileContract,
    updateContract,
    addContract,
    removeContract,
    selectContract,
    updateSettings,
    showError,
    hideError,
    showShortcuts,
    hideShortcuts,
};

export default actions;
