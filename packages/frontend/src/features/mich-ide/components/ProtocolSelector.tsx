import React from 'react';
import { useDispatch } from 'react-redux';

import createStyles from '@mui/styles/createStyles';
import makeStyles from '@mui/styles/makeStyles';
import MenuItem from '@mui/material/MenuItem';
import Select, { SelectChangeEvent } from '@mui/material/Select';

import selectors from '../../../store/selectors';
import actions from '../../../store/root-action';
import { DefaultProtocol, Protocol } from '../../../constants/protocol';

const useStyles = makeStyles(() =>
    createStyles({
        select: {
            padding: 12,
            minWidth: 90,
        },
    }),
);

const ProtocolSelector: React.FC = () => {
    const classes = useStyles();
    const settings = selectors.michelsonEditor.useSettings();
    const dispatch = useDispatch();

    const handleProtocolChange = (event: SelectChangeEvent<string>) => {
        dispatch(actions.michelsonEditor.updateSettings({ protocol: event.target.value }));
    };

    return (
        <Select
            variant="filled"
            value={settings.protocol || DefaultProtocol}
            classes={{ filled: classes.select }}
            onChange={handleProtocolChange}
        >
            {Object.values(Protocol).map((protocol) => (
                <MenuItem key={protocol} value={protocol}>
                    {protocol}
                </MenuItem>
            ))}
        </Select>
    );
};

export default ProtocolSelector;
