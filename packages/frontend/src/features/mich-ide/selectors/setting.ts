import { RootState } from 'SmartPyTypes';
import { MichelsonIDESettings } from 'SmartPyModels';
import { useSelector } from 'react-redux';

export const useSettings = () =>
    useSelector<RootState, MichelsonIDESettings>((state: RootState) => ({
        ...state.michelsonEditor.settings,
    }));
