import { ScriptProps } from '../../../utils/asyncScriptInjector';
import { getBase } from '../../../utils/url';

export default [
    {
        name: 'smart.js',
        src: `${getBase()}/static/js/smart.js`,
        type: 'text/javascript',
    },
    {
        name: 'smartmljs.bc.js',
        src: `${getBase()}/static/js/smartmljs.bc.js`,
        type: 'text/javascript',
    },
] as ScriptProps[];
