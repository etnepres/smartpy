import React from 'react';

import { styled, Container as MuiContainer, Typography, Divider } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import ArrowBackOutlinedIcon from '@mui/icons-material/ArrowBackOutlined';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import DeleteForeverOutlinedIcon from '@mui/icons-material/DeleteForeverOutlined';

import useTranslation from 'src/features/i18n/hooks/useTranslation';
import useWalletContext from 'src/features/wallet/hooks/useWalletContext';
import Fab from 'src/features/common/elements/Fab';
import RouterFab from 'src/features/navigation/elements/RouterFab';
import UnlockWalletDialog from '../views/UnlockWalletDialog';
import { StorageState } from 'smartpy-wallet-storage';
import CreateWalletDialog from '../views/CreateWalletDialog';
import AddAccountDialog from '../views/AddAccountDialog';
import AccountsView from '../views/Accounts';

const Container = styled(MuiContainer)(() => ({
    display: 'flex',
    flexDirection: 'column',
    padding: 20,
}));

const TopDiv = styled('div')(() => ({
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 20,
}));

const LockResetDiv = styled('div')(() => ({
    display: 'flex',
    justifyContent: 'end',
    alignItems: 'center',
    marginBottom: 10,
}));

const Accounts = () => {
    const [addingAccount, setAddingAccount] = React.useState(false);
    const { accounts, removeAccounts, state, lock, resetAccounts } = useWalletContext();
    const t = useTranslation();

    switch (state) {
        case StorageState.LOCKED:
            return <UnlockWalletDialog open={true} />;
        case StorageState.EMPTY:
            return <CreateWalletDialog open={true} />;
    }

    return (
        <Container>
            <TopDiv>
                <RouterFab color="primary" aria-label={t('wallet.labels.goBackToMenu')} to="/wallet">
                    <ArrowBackOutlinedIcon />
                    {t('wallet.labels.goBackToMenu')}
                </RouterFab>
                <Fab
                    color="secondary"
                    sx={{ borderWidth: 2, borderStyle: 'solid', borderColor: 'primary.main' }}
                    onClick={() => setAddingAccount(true)}
                >
                    <AddIcon /> {t('wallet.labels.addAccount')}
                </Fab>
            </TopDiv>
            <Typography textAlign="center" variant="overline">
                {t('wallet.labels.accounts')}
            </Typography>
            <Divider sx={{ marginTop: 1, marginBottom: 1 }} />
            <LockResetDiv>
                <Fab
                    sx={{
                        backgroundColor: 'error.light',
                    }}
                    color="secondary"
                    onClick={resetAccounts}
                >
                    <DeleteForeverOutlinedIcon sx={{ marginRight: 1 }} /> {t('wallet.labels.resetAccounts')}
                </Fab>
                <Divider flexItem orientation="vertical" sx={{ margin: 1 }} />
                <Fab
                    color="secondary"
                    sx={{ borderWidth: 2, borderStyle: 'solid', borderColor: 'warning.light' }}
                    onClick={lock}
                >
                    <LockOutlinedIcon sx={{ marginRight: 1 }} /> {t('common.lock')}
                </Fab>
            </LockResetDiv>
            <AccountsView removeAccounts={removeAccounts} accounts={accounts} />
            <AddAccountDialog
                fullWidth
                open={addingAccount}
                onClose={() => setAddingAccount(false)}
                aria-labelledby="add-account-dialog-title"
            />
        </Container>
    );
};

export default Accounts;
