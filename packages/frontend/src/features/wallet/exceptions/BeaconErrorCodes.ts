enum BeaconErrorCodes {
    NOT_INSTALLED = 'wallet.beacon.not_installed',
    NOT_CONNECTED = 'wallet.beacon.not_connected',
    NOT_ABLE_TO_ORIGINATE_CONTRACT = 'wallet.beacon.not_able_to_originate_contract',
    NOT_ABLE_TO_GET_ACCOUNT = 'wallet.beacon.not_able_to_get_account',
    NOT_ABLE_TO_GET_BALANCE = 'wallet.beacon.not_able_to_get_balance',
}

export default BeaconErrorCodes;
