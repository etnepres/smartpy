enum SecretKeyErrorCodes {
    NOT_ABLE_TO_GET_ACCOUNT = 'wallet.secretKey.not_able_to_get_account',
    NOT_ABLE_TO_REVEAL_ACCOUNT = 'wallet.secretKey.not_able_to_reveal_account',
    NOT_ABLE_TO_IMPORT_ACCOUNT = 'wallet.secretKey.not_able_to_import_account',
    NOT_ABLE_TO_ORIGINATE_CONTRACT = 'wallet.secretKey.not_able_to_originate_contract',
    NOT_ABLE_TO_GET_BALANCE = 'wallet.secretKey.not_able_to_get_balance',
    NOT_ABLE_TO_PREPARE_ORIGINATION = 'wallet.secretKey.not_able_to_prepare_origination',
}

export default SecretKeyErrorCodes;
