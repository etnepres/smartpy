import { RootState } from 'SmartPyTypes';
import { useSelector } from 'react-redux';
import { AccountInformation } from 'SmartPyModels';

export const useAccountInfo = () =>
    useSelector<RootState, AccountInformation>(({ wallet }: RootState) => wallet.accountInfo);
