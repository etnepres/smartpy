import React from 'react';
import { useDispatch } from 'react-redux';

import {
    Alert,
    Button,
    Chip,
    Divider,
    FormControl,
    Grid,
    InputLabel,
    MenuItem,
    Paper,
    Select,
    SelectChangeEvent,
    styled,
    Theme,
    Tooltip,
    Typography,
    useMediaQuery,
} from '@mui/material';

import { createStyles, makeStyles } from '@mui/styles';

import CircularProgressWithText from '../../loader/components/CircularProgressWithText';
import { useAccountInfo } from '../selectors/account';
import { convertUnitWithSymbol, AmountUnit } from '../../../utils/units';
import AddressAvatar from '../../common/components/AddressAvatar';
import useTranslation from '../../i18n/hooks/useTranslation';
import { copyToClipboard } from '../../../utils/clipboard';
import { shortenText } from '../../../utils/style/shortenner';
import { appendClasses } from '../../../utils/style/classes';
import { updateAccountInfo, updateNetworkInfo } from '../actions';
import WalletServices from '../services';
import logger from '../../../services/logger';
import { AccountSource } from '../constants/sources';
import debounce from '../../../utils/debounce';
import { Network } from 'src/constants/networks';
import { smartpy as RPC } from 'src/constants/rpc';
import { useNetworkInfo } from '../selectors/network';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        centralizedContainer: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'column',
            padding: 10,
        },
        divider: {
            margin: theme.spacing(1, 0, 1, 0),
        },
        chip: {
            minHeight: 32,
        },
        card: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'stretch',
            backgroundColor: theme.palette.background.default,
            boxShadow: '0 2px 4px rgba(0,0,0,0.15), 0 4px 2px rgba(0,0,0,0.22)',
            padding: theme.spacing(1),
        },
        avatarSection: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'column',
            flexGrow: 1,
            borderRadius: 4,
            padding: 10,
            backgroundColor: theme.palette.background.paper,
        },
        infoSection: {
            width: 424,
        },
        alignSelf: {
            alignSelf: 'stretch',
        },
        marginTop: {
            marginTop: 10,
        },
        section: {
            padding: 10,
            boxShadow: '0 2px 4px rgba(0,0,0,0.15), 0 4px 2px rgba(0,0,0,0.22)',
        },
    }),
);

const debouncer = debounce(1000);

const AvatarSection = styled('div')(({ theme }) => ({
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    flexGrow: 1,
    borderRadius: 4,
    padding: 10,
    backgroundColor: theme.palette.background.paper,
}));

interface OwnProps {
    skipTitles?: boolean;
    disableNetworkSelection?: boolean;
}

const AccountInfo: React.FC<OwnProps> = ({ disableNetworkSelection = false, skipTitles = false }) => {
    const isMobile = useMediaQuery('(max-width:600px)');
    const classes = useStyles();
    const accountInformation = useAccountInfo();
    const { network } = useNetworkInfo();
    const t = useTranslation();
    const dispatch = useDispatch();

    React.useEffect(() => {
        debouncer(async () => {
            if (accountInformation?.pkh && accountInformation.source === AccountSource.SMARTPY_FAUCET) {
                const activationRequired = await WalletServices[AccountSource.SMARTPY_FAUCET].requiresActivation();
                if (accountInformation.activationRequired !== activationRequired) {
                    dispatch(
                        updateAccountInfo({
                            activationRequired,
                        }),
                    );
                }
            }
        });
    }, [
        accountInformation.activating,
        accountInformation.activationRequired,
        accountInformation.revealRequired,
        accountInformation.source,
        accountInformation?.pkh,
        dispatch,
    ]);

    const revealAccount = React.useCallback(async () => {
        dispatch(updateAccountInfo({ revealing: true, errors: '' }));
        try {
            if (accountInformation?.source && WalletServices[accountInformation.source]) {
                await WalletServices[accountInformation.source].reveal();
                dispatch(updateAccountInfo({ revealRequired: false }));
            }
        } catch (e) {
            logger.debug('Failed to reveal account.', e);
            dispatch(updateAccountInfo({ errors: e.message }));
        }
        dispatch(updateAccountInfo({ revealing: false }));
    }, [accountInformation.source, dispatch]);

    const activateAccount = React.useCallback(async () => {
        dispatch(updateAccountInfo({ activating: true, errors: '' }));
        try {
            if (accountInformation.source === AccountSource.SMARTPY_FAUCET) {
                await WalletServices[AccountSource.SMARTPY_FAUCET].activateAccount();
                const balance = await WalletServices[AccountSource.SMARTPY_FAUCET].getBalance();
                dispatch(updateAccountInfo({ activationRequired: false, balance }));
            }
        } catch (e) {
            logger.debug('Failed to activate account.', e);
            dispatch(updateAccountInfo({ errors: e.message }));
        }
        dispatch(updateAccountInfo({ activating: false }));
    }, [accountInformation.source, dispatch]);

    const ActivationOrRevealButtons = React.useMemo(() => {
        if (accountInformation.activationRequired) {
            return (
                <>
                    <Divider className={classes.divider} />
                    {accountInformation.activating ? (
                        <CircularProgressWithText size={24} msg={t('wallet.accountInfo.activating')} />
                    ) : (
                        <Button variant="outlined" fullWidth onClick={activateAccount}>
                            {t('wallet.accountInfo.activate')}
                        </Button>
                    )}
                </>
            );
        }

        if (accountInformation.revealRequired) {
            return (
                <>
                    <Divider className={classes.divider} />
                    {accountInformation.revealing ? (
                        <CircularProgressWithText size={24} msg={t('wallet.accountInfo.revealing')} />
                    ) : (
                        <Button variant="outlined" fullWidth onClick={revealAccount}>
                            {t('wallet.accountInfo.reveal')}
                        </Button>
                    )}
                </>
            );
        }
    }, [
        accountInformation.activationRequired,
        accountInformation.revealRequired,
        accountInformation.activating,
        accountInformation.revealing,
        classes.divider,
        t,
        activateAccount,
        revealAccount,
    ]);

    const copyAddress = React.useCallback(() => {
        if (accountInformation?.pkh) {
            copyToClipboard(accountInformation.pkh);
        }
    }, [accountInformation]);

    const handleNetworkSelection = React.useCallback(
        async (e: SelectChangeEvent<string>) => {
            const network = e.target.value;
            const rpc = RPC[network];
            if (rpc && accountInformation.source) {
                await WalletServices[accountInformation.source].setRPC(rpc);
                const info = await WalletServices[accountInformation.source].getInformation();
                dispatch(updateNetworkInfo({ rpc, network }));
                dispatch(updateAccountInfo(info));
            }
        },
        [accountInformation.source, dispatch],
    );

    if (accountInformation.isLoading) {
        return <CircularProgressWithText size={64} margin={40} msg={t('common.loading')} />;
    }

    if (accountInformation.pkh) {
        return (
            <div className={classes.centralizedContainer}>
                {!skipTitles ? <Typography variant="overline">{t('wallet.accountInfo.title')}</Typography> : null}
                {!skipTitles && !accountInformation.errors ? (
                    <div className={classes.centralizedContainer}>
                        <Alert severity="success">{t('wallet.accountInfo.loadedWithSuccess')}</Alert>
                    </div>
                ) : null}
                {accountInformation.errors ? (
                    <div className={classes.centralizedContainer}>
                        <Alert severity="error">{accountInformation.errors}</Alert>
                    </div>
                ) : null}
                <Paper className={classes.card}>
                    <Grid container alignItems="stretch" justifyContent="center">
                        <Grid item xs={12} md={5} display="flex">
                            <AvatarSection>
                                <AddressAvatar address={accountInformation.pkh} size="large" />
                                <Divider className={classes.divider} />
                                {disableNetworkSelection ? (
                                    <Chip
                                        variant="outlined"
                                        color="primary"
                                        className={appendClasses(classes.alignSelf, classes.chip)}
                                        label={network}
                                    />
                                ) : (
                                    <FormControl fullWidth size="small">
                                        <InputLabel id="network">Network</InputLabel>
                                        <Select
                                            name="network"
                                            labelId="network"
                                            label="Network"
                                            value={network || Network.MAINNET}
                                            onChange={handleNetworkSelection}
                                        >
                                            {Object.values(Network)
                                                .filter((n) => n !== Network.CUSTOM)
                                                .map((network) => {
                                                    return (
                                                        <MenuItem value={network} key={network}>
                                                            {network}
                                                        </MenuItem>
                                                    );
                                                })}
                                        </Select>
                                    </FormControl>
                                )}
                            </AvatarSection>
                        </Grid>
                        <Grid
                            item
                            xs={12}
                            md={7}
                            className={appendClasses(classes.centralizedContainer, isMobile ? '' : classes.infoSection)}
                        >
                            <Tooltip title={t('common.copy') as string} aria-label="copy" placement="top">
                                <Chip
                                    className={appendClasses(classes.alignSelf, classes.chip)}
                                    label={isMobile ? shortenText(accountInformation.pkh, 20) : accountInformation.pkh}
                                    color="primary"
                                    clickable
                                    onClick={copyAddress}
                                />
                            </Tooltip>
                            <Divider className={classes.divider} />
                            <Chip
                                className={appendClasses(classes.alignSelf, classes.chip)}
                                label={convertUnitWithSymbol(
                                    accountInformation.balance || 0,
                                    AmountUnit.uTez,
                                    AmountUnit.tez,
                                )}
                            />
                            {ActivationOrRevealButtons}
                        </Grid>
                    </Grid>
                </Paper>
            </div>
        );
    }

    return null;
};

export default AccountInfo;
