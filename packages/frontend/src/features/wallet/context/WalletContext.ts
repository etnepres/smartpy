import { createContext } from 'react';

import { OriginatedContract } from 'smartpy-wallet-storage/dist/types/services/contract/storage';
import { FaucetAccount } from 'smartpy-wallet-storage/dist/types/services/faucet/storage';

import { StorageState, TezosAccount } from 'smartpy-wallet-storage';

export interface IWalletContext {
    loading: boolean;
    state: StorageState;
    contracts: OriginatedContract[];
    faucetAccounts: FaucetAccount[];
    accounts: TezosAccount[];
    removeContracts: (ids: string[]) => void;
    removeFaucetAccounts: (ids: string[]) => void;
    removeAccounts: (ids: string[]) => void;
    addContract: (contract: { name: string; address: string }) => void;
    addFaucetAccount: (contract: FaucetAccount) => void;
    addAccount: (account: Omit<TezosAccount, 'id'>) => void;
    updateContract: (contract: OriginatedContract) => void;
    updateFaucetAccount: (contract: FaucetAccount) => void;
    updateAccount: (account: TezosAccount) => void;
    unlock: (pw: string) => void;
    lock: () => void;
    resetAccounts: () => void;
    initWallet: (pw: string) => void;
}

const contextStub = {
    loading: true,
    state: StorageState.LOCKED,
    contracts: [],
    faucetAccounts: [],
    accounts: [],
    removeContracts: () => null,
    removeFaucetAccounts: () => null,
    removeAccounts: () => null,
    addContract: () => null,
    addFaucetAccount: () => null,
    addAccount: () => null,
    updateContract: () => null,
    updateFaucetAccount: () => null,
    updateAccount: () => null,
    unlock: () => null,
    lock: () => null,
    resetAccounts: () => null,
    initWallet: () => null,
};

const WalletContext = createContext<IWalletContext>(contextStub);

export default WalletContext;
