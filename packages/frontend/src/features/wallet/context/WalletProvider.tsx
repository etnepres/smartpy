import React from 'react';
import Logger from 'src/services/logger';
import WalletContext from './WalletContext';
import WalletStorage from 'smartpy-wallet-storage';
import { OriginatedContract } from 'smartpy-wallet-storage/dist/types/services/contract/storage';
import { FaucetAccount } from 'smartpy-wallet-storage/dist/types/services/faucet/storage';
import { StorageState, TezosAccount } from 'smartpy-wallet-storage';
import { lookupContractNetwork } from 'src/utils/tezosRpc';

const WalletProvider: React.FC = ({ children }) => {
    const [loading, setLoading] = React.useState(true);
    const [contracts, setContracts] = React.useState<OriginatedContract[]>([]);
    const [faucetAccounts, setFaucetAccounts] = React.useState<FaucetAccount[]>([]);
    const [state, setState] = React.useState(StorageState.LOCKED);
    const [accounts, setAccounts] = React.useState<TezosAccount[]>([]);

    const fetchContractsFromStorage = () => {
        const contracts = WalletStorage.contract.findAll();
        setContracts(Object.values(contracts));
    };

    const fetchFaucetAccountsFromStorage = () => {
        const accounts = WalletStorage.faucet.findAll();
        setFaucetAccounts(Object.values(accounts));
    };

    const fetchAccountsFromStorage = () => {
        const _state = WalletStorage.account.state() as StorageState;
        setState(_state);
        if (_state === StorageState.UNLOCKED) {
            const accounts = WalletStorage.account.findAll();
            console.log(accounts);
            setAccounts(Object.values(accounts));
        }
    };

    React.useEffect(() => {
        try {
            fetchContractsFromStorage();
            fetchFaucetAccountsFromStorage();
            fetchAccountsFromStorage();
            setLoading(false);
        } catch (e) {
            Logger.debug(e);
        }
    }, []);

    const unlock = (pw: string) => {
        WalletStorage.account.unlock(pw);
        fetchAccountsFromStorage();
    };

    const lock = () => {
        WalletStorage.account.lock();
        fetchAccountsFromStorage();
    };

    const resetAccounts = () => {
        WalletStorage.account.clear();
        fetchAccountsFromStorage();
    };

    const initWallet = (pw: string) => {
        WalletStorage.account.init(pw);
        fetchAccountsFromStorage();
    };

    const removeContracts = (ids: string[]) => {
        try {
            ids.forEach(WalletStorage.contract.removeById);
            fetchContractsFromStorage();
        } catch (e) {
            Logger.debug(e);
        }
    };

    const removeFaucetAccounts = (ids: string[]) => {
        try {
            ids.forEach(WalletStorage.faucet.removeById);
            fetchFaucetAccountsFromStorage();
        } catch (e) {
            Logger.debug(e);
        }
    };

    const removeAccounts = (ids: string[]) => {
        try {
            if (WalletStorage.account.state() === StorageState.UNLOCKED) {
                ids.forEach(WalletStorage.account.removeById);
                fetchAccountsFromStorage();
            }
        } catch (e) {
            Logger.debug(e);
        }
    };

    const addContract = async (contract: { name: string; address: string }) => {
        try {
            WalletStorage.contract.persist({
                ...contract,
                network: (await lookupContractNetwork(contract.address)) || undefined,
            });
            fetchContractsFromStorage();
        } catch (e) {
            Logger.debug(e);
        }
    };

    const updateContract = async (contract: OriginatedContract) => {
        try {
            WalletStorage.contract.merge({
                ...contract,
            });
            fetchContractsFromStorage();
        } catch (e) {
            Logger.debug(e);
        }
    };

    const addFaucetAccount = async (account: FaucetAccount) => {
        try {
            WalletStorage.faucet.persist(account);
            fetchFaucetAccountsFromStorage();
        } catch (e) {
            Logger.debug(e);
        }
    };

    const updateFaucetAccount = async (account: FaucetAccount) => {
        try {
            WalletStorage.faucet.merge({
                ...account,
            });
            fetchFaucetAccountsFromStorage();
        } catch (e) {
            Logger.debug(e);
        }
    };

    const addAccount = async (account: Omit<TezosAccount, 'id'>) => {
        try {
            WalletStorage.account.persist(account);
            fetchAccountsFromStorage();
        } catch (e) {
            Logger.debug(e);
        }
    };

    const updateAccount = async (account: TezosAccount) => {
        try {
            WalletStorage.account.merge({
                ...account,
            });
            fetchAccountsFromStorage();
        } catch (e) {
            Logger.debug(e);
        }
    };

    return (
        <WalletContext.Provider
            value={{
                loading,
                state,
                contracts,
                accounts,
                faucetAccounts,
                removeContracts,
                removeFaucetAccounts,
                removeAccounts,
                addContract,
                addFaucetAccount,
                addAccount,
                updateContract,
                updateFaucetAccount,
                updateAccount,
                unlock,
                lock,
                resetAccounts,
                initWallet,
            }}
        >
            {children}
        </WalletContext.Provider>
    );
};

export default WalletProvider;
