import { InMemorySigner } from '@taquito/signer';
import { AccountSource } from '../../constants/sources';
import WalletError from '../../exceptions/WalletError';
import SecretKeyErrorCodes from '../../exceptions/SecretKeyErrorCodes';
import AbstractWallet from '../AbstractWallet';
import LocalSigner from './LocalSigner';

export type SecretKeyParams = {
    secretKey: string;
    password?: string;
};

class SecretKeytWallet extends AbstractWallet {
    constructor() {
        super(AccountSource.SMARTPY_SECRET_KEY);
    }

    /**
     * @description Connect to the wallet.
     *
     * @param rpc RPC address.
     * @param params The secret seed and an optional password.
     *
     * @returns A promise that resolves to void;
     */
    public import = async (rpc: string, params: SecretKeyParams) => {
        this.signer = await LocalSigner.fromSecretKey(params.secretKey, params.password);

        if (!this.signer) {
            throw new WalletError(SecretKeyErrorCodes.NOT_ABLE_TO_IMPORT_ACCOUNT);
        }

        this.rpc = rpc;
        this.pkh = await this.signer.publicKeyHash();
    };
}

export default SecretKeytWallet;
