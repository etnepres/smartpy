import { InMemorySigner } from '@taquito/signer';
import { Signer } from '../Signer';

class LocalSigner implements Signer {
    private signer: InMemorySigner;

    constructor(signer: InMemorySigner) {
        this.signer = signer;
    }

    public static fromFaucet(email: string, password: string, mnemonic: string) {
        return new LocalSigner(InMemorySigner.fromFundraiser(email, password, mnemonic));
    }

    public static async fromSecretKey(key: string, passphrase?: string): Promise<LocalSigner> {
        return new LocalSigner(await InMemorySigner.fromSecretKey(key, passphrase));
    }

    public sign = async (op: any) => this.signer.sign(op, new Uint8Array([0x3]));

    public publicKey = async () => this.signer.publicKey();

    public publicKeyHash = async () => this.signer.publicKeyHash();

    public secretKey = async () => this.signer.secretKey();

    async isReady() {
        throw new Error('Method not implemented.');
    }
}

export default LocalSigner;
