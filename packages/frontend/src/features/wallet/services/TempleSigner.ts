import { TempleWallet } from '@temple-wallet/dapp';
import bs58check from 'bs58check';

import { Signer } from './Signer';

class TempleSigner implements Signer {
    private client;
    constructor(client: TempleWallet) {
        this.client = client;
    }

    public sign = async (bytes: string) => {
        const signature = await this.client.sign('03' + bytes); // 0x03 generic prefix

        let sbytes = bs58check.decode(signature);
        if (signature.startsWith('edsig') || signature.startsWith('spsig1')) {
            sbytes = sbytes.slice(5).toString('hex');
        } else if (signature.startsWith('p2sig')) {
            sbytes = sbytes.slice(4).toString('hex');
        } else {
            sbytes = sbytes.slice(3).toString('hex');
        }

        return {
            bytes,
            sig: sbytes,
            prefixSig: signature,
            sbytes: `${bytes}${sbytes}`,
        };
    };

    public publicKeyHash = async () => await this.client.getPKH();

    public publicKey = async () => this.client.permission?.publicKey as string;

    public secretKey(): Promise<string | undefined> {
        throw new Error('Method not implemented.');
    }

    public isReady() {
        throw new Error('Method not implemented.');
    }
}

export default TempleSigner;
