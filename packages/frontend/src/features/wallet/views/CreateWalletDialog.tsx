import React from 'react';
import { Alert, Dialog, DialogActions, DialogContent, DialogProps, DialogTitle, Divider, Slide } from '@mui/material';

import useWalletContext from '../hooks/useWalletContext';
import TextField from 'src/features/common/elements/TextField';
import Logger from 'src/services/logger';
import useTranslation from 'src/features/i18n/hooks/useTranslation';
import Button from 'src/features/common/elements/Button';

const Transition = React.forwardRef(function Transition(
    props: { children: React.ReactElement<any, any> },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="up" ref={ref} {...props} />;
});

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface OwnProps extends DialogProps {}

const CreateWalletDialog: React.FC<OwnProps> = (props) => {
    const [password, setPassword] = React.useState('');
    const [confirmPassword, setConfirmPassword] = React.useState('');
    const [error, setError] = React.useState('');
    const { initWallet } = useWalletContext();
    const t = useTranslation();

    const createWallet = () => {
        try {
            if (password !== confirmPassword) {
                return setError(t('wallet.labels.passwordDontMatch'));
            }
            if (password.length < 6) {
                return setError(t('wallet.labels.passwordTooShort'));
            }
            initWallet(password);
        } catch (e) {
            setError(e?.message);
            Logger.debug(e);
        }
    };

    const handleTextChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        switch (e.target.name) {
            case 'pw':
                return setPassword(e.target.value);
            case 'confirmPw':
                return setConfirmPassword(e.target.value);
        }
    };

    return (
        <Dialog fullWidth {...props} TransitionComponent={Transition}>
            <DialogTitle>{t('wallet.labels.createWallet')}</DialogTitle>
            <DialogContent dividers>
                {error ? (
                    <Alert severity="error" sx={{ marginBottom: 3 }}>
                        {error}
                    </Alert>
                ) : null}
                <TextField
                    fullWidth
                    required
                    name="pw"
                    label={t('common.password')}
                    type="password"
                    onChange={handleTextChange}
                />
                <Divider sx={{ marginTop: 1, marginBottom: 1 }} />
                <TextField
                    fullWidth
                    required
                    name="confirmPw"
                    label={t('common.confirmPassword')}
                    type="password"
                    onChange={handleTextChange}
                />
            </DialogContent>
            <DialogActions>
                <Button autoFocus color="primary" onClick={createWallet}>
                    {t('wallet.labels.createWallet')}
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default CreateWalletDialog;
