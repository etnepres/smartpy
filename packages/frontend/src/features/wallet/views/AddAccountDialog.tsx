import React from 'react';
import WalletStorage from 'smartpy-wallet-storage';

import { Alert, Box, Dialog, DialogActions, DialogContent, DialogProps } from '@mui/material';
import AppBar from '@mui/material/AppBar';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Divider from '@mui/material/Divider';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';

import useTranslation from '../../i18n/hooks/useTranslation';
import TabPanel from '../../common/elements/TabPanel';

import WalletServices from '../services';
import { useNetworkInfo } from '../selectors/network';
import { AccountSource } from '../constants/sources';
import useWalletContext from '../hooks/useWalletContext';
import Logger from 'src/services/logger';

interface AddAccountDialogProps extends DialogProps {
    onClose: () => void;
}

const AddAccountDialog: React.FC<AddAccountDialogProps> = ({ ...props }) => {
    const t = useTranslation();
    const { addAccount } = useWalletContext();
    const [name, setName] = React.useState('');
    const [secretKey, setSecretKey] = React.useState('');
    const [error, setError] = React.useState('');
    const { rpc } = useNetworkInfo();
    const [tab, setTab] = React.useState(0);

    const createAccount = async () => {
        try {
            if (name && secretKey) {
                await WalletServices[AccountSource.SMARTPY_SECRET_KEY].import(rpc, { secretKey });
                const privateKeyHash = await WalletServices[AccountSource.SMARTPY_SECRET_KEY].publicKeyHash();
                const publicKey = await WalletServices[AccountSource.SMARTPY_SECRET_KEY].publicKey();
                privateKeyHash &&
                    addAccount({
                        address: privateKeyHash,
                        accountType: WalletStorage.constants.accountTypes.NORMAL,
                        name: name,
                        privateKey: secretKey,
                        publicKey,
                    });
                props?.onClose();
            }
        } catch (e) {
            setError(e?.message);
            Logger.debug(e);
        }
    };

    const handleTextChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        switch (e.target.name) {
            case 'name':
                return setName(e.target.value);
            case 'secretKey':
                return setSecretKey(e.target.value);
        }
    };

    const handleTabChange = (event: React.SyntheticEvent<Element, Event>, newValue: number) => {
        setTab(newValue);
    };

    return (
        <Dialog {...props} fullWidth>
            <DialogContent dividers>
                {error ? (
                    <Alert severity="error" sx={{ marginBottom: 3 }}>
                        {error}
                    </Alert>
                ) : null}
                <AppBar position="static" color="default">
                    <Tabs value={tab} onChange={handleTabChange} aria-label="Faucet Menu" variant="fullWidth">
                        <Tab label={t('wallet.labels.fromSecret')} key={0} />
                    </Tabs>
                </AppBar>

                <TabPanel value={tab} index={0}>
                    <Box sx={{ padding: 3 }}>
                        <TextField
                            fullWidth
                            required
                            name="name"
                            label={t('common.name')}
                            onChange={handleTextChange}
                        />
                        <Divider sx={{ marginTop: 2, marginBottom: 2 }} />
                        <TextField
                            fullWidth
                            required
                            name="secretKey"
                            label={`${t('common.secret')} (edsk)`}
                            onChange={handleTextChange}
                        />
                    </Box>
                </TabPanel>
            </DialogContent>

            <DialogActions>
                <Button autoFocus color="primary" onClick={createAccount}>
                    {t('wallet.labels.createAccount')}
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default AddAccountDialog;
