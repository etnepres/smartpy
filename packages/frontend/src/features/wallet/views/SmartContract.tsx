import React from 'react';

import {
    styled,
    Container as MuiContainer,
    TableBody,
    TableCell,
    TableHead,
    Typography,
    Divider,
    TablePagination,
    TableSortLabel,
    TableCellProps,
    Chip,
    Tooltip,
} from '@mui/material';
import ArrowBackOutlinedIcon from '@mui/icons-material/ArrowBackOutlined';
import EditOutlinedIcon from '@mui/icons-material/EditOutlined';

import useTranslation from 'src/features/i18n/hooks/useTranslation';
import Table from 'src/features/common/components/Table';
import TableRow from 'src/features/common/components/TableRow';
import { TimestampMatchers, StringMatchers } from 'src/utils/matchers';
import { useParams } from 'react-router-dom';
import { getBase } from 'src/utils/url';
import Fab from 'src/features/common/elements/Fab';
import { fetchContractOperations, Operation } from 'src/utils/tzkt';
import RouterFab from 'src/features/navigation/elements/RouterFab';
import useWalletContext from '../hooks/useWalletContext';
import TextField from 'src/features/common/elements/TextField';

enum Order {
    Asc = 'asc',
    Desc = 'desc',
}
enum Field {
    Type = 'type',
    Status = 'status',
    Timestamp = 'timestamp',
}

enum StatusColor {
    failed = 'error',
    applied = 'success',
}

const sortByField = (operattions: Operation[], field: Field, order: Order) => {
    const _operattions = [...operattions];

    return _operattions.sort((v1, v2) => {
        if (field === Field.Timestamp) {
            return (order === Order.Asc ? 1 : -1) * TimestampMatchers.compare(v1[field], v2[field]);
        }
        return (order === Order.Asc ? 1 : -1) * StringMatchers.compare(v1[field], v2[field]);
    });
};

const Container = styled(MuiContainer)(({ theme }) => ({
    display: 'flex',
    flexDirection: 'column',
    alignContent: 'stretch',
    justifyContent: 'center',
    height: 780,
    padding: 20,
}));

const NameDiv = styled('div')(({ theme }) => ({
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    width: 300,
}));

const TopButtons = styled('div')(({ theme }) => ({
    display: 'flex',
    justifyContent: 'center',
    margin: 20,
    [theme.breakpoints.down('sm')]: {
        display: 'flex',
        flexWrap: 'wrap',
    },
}));

const FabButton = styled(Fab)<{ target?: string }>(({ theme }) => ({
    margin: 5,
    borderWidth: 2,
    borderStyle: 'solid',
    borderColor: theme.palette.primary.main,
    [theme.breakpoints.down('sm')]: {
        flexGrow: 1,
    },
}));

const SortedTableCell = ({
    order,
    isSorted,
    onSort,
    fieldName,
    ...props
}: {
    order: Order;
    isSorted: boolean;
    onSort: (field: Field) => void;
    fieldName: Field;
} & TableCellProps) => (
    <TableCell {...props}>
        <TableSortLabel active={isSorted} direction={isSorted ? order : Order.Asc} onClick={() => onSort(fieldName)}>
            <Typography variant="overline">{fieldName}</Typography>
        </TableSortLabel>
    </TableCell>
);

const SmartContractView = () => {
    const isMounted = React.useRef(false);
    const { address } = useParams<{ address: string }>();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);
    const [sortField, setSortField] = React.useState<Field>(Field.Timestamp);
    const [order, setOrder] = React.useState<Order>(Order.Desc);
    const [operations, setOperations] = React.useState<Operation[]>([]);
    const t = useTranslation();
    const { updateContract, contracts } = useWalletContext();
    const [editName, setEditName] = React.useState(false);

    const contract = React.useMemo(() => contracts.find((c) => c.address === address), [address, contracts]);
    React.useEffect(() => {
        isMounted.current = true;
        fetchContractOperations(address).then((ops) => {
            if (isMounted.current) {
                setOperations(ops);
            }
        });
        return () => {
            isMounted.current = false;
        };
    }, [address]);

    const onSort = React.useCallback(
        (field: Field) => {
            if (sortField === field) {
                setOrder(order === Order.Asc ? Order.Desc : Order.Asc);
            } else {
                setSortField(field);
                setOrder(Order.Desc);
            }
        },
        [order, sortField],
    );

    const filteredOperations = React.useMemo(() => {
        let _operattions = operations.slice(page * rowsPerPage, (page + 1) * rowsPerPage);
        _operattions = sortByField(_operattions, sortField, order);

        return _operattions;
    }, [operations, page, rowsPerPage, sortField, order]);

    const handleNameUpdate = (e: React.FocusEvent<HTMLInputElement>) => {
        if (contract) {
            updateContract({
                ...contract,
                name: e.target.value,
            });
            setEditName(false);
        }
    };

    const handleKeyDownCapture = (e: React.KeyboardEvent<HTMLInputElement>) => {
        if (e.key === 'Enter' && contract) {
            updateContract({
                ...contract,
                name: (e.target as any).value,
            });
            setEditName(false);
        }
    };

    return (
        <Container>
            <div>
                <RouterFab
                    color="primary"
                    aria-label="Smart Contracts"
                    to="/wallet/smart-contracts"
                    sx={{ marginBottom: 3 }}
                >
                    <ArrowBackOutlinedIcon />
                    {t('wallet.contractView.goBack')}
                </RouterFab>
            </div>
            <Divider sx={{ marginTop: 1, marginBottom: 1 }} />
            <NameDiv>
                {editName ? (
                    <TextField
                        fullWidth
                        autoFocus={editName}
                        defaultValue={contract?.name || ''}
                        onBlur={handleNameUpdate}
                        onKeyDownCapture={handleKeyDownCapture}
                        inputProps={{
                            autoComplete: 'off',
                        }}
                        size="small"
                    />
                ) : (
                    <Tooltip title={t('common.edit') as string} placement="top">
                        <>
                            <Typography
                                variant="caption"
                                textAlign="center"
                                sx={{
                                    fontSize: '1em',
                                    padding: 1,
                                    borderRadius: 3,
                                    border: '2px solid',
                                    borderColor: 'transparent',
                                    ':hover': { borderColor: 'primary.dark' },
                                }}
                                onClick={() => setEditName(true)}
                            >
                                {contract?.name || ''}
                            </Typography>
                            <EditOutlinedIcon fontSize="small" />
                        </>
                    </Tooltip>
                )}
            </NameDiv>
            <Divider sx={{ marginTop: 1, marginBottom: 1 }} />
            <TopButtons>
                <FabButton
                    variant="extended"
                    color="secondary"
                    aria-label={t('wallet.exploreWith.smartpy')}
                    href={`${getBase()}/explorer.html?address=${address}`}
                    target="_blank"
                >
                    {t('wallet.exploreWith.smartpy')}
                </FabButton>
                <FabButton
                    variant="extended"
                    color="secondary"
                    sx={{ borderColor: '#abda82' }}
                    aria-label={t('wallet.exploreWith.betterCallDev')}
                    href={`https://better-call.dev/search?text=${address}`}
                    target="_blank"
                >
                    {t('wallet.exploreWith.betterCallDev')}
                </FabButton>
            </TopButtons>
            <Typography variant="overline">{t('wallet.labels.operations')}</Typography>
            <Divider sx={{ marginTop: 1, marginBottom: 1 }} />
            <Table
                sx={{ flexGrow: 1 }}
                header={
                    <TableHead>
                        <TableRow>
                            <SortedTableCell
                                order={order}
                                fieldName={Field.Type}
                                onSort={onSort}
                                isSorted={sortField === Field.Type}
                            />
                            <SortedTableCell
                                order={order}
                                fieldName={Field.Status}
                                onSort={onSort}
                                isSorted={sortField === Field.Status}
                            />
                            <TableCell align="right">
                                <Typography variant="overline">Entrypoint</Typography>
                            </TableCell>
                            <TableCell align="right">
                                <Typography variant="overline">Destination</Typography>
                            </TableCell>
                            <SortedTableCell
                                align="right"
                                order={order}
                                fieldName={Field.Timestamp}
                                onSort={onSort}
                                isSorted={sortField === Field.Timestamp}
                            />
                        </TableRow>
                    </TableHead>
                }
                body={
                    <TableBody>
                        {filteredOperations.map((op) => (
                            <TableRow key={op.id}>
                                <TableCell>
                                    <Typography sx={{ fontWeight: 'bold' }}>{op.type}</Typography>
                                </TableCell>
                                <TableCell>
                                    <Chip label={op.status} color={StatusColor[op.status]} sx={{ opacity: 0.8 }} />
                                </TableCell>
                                <TableCell align="right">
                                    {op.parameter?.entrypoint ? <Chip label={op.parameter?.entrypoint} /> : null}
                                </TableCell>
                                <TableCell align="right">
                                    {op.target?.address ? <Chip label={op.target?.address} /> : null}
                                </TableCell>
                                <TableCell align="right"> {new Date(op.timestamp).toLocaleString()}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                }
                pagination={
                    <TablePagination
                        rowsPerPageOptions={[5, 10]}
                        component="div"
                        count={operations.length}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        onPageChange={(_, page) => setPage(page)}
                        onRowsPerPageChange={(e) => setRowsPerPage(Number(e.target.value))}
                    />
                }
            />
        </Container>
    );
};

export default SmartContractView;
