import React from 'react';

import {
    styled,
    TableBody,
    TableCell,
    TableHead,
    Typography,
    Divider,
    TablePagination,
    TableSortLabel,
    TableCellProps,
    Chip,
    Paper,
    AppBar,
    Tabs,
    Tab,
    TextField,
    Tooltip,
} from '@mui/material';
import EditOutlinedIcon from '@mui/icons-material/EditOutlined';
import PlayArrowOutlinedIcon from '@mui/icons-material/PlayArrowOutlined';
import ListAltOutlinedIcon from '@mui/icons-material/ListAltOutlined';
import InfoIcon from '@mui/icons-material/Info';

import Table from 'src/features/common/components/Table';
import TableRow from 'src/features/common/components/TableRow';
import { TimestampMatchers, StringMatchers } from 'src/utils/matchers';
import Fab from 'src/features/common/elements/Fab';
import { Operation } from 'src/utils/tzkt';
import { Network } from 'src/constants/networks';
import * as Explorer from 'src/constants/explorer';
import TabPanel from 'src/features/common/elements/TabPanel';
import useTranslation from 'src/features/i18n/hooks/useTranslation';
import AccountInfo from '../components/AccountInfo';
import TransactionView from './Transaction';

enum Order {
    Asc = 'asc',
    Desc = 'desc',
}
enum Field {
    Type = 'type',
    Status = 'status',
    Timestamp = 'timestamp',
}

enum StatusColor {
    failed = 'error',
    applied = 'success',
}

const sortByField = (operattions: Operation[], field: Field, order: Order) => {
    const _operattions = [...operattions];

    return _operattions.sort((v1, v2) => {
        if (field === Field.Timestamp) {
            return (order === Order.Asc ? 1 : -1) * TimestampMatchers.compare(v1[field], v2[field]);
        }
        return (order === Order.Asc ? 1 : -1) * StringMatchers.compare(v1[field], v2[field]);
    });
};

const NameDiv = styled('div')(({ theme }) => ({
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    width: 300,
}));

const TopButtons = styled('div')(({ theme }) => ({
    display: 'flex',
    justifyContent: 'center',
    margin: 20,
    [theme.breakpoints.down('sm')]: {
        display: 'flex',
        flexWrap: 'wrap',
    },
}));

const FabButton = styled(Fab)<{ target?: string }>(({ theme }) => ({
    margin: 5,
    borderWidth: 2,
    borderStyle: 'solid',
    borderColor: theme.palette.primary.main,
    [theme.breakpoints.down('sm')]: {
        flexGrow: 1,
    },
}));

const SortedTableCell = ({
    order,
    isSorted,
    onSort,
    fieldName,
    ...props
}: {
    order: Order;
    isSorted: boolean;
    onSort: (field: Field) => void;
    fieldName: Field;
} & TableCellProps) => (
    <TableCell {...props}>
        <TableSortLabel active={isSorted} direction={isSorted ? order : Order.Asc} onClick={() => onSort(fieldName)}>
            <Typography variant="overline">{fieldName}</Typography>
        </TableSortLabel>
    </TableCell>
);

interface AccountViewProps {
    network: Network;
    operations: Operation[];
    updateAccount: (account: { name: string }) => void;
    account: {
        address: string;
        name: string;
        publicKey?: string;
        privateKey?: string;
    };
}

const AccountView: React.FC<AccountViewProps> = ({ network, account, operations, updateAccount }) => {
    const [tab, setTab] = React.useState(0);
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);
    const [sortField, setSortField] = React.useState<Field>(Field.Timestamp);
    const [order, setOrder] = React.useState<Order>(Order.Desc);
    const [editName, setEditName] = React.useState(false);
    const t = useTranslation();

    const onSort = React.useCallback(
        (field: Field) => {
            if (sortField === field) {
                setOrder(order === Order.Asc ? Order.Desc : Order.Asc);
            } else {
                setSortField(field);
                setOrder(Order.Desc);
            }
        },
        [order, sortField],
    );

    const filteredOperations = React.useMemo(() => {
        let _operattions = operations.slice(page * rowsPerPage, (page + 1) * rowsPerPage);
        _operattions = sortByField(_operattions, sortField, order);

        return _operattions;
    }, [operations, page, rowsPerPage, sortField, order]);

    const handleTabChange = (_: React.SyntheticEvent<Element, Event>, newValue: number) => {
        setTab(newValue);
    };

    const handleNameUpdate = (e: React.FocusEvent<HTMLInputElement>) => {
        updateAccount({ name: e.target.value });
        setEditName(false);
    };

    const handleKeyDownCapture = (e: React.KeyboardEvent<HTMLInputElement>) => {
        if (e.key === 'Enter') {
            updateAccount({ name: (e.target as any).value });
            setEditName(false);
        }
    };

    return (
        <>
            <NameDiv>
                {editName ? (
                    <TextField
                        fullWidth
                        autoFocus={editName}
                        defaultValue={account.name}
                        onBlur={handleNameUpdate}
                        onKeyDownCapture={handleKeyDownCapture}
                        inputProps={{
                            autoComplete: 'off',
                        }}
                        size="small"
                    />
                ) : (
                    <Tooltip title={t('common.edit') as string} placement="top">
                        <>
                            <Typography
                                variant="caption"
                                textAlign="center"
                                sx={{
                                    fontSize: '1em',
                                    padding: 1,
                                    borderRadius: 3,
                                    border: '2px solid',
                                    borderColor: 'transparent',
                                    ':hover': { borderColor: 'primary.dark' },
                                }}
                                onClick={() => setEditName(true)}
                            >
                                {account.name}
                            </Typography>
                            <EditOutlinedIcon fontSize="small" />
                        </>
                    </Tooltip>
                )}
            </NameDiv>
            <Divider sx={{ marginTop: 1, marginBottom: 1 }} />
            <AccountInfo disableNetworkSelection skipTitles />
            <Divider sx={{ marginTop: 1, marginBottom: 1 }} />
            <TopButtons>
                <FabButton
                    variant="extended"
                    sx={{ borderColor: 'warning.light' }}
                    color="secondary"
                    aria-label={t('wallet.exploreWith.tzkt')}
                    href={`${Explorer.tzkt[network]}/${account.address}`}
                    target="_blank"
                >
                    {t('wallet.exploreWith.tzkt')}
                </FabButton>
                <FabButton
                    variant="extended"
                    color="secondary"
                    aria-label={t('wallet.exploreWith.tzstats')}
                    href={`${Explorer.tzstats[network]}/${account.address}`}
                    target="_blank"
                >
                    {t('wallet.exploreWith.tzstats')}
                </FabButton>
            </TopButtons>
            <Divider sx={{ marginTop: 1, marginBottom: 1 }} />
            <Paper sx={{ flexGrow: 1 }}>
                <AppBar position="static" color="default">
                    <Tabs value={tab} onChange={handleTabChange} aria-label="Faucet Menu" variant="fullWidth">
                        <Tab label={t('wallet.labels.operations')} icon={<ListAltOutlinedIcon />} />
                        <Tab label={t('wallet.labels.interact')} icon={<PlayArrowOutlinedIcon />} />
                        <Tab label={t('wallet.labels.accountInfo')} icon={<InfoIcon />} />
                    </Tabs>
                </AppBar>

                <TabPanel value={tab} index={0}>
                    <Table
                        header={
                            <TableHead>
                                <TableRow>
                                    <SortedTableCell
                                        order={order}
                                        fieldName={Field.Type}
                                        onSort={onSort}
                                        isSorted={sortField === Field.Type}
                                    />
                                    <SortedTableCell
                                        order={order}
                                        fieldName={Field.Status}
                                        onSort={onSort}
                                        isSorted={sortField === Field.Status}
                                    />
                                    <TableCell align="right">
                                        <Typography variant="overline">Entrypoint</Typography>
                                    </TableCell>
                                    <TableCell align="right">
                                        <Typography variant="overline">Destination</Typography>
                                    </TableCell>
                                    <SortedTableCell
                                        align="right"
                                        order={order}
                                        fieldName={Field.Timestamp}
                                        onSort={onSort}
                                        isSorted={sortField === Field.Timestamp}
                                    />
                                </TableRow>
                            </TableHead>
                        }
                        body={
                            <TableBody>
                                {filteredOperations.map((op) => (
                                    <TableRow key={op.id}>
                                        <TableCell>
                                            <Typography sx={{ fontWeight: 'bold' }}>{op.type}</Typography>
                                        </TableCell>
                                        <TableCell>
                                            {op.status ? (
                                                <Chip
                                                    label={op.status}
                                                    color={StatusColor[op.status]}
                                                    sx={{ opacity: 0.8 }}
                                                />
                                            ) : null}
                                        </TableCell>
                                        <TableCell align="right">
                                            {op.parameter?.entrypoint ? (
                                                <Chip label={op.parameter?.entrypoint} />
                                            ) : null}
                                        </TableCell>
                                        <TableCell align="right">
                                            {op.target?.address ? <Chip label={op.target?.address} /> : null}
                                        </TableCell>
                                        <TableCell align="right"> {new Date(op.timestamp).toLocaleString()}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        }
                        pagination={
                            <TablePagination
                                rowsPerPageOptions={[5, 10]}
                                component="div"
                                count={operations.length}
                                rowsPerPage={rowsPerPage}
                                page={page}
                                onPageChange={(_, page) => setPage(page)}
                                onRowsPerPageChange={(e) => setRowsPerPage(Number(e.target.value))}
                            />
                        }
                    />
                </TabPanel>
                <TabPanel value={tab} index={1}>
                    <TransactionView />
                </TabPanel>
                <TabPanel value={tab} index={2}>
                    <Paper sx={{ padding: 4 }}>
                        <TextField
                            inputProps={{
                                autoComplete: 'off',
                            }}
                            disabled
                            fullWidth
                            label={t('wallet.labels.publicKey')}
                            margin="normal"
                            variant="outlined"
                            value={account.publicKey}
                        />
                        <TextField
                            inputProps={{
                                autoComplete: 'off',
                            }}
                            disabled
                            fullWidth
                            label={t('wallet.labels.privateKey')}
                            margin="normal"
                            variant="outlined"
                            value={account.privateKey}
                        />
                    </Paper>
                </TabPanel>
            </Paper>
        </>
    );
};

export default AccountView;
