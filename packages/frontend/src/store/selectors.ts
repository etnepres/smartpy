import * as editorSelectors from '../features/py-ide/selectors';
import * as tsEditorSelectors from '../features/ts-ide/selectors';
import * as mlEditorSelectors from '../features/ml-ide/selectors';
import * as michelsonEditorSelectors from '../features/mich-ide/selectors';
import * as themeSelectors from '../features/theme/selectors';

const selectors = {
    editor: editorSelectors,
    tsIDE: tsEditorSelectors,
    mlIDE: mlEditorSelectors,
    michelsonEditor: michelsonEditorSelectors,
    theme: themeSelectors,
};

export default selectors;
