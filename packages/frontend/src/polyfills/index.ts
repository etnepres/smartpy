import { keccak_256 } from 'js-sha3';
import Timelock from '@smartpy/timelock';
import './eztz';
import './sodium';

window.smartpyContext = window.smartpyContext || {};

window.smartpyContext.Keccak256 = keccak_256;
window.smartpyContext.Timelock = Timelock;

// @TODO[low]: Rodrigo Quelhas - Find a way to disable babel transformation of big int exponentiation (**) to Math.pow
window.smartpyContext.Bls12 = (window as any).Bls12;
