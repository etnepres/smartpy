/* eslint-disable @typescript-eslint/no-var-requires */
const WorkerPlugin = require('worker-plugin');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const ForkTSCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const CracoEsbuildPlugin = require('craco-esbuild');

const routes = ['ide$', 'michelson$', 'nodes$', 'wallet$'];

module.exports = {
    plugins: [
        {
            plugin: CracoEsbuildPlugin,
            options: {
                esbuildLoaderOptions: {
                    // Optional. Defaults to auto-detect loader.
                    loader: 'tsx',
                    target: 'es2019',
                },
                esbuildMinimizerOptions: {
                    // Optional. Defaults to:
                    target: 'es2019',
                    css: true, // if true, OptimizeCssAssetsWebpackPlugin will also be replaced by esbuild.
                },
                skipEsbuildJest: true, // Optional. Set to true if you want to use babel for jest tests,
            },
        },
    ],
    eslint: { enable: false },
    webpack: {
        configure: (config) => {
            // Remove ModuleScopePlugin which throws when we try to import something
            // outside of src/.
            config.resolve.plugins.pop();

            // Resolve the path aliases.
            config.resolve.plugins.push(new TsconfigPathsPlugin());

            // Let Babel compile outside of src/.
            const oneOfRule = config.module.rules.find((rule) => rule.oneOf);
            const tsRule = oneOfRule.oneOf.find((rule) => rule.test.toString().includes('ts|tsx'));

            tsRule.include = undefined;
            tsRule.exclude = /node_modules/;

            config.module.rules.push({
                test: /\.wasm$/,
                type: 'javascript/auto',
            });

            return config;
        },
        plugins: {
            remove: [
                // This plugin is too old and causes problems in monorepos.
                'ForkTsCheckerWebpackPlugin',
            ],
            add: [
                new WorkerPlugin(),
                // Use newer version of ForkTSCheckerWebpackPlugin to type check files across the monorepo.
                new ForkTSCheckerWebpackPlugin({
                    issue: {
                        // The exclude rules are copied from CRA.
                        exclude: [
                            {
                                file: '**/src/**/__tests__/**',
                            },
                            {
                                file: '**/src/**/?(*.)(spec|test).*',
                            },
                            {
                                file: '**/src/setupTests.*',
                            },
                        ],
                    },
                }),
            ],
        },
    },
    devServer: (devServerConfig) => ({
        ...devServerConfig,
        historyApiFallback: true,
        proxy: routes.reduce(
            (state, route) => ({
                ...state,
                [`/${route}`]: {
                    target: `${process.env.HTTPS === 'true' ? 'https' : 'http'}://[::1]:${process.env.PORT || 3000}`,
                    pathRewrite: { [`^/${route}`]: '' },
                    secure: false,
                    changeOrigin: true,
                },
            }),
            {},
        ),
    }),
};
