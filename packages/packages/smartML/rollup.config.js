import path from 'path';
import typescript from '@rollup/plugin-typescript';
import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import { terser } from 'rollup-plugin-terser';
import pkg from './package.json';
import nodePolyfills from 'rollup-plugin-polyfill-node';
import json from '@rollup/plugin-json';

const config = {
    input: 'src/index.ts',
    output: [
        {
            file: pkg.main,
            strict: false,
            format: 'cjs',
            exports: 'default',
            banner: `/* === Version: ${pkg.version} === */`,
        },
        {
            file: pkg.module,
            strict: false,
            format: 'es',
            banner: `/* === Version: ${pkg.version} === */`,
        },
        {
            name: 'SmartTS',
            file: pkg.minified,
            strict: false,
            format: 'iife',
            exports: 'default',
            compact: true,
            banner: `/* === Version: ${pkg.version} === */`,
        },
    ],
    plugins: [
        commonjs(),
        nodePolyfills(),
        typescript({
            tsconfig: path.resolve(__dirname, './tsconfig.json'),
        }),
        resolve({ browser: true }),
        json(),
        terser(),
    ],
};

export default config;
