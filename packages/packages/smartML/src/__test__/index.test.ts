/**
 * @jest-environment jsdom
 */

import smartML from '..';

describe("Namespace", () => {
    it("List all available methods", () => {
        expect(Object.keys(smartML)).toEqual([
            'importType',
            'importContract',
            'compileContractStorage',
            'compileContract',
            'update_michelson_view',
            'buildTransfer',
            'stringOfException',
            'js_string',
            'callGui',
            'explore',
            'runSmartMLScript',
            'runSmartMLScriptScenarioName',
            'runScenarioInBrowser',
            'lazy_tab',
            'default'
        ])
    })
})
