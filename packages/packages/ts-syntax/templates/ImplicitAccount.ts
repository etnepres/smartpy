@Contract
export class ImplicitAccount {
    @EntryPoint
    ep(keyHash: TKey_hash): void {
        Sp.verify(('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w' as TAddress) === Sp.toAddress(Sp.implicitAccount(keyHash)));
    }
}

Dev.test({ name: 'ImplicitAccount' }, () => {
    const c1 = Scenario.originate(new ImplicitAccount());
    Scenario.transfer(c1.ep('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'));
});

Dev.compileContract('ImplicitAccount', new ImplicitAccount());
