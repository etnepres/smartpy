interface TStorage {
    value: TOption<TNat>;
}

@Contract
export class Option {
    storage: TStorage = {
        value: Sp.none,
    };

    @EntryPoint
    set(value: TNat): void {
        if (!this.storage.value.isSome()) {
            this.storage.value = Sp.some(value);
        }
    }

    @EntryPoint
    openAndAdd(v: TNat): void {
        const value = this.storage.value.openSome('Error: is None');
        if (value === 10) {
            this.storage.value = Sp.some(value + v);
        }
    }
}

Dev.test({ name: 'Option' }, () => {
    const c1 = Scenario.originate(new Option());
    Scenario.transfer(c1.set(10));
    Scenario.transfer(c1.openAndAdd(10));
});

Dev.compileContract('Option_compiled', new Option());
