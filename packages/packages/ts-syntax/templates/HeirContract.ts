/* This contract is a demo example
 *
 * Inheritance contract
 *
 * The owner of the contract can regularly call the `alive` entrypoint
 * to prove that he is still alive.
 *
 * If the contract is not called since `delay` seconds (2 years in this example)
 * The heir can withdraw tezzies by calling `heirWithdraw`
 *
 * At anytime the owner can push some tezzies by using default entrypoint
 * or withdraw some tezzies by calling `withdraw`
 *
 */

const alice: TAddress = 'tz1M43YV5vVt7HALGJeWgQ1GRQfiGExA1Jbf';
const heir: TAddress = 'tz1L3Pgin3YSeqZJb6UF9Fis9LBjp3L1YusM';
const delay: TNat = 3600 * 24 * 365 * 2;

interface TStorage {
    owner: TAddress;
    heir: TAddress;
    timeout: TOption<TTimestamp>;
}

@Contract
export class Inheritance {
    storage: TStorage = {
        owner: alice,
        heir: heir,
        timeout: Sp.some('2023-01-01T00:00:00.000Z'),
    };

    @EntryPoint
    alive(): void {
        Sp.verify(Sp.sender == this.storage.owner);
        this.storage.timeout = Sp.some(Sp.now.addSeconds(delay));
    }

    @EntryPoint
    withdraw(to_: TAddress, amount: TMutez): void {
        Sp.verify(Sp.sender == this.storage.owner);
        Sp.verify(this.storage.timeout.isSome());
        const contract: TContract<TUnit> = Sp.contract<TUnit>(to_, 'default').openSome('Invalid Interface');
        Sp.transfer(Sp.unit, amount, contract);
        this.storage.timeout = Sp.some(Sp.now.addSeconds(delay));
    }

    @EntryPoint
    heirWithdraw(to_: TAddress, amount: TMutez): void {
        Sp.verify(Sp.sender == this.storage.heir);
        Sp.verify(!this.storage.timeout.isSome() || Sp.now > this.storage.timeout.openSome());
        const contract: TContract<TUnit> = Sp.contract<TUnit>(to_, 'default').openSome('Invalid Interface');
        Sp.transfer(Sp.unit, amount, contract);
        this.storage.timeout = Sp.none;
    }
}

Dev.test({ name: 'Inheritance contract' }, () => {
    const c1 = Scenario.originate(new Inheritance());
    Scenario.transfer(c1.alive(), {
        sender: alice,
        now: '2021-08-23T20:32:14.124Z',
        amount: 5000000 as TMutez,
    });
    Scenario.transfer(c1.withdraw(alice, 1000000), {
        sender: alice,
        now: '2021-08-23T20:32:17.725Z',
        amount: 0 as TMutez,
    });
    Scenario.transfer(c1.withdraw(heir, 4000000), {
        sender: heir,
        now: '2021-08-28T14:00:30.418Z',
        amount: 0 as TMutez,
        valid: false,
    });
    Scenario.transfer(c1.heirWithdraw(heir, 4000000), {
        sender: heir,
        now: '2021-08-28T14:10:05.118Z',
        amount: 0 as TMutez,
        valid: false,
    });
    Scenario.transfer(c1.heirWithdraw(heir, 4000000), {
        sender: heir,
        now: '2024-01-01T23:30:42.218Z',
        amount: 0 as TMutez,
    });
});

Dev.compileContract('compile_contract', new Inheritance());
