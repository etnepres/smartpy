export interface StorageSpec {
    value: TNat;
}

export const InitialStorage = {
    value: 1,
};

export class Minimal {
    storage: StorageSpec = InitialStorage;

    @EntryPoint
    ep(value: TNat): void {
        this.storage.value = value;
    }
}

Dev.test({ name: 'Minimal' }, () => {
    // Originate `Minimal` contract with an initial balance of 1 xtz
    const c1 = Scenario.originate(new Minimal(), { initialBalance: 1000000 });
    Scenario.transfer(c1.ep(1));
});

Dev.compileContract('minimal', new Minimal());
