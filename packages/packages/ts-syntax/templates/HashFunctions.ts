type StorageSpec = TOption<{
    blake2b: TBytes;
    sha256: TBytes;
    sha512: TBytes;
    sha3: TBytes;
    keccak: TBytes;
}>;

@Contract
export class HashFunctions {
    storage: StorageSpec = Sp.none;

    @EntryPoint
    ep(value: TString): void {
        const bytes = Sp.pack(value);
        this.storage = Sp.some({
            blake2b: Sp.blake2b(bytes),
            sha256: Sp.sha256(bytes),
            sha512: Sp.sha512(bytes),
            sha3: Sp.sha3(bytes),
            keccak: Sp.keccak(bytes),
        });
    }
}

Dev.test({ name: 'HashFunctions' }, () => {
    const c1 = Scenario.originate(new HashFunctions());
    Scenario.transfer(c1.ep('A String'));
});

Dev.compileContract('Hash Functions', new HashFunctions());
