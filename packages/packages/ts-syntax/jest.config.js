module.exports = {
    transform: {
        '^.+\\.ts$': 'ts-jest',
    },
    testEnvironment: 'node',
    verbose: true,
    testRegex: '\\.test\\.ts$',
    modulePathIgnorePatterns: ['__BASELINES__'],
    moduleFileExtensions: ['ts', 'js', 'json'],
    coveragePathIgnorePatterns: ['/node_modules/', '/test/'],
    collectCoverageFrom: ['src/**/*.{js,ts}'],
};
