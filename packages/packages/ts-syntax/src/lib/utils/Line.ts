import type { SourceFile, Node } from 'typescript';
import type { FileLineInfo } from '../../@types/common';

const getLineInfo = (sourceFile: SourceFile, node: Node): FileLineInfo => {
    const { line, character } = sourceFile.getLineAndCharacterOfPosition(node.getStart(sourceFile));
    return {
        line: line + 1,
        character: character + 1,
        fileName: sourceFile.fileName,
    };
};

const getFileLine = (line?: FileLineInfo): string =>
    line ? `("${line.fileName ? line.fileName : ''}" ${Number(line.line)})` : 'None';

const getFileLineSneakCased = (line: FileLineInfo): string =>
    `${line.fileName ? line.fileName.split('/').reverse()[0].replace('.ts', '') : ''}_${Number(line.line)}`;

const LineUtils = {
    getLineAndCharacter: getLineInfo,
    getLineNumber: getFileLine,
    getFileLineSneakCased,
};

export default LineUtils;
