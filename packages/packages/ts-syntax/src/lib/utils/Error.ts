import type { FileLineInfo } from '../../@types/common';

interface ST_ErrorParams {
    msg: string;
    line: FileLineInfo;
    text?: string;
    fileName?: string;
}

export const failWith = (msg: string): never => {
    throw new Error(msg);
};

export class ST_Error extends Error {
    public name = 'ST_Error';

    constructor(public message: string, public line: FileLineInfo, public snippet?: string) {
        super(message); // 'Error' breaks prototype chain here

        Object.setPrototypeOf(this, new.target.prototype); // Restore prototype chain
    }
}

export const failWithInfo = (obj: ST_ErrorParams): never => {
    const error = new ST_Error(obj.msg, obj.line, obj.text);
    throw error;
};

const ErrorUtils = {
    failWith,
    failWithInfo,
};

export default ErrorUtils;
