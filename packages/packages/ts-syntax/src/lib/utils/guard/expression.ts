import type { ST_Expression, ST_TypedExpression } from '../../../@types/expression';
import { ST_ExpressionKind } from '../../../enums/expression';

export const hasType = (expr: ST_Expression): expr is ST_TypedExpression => !!(expr as any).type;

export const isAttrAccessExpr = (
    expr: ST_Expression,
): expr is Extract<ST_Expression, { kind: ST_ExpressionKind.AttrAccessExpr }> => !!(expr as any).prev;

export const isExpression = (arg: { kind: string }): arg is ST_Expression => arg.kind in ST_ExpressionKind;

export const isLiteralExpr = (
    expr: ST_Expression,
): expr is Extract<ST_Expression, { kind: ST_ExpressionKind.LiteralExpr }> =>
    expr.kind === ST_ExpressionKind.LiteralExpr;

const expression = {
    isLiteralExpr,
    hasType,
    isAttrAccessExpr,
    isExpression,
};

export default expression;
