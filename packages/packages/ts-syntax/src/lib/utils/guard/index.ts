import type from './type';
import literal from './literal';
import expression from './expression';

const guards = {
    type,
    literal,
    expression,
};

export default guards;
