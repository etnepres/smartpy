import type { Translators } from '../Transpiler';
import type Transpiler from '../Transpiler';

export class TranslatorBase {
    constructor(public transpiler: Transpiler) {}

    public get translators(): Translators {
        return this.transpiler.translators;
    }
}
