import type { ST_TypeDef } from '../@types/type';
import TypeBuilder from '../lib/utils/builders/type';
import { ST_ExpressionKind } from './expression';
import { FrontendType } from './type';

export enum ST_Enum {
    Layout = 'Layout',
}

export enum ST_Namespace {
    Sp = 'Sp',
    Dev = 'Dev',
    Scenario = 'Scenario',
}

export enum ST_NamespaceDevMethod {
    compileContract = 'compileContract',
    test = 'test',
}
export enum ST_NamespaceScenarioMethod {
    h1 = 'h1',
    h2 = 'h2',
    h3 = 'h3',
    h4 = 'h4',
    p = 'p',
    tableOfContents = 'tableOfContents',
    show = 'show',
    originate = 'originate',
    transfer = 'transfer',
    verify = 'verify',
    verifyEqual = 'verifyEqual',
    testAccount = 'testAccount',
    makeSignature = 'makeSignature',
    compute = 'compute',
    isFailing = 'isFailing',
    catchException = 'catchException',
    prepareConstantValue = 'prepareConstantValue',
}
export enum ST_NamespaceSpMethod {
    bigMap = 'bigMap',
    some = 'some',
    ediv = 'ediv',
    never = 'never',
    verify = 'verify',
    verifyEqual = 'verifyEqual',
    failWith = 'failWith',
    transfer = 'transfer',
    unpack = 'unpack',
    pack = 'pack',
    variant = 'variant',
    constant = 'constant',
    openChest = 'openChest',

    // Contract and Addresses
    selfEntryPoint = 'selfEntryPoint',
    contract = 'contract',
    toAddress = 'toAddress',
    createContract = 'createContract',
    createContractOperation = 'createContractOperation',
    view = 'view',

    votingPower = 'votingPower',
    setDelegate = 'setDelegate',
    implicitAccount = 'implicitAccount',
    hashKey = 'hashKey',
    concat = 'concat',

    // Crypto
    pairingCheck = 'pairingCheck',
    checkSignature = 'checkSignature',
    // Hash functions
    blake2b = 'blake2b',
    sha256 = 'sha256',
    sha512 = 'sha512',
    sha3 = 'sha3',
    keccak = 'keccak',
}

export enum ST_SpValue {
    unit = 'unit',
    none = 'none',
    sender = 'sender',
    source = 'source',
    amount = 'amount',
    balance = 'balance',
    chainId = 'chainId',
    level = 'level',
    totalVotingPower = 'totalVotingPower',
    now = 'now',
    self = 'self',
    selfAddress = 'selfAddress',
}

export const ST_KeywordSpValue = {
    [ST_SpValue.unit]: 'unit',
    [ST_SpValue.none]: 'none',
    [ST_SpValue.sender]: 'sender',
    [ST_SpValue.source]: 'source',
    [ST_SpValue.amount]: 'amount',
    [ST_SpValue.balance]: 'balance',
    [ST_SpValue.chainId]: 'chain_id',
    [ST_SpValue.level]: 'level',
    [ST_SpValue.totalVotingPower]: 'total_voting_power',
    [ST_SpValue.now]: 'now',
    [ST_SpValue.self]: 'self',
    [ST_SpValue.selfAddress]: 'self_address',
};

export const ST_NativeMethod = {
    [ST_Namespace.Sp]: ST_NamespaceSpMethod,
    [ST_Namespace.Dev]: ST_NamespaceDevMethod,
    [ST_Namespace.Scenario]: ST_NamespaceScenarioMethod,
};

export const SpValue: Record<ST_Namespace.Sp, Record<ST_SpValue, string>> = {
    [ST_Namespace.Sp]: {
        [ST_SpValue.unit]: ST_KeywordSpValue.unit,
        [ST_SpValue.none]: ST_KeywordSpValue.none,
        [ST_SpValue.sender]: ST_KeywordSpValue.sender,
        [ST_SpValue.source]: ST_KeywordSpValue.source,
        [ST_SpValue.amount]: ST_KeywordSpValue.amount,
        [ST_SpValue.balance]: ST_KeywordSpValue.balance,
        [ST_SpValue.self]: ST_KeywordSpValue.self,
        [ST_SpValue.selfAddress]: ST_KeywordSpValue.selfAddress,
        [ST_SpValue.chainId]: ST_KeywordSpValue.chainId,
        [ST_SpValue.level]: ST_KeywordSpValue.level,
        [ST_SpValue.totalVotingPower]: ST_KeywordSpValue.totalVotingPower,
        [ST_SpValue.now]: ST_KeywordSpValue.now,
    },
};

export const TypeOfNativeValue: Record<ST_Namespace.Sp, Record<ST_SpValue, ST_TypeDef>> = {
    [ST_Namespace.Sp]: {
        [ST_SpValue.self]: {
            type: FrontendType.TContract,
            inputType: TypeBuilder.unknown(),
        },
        [ST_SpValue.selfAddress]: TypeBuilder.address(),
        [ST_SpValue.unit]: TypeBuilder.unit(),
        [ST_SpValue.none]: TypeBuilder.option(TypeBuilder.unknown(), true),
        [ST_SpValue.sender]: TypeBuilder.address(),
        [ST_SpValue.source]: TypeBuilder.address(),
        [ST_SpValue.amount]: TypeBuilder.mutez(),
        [ST_SpValue.balance]: TypeBuilder.mutez(),
        [ST_SpValue.chainId]: TypeBuilder.chainId(),
        [ST_SpValue.level]: TypeBuilder.nat(),
        [ST_SpValue.totalVotingPower]: TypeBuilder.nat(),
        [ST_SpValue.now]: TypeBuilder.timestamp(),
    },
};

export const ArgTypesOfSpMethod: Record<ST_NamespaceSpMethod, (ST_TypeDef & { optional: boolean })[]> = {
    [ST_NamespaceSpMethod.some]: [{ ...TypeBuilder.unknown(), optional: false }],
    [ST_NamespaceSpMethod.ediv]: [
        { ...TypeBuilder.unknown(), optional: false },
        { ...TypeBuilder.unknown(), optional: false },
    ],
    [ST_NamespaceSpMethod.unpack]: [{ ...TypeBuilder.bytes(), optional: false }],
    [ST_NamespaceSpMethod.pack]: [{ ...TypeBuilder.unknown(), optional: false }],
    [ST_NamespaceSpMethod.never]: [],
    [ST_NamespaceSpMethod.bigMap]: [{ ...TypeBuilder.unknown(), optional: false }],
    [ST_NamespaceSpMethod.verify]: [
        { ...TypeBuilder.bool(), optional: false },
        { ...TypeBuilder.string(), optional: true },
    ],
    [ST_NamespaceSpMethod.verifyEqual]: [
        { ...TypeBuilder.unknown(), optional: false },
        { ...TypeBuilder.unknown(), optional: false },
        { ...TypeBuilder.unknown(), optional: true },
    ],
    [ST_NamespaceSpMethod.variant]: [
        { ...TypeBuilder.string(), optional: false },
        { ...TypeBuilder.unknown(), optional: false },
    ],
    [ST_NamespaceSpMethod.failWith]: [{ ...TypeBuilder.unknown(), optional: false }],
    [ST_NamespaceSpMethod.transfer]: [
        { ...TypeBuilder.unknown(), optional: false },
        { ...TypeBuilder.mutez(), optional: false },
        { type: FrontendType.TContract, inputType: TypeBuilder.unknown(), optional: false },
    ],
    [ST_NamespaceSpMethod.constant]: [{ ...TypeBuilder.string(), optional: false }],
    [ST_NamespaceSpMethod.view]: [
        { ...TypeBuilder.string(), optional: false },
        { ...TypeBuilder.address(), optional: false },
        { ...TypeBuilder.unknown(), optional: true },
    ],
    // Contract and Addresses
    [ST_NamespaceSpMethod.selfEntryPoint]: [{ ...TypeBuilder.string(), optional: true }],
    [ST_NamespaceSpMethod.contract]: [
        { ...TypeBuilder.address(), optional: false },
        { ...TypeBuilder.string(), optional: true },
    ],
    [ST_NamespaceSpMethod.createContract]: [
        { ...TypeBuilder.contract(TypeBuilder.unknown()), optional: false },
        { ...TypeBuilder.unknown(), optional: true },
        { ...TypeBuilder.mutez(), optional: true },
        { ...TypeBuilder.keyHash(), optional: true },
    ],
    [ST_NamespaceSpMethod.createContractOperation]: [
        { ...TypeBuilder.contract(TypeBuilder.unknown()), optional: false },
        { ...TypeBuilder.unknown(), optional: true },
        { ...TypeBuilder.mutez(), optional: true },
        { ...TypeBuilder.keyHash(), optional: true },
    ],
    [ST_NamespaceSpMethod.toAddress]: [{ ...TypeBuilder.contract(TypeBuilder.unknown()), optional: false }],
    [ST_NamespaceSpMethod.implicitAccount]: [{ ...TypeBuilder.keyHash(), optional: false }],
    [ST_NamespaceSpMethod.hashKey]: [{ ...TypeBuilder.key(), optional: false }],
    [ST_NamespaceSpMethod.votingPower]: [{ ...TypeBuilder.keyHash(), optional: false }],
    [ST_NamespaceSpMethod.setDelegate]: [{ ...TypeBuilder.option(TypeBuilder.keyHash()), optional: false }],
    [ST_NamespaceSpMethod.concat]: [{ ...TypeBuilder.unknown(), optional: false }],
    [ST_NamespaceSpMethod.openChest]: [
        { ...TypeBuilder.chest_key(), optional: false },
        { ...TypeBuilder.chest(), optional: false },
        { ...TypeBuilder.nat(), optional: false },
    ],
    // Crypto
    [ST_NamespaceSpMethod.checkSignature]: [
        { ...TypeBuilder.key(), optional: false },
        { ...TypeBuilder.signature(), optional: false },
        { ...TypeBuilder.bytes(), optional: false },
    ],
    [ST_NamespaceSpMethod.pairingCheck]: [
        {
            ...TypeBuilder.list([TypeBuilder.tuple([TypeBuilder.bls12_381_g1(), TypeBuilder.bls12_381_g2()])]),
            optional: false,
        },
    ],
    // Hash functions
    [ST_NamespaceSpMethod.blake2b]: [{ ...TypeBuilder.bytes(), optional: false }],
    [ST_NamespaceSpMethod.sha256]: [{ ...TypeBuilder.bytes(), optional: false }],
    [ST_NamespaceSpMethod.sha512]: [{ ...TypeBuilder.bytes(), optional: false }],
    [ST_NamespaceSpMethod.sha3]: [{ ...TypeBuilder.bytes(), optional: false }],
    [ST_NamespaceSpMethod.keccak]: [{ ...TypeBuilder.bytes(), optional: false }],
};

export const ArgTypesOfDevMethod: Record<ST_NamespaceDevMethod, (ST_TypeDef & { optional: boolean })[]> = {
    [ST_NamespaceDevMethod.compileContract]: [
        { ...TypeBuilder.string(), optional: false },
        { ...TypeBuilder.unknown(), optional: false },
    ],
    [ST_NamespaceDevMethod.test]: [
        { ...TypeBuilder.string(), optional: false },
        {
            type: FrontendType.TLambda,
            inputTypes: {},
            outputType: TypeBuilder.unit(),
            optional: false,
        },
    ],
};

export const ArgTypesOfScenarioMethod: Record<ST_NamespaceScenarioMethod, (ST_TypeDef & { optional: boolean })[]> = {
    [ST_NamespaceScenarioMethod.h1]: [{ ...TypeBuilder.string(), optional: false }],
    [ST_NamespaceScenarioMethod.h2]: [{ ...TypeBuilder.string(), optional: false }],
    [ST_NamespaceScenarioMethod.h3]: [{ ...TypeBuilder.string(), optional: false }],
    [ST_NamespaceScenarioMethod.h4]: [{ ...TypeBuilder.string(), optional: false }],
    [ST_NamespaceScenarioMethod.p]: [{ ...TypeBuilder.string(), optional: false }],
    [ST_NamespaceScenarioMethod.tableOfContents]: [],
    [ST_NamespaceScenarioMethod.show]: [{ ...TypeBuilder.unknown(), optional: false }],
    [ST_NamespaceScenarioMethod.originate]: [{ ...TypeBuilder.unknown(), optional: false }],
    [ST_NamespaceScenarioMethod.transfer]: [
        { ...TypeBuilder.unknown(), optional: false },
        {
            ...TypeBuilder.record({
                properties: { amount: TypeBuilder.mutez() },
            }),
            optional: true,
        },
    ],
    [ST_NamespaceScenarioMethod.verify]: [{ ...TypeBuilder.bool(), optional: false }],
    [ST_NamespaceScenarioMethod.verifyEqual]: [
        { ...TypeBuilder.unknown(), optional: false },
        { ...TypeBuilder.unknown(), optional: false },
    ],
    [ST_NamespaceScenarioMethod.testAccount]: [{ ...TypeBuilder.string(), optional: false }],
    [ST_NamespaceScenarioMethod.makeSignature]: [
        { ...TypeBuilder.string(), optional: false },
        { ...TypeBuilder.bytes(), optional: false },
    ],
    [ST_NamespaceScenarioMethod.compute]: [{ ...TypeBuilder.unknown(), optional: false }],
    [ST_NamespaceScenarioMethod.isFailing]: [{ ...TypeBuilder.unknown(), optional: false }],
    [ST_NamespaceScenarioMethod.catchException]: [{ ...TypeBuilder.unknown(), optional: false }],
    [ST_NamespaceScenarioMethod.prepareConstantValue]: [
        { ...TypeBuilder.unknown(), optional: false },
        { ...TypeBuilder.string(), optional: true },
    ],
};

export const ReturnTypeOfScenarioMethod: Record<ST_NamespaceScenarioMethod, ST_TypeDef> = {
    [ST_NamespaceScenarioMethod.h1]: TypeBuilder.unit(),
    [ST_NamespaceScenarioMethod.h2]: TypeBuilder.unit(),
    [ST_NamespaceScenarioMethod.h3]: TypeBuilder.unit(),
    [ST_NamespaceScenarioMethod.h4]: TypeBuilder.unit(),
    [ST_NamespaceScenarioMethod.p]: TypeBuilder.unit(),
    [ST_NamespaceScenarioMethod.tableOfContents]: TypeBuilder.unit(),
    [ST_NamespaceScenarioMethod.show]: TypeBuilder.unit(),
    [ST_NamespaceScenarioMethod.originate]: TypeBuilder.unit(),
    [ST_NamespaceScenarioMethod.transfer]: TypeBuilder.unit(),
    [ST_NamespaceScenarioMethod.verify]: TypeBuilder.unit(),
    [ST_NamespaceScenarioMethod.verifyEqual]: TypeBuilder.unit(),
    [ST_NamespaceScenarioMethod.testAccount]: TypeBuilder.record({
        properties: {
            address: TypeBuilder.address(),
        },
    }),
    [ST_NamespaceScenarioMethod.makeSignature]: TypeBuilder.signature(),
    [ST_NamespaceScenarioMethod.isFailing]: TypeBuilder.bool(),
    [ST_NamespaceScenarioMethod.compute]: TypeBuilder.unit(),
    [ST_NamespaceScenarioMethod.catchException]: TypeBuilder.option(TypeBuilder.unknown()),
    [ST_NamespaceScenarioMethod.prepareConstantValue]: TypeBuilder.unknown(),
};

export const TypeOfScenarioMethod: Record<
    ST_NamespaceScenarioMethod,
    { argTypes: (ST_TypeDef & { optional: boolean })[]; outputType: ST_TypeDef }
> = {
    [ST_NamespaceScenarioMethod.h1]: {
        argTypes: ArgTypesOfScenarioMethod.h1,
        outputType: ReturnTypeOfScenarioMethod.h1,
    },
    [ST_NamespaceScenarioMethod.h2]: {
        argTypes: ArgTypesOfScenarioMethod.h2,
        outputType: ReturnTypeOfScenarioMethod.h2,
    },
    [ST_NamespaceScenarioMethod.h3]: {
        argTypes: ArgTypesOfScenarioMethod.h3,
        outputType: ReturnTypeOfScenarioMethod.h3,
    },
    [ST_NamespaceScenarioMethod.h4]: {
        argTypes: ArgTypesOfScenarioMethod.h4,
        outputType: ReturnTypeOfScenarioMethod.h4,
    },
    [ST_NamespaceScenarioMethod.p]: {
        argTypes: ArgTypesOfScenarioMethod.p,
        outputType: ReturnTypeOfScenarioMethod.p,
    },
    [ST_NamespaceScenarioMethod.tableOfContents]: {
        argTypes: ArgTypesOfScenarioMethod.tableOfContents,
        outputType: ReturnTypeOfScenarioMethod.tableOfContents,
    },
    [ST_NamespaceScenarioMethod.show]: {
        argTypes: ArgTypesOfScenarioMethod.show,
        outputType: ReturnTypeOfScenarioMethod.show,
    },
    [ST_NamespaceScenarioMethod.originate]: {
        argTypes: ArgTypesOfScenarioMethod.originate,
        outputType: ReturnTypeOfScenarioMethod.originate,
    },
    [ST_NamespaceScenarioMethod.transfer]: {
        argTypes: ArgTypesOfScenarioMethod.transfer,
        outputType: ReturnTypeOfScenarioMethod.transfer,
    },
    [ST_NamespaceScenarioMethod.verify]: {
        argTypes: ArgTypesOfScenarioMethod.verify,
        outputType: ReturnTypeOfScenarioMethod.verify,
    },
    [ST_NamespaceScenarioMethod.verifyEqual]: {
        argTypes: ArgTypesOfScenarioMethod.verifyEqual,
        outputType: ReturnTypeOfScenarioMethod.verifyEqual,
    },
    [ST_NamespaceScenarioMethod.testAccount]: {
        argTypes: ArgTypesOfScenarioMethod.testAccount,
        outputType: ReturnTypeOfScenarioMethod.testAccount,
    },
    [ST_NamespaceScenarioMethod.makeSignature]: {
        argTypes: ArgTypesOfScenarioMethod.makeSignature,
        outputType: ReturnTypeOfScenarioMethod.makeSignature,
    },
    [ST_NamespaceScenarioMethod.compute]: {
        argTypes: ArgTypesOfScenarioMethod.compute,
        outputType: ReturnTypeOfScenarioMethod.compute,
    },
    [ST_NamespaceScenarioMethod.isFailing]: {
        argTypes: ArgTypesOfScenarioMethod.isFailing,
        outputType: ReturnTypeOfScenarioMethod.isFailing,
    },
    [ST_NamespaceScenarioMethod.catchException]: {
        argTypes: ArgTypesOfScenarioMethod.catchException,
        outputType: ReturnTypeOfScenarioMethod.catchException,
    },
    [ST_NamespaceScenarioMethod.prepareConstantValue]: {
        argTypes: ArgTypesOfScenarioMethod.prepareConstantValue,
        outputType: ReturnTypeOfScenarioMethod.prepareConstantValue,
    },
};

export const ReturnTypeOfSpMethod: Record<ST_NamespaceSpMethod, ST_TypeDef> = {
    [ST_NamespaceSpMethod.never]: TypeBuilder.never(),
    [ST_NamespaceSpMethod.verify]: TypeBuilder.unit(),
    [ST_NamespaceSpMethod.verifyEqual]: TypeBuilder.unit(),
    [ST_NamespaceSpMethod.failWith]: TypeBuilder.unit(),
    [ST_NamespaceSpMethod.transfer]: TypeBuilder.unit(),
    [ST_NamespaceSpMethod.some]: TypeBuilder.option(TypeBuilder.unknown()),
    [ST_NamespaceSpMethod.ediv]: TypeBuilder.option(TypeBuilder.tuple([TypeBuilder.unknown(), TypeBuilder.unknown()])),
    [ST_NamespaceSpMethod.bigMap]: {
        type: FrontendType.TBig_map,
        keyType: TypeBuilder.unknown(),
        valueType: TypeBuilder.unknown(),
    },
    [ST_NamespaceSpMethod.unpack]: TypeBuilder.option(TypeBuilder.unknown()),
    [ST_NamespaceSpMethod.pack]: TypeBuilder.bytes(),
    [ST_NamespaceSpMethod.variant]: TypeBuilder.unknown(),
    [ST_NamespaceSpMethod.constant]: TypeBuilder.unknown(),
    [ST_NamespaceSpMethod.view]: TypeBuilder.unknown(),
    // Contract and Addresses
    [ST_NamespaceSpMethod.selfEntryPoint]: TypeBuilder.contract(TypeBuilder.unknown()),
    [ST_NamespaceSpMethod.toAddress]: TypeBuilder.address(),
    [ST_NamespaceSpMethod.contract]: TypeBuilder.contract(TypeBuilder.unknown()),
    [ST_NamespaceSpMethod.implicitAccount]: TypeBuilder.contract(TypeBuilder.unit()),
    [ST_NamespaceSpMethod.hashKey]: TypeBuilder.keyHash(),
    [ST_NamespaceSpMethod.createContract]: TypeBuilder.address(),
    [ST_NamespaceSpMethod.createContractOperation]: TypeBuilder.record({
        properties: { address: TypeBuilder.address(), operation: TypeBuilder.operation() },
    }),
    //
    [ST_NamespaceSpMethod.votingPower]: TypeBuilder.nat(),
    [ST_NamespaceSpMethod.setDelegate]: TypeBuilder.unit(),
    [ST_NamespaceSpMethod.concat]: TypeBuilder.unit(),
    [ST_NamespaceSpMethod.openChest]: TypeBuilder.bytes(),
    // Cryptography
    [ST_NamespaceSpMethod.checkSignature]: TypeBuilder.bool(),
    [ST_NamespaceSpMethod.pairingCheck]: TypeBuilder.bool(),
    // Hash functions
    [ST_NamespaceSpMethod.blake2b]: TypeBuilder.bytes(),
    [ST_NamespaceSpMethod.sha256]: TypeBuilder.bytes(),
    [ST_NamespaceSpMethod.sha512]: TypeBuilder.bytes(),
    [ST_NamespaceSpMethod.sha3]: TypeBuilder.bytes(),
    [ST_NamespaceSpMethod.keccak]: TypeBuilder.bytes(),
};

export const ReturnTypeOfNativeMethod = {
    [ST_Namespace.Sp]: ReturnTypeOfSpMethod,
    [ST_Namespace.Scenario]: ReturnTypeOfScenarioMethod,
};

export const ExpressionOfNativeCall: Record<ST_Namespace.Sp, Record<ST_NamespaceSpMethod, ST_ExpressionKind>> = {
    [ST_Namespace.Sp]: {
        [ST_NamespaceSpMethod.verify]: ST_ExpressionKind.Verify,
        [ST_NamespaceSpMethod.verifyEqual]: ST_ExpressionKind.Verify,
        [ST_NamespaceSpMethod.failWith]: ST_ExpressionKind.FailWith,
        [ST_NamespaceSpMethod.some]: ST_ExpressionKind.Some,
        [ST_NamespaceSpMethod.ediv]: ST_ExpressionKind.Ediv,
        [ST_NamespaceSpMethod.bigMap]: ST_ExpressionKind.AsExpression,
        [ST_NamespaceSpMethod.transfer]: ST_ExpressionKind.Transfer,
        [ST_NamespaceSpMethod.unpack]: ST_ExpressionKind.Unpack,
        [ST_NamespaceSpMethod.pack]: ST_ExpressionKind.Pack,
        [ST_NamespaceSpMethod.never]: ST_ExpressionKind.Never,
        [ST_NamespaceSpMethod.variant]: ST_ExpressionKind.Variant,
        [ST_NamespaceSpMethod.constant]: ST_ExpressionKind.Constant,
        [ST_NamespaceSpMethod.view]: ST_ExpressionKind.StaticViewAccessExpr,
        // Contract and Addresses
        [ST_NamespaceSpMethod.selfEntryPoint]: ST_ExpressionKind.SelfEntryPoint,
        [ST_NamespaceSpMethod.contract]: ST_ExpressionKind.Contract,
        [ST_NamespaceSpMethod.createContract]: ST_ExpressionKind.CreateContract,
        [ST_NamespaceSpMethod.createContractOperation]: ST_ExpressionKind.CreateContractOperation,
        [ST_NamespaceSpMethod.toAddress]: ST_ExpressionKind.ToAddress,
        [ST_NamespaceSpMethod.implicitAccount]: ST_ExpressionKind.ImplicitAccount,
        [ST_NamespaceSpMethod.hashKey]: ST_ExpressionKind.HashKey,
        [ST_NamespaceSpMethod.votingPower]: ST_ExpressionKind.VotingPower,
        [ST_NamespaceSpMethod.setDelegate]: ST_ExpressionKind.SetDelegate,
        [ST_NamespaceSpMethod.concat]: ST_ExpressionKind.Concat,
        [ST_NamespaceSpMethod.openChest]: ST_ExpressionKind.OpenChest,
        // Cryptography
        [ST_NamespaceSpMethod.pairingCheck]: ST_ExpressionKind.PairingCheck,
        [ST_NamespaceSpMethod.checkSignature]: ST_ExpressionKind.CheckSignature,
        // Hash functions
        [ST_NamespaceSpMethod.blake2b]: ST_ExpressionKind.Blake2b,
        [ST_NamespaceSpMethod.sha256]: ST_ExpressionKind.Sha256,
        [ST_NamespaceSpMethod.sha512]: ST_ExpressionKind.Sha512,
        [ST_NamespaceSpMethod.sha3]: ST_ExpressionKind.Sha3,
        [ST_NamespaceSpMethod.keccak]: ST_ExpressionKind.Keccak,
    },
};

export const NativeCallOfExpression: Record<ST_Namespace.Sp, Record<string, ST_NamespaceSpMethod>> = {
    [ST_Namespace.Sp]: Object.entries(ExpressionOfNativeCall.Sp).reduce((acc, [k, v]) => ({ ...acc, [v]: k }), {}),
};

export const ValueOfEnum = {
    [ST_Enum.Layout]: {
        right_comb: 'right_comb',
    },
};

export enum PropertyMethod {
    hasKey = 'hasKey',
    get = 'get',
    set = 'set',
    remove = 'remove',
    size = 'size',
    push = 'push',
    isSome = 'isSome',
    openSome = 'openSome',
    fst = 'fst',
    snd = 'snd',
    plus = 'plus',
    minus = 'minus',
    negate = 'negate',
    toInt = 'toInt',
    toNat = 'toNat',
    abs = 'abs',
    multiply = 'multiply',
    concat = 'concat',
    slice = 'slice',
    add = 'add',
    contains = 'contains',
    elements = 'elements',
    reverse = 'reverse',
    keys = 'keys',
    values = 'values',
    entries = 'entries',
    isVariant = 'isVariant',
    openVariant = 'openVariant',
    // Timestamp methods
    addSeconds = 'addSeconds',
    addMinutes = 'addMinutes',
    addHours = 'addHours',
    addDays = 'addDays',
    toSeconds = 'toSeconds',
}

export const TypeByMethod: Record<PropertyMethod, ST_TypeDef> = {
    [PropertyMethod.hasKey]: TypeBuilder.bool(),
    [PropertyMethod.get]: TypeBuilder.unknown(),
    [PropertyMethod.set]: TypeBuilder.unit(),
    [PropertyMethod.remove]: TypeBuilder.unit(),
    [PropertyMethod.size]: TypeBuilder.nat(),
    [PropertyMethod.push]: TypeBuilder.unit(),
    [PropertyMethod.isSome]: TypeBuilder.bool(),
    [PropertyMethod.openSome]: TypeBuilder.unknown(),
    [PropertyMethod.fst]: TypeBuilder.unknown(),
    [PropertyMethod.snd]: TypeBuilder.unknown(),
    [PropertyMethod.plus]: TypeBuilder.unknown(),
    [PropertyMethod.minus]: TypeBuilder.unknown(),
    [PropertyMethod.negate]: TypeBuilder.unknown(),
    [PropertyMethod.multiply]: TypeBuilder.unknown(),
    [PropertyMethod.toInt]: TypeBuilder.int(),
    [PropertyMethod.toNat]: TypeBuilder.nat(),
    [PropertyMethod.abs]: TypeBuilder.nat(),
    [PropertyMethod.concat]: TypeBuilder.unknown(),
    [PropertyMethod.slice]: TypeBuilder.unknown(),
    [PropertyMethod.add]: TypeBuilder.unknown(),
    [PropertyMethod.contains]: TypeBuilder.bool(),
    [PropertyMethod.elements]: TypeBuilder.list([TypeBuilder.unknown()]),
    [PropertyMethod.reverse]: TypeBuilder.list([TypeBuilder.unknown()]),
    [PropertyMethod.keys]: TypeBuilder.list([TypeBuilder.unknown()]),
    [PropertyMethod.values]: TypeBuilder.list([TypeBuilder.unknown()]),
    [PropertyMethod.entries]: TypeBuilder.list([TypeBuilder.unknown()]),
    [PropertyMethod.isVariant]: TypeBuilder.bool(),
    [PropertyMethod.openVariant]: TypeBuilder.unknown(),
    [PropertyMethod.addSeconds]: TypeBuilder.timestamp(),
    [PropertyMethod.addMinutes]: TypeBuilder.timestamp(),
    [PropertyMethod.addHours]: TypeBuilder.timestamp(),
    [PropertyMethod.addDays]: TypeBuilder.timestamp(),
    [PropertyMethod.toSeconds]: TypeBuilder.nat(),
};

export const MethodsByType: Record<FrontendType, PropertyMethod[]> = {
    [FrontendType.TAddress]: [],
    [FrontendType.TBig_map]: [PropertyMethod.hasKey, PropertyMethod.get, PropertyMethod.set, PropertyMethod.remove],
    [FrontendType.TBls12_381_fr]: [
        PropertyMethod.plus,
        PropertyMethod.negate,
        PropertyMethod.toInt,
        PropertyMethod.multiply,
    ],
    [FrontendType.TBls12_381_g1]: [PropertyMethod.plus, PropertyMethod.negate, PropertyMethod.multiply],
    [FrontendType.TBls12_381_g2]: [PropertyMethod.plus, PropertyMethod.negate, PropertyMethod.multiply],
    [FrontendType.TBool]: [],
    [FrontendType.TBytes]: [PropertyMethod.size, PropertyMethod.concat, PropertyMethod.slice],
    [FrontendType.TChain_id]: [],
    [FrontendType.TInt]: [PropertyMethod.multiply, PropertyMethod.toNat, PropertyMethod.abs],
    [FrontendType.TKey]: [],
    [FrontendType.TKey_hash]: [],
    [FrontendType.TLambda]: [],
    [FrontendType.TList]: [PropertyMethod.push, PropertyMethod.size, PropertyMethod.reverse],
    [FrontendType.TMap]: [
        PropertyMethod.hasKey,
        PropertyMethod.get,
        PropertyMethod.set,
        PropertyMethod.remove,
        PropertyMethod.size,
        PropertyMethod.keys,
        PropertyMethod.keys,
        PropertyMethod.values,
        PropertyMethod.entries,
    ],
    [FrontendType.TMutez]: [],
    [FrontendType.TNat]: [PropertyMethod.multiply, PropertyMethod.toInt],
    [FrontendType.TNever]: [],
    [FrontendType.TOption]: [PropertyMethod.isSome, PropertyMethod.openSome],
    [FrontendType.TContract]: [],
    [FrontendType.TTuple]: [PropertyMethod.fst, PropertyMethod.snd],
    [FrontendType.TOperation]: [],
    [FrontendType.TSet]: [
        PropertyMethod.size,
        PropertyMethod.add,
        PropertyMethod.remove,
        PropertyMethod.contains,
        PropertyMethod.elements,
    ],
    [FrontendType.TSignature]: [],
    [FrontendType.TString]: [PropertyMethod.size, PropertyMethod.concat, PropertyMethod.slice],
    [FrontendType.TTimestamp]: [
        PropertyMethod.minus,
        PropertyMethod.addDays,
        PropertyMethod.addHours,
        PropertyMethod.addMinutes,
        PropertyMethod.addSeconds,
        PropertyMethod.toSeconds,
    ],
    [FrontendType.TUnit]: [],
    [FrontendType.TChest]: [],
    [FrontendType.TChest_key]: [],
    //
    [FrontendType.TFunction]: [],
    [FrontendType.TIntOrNat]: [],
    [FrontendType.TRecord]: [],
    [FrontendType.TVariant]: [PropertyMethod.isVariant, PropertyMethod.openVariant],
    [FrontendType.TUnknown]: [],
};

export const ExpressionByMethod: Record<PropertyMethod, ST_ExpressionKind | 'Transform'> = {
    [PropertyMethod.hasKey]: ST_ExpressionKind.Contains,
    [PropertyMethod.get]: ST_ExpressionKind.GetItem,
    [PropertyMethod.set]: ST_ExpressionKind.SetItem,
    [PropertyMethod.size]: ST_ExpressionKind.Size,
    [PropertyMethod.push]: ST_ExpressionKind.Push,
    [PropertyMethod.isSome]: ST_ExpressionKind.IsVariant,
    [PropertyMethod.isVariant]: ST_ExpressionKind.IsVariant,
    [PropertyMethod.openSome]: ST_ExpressionKind.OpenVariant,
    [PropertyMethod.openVariant]: ST_ExpressionKind.OpenVariant,
    [PropertyMethod.fst]: ST_ExpressionKind.PairCAR,
    [PropertyMethod.snd]: ST_ExpressionKind.PairCDR,
    [PropertyMethod.plus]: ST_ExpressionKind.BinaryExpr,
    [PropertyMethod.minus]: ST_ExpressionKind.BinaryExpr,
    [PropertyMethod.multiply]: ST_ExpressionKind.BinaryExpr,
    [PropertyMethod.concat]: ST_ExpressionKind.BinaryExpr,
    [PropertyMethod.negate]: ST_ExpressionKind.Negate,
    [PropertyMethod.toInt]: ST_ExpressionKind.ToInt,
    [PropertyMethod.toNat]: ST_ExpressionKind.ToNat,
    [PropertyMethod.abs]: ST_ExpressionKind.ABS,
    [PropertyMethod.slice]: ST_ExpressionKind.Slice,
    [PropertyMethod.add]: ST_ExpressionKind.Add,
    [PropertyMethod.remove]: ST_ExpressionKind.Remove,
    [PropertyMethod.contains]: ST_ExpressionKind.Contains,
    [PropertyMethod.elements]: ST_ExpressionKind.GetElements,
    [PropertyMethod.reverse]: ST_ExpressionKind.Reverse,
    [PropertyMethod.keys]: ST_ExpressionKind.GetKeys,
    [PropertyMethod.values]: ST_ExpressionKind.GetValues,
    [PropertyMethod.entries]: ST_ExpressionKind.GetEntries,
    // Timestamp methods
    [PropertyMethod.addSeconds]: ST_ExpressionKind.AddSeconds,
    [PropertyMethod.addMinutes]: ST_ExpressionKind.AddSeconds,
    [PropertyMethod.addHours]: ST_ExpressionKind.AddSeconds,
    [PropertyMethod.addDays]: ST_ExpressionKind.AddSeconds,
    [PropertyMethod.toSeconds]: 'Transform',
};

export const EntryPointCallOptionTypes = {
    level: TypeBuilder.nat(),
    amount: TypeBuilder.mutez(),
    chainId: TypeBuilder.chainId(),
    sender: undefined,
    source: undefined,
    now: TypeBuilder.timestamp(),
    votingPowers: TypeBuilder.map(TypeBuilder.keyHash(), TypeBuilder.int()),
    exception: undefined,
    valid: TypeBuilder.bool(),
    show: TypeBuilder.bool(),
};

export const EntryPointCallOptionNames = {
    level: 'level',
    amount: 'amount',
    chainId: 'chain_id',
    sender: 'sender',
    source: 'source',
    now: 'time',
    votingPowers: 'voting_powers',
    exception: 'exception',
    valid: 'valid',
    show: 'show',
};

export enum ST_ContractValue {
    balance = 'balance',
    address = 'address',
    baker = 'baker',
    storage = 'storage',
    typed = 'typed',
}
