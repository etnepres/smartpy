export enum ST_FileExtension {
    TS = 'ts',
    JSON = 'json',
}
