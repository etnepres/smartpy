import type { CompilerOptions as CompilerOptionsType } from 'typescript';
import { ScriptTarget, ModuleKind, ImportsNotUsedAsValues } from 'typescript';

export const CompilerOptions: CompilerOptionsType = {
    experimentalDecorators: true,
    noUnusedParameters: true,
    importsNotUsedAsValues: ImportsNotUsedAsValues.Error,
    noUnusedLocals: true,
    allowNonTsExtensions: true,
    noFallthroughCasesInSwitch: true,
    baseUrl: './',
    paths: {
        './test_ImportsNested': ['*'],
        '*.ts': ['*'],
    },
    target: ScriptTarget.ES2020,
    module: ModuleKind.ES2020,
};

export default CompilerOptions;
