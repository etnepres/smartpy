import type { LineAndCharacter } from 'typescript';

export type Nullable<T> = T | null | undefined;

export interface FileLineInfo extends LineAndCharacter {
    fileName: string;
}

export type FileInfo = {
    baseDir?: string;
    name: string;
    code: string;
};
