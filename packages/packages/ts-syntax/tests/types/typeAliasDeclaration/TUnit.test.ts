import { FrontendType } from '../../../src/enums/type';
import Transpiler from '../../../src/lib/Transpiler';
import { ExpectAnyLine } from '../../ExpectAnyLine';

test(`void => ${FrontendType.TUnit}`, async () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type unit_type = void;`,
    };
    const transpiler = new Transpiler(sourceCode);
    await transpiler.transpile();

    expect(transpiler.output.globalTypeDefs).toEqual({
        unit_type: {
            line: ExpectAnyLine,
            type: FrontendType.TUnit,
        },
    });
});

test(`${FrontendType.TUnit} => ${FrontendType.TUnit}`, async () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type unit_type = ${FrontendType.TUnit}`,
    };
    const transpiler = new Transpiler(sourceCode);
    await transpiler.transpile();

    expect(transpiler.output.globalTypeDefs).toEqual({
        unit_type: {
            line: ExpectAnyLine,
            type: FrontendType.TUnit,
        },
    });
});
