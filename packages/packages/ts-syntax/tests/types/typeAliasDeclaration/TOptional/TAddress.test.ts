import { FrontendType } from '../../../../src/enums/type';
import Transpiler from '../../../../src/lib/Transpiler';
import { ExpectAnyLine } from '../../../ExpectAnyLine';

test(`${FrontendType.TOption}<${FrontendType.TNat}>`, async () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type optional_nat_type = ${FrontendType.TOption}<${FrontendType.TNat}>;`,
    };
    const transpiler = new Transpiler(sourceCode);
    await transpiler.transpile();

    expect(transpiler.output.globalTypeDefs).toEqual({
        optional_nat_type: {
            line: ExpectAnyLine,
            type: FrontendType.TOption,
            innerType: {
                line: ExpectAnyLine,
                type: FrontendType.TNat,
            },
        },
    });
});
