import { FrontendType } from '../../../src/enums/type';
import Transpiler from '../../../src/lib/Transpiler';
import { ExpectAnyLine } from '../../ExpectAnyLine';

test(FrontendType.TBytes, async () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type bytes_type = ${FrontendType.TBytes};`,
    };
    const transpiler = new Transpiler(sourceCode);
    await transpiler.transpile();

    expect(transpiler.output.globalTypeDefs).toEqual({
        bytes_type: {
            line: ExpectAnyLine,
            type: FrontendType.TBytes,
        },
    });
});
