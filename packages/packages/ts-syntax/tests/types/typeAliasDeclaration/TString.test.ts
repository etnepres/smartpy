import { FrontendType } from '../../../src/enums/type';
import Transpiler from '../../../src/lib/Transpiler';
import { ExpectAnyLine } from '../../ExpectAnyLine';

test(`string => ${FrontendType.TString}`, async () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type string_type = string;`,
    };
    const transpiler = new Transpiler(sourceCode);
    await transpiler.transpile();

    expect(transpiler.output.globalTypeDefs).toEqual({
        string_type: {
            line: ExpectAnyLine,
            type: FrontendType.TString,
        },
    });
});

test(`${FrontendType.TString} => ${FrontendType.TString}`, async () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type string_type = ${FrontendType.TString};`,
    };
    const transpiler = new Transpiler(sourceCode);
    await transpiler.transpile();

    expect(transpiler.output.globalTypeDefs).toEqual({
        string_type: {
            line: ExpectAnyLine,
            type: FrontendType.TString,
        },
    });
});
