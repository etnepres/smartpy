import { FrontendType } from '../../../src/enums/type';
import Transpiler from '../../../src/lib/Transpiler';
import { ExpectAnyLine } from '../../ExpectAnyLine';

test(FrontendType.TAddress, async () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type address_type = ${FrontendType.TAddress}`,
    };
    const transpiler = new Transpiler(sourceCode);
    await transpiler.transpile();

    expect(transpiler.output.globalTypeDefs).toEqual({
        address_type: {
            line: ExpectAnyLine,
            type: FrontendType.TAddress,
        },
    });
});
