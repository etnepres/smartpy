import { FrontendType } from '../../../src/enums/type';
import Transpiler from '../../../src/lib/Transpiler';
import { ExpectAnyLine } from '../../ExpectAnyLine';

test(`boolean => ${FrontendType.TBool}`, async () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type bool_type = boolean;`,
    };
    const transpiler = new Transpiler(sourceCode);
    await transpiler.transpile();

    expect(transpiler.output.globalTypeDefs).toEqual({
        bool_type: {
            line: ExpectAnyLine,
            type: FrontendType.TBool,
        },
    });
});

test(`${FrontendType.TBool} => ${FrontendType.TBool}`, async () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type bool_type = ${FrontendType.TBool}`,
    };
    const transpiler = new Transpiler(sourceCode);
    await transpiler.transpile();

    expect(transpiler.output.globalTypeDefs).toEqual({
        bool_type: {
            line: ExpectAnyLine,
            type: FrontendType.TBool,
        },
    });
});
