import { FrontendType } from '../../../src/enums/type';
import Transpiler from '../../../src/lib/Transpiler';
import { ExpectAnyLine } from '../../ExpectAnyLine';

test(`never => ${FrontendType.TSignature}`, async () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type never_type = never;`,
    };
    const transpiler = new Transpiler(sourceCode);
    await transpiler.transpile();

    expect(transpiler.output.globalTypeDefs).toEqual({
        never_type: {
            line: ExpectAnyLine,
            type: FrontendType.TNever,
        },
    });
});

test(`${FrontendType.TSignature} => ${FrontendType.TSignature}`, async () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type never_type = ${FrontendType.TNever};`,
    };
    const transpiler = new Transpiler(sourceCode);
    await transpiler.transpile();

    expect(transpiler.output.globalTypeDefs).toEqual({
        never_type: {
            line: ExpectAnyLine,
            type: FrontendType.TNever,
        },
    });
});
