import { FrontendType } from '../../../../src/enums/type';
import Transpiler from '../../../../src/lib/Transpiler';
import { ExpectAnyLine } from '../../../ExpectAnyLine';

test(`${FrontendType.TSet}<${FrontendType.TNat}>`, async () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type set__nat__type = ${FrontendType.TSet}<${FrontendType.TNat}>;`,
    };
    const transpiler = new Transpiler(sourceCode);
    await transpiler.transpile();

    expect(transpiler.output.globalTypeDefs).toEqual({
        set__nat__type: {
            type: FrontendType.TSet,
            line: ExpectAnyLine,
            innerTypes: [{ line: ExpectAnyLine, type: FrontendType.TNat }],
        },
    });
});
