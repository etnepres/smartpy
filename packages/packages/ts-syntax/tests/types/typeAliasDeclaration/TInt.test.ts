import { FrontendType } from '../../../src/enums/type';
import Transpiler from '../../../src/lib/Transpiler';
import { ExpectAnyLine } from '../../ExpectAnyLine';

test(FrontendType.TInt, async () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type int_type = ${FrontendType.TInt};`,
    };
    const transpiler = new Transpiler(sourceCode);
    await transpiler.transpile();

    expect(transpiler.output.globalTypeDefs).toEqual({
        int_type: {
            line: ExpectAnyLine,
            type: FrontendType.TInt,
        },
    });
});
