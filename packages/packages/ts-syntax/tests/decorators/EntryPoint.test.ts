import Transpiler from '../../src/lib/Transpiler';
import { ExpectAnyLine } from '../ExpectAnyLine';

test('EntryPoint Decorator', async () => {
    const sourceCode = {
        name: 'code.ts',
        code: `
        @Contract
        class Sample {

            @EntryPoint({ name: 'some_ep', mock: false, lazy: true })
            ep1() {
                //
            }

            @EntryPoint({ mock: false, lazy_no_code: true })
            ep2() {
                //
            }

            @EntryPoint({ lazy: true })
            ep3() {
                //
            }
        }

        Dev.compileContract('compile_contract', new Sample());
        `,
    };
    const transpiler = new Transpiler(sourceCode);
    await transpiler.transpile();

    expect(transpiler.output.classes['Sample']).toMatchSnapshot({
        name: 'Sample',
        line: ExpectAnyLine,
        decorators: {
            Contract: {
                kind: 'Contract',
                name: 'Sample',
                flags: [],
            },
        },
        entry_points: {
            ep2: {
                functionRef: 'ep2',
                name: 'ep2',
                mock: false,
                lazy: false,
                lazy_no_code: true,
            },
            ep1: {
                functionRef: 'ep1',
                name: 'some_ep',
                mock: false,
                lazy: true,
                lazy_no_code: false,
            },
            ep3: {
                functionRef: 'ep3',
                name: 'ep3',
                mock: false,
                lazy: true,
                lazy_no_code: false,
            },
        },
        scope: {
            properties: {},
            functions: {
                ep2: {
                    name: 'ep2',
                    type: {
                        type: 'TFunction',
                        inputTypes: {},
                        outputType: {
                            type: 'TUnit',
                        },
                        line: ExpectAnyLine,
                    },
                    typeDefs: {},
                    properties: {},
                    statements: {},
                    decorators: {
                        EntryPoint: {
                            kind: 'EntryPoint',
                            name: 'ep2',
                            mock: false,
                            lazy: false,
                            lazy_no_code: true,
                        },
                    },
                    line: ExpectAnyLine,
                },
                ep1: {
                    name: 'ep1',
                    type: {
                        type: 'TFunction',
                        inputTypes: {},
                        outputType: {
                            type: 'TUnit',
                        },
                        line: ExpectAnyLine,
                    },
                    typeDefs: {},
                    properties: {},
                    statements: {},
                    decorators: {
                        EntryPoint: {
                            kind: 'EntryPoint',
                            name: 'some_ep',
                            mock: false,
                            lazy: true,
                            lazy_no_code: false,
                        },
                    },
                    line: ExpectAnyLine,
                },
                ep3: {
                    name: 'ep3',
                    type: {
                        type: 'TFunction',
                        inputTypes: {},
                        outputType: {
                            type: 'TUnit',
                        },
                        line: ExpectAnyLine,
                    },
                    typeDefs: {},
                    properties: {},
                    statements: {},
                    decorators: {
                        EntryPoint: {
                            kind: 'EntryPoint',
                            name: 'ep3',
                            mock: false,
                            lazy: true,
                            lazy_no_code: false,
                        },
                    },
                    line: ExpectAnyLine,
                },
            },
        },
    });
});
