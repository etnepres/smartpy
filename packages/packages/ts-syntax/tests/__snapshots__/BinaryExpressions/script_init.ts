type TStorage = {
    counter1: TInt;
    counter2: TInt;
    counter3: TNat;
};

@Contract
class BinaryExpressions {
    storage = {
        counter1: 0,
        counter2: 0,
        counter3: 0,
    } as TStorage;

    @EntryPoint
    equal() {
        this.storage.counter1 = 1;
    }

    @EntryPoint
    plus() {
        this.storage.counter1 = 1 + 1;
        this.storage.counter1 = this.storage.counter1 + 1;
        this.storage.counter1 += this.storage.counter2 + 1;
    }

    @EntryPoint
    minus() {
        this.storage.counter1 = 1 - 1;
        this.storage.counter1 = this.storage.counter1 - 1;
        this.storage.counter1 -= this.storage.counter2 - 1;
    }

    @EntryPoint
    mul() {
        this.storage.counter1 = 1 * 1;
        this.storage.counter1 = this.storage.counter1 * 1;
        this.storage.counter1 *= this.storage.counter2 * 1;
    }

    @EntryPoint
    div() {
        this.storage.counter3 = 1 / 1;
    }
}
