import * as fs from 'fs';
import { getCode } from '../loadTemplate';
import SmartTS from '../../src/';

// Change the following regex to filter the templates being tested
const TEMPLATE_FILTER = /\w*[.]ts/g;

jest.setTimeout(50000);

async function runTest(filePath: string) {
    test(`Template ${filePath}`, async () => {
        const code = getCode(filePath);
        const [name, ...rest] = filePath.split('/').reverse();
        const baseDir = rest
            .filter((s) => !['.'].includes(s))
            .reverse()
            .join('/');
        const result = await SmartTS.transpile({
            baseDir,
            name,
            code,
        });

        // Snapshot the S-expression result
        expect(result).toMatchSnapshot(`Transpilation Result (${filePath})`);
    });
}

async function runTestsFromFolder(path: string) {
    const files = fs.readdirSync(path, { encoding: 'utf-8' });
    files.map(async (fileName) => {
        fileName = `${path}/${fileName}`;
        try {
            if (fs.lstatSync(fileName).isDirectory()) {
                runTestsFromFolder(fileName);
            } else if (fileName.match(TEMPLATE_FILTER)) {
                await runTest(fileName.replace('.ts', ''));
            }
        } catch (e: any) {
            if (process.env.TRACE_ERRORS) {
                console.trace(e.stderr?.toString() || e);
            }

            throw new Error(`Failed to generate scenario for template ${fileName}.\n\n${e} (${e?.snippet || ''})`);
        }
    });
}

describe('Build scenarios for ', () => {
    runTestsFromFolder('./templates');
});
