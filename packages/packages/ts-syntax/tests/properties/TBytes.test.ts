import Transpiler from '../../src/lib/Transpiler';
import { FrontendType } from '../../src/enums/type';

test(`${FrontendType.TBytes}`, async () => {
    const sourceCode = {
        name: 'code.ts',
        code: `let bytes: ${FrontendType.TBytes} = "0x00"`,
    };
    const transpiler = new Transpiler(sourceCode);
    await transpiler.transpile();

    expect(transpiler.output.globalProperties).toMatchSnapshot();
});
