import Transpiler from '../../src/lib/Transpiler';
import { FrontendType } from '../../src/enums/type';

test(`${FrontendType.TMutez}`, async () => {
    const sourceCode = {
        name: 'code.ts',
        code: `let mutez: ${FrontendType.TMutez} = 1`,
    };
    const transpiler = new Transpiler(sourceCode);
    await transpiler.transpile();

    expect(transpiler.output.globalProperties).toMatchSnapshot();
});
