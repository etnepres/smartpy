# SmartPy Contract Originator

## Requirements

| Requirement |  version  |
|:-----------:|:---------:|
| NodeJs      | >= v10.x  |


## Install the originator
```sh
npm i -g @smartpy/originator
```

## Originate a contract
```sh
# Using Micheline format (.tz)
smartpy-originator originate-contract --code code.tz --storage storage.tz --rpc https://mainnet.smartpy.io

# Using Michelson format (.json)
smartpy-originator originate-contract --code code.json --storage storage.json --rpc https://mainnet.smartpy.io

# By default, the originator will use a faucet account.
# But you can provide your own private key as an argument
smartpy-originator originate-contract --code code.json --storage storage.json --rpc https://mainnet.smartpy.io --private-key edsk...
```

## Help
```
smartpy-originator help originate-contract
```

<img height="48" href="https://smartpy.io" src="https://smartpy.io/static/img/logo.png">
