import Commander from 'commander';

import { originatorCommands } from './Originator';

originatorCommands(Commander);

export default Commander;
