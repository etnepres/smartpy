import { TezosToolkit } from '@taquito/taquito';
import { TransactionOperation } from '@taquito/taquito/dist/types/operations/transaction-operation';

export async function invokeContractWithParameter(
    tezos: TezosToolkit,
    address: string,
    parameter: object,
    entrypoint: string,
    amount: number = 0
): Promise<TransactionOperation> {
    const op = await tezos.contract.transfer({
        to: address,
        amount: amount,
        fee: undefined,
        storageLimit: undefined,
        gasLimit: undefined,
        parameter: {entrypoint:entrypoint, value: parameter},
    });

    // Await for one block confirmation
    await op.confirmation(1);

    return op;
}

export default {
    invokeContractWithParameter,
};
