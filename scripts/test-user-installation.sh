#!/usr/bin/env bash

# This test runs some of the commands that a new SmarPy.sh user would run
# and checks that they do what they are supposed to.
#

set -e

say () {
    {
        printf "[TestUserInstall] "
        printf "$@"
        printf "\n"
    } >&2
}

export PAGER=cat

test_output="$PWD/_build/test-user-install"
rm -fr "$test_output"
mkdir -p "$test_output"

say "Getting SmartPy.sh"
cp smartpy-cli/*.sh $test_output

say "Trying pre-install usage SmartPy.sh"
sh $test_output/install.sh --help 2>&1 | grep "medium.com" > /dev/null

say "Trying local-install-from"
install_path="$test_output/install"
sh $test_output/install.sh local-install-from \
   $PWD/smartpy-cli "$install_path" 2>&1 | sed 's/^/ | /'

if ! [ -f "$install_path/smartml-cli.js" ] ; then
    say "Installation failed"
    exit 5
fi

say "Trying post-install usage SmartPy.sh"
sh $install_path/SmartPy.sh --help 2>&1 | grep 'Usage' > /dev/null || (sh $install_path/SmartPy.sh --help  && exit 6)

say "Trying compile"
compile_and_verify_path() {
    say "Testing compile with output path: $1"
    sh $install_path/SmartPy.sh compile $install_path/templates/welcome.py "MyContract(42,24)" "$1"

    if [ "$(cat "$1/welcome_storage_init.tz")" != "(Pair 42 24)" ] ; then
        say "Wrong compilation"
        exit 7
    fi
}
compile_and_verify_path "$test_output/welcome-build"
compile_and_verify_path "."

welcometestout="$test_output/welcome-test"
sh $install_path/SmartPy.sh test \
   $install_path/templates/welcome.py "$welcometestout"


### TODO

exit 0

if [ "$with_docker" = "false" ] ; then
    say "Skipping docker sandbox"
else
    welcomesbox="$test_output/welcome-sbox"
    sh $install_path/SmartPy.sh \
       run-smartpy-test-with-docker-sandbox \
       $install_path/templates/welcome.py "$welcomesbox" || {
        say "docker-sandbox for welcome.py expected to fail (cf. !192)"
    }
    grep ERROR "$welcomesbox/sandbox-scenario/welcome_scenario-Welcome/result.tsv"
    grep F:V "$welcomesbox/sandbox-scenario/welcome_scenario-Welcome/result.tsv"
fi
