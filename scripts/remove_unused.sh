#!/usr/bin/env bash

set -euo pipefail

dir=$1
shift
oks=$@

desired() {
    for x in $oks
    do
        x=${x%.ok}
        x=${x#_build/test/}
        echo ^test_baselines/$x
    done
}

find $dir -type f | (grep -vf <(desired) || :) | xargs rm -fv

find $dir -type d -empty -delete
