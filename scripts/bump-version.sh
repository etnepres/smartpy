#!/usr/bin/env bash

function usage() {
    echo -e "
        Usage:
            $(basename "$0") [ <newversion> | major | minor | patch ]
    "
}

function make_version() {
    npm --prefix "$(realpath "$(dirname "$0")")/../packages" run version "$1" &> /dev/null
    new_version=$(npm --prefix packages/frontend -s run env echo '$npm_package_version')

    sed -i "s%VERSION=\".*\"%VERSION=\"$new_version\"%g" "$(realpath "$(dirname "$0")")"/../smartpy-cli/SmartPy.sh

    echo "Updating frontend snapshots..."
    npm --prefix "$(realpath "$(dirname "$0")")/../packages/frontend" run update-snaps

    echo -e "

    New Version: $new_version

    Updated files:

        * packages/frontend/package.json
        * packages/packages/**/package.json
        * smartpy-cli/SmartPy.sh
    "
}

if [ -z "$WITH_REV" ]; then
    [ "$#" -ne 1 ] && usage && exit 1;

    make_version "$@"
else
    new_version="$(npm --prefix packages/frontend -s run env echo '$npm_package_version')-$(git rev-parse --short HEAD)"
    make_version "$new_version"
fi
