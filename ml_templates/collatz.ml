open SmartML

module OnEven = struct
  let%entry_point run params = transfer (params.x / 2) (mutez 0) params.k

  let init =
    Basics.build_contract
      ~flags:[Bool_Flag (Single_entry_point_annotation, false)]
      ~storage:[%expr ()]
      [run]
end

module OnOdd = struct
  let%entry_point run params =
    set_type params.x nat;
    transfer ((3 * params.x) + 1) (mutez 0) params.k

  let init =
    Basics.build_contract
      ~flags:[Bool_Flag (Single_entry_point_annotation, false)]
      ~storage:[%expr ()]
      [run]
end

module Collatz = struct
  let%entry_point run params =
    if params > 1
    then (
      data.counter <- data.counter + 1;
      let p = {k = self_entry_point "run"; x = params} in
      if params % 2 = 0
      then
        transfer
          p
          (mutez 0)
          (open_some (contract {k = contract nat; x = nat} data.on_even))
      else
        transfer
          p
          (mutez 0)
          (open_some (contract {k = contract nat; x = nat} data.on_odd)) )

  let%entry_point reset () = data.counter <- 0

  let init on_even on_odd =
    Basics.build_contract
      ~storage:
        [%expr {on_even = [%e on_even]; on_odd = [%e on_odd]; counter = 0}]
      [run; reset]
end

let () =
  Target.register_test
    ~name:"Collatz"
    [%actions
      h1 "Collatz template - Inter-Contract Calls";
      let on_even = register_contract OnEven.init in
      let on_odd = register_contract OnOdd.init in
      let collatz =
        register_contract
          (Collatz.init [%expr on_even.address] [%expr on_odd.address])
      in
      (* See https://oeis.org/A006577/list *)
      call collatz.run 42;
      verify (collatz.data.counter = 8);
      call collatz.reset ();
      call collatz.run 5;
      verify (collatz.data.counter = 5)]
