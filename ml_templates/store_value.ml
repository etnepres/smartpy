open SmartML

module Store_value = struct
  let%entry_point divide params =
    verify (params.divisor > 5);
    data.storedValue <- data.storedValue / params.divisor

  let%entry_point double () = data.storedValue <- data.storedValue * 2

  let%entry_point replace params = data.storedValue <- params.value

  let init v =
    Basics.build_contract
      ~storage:[%expr {storedValue = [%e v]}]
      [divide; double; replace]
end

let () =
  Target.register_test
    ~name:"StoreValue"
    [%actions
      h1 "Store Value";
      let c = register_contract (Store_value.init [%expr 12]) in
      call c.replace {value = 15};
      (* TODO p "Some computation".show (call c.data.storedValue * 12);*)
      call c.replace {value = 25};
      call c.double ();
      call c.divide {divisor = 2} ~valid:false
      (* TODO exception="WrongCondition: params.divisor > 5"*);
      verify (c.data.storedValue = 50);
      call c.divide {divisor = 6};
      verify (c.data.storedValue = 8)]
