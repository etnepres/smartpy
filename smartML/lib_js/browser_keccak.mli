(* Copyright 2019-2021 Smart Chain Arena LLC. *)

(** Bindings for keccak256 (generated with [gen_js_api]). *)

val keccak256 : string -> string [@@js.global "smartpyContext.Keccak256"]
