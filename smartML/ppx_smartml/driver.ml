(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Ppxlib

let () =
  Ppx_smartml_lib.Transformer.register_via_ppxlib ();
  Driver.run_as_ppx_rewriter ()
