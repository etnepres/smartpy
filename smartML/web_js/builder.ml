(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Tools

let buildEntrypointCallJSON ?(entry_point = "default") ~compiled_parameter =
  Yojson.Basic.to_string
    (`Assoc
      [ ("entrypoint", `String entry_point)
      ; ( "value"
        , Utils.Misc.json_to_json
            (Micheline.to_json
               (Michelson.To_micheline.literal compiled_parameter)) ) ])

let buildTransferParametersJSON ~compiled_parameter =
  Format.asprintf
    "%a"
    (Micheline.pp_as_json ())
    (Michelson.To_micheline.literal compiled_parameter)

let buildTransferParametersMicheline ~compiled_parameter : string =
  Michelson.string_of_literal compiled_parameter

let buildTransferParametersCLI
    ?(entry_point = "default") ~account ~destination ~compiled_parameter :
    string =
  Printf.sprintf
    "tezos-client transfer 0 from %s to %s --entrypoint %s --arg '%s'"
    account
    destination
    entry_point
    (buildTransferParametersMicheline ~compiled_parameter)
