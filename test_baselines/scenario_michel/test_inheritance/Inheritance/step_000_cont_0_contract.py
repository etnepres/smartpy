import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TIntOrNat, y = sp.TIntOrNat, z = sp.TIntOrNat).layout(("x", ("y", "z"))))
    self.init(x = 1,
              y = 2,
              z = 42)

  @sp.entry_point
  def myEntryPoint(self, params):
    sp.verify(self.data.x <= 100)
    self.data.x += params
    self.data.y = 12345
    self.data.y += params + 2
    self.data.x += 10

  @sp.entry_point
  def myEntryPoint2(self):
    pass

sp.add_compilation_target("test", Contract())