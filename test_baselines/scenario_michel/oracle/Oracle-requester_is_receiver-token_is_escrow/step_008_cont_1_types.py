import smartpy as sp

tstorage = sp.TRecord(active = sp.TBool, admin = sp.TAddress, max_amount = sp.TNat, token = sp.TAddress).layout(("active", ("admin", ("max_amount", "token"))))
tparameter = sp.TVariant(configure = sp.TRecord(active = sp.TBool, admin = sp.TAddress, max_amount = sp.TNat, token = sp.TAddress).layout(("active", ("admin", ("max_amount", "token")))), request_tokens = sp.TSet(sp.TAddress)).layout(("configure", "request_tokens"))
tprivates = { }
tviews = { }
