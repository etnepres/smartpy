import smartpy as sp

tstorage = sp.TRecord(a = sp.TNat).layout("a")
tparameter = sp.TVariant(test_and = sp.TNat, test_exclusive_or = sp.TNat, test_or = sp.TNat, test_shift_left = sp.TNat, test_shift_right = sp.TNat).layout((("test_and", "test_exclusive_or"), ("test_or", ("test_shift_left", "test_shift_right"))))
tprivates = { }
tviews = { }
