import smartpy as sp

tstorage = sp.TRecord(storedValue = sp.TIntOrNat).layout("storedValue")
tparameter = sp.TVariant(replace = sp.TRecord(value = sp.TIntOrNat).layout("value")).layout("replace")
tprivates = { }
tviews = { "get_value": ((), sp.TIntOrNat) }
