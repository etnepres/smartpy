open SmartML

module Contract = struct
  let%entry_point a () =
    data.steps <- data.steps + ".a";
    transfer () (tez 0) (self_entry_point "aa")

  let%entry_point aa () =
    data.steps <- data.steps + ".aa"

  let%entry_point b () =
    data.steps <- data.steps + ".b";
    if data.steps = "check.a.b" then
      data.conclusion <- "BFS"
    else
      data.conclusion <- "DFS"

  let%entry_point check () =
    data.steps <- "check";
    data.conclusion <- "";
    transfer () (tez 0) (self_entry_point "a");
    transfer () (tez 0) (self_entry_point "b")

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {conclusion = string; steps = string}]
      ~storage:[%expr
                 {conclusion = "";
                  steps = ""}]
      [a; aa; b; check]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())