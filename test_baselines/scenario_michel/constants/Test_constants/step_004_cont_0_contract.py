import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TIntOrNat)
    self.init(0)

  @sp.entry_point
  def ep(self, params):
    self.data = sp.constant_var("0")(params) + sp.constant_var("1")

sp.add_compilation_target("test", Contract())