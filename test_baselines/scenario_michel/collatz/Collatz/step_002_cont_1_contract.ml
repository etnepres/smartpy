open SmartML

module Contract = struct
  let%entry_point run params =
    set_type params.x nat;
    transfer ((3 * params.x) + 1) (tez 0) params.k

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [run]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())