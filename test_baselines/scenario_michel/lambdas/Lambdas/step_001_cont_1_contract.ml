open SmartML

module Contract = struct
  let%entry_point abs_test params =
    data.abcd <- params self.abs

  let%entry_point comp_test () =
    data.abcd <- {f = (fun _x7 -> result (_x7 + 3)); x = 2} self.comp

  let%entry_point f () =
    let%mutable toto = (fun _x8 -> result ((fst _x8) + (snd _x8))) in ();
    let%mutable titi = (apply_lambda 5 toto) in ();
    data.value <- 8 titi

  let%entry_point flambda () =
    data.value <- ((15 self.flam) self.flam) + (12345 self.square_root)

  let%entry_point h () =
    data.fff <- some (fun _x9 -> verify (_x9 >= 0);
let%mutable y = _x9 in ();
while (y * y) > _x9 do
  y <- ((_x9 / y) + y) / 2
done;
verify (((y * y) <= _x9) && (_x9 < ((y + 1) * (y + 1))));
result y)

  let%entry_point hh params =
    data.value <- params (open_some data.fff)

  let%entry_point i () =
    let%mutable ch1 = (fun _x10 -> verify (_x10 >= 0)) in ();
    let%mutable ch2 = (fun _x11 -> verify (_x11 >= 0);
result (_x11 - 2)) in ();
    let%mutable ch3 = (fun _x12 -> verify (_x12 >= 0);
result true) in ();
    let%mutable ch4 = (fun _x13 -> let%mutable ch3b = (fun _x14 -> verify (_x14 >= 0);
result false) in ();
verify (_x13 >= 0);
result (3 * _x13)) in ();
    data.value <- 12 ch4;
    let%mutable compute_lambdas_96 = (() self.not_pure) in ();
    verify (compute_lambdas_96 = data.value)

  let%entry_point operation_creation () =
    let%mutable f = (fun _x15 -> let%mutable __operations__ = (set_type_expr [] (list operation)) in ();
let%mutable create_contract_lambdas_101 = create contract ... in ();
operations <- create_contract_lambdas_101.operation :: operations;
let%mutable create_contract_lambdas_102 = create contract ... in ();
operations <- create_contract_lambdas_102.operation :: operations;
result operations) in ();
    List.iter (fun op ->
      operations <- op :: operations
    ) (12345001 f);
    List.iter (fun op ->
      operations <- op :: operations
    ) (12345002 f)

  let%entry_point operation_creation_result () =
    let%mutable f = (fun _x16 -> let%mutable __operations__ = (set_type_expr [] (list operation)) in ();
let%mutable create_contract_lambdas_110 = create contract ... in ();
operations <- create_contract_lambdas_110.operation :: operations;
let%var __s1 = 4 in
result (operations, __s1)) in ();
    let%mutable x = (12345001 f) in ();
    let%mutable y = (12345002 f) in ();
    List.iter (fun op ->
      operations <- op :: operations
    ) (fst x);
    List.iter (fun op ->
      operations <- op :: operations
    ) (fst y);
    let%mutable sum = ((snd x) + (snd y)) in ()

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {abcd = int; f = lambda intOrNat intOrNat; fff = option lambda nat nat; ggg = option intOrNat; value = nat}]
      ~storage:[%expr
                 {abcd = 0;
                  f = lambda(lambda intOrNat intOrNat);
                  fff = None;
                  ggg = Some(42);
                  value = 0}]
      [abs_test; comp_test; f; flambda; h; hh; i; operation_creation; operation_creation_result]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())