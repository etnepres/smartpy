import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(result = sp.TOption(sp.TLambda(sp.TUnit, sp.TUnit, with_operations=True))).layout("result"))
    self.init(result = sp.none)

  @sp.entry_point
  def exec_lambda(self, params):
    self.data.result = sp.some(params)

  @sp.entry_point
  def nothing(self):
    pass

sp.add_compilation_target("test", Contract())