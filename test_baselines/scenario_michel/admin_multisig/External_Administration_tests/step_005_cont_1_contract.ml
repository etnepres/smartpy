open SmartML

module Contract = struct
  let%entry_point administrate params =
    verify (sender = data.admin) ~msg:"NOT ADMIN";
    List.iter (fun action ->
      match action with
        | `changeActive changeActive ->
          data.active <- changeActive
        | `changeAdmin changeAdmin ->
          data.admin <- changeAdmin

    ) (open_some (unpack params (list (`changeActive bool + `changeAdmin address))) ~message:"Actions are invalid")

  let%entry_point verifyActive () =
    verify data.active ~msg:"NOT ACTIVE"

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {active = bool; admin = address}]
      ~storage:[%expr
                 {active = false;
                  admin = address "tz1UyQDepgtUBnWjyzzonqeDwaiWoQzRKSP5"}]
      [administrate; verifyActive]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())