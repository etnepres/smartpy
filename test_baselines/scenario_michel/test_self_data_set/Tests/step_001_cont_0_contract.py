import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(address = sp.TOption(sp.TInt)).layout("address"))

  @sp.entry_point
  def a(self, params):
    self.data = params

  @sp.entry_point
  def b(self, params):
    self.data.address = params

sp.add_compilation_target("test", Contract())