open SmartML

module Contract = struct
  let%entry_point compute params =
    transfer params (tez 0) (self_entry_point "run")

  let%entry_point reset () =
    data.counter <- 0

  let%entry_point run params =
    verify ((data.counter * params) <> 9);
    if params > 1 then
      (
        data.counter <- data.counter + 1;
        transfer (params - 1) (tez 0) (self_entry_point "run")
      )

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {counter = int}]
      ~storage:[%expr
                 {counter = 0}]
      [compute; reset; run]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())