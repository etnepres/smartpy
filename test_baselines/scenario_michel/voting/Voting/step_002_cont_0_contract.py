import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(votes = sp.TList(sp.TRecord(sender = sp.TAddress, vote = sp.TString).layout(("sender", "vote")))).layout("votes"))
    self.init(votes = [])

  @sp.entry_point
  def vote(self, params):
    sp.set_type(params.vote, sp.TString)
    self.data.votes.push(sp.record(sender = sp.sender, vote = params.vote))

sp.add_compilation_target("test", Contract())