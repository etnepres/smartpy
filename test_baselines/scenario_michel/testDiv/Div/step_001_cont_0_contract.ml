open SmartML

module Contract = struct
  let%entry_point test params =
    data.a <- ediv 1 0;
    data.b <- ediv (-1) 0;
    data.c <- ediv 1 12;
    data.d <- ediv (-1) 12;
    data.e <- ediv (-1) (-12);
    data.f <- ediv 15 12;
    data.g <- ediv (-15) 12;
    data.h <- ediv (-15) (-12);
    data.i <- ediv (tez 2) (mutez 100);
    data.j <- ediv (tez 2) (mutez 101);
    data.k <- ediv (tez 2) (tez 100);
    data.l <- ediv (tez 2) 15;
    data.m <- ediv amount (set_type_expr params nat)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {a = option (pair nat nat); b = option (pair int nat); c = option (pair nat nat); d = option (pair int nat); e = option (pair int nat); f = option (pair nat nat); g = option (pair int nat); h = option (pair int nat); i = option (pair nat mutez); j = option (pair nat mutez); k = option (pair nat mutez); l = option (pair mutez mutez); m = option (pair mutez mutez)}]
      ~storage:[%expr
                 {a = None;
                  b = None;
                  c = None;
                  d = None;
                  e = None;
                  f = None;
                  g = None;
                  h = None;
                  i = None;
                  j = None;
                  k = None;
                  l = None;
                  m = None}]
      [test]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())