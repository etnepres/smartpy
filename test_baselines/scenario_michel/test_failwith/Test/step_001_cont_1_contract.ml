open SmartML

module Contract = struct
  let%entry_point ep params =
    if params = 0 then
      failwith "zero"
    else
      failwith "non-zero"

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [ep]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())