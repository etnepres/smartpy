import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)
    self.init()

  @sp.entry_point
  def ep(self, params):
    sp.if params == 0:
      sp.failwith('zero')
    sp.else:
      sp.failwith('non-zero')

sp.add_compilation_target("test", Contract())