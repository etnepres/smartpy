open SmartML

module Contract = struct
  let%entry_point handle params =
    List.iter (fun operation ->
      let%mutable result = (open_some (sapling_verify_update data.ledger operation.transaction)) in ();
      data.ledger <- snd result;
      let%mutable amount = (fst result) in ();
      let%mutable amount_tez = (mutez abs amount) in ();
      if amount > 0 then
        transfer () amount_tez (implicit_account (open_some operation.key))
      else
        (
          verify (not (is_some operation.key));
          verify (amount = amount_tez)
        )
    ) params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {ledger = sapling_state 8}]
      ~storage:[%expr
                 {ledger = []}]
      [handle]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())