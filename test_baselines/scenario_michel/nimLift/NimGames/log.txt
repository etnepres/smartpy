Comment...
 h1: Nim games
Comment...
 h2: Contract
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 => test_baselines/scenario_michel/nimLift/NimGames/step_002_cont_0_pre_michelson.michel 348
 -> (Pair {} 0)
 => test_baselines/scenario_michel/nimLift/NimGames/step_002_cont_0_storage.tz 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_002_cont_0_storage.json 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_002_cont_0_sizes.csv 2
 => test_baselines/scenario_michel/nimLift/NimGames/step_002_cont_0_storage.py 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_002_cont_0_types.py 7
 => test_baselines/scenario_michel/nimLift/NimGames/step_002_cont_0_contract.tz 440
 => test_baselines/scenario_michel/nimLift/NimGames/step_002_cont_0_contract.json 412
 => test_baselines/scenario_michel/nimLift/NimGames/step_002_cont_0_contract.py 36
 => test_baselines/scenario_michel/nimLift/NimGames/step_002_cont_0_contract.ml 42
Comment...
 h2: First: define a few games with build
 => test_baselines/scenario_michel/nimLift/NimGames/step_004_cont_0_params.py 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_004_cont_0_params.tz 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_004_cont_0_params.json 1
Executing build(sp.record(bound = 2, size = 5))...
 -> (Pair {Elt 0 (Pair 2 (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 3; Elt 3 4; Elt 4 5} (Pair False (Pair 1 (Pair 5 0))))))} 1)
 => test_baselines/scenario_michel/nimLift/NimGames/step_005_cont_0_params.py 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_005_cont_0_params.tz 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_005_cont_0_params.json 1
Executing build(sp.record(bound = 2, size = 6))...
 -> (Pair {Elt 0 (Pair 2 (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 3; Elt 3 4; Elt 4 5} (Pair False (Pair 1 (Pair 5 0)))))); Elt 1 (Pair 2 (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 3; Elt 3 4; Elt 4 5; Elt 5 6} (Pair False (Pair 1 (Pair 6 0))))))} 2)
 => test_baselines/scenario_michel/nimLift/NimGames/step_006_cont_0_params.py 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_006_cont_0_params.tz 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_006_cont_0_params.json 1
Executing build(sp.record(bound = 2, size = 7))...
 -> (Pair {Elt 0 (Pair 2 (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 3; Elt 3 4; Elt 4 5} (Pair False (Pair 1 (Pair 5 0)))))); Elt 1 (Pair 2 (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 3; Elt 3 4; Elt 4 5; Elt 5 6} (Pair False (Pair 1 (Pair 6 0)))))); Elt 2 (Pair 2 (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 3; Elt 3 4; Elt 4 5; Elt 5 6; Elt 6 7} (Pair False (Pair 1 (Pair 7 0))))))} 3)
Comment...
 h2: Message execution
Comment...
 h3: A first move
 => test_baselines/scenario_michel/nimLift/NimGames/step_009_cont_0_params.py 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_009_cont_0_params.tz 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_009_cont_0_params.json 1
Executing remove(sp.record(cell = 2, gameId = 0, k = 1))...
 -> (Pair {Elt 0 (Pair 2 (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 2; Elt 3 4; Elt 4 5} (Pair False (Pair 2 (Pair 5 0)))))); Elt 1 (Pair 2 (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 3; Elt 3 4; Elt 4 5; Elt 5 6} (Pair False (Pair 1 (Pair 6 0)))))); Elt 2 (Pair 2 (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 3; Elt 3 4; Elt 4 5; Elt 5 6; Elt 6 7} (Pair False (Pair 1 (Pair 7 0))))))} 3)
Comment...
 h3: A second move
 => test_baselines/scenario_michel/nimLift/NimGames/step_011_cont_0_params.py 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_011_cont_0_params.tz 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_011_cont_0_params.json 1
Executing remove(sp.record(cell = 2, gameId = 0, k = 2))...
 -> (Pair {Elt 0 (Pair 2 (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 0; Elt 3 4; Elt 4 5} (Pair False (Pair 1 (Pair 5 0)))))); Elt 1 (Pair 2 (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 3; Elt 3 4; Elt 4 5; Elt 5 6} (Pair False (Pair 1 (Pair 6 0)))))); Elt 2 (Pair 2 (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 3; Elt 3 4; Elt 4 5; Elt 5 6; Elt 6 7} (Pair False (Pair 1 (Pair 7 0))))))} 3)
Comment...
 h3: An illegal move
 => test_baselines/scenario_michel/nimLift/NimGames/step_013_cont_0_params.py 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_013_cont_0_params.tz 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_013_cont_0_params.json 1
Executing remove(sp.record(cell = 2, gameId = 0, k = 1))...
 -> --- Expected failure in transaction --- Wrong condition: (params.k <= self.data.games[params.gameId].deck[params.cell] : sp.TBool) (python/templates/nimLift.py, line 44)
 (python/templates/nimLift.py, line 44)
Comment...
 h3: Another illegal move
 => test_baselines/scenario_michel/nimLift/NimGames/step_015_cont_0_params.py 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_015_cont_0_params.tz 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_015_cont_0_params.json 1
Executing claim(sp.record(gameId = 1))...
 -> --- Expected failure in transaction --- Wrong condition: (sp.sum(self.data.games[params.gameId].deck.values()) == 0 : sp.TBool) (python/templates/nimLift.py, line 52)
 (python/templates/nimLift.py, line 52)
Comment...
 h3: A third move
 => test_baselines/scenario_michel/nimLift/NimGames/step_017_cont_0_params.py 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_017_cont_0_params.tz 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_017_cont_0_params.json 1
Executing remove(sp.record(cell = 1, gameId = 0, k = 2))...
 -> (Pair {Elt 0 (Pair 2 (Pair False (Pair {Elt 0 1; Elt 1 0; Elt 2 0; Elt 3 4; Elt 4 5} (Pair False (Pair 2 (Pair 5 0)))))); Elt 1 (Pair 2 (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 3; Elt 3 4; Elt 4 5; Elt 5 6} (Pair False (Pair 1 (Pair 6 0)))))); Elt 2 (Pair 2 (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 3; Elt 3 4; Elt 4 5; Elt 5 6; Elt 6 7} (Pair False (Pair 1 (Pair 7 0))))))} 3)
Comment...
 h3: More moves
 => test_baselines/scenario_michel/nimLift/NimGames/step_019_cont_0_params.py 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_019_cont_0_params.tz 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_019_cont_0_params.json 1
Executing remove(sp.record(cell = 0, gameId = 0, k = 1))...
 -> (Pair {Elt 0 (Pair 2 (Pair False (Pair {Elt 0 0; Elt 1 0; Elt 2 0; Elt 3 4; Elt 4 5} (Pair False (Pair 1 (Pair 5 0)))))); Elt 1 (Pair 2 (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 3; Elt 3 4; Elt 4 5; Elt 5 6} (Pair False (Pair 1 (Pair 6 0)))))); Elt 2 (Pair 2 (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 3; Elt 3 4; Elt 4 5; Elt 5 6; Elt 6 7} (Pair False (Pair 1 (Pair 7 0))))))} 3)
 => test_baselines/scenario_michel/nimLift/NimGames/step_020_cont_0_params.py 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_020_cont_0_params.tz 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_020_cont_0_params.json 1
Executing remove(sp.record(cell = 3, gameId = 0, k = 1))...
 -> (Pair {Elt 0 (Pair 2 (Pair False (Pair {Elt 0 0; Elt 1 0; Elt 2 0; Elt 3 3; Elt 4 5} (Pair False (Pair 2 (Pair 5 0)))))); Elt 1 (Pair 2 (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 3; Elt 3 4; Elt 4 5; Elt 5 6} (Pair False (Pair 1 (Pair 6 0)))))); Elt 2 (Pair 2 (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 3; Elt 3 4; Elt 4 5; Elt 5 6; Elt 6 7} (Pair False (Pair 1 (Pair 7 0))))))} 3)
 => test_baselines/scenario_michel/nimLift/NimGames/step_021_cont_0_params.py 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_021_cont_0_params.tz 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_021_cont_0_params.json 1
Executing remove(sp.record(cell = 3, gameId = 0, k = 1))...
 -> (Pair {Elt 0 (Pair 2 (Pair False (Pair {Elt 0 0; Elt 1 0; Elt 2 0; Elt 3 2; Elt 4 5} (Pair False (Pair 1 (Pair 5 0)))))); Elt 1 (Pair 2 (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 3; Elt 3 4; Elt 4 5; Elt 5 6} (Pair False (Pair 1 (Pair 6 0)))))); Elt 2 (Pair 2 (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 3; Elt 3 4; Elt 4 5; Elt 5 6; Elt 6 7} (Pair False (Pair 1 (Pair 7 0))))))} 3)
 => test_baselines/scenario_michel/nimLift/NimGames/step_022_cont_0_params.py 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_022_cont_0_params.tz 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_022_cont_0_params.json 1
Executing remove(sp.record(cell = 3, gameId = 0, k = 2))...
 -> (Pair {Elt 0 (Pair 2 (Pair False (Pair {Elt 0 0; Elt 1 0; Elt 2 0; Elt 3 0; Elt 4 5} (Pair False (Pair 2 (Pair 5 0)))))); Elt 1 (Pair 2 (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 3; Elt 3 4; Elt 4 5; Elt 5 6} (Pair False (Pair 1 (Pair 6 0)))))); Elt 2 (Pair 2 (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 3; Elt 3 4; Elt 4 5; Elt 5 6; Elt 6 7} (Pair False (Pair 1 (Pair 7 0))))))} 3)
 => test_baselines/scenario_michel/nimLift/NimGames/step_023_cont_0_params.py 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_023_cont_0_params.tz 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_023_cont_0_params.json 1
Executing remove(sp.record(cell = 4, gameId = 0, k = 1))...
 -> (Pair {Elt 0 (Pair 2 (Pair False (Pair {Elt 0 0; Elt 1 0; Elt 2 0; Elt 3 0; Elt 4 4} (Pair False (Pair 1 (Pair 5 0)))))); Elt 1 (Pair 2 (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 3; Elt 3 4; Elt 4 5; Elt 5 6} (Pair False (Pair 1 (Pair 6 0)))))); Elt 2 (Pair 2 (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 3; Elt 3 4; Elt 4 5; Elt 5 6; Elt 6 7} (Pair False (Pair 1 (Pair 7 0))))))} 3)
 => test_baselines/scenario_michel/nimLift/NimGames/step_024_cont_0_params.py 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_024_cont_0_params.tz 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_024_cont_0_params.json 1
Executing remove(sp.record(cell = 4, gameId = 0, k = 2))...
 -> (Pair {Elt 0 (Pair 2 (Pair False (Pair {Elt 0 0; Elt 1 0; Elt 2 0; Elt 3 0; Elt 4 2} (Pair False (Pair 2 (Pair 5 0)))))); Elt 1 (Pair 2 (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 3; Elt 3 4; Elt 4 5; Elt 5 6} (Pair False (Pair 1 (Pair 6 0)))))); Elt 2 (Pair 2 (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 3; Elt 3 4; Elt 4 5; Elt 5 6; Elt 6 7} (Pair False (Pair 1 (Pair 7 0))))))} 3)
Comment...
 h3: A failed attempt to claim
 => test_baselines/scenario_michel/nimLift/NimGames/step_026_cont_0_params.py 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_026_cont_0_params.tz 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_026_cont_0_params.json 1
Executing claim(sp.record(gameId = 0))...
 -> --- Expected failure in transaction --- Wrong condition: (sp.sum(self.data.games[params.gameId].deck.values()) == 0 : sp.TBool) (python/templates/nimLift.py, line 52)
 (python/templates/nimLift.py, line 52)
Comment...
 h3: A last removal
 => test_baselines/scenario_michel/nimLift/NimGames/step_028_cont_0_params.py 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_028_cont_0_params.tz 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_028_cont_0_params.json 1
Executing remove(sp.record(cell = 4, gameId = 0, k = 2))...
 -> (Pair {Elt 0 (Pair 2 (Pair False (Pair {Elt 0 0; Elt 1 0; Elt 2 0; Elt 3 0; Elt 4 0} (Pair False (Pair 1 (Pair 5 0)))))); Elt 1 (Pair 2 (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 3; Elt 3 4; Elt 4 5; Elt 5 6} (Pair False (Pair 1 (Pair 6 0)))))); Elt 2 (Pair 2 (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 3; Elt 3 4; Elt 4 5; Elt 5 6; Elt 6 7} (Pair False (Pair 1 (Pair 7 0))))))} 3)
Comment...
 h3: And a final claim
 => test_baselines/scenario_michel/nimLift/NimGames/step_030_cont_0_params.py 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_030_cont_0_params.tz 1
 => test_baselines/scenario_michel/nimLift/NimGames/step_030_cont_0_params.json 1
Executing claim(sp.record(gameId = 0))...
 -> (Pair {Elt 0 (Pair 2 (Pair True (Pair {Elt 0 0; Elt 1 0; Elt 2 0; Elt 3 0; Elt 4 0} (Pair False (Pair 1 (Pair 5 1)))))); Elt 1 (Pair 2 (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 3; Elt 3 4; Elt 4 5; Elt 5 6} (Pair False (Pair 1 (Pair 6 0)))))); Elt 2 (Pair 2 (Pair False (Pair {Elt 0 1; Elt 1 2; Elt 2 3; Elt 3 4; Elt 4 5; Elt 5 6; Elt 6 7} (Pair False (Pair 1 (Pair 7 0))))))} 3)
