open SmartML

module Contract = struct
  let%entry_point test_pair () =
    data.x <- convert (5, 6);
    verify (data.x = {a = 5; b = 6})

  let%entry_point test_record () =
    data.x <- convert (set_type_expr {c = 3; d = 4} {c = int; d = int});
    verify (data.x = {a = 3; b = 4})

  let%entry_point test_variant () =
    data.y <- convert (set_type_expr (variant C 43) (`C int + `D int));
    verify (data.y = (variant A 43))

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x = {a = int; b = int}; y = `A int + `B int}]
      ~storage:[%expr
                 {x = {a = 1; b = 2};
                  y = A(42)}]
      [test_pair; test_record; test_variant]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())