import smartpy as sp

cl = sp.io.import_template("chess_logic.py")

class Chess:
    def __init__(self):
        self.name = "Chess"
        self.t_initial_config  = sp.TUnit
        self.t_game_state = sp.TRecord(
            board_state = cl.Types.t_board_state,
            status = sp.TVariant(
                play = sp.TUnit,
                force_play = sp.TUnit,
                claim_stalemate = sp.TUnit
            )
        )
        self.t_move_data  = sp.TVariant(
            play = cl.Types.t_move,
            claim_stalemate = sp.TUnit,
            answer_stalemate = sp.TVariant(
                accept = sp.TUnit,
                answer = cl.Types.t_move,
            ),
        )
        self.t_outcome    = ["player_1_won", "player_2_won", "draw"]

    def init(self, params):
        sp.set_type(params, self.t_initial_config)
        sp.result(sp.set_type_expr(sp.record(
            board_state = cl.initial_board_state(),
            status = sp.variant("play", sp.unit)
        ), self.t_game_state))

    # Standard
    def apply_(self, move_data, move_nb, player, state):
        move_piece = sp.build_lambda(self.move_piece)
        get_movable_to = sp.build_lambda(self.get_movable_to)

        with move_data.match_cases() as arg:
            with arg.match("play") as move:
                sp.verify(
                    state.status.is_variant("play") |
                    state.status.is_variant("force_play"),
                    message = "Opponent claimed stalemate, you must answer it"
                )
                sp.result(self.play(move, state.board_state, move_piece, get_movable_to))
            with arg.match("claim_stalemate"):
                sp.verify(state.status.is_variant("play"))
                new_state = sp.record(board_state = state.board_state, status = sp.variant("claim_stalemate", sp.unit))
                sp.result(self.claim_stalemate(state))
            with arg.match("answer_stalemate") as answer_stalemate:
                sp.verify(state.status.is_variant("claim_stalemate"))
                sp.result(self.answer_stalemate(answer_stalemate, state, move_piece, get_movable_to))

    # Entry points

    def play(self, move, board_state, move_piece, get_movable_to):
        board_state, is_draw = sp.match_pair(
            move_piece(
                sp.record(
                    board_state = board_state,
                    get_movable_to = get_movable_to,
                    move = move,
                )
            )
        )
        state = sp.compute(sp.record(board_state = board_state, status = sp.variant("play", sp.unit)))
        state.board_state.nextPlayer *= -1
        outcome = sp.local("outcome", sp.none)
        with sp.if_(is_draw):
            outcome.value = sp.some(sp.bounded("draw"))
        with sp.else_():
            with sp.if_(cl.is_checkmate(board_state, get_movable_to)):
                outcome.value = sp.eif(board_state.nextPlayer == 1, sp.some(sp.bounded("player_2_won")), sp.some(sp.bounded("player_1_won")))

        return (state, outcome.value)

    def claim_stalemate(self, state):
        return (
            sp.record(board_state = state.board_state, status = sp.variant("claim_stalemate", sp.unit)),
            sp.none
        )

    def answer_stalemate(self, answer, state, move_piece, get_movable_to):
        outcome = sp.local("outcome", sp.none)
        state = sp.local("state", state).value

        with answer.match_cases() as arg:
            with arg.match("accept"):
                outcome.value = sp.some(sp.bounded("draw"))
            with arg.match("answer") as refuse_move:
                state.board_state.nextPlayer *= -1
                # Verify if the refuse move is valid
                move_piece(sp.record(move = refuse_move, get_movable_to = get_movable_to))
                state.board_state.nextPlayer *= -1
                state.status = sp.variant("force_play", sp.unit)

        return (state, outcome.value)

    # Lambdas

    def get_movable_to(self, params):
        sp.result(cl.Lambda_ready.get_movable_to(params))

    def move_piece(self, params):
        board_state, is_draw = cl.Lambda_ready.move_piece(params.board_state, params.move, params.get_movable_to)
        sp.result((board_state, is_draw))

# Tests
if "templates" not in __name__:
    GameTester = sp.io.import_template("state_channel_games/game_tester.py").GameTester

    player1 = sp.test_account('player1')
    player2 = sp.test_account('player2')
    p1 = sp.record(addr = player1.address, pk = player1.public_key)
    p2 = sp.record(addr = player2.address, pk = player2.public_key)

    class GameTesterChess(GameTester):
        @sp.offchain_view(pure = True)
        def build_fen(self):
            sp.result(
                cl.Lambda_ready.build_fen(self.data.state.board_state)
            )

    def play(f, t, promotion = sp.none, claim_repeat = sp.none):
        return sp.variant("play", sp.record(f = f, t = t, promotion = promotion, claim_repeat = claim_repeat))

    @sp.add_test(name="Chess")
    def test():
        scenario = sp.test_scenario()
        c1 = GameTesterChess(Chess(), p1, p2)
        scenario += c1
        c1.play(move_data = play(f = sp.record(i = 1, j = 4), t = sp.record(i = 3, j = 4)), move_nb = 0).run(sender = player1)
        c1.play(move_data = play(f = sp.record(i = 6, j = 4), t = sp.record(i = 4, j = 4)), move_nb = 1).run(sender = player2)
        c1.play(move_data = play(f = sp.record(i = 0, j = 6), t = sp.record(i = 2, j = 5)), move_nb = 2).run(sender = player1)
        c1.play(move_data = play(f = sp.record(i = 7, j = 1), t = sp.record(i = 5, j = 2)), move_nb = 3).run(sender = player2)
        c1.play(move_data = play(f = sp.record(i = 0, j = 5), t = sp.record(i = 4, j = 1)), move_nb = 4).run(sender = player1)
        c1.play(move_data = play(f = sp.record(i = 6, j = 0), t = sp.record(i = 5, j = 0)), move_nb = 5).run(sender = player2)
        c1.play(move_data = play(f = sp.record(i = 4, j = 1), t = sp.record(i = 5, j = 2)), move_nb = 6).run(sender = player1)
        c1.play(move_data = play(f = sp.record(i = 6, j = 3), t = sp.record(i = 5, j = 2)), move_nb = 7).run(sender = player2)
        c1.play(move_data = play(f = sp.record(i = 0, j = 4), t = sp.record(i = 0, j = 6)), move_nb = 8).run(sender = player1)
        scenario.show(c1.build_fen())

    @sp.add_test(name = "Chess - Promotion")
    def test():
        scenario = sp.test_scenario()
        c1 = GameTesterChess(Chess(), p1, p2)
        scenario += c1
        c1.play(move_data = play(f = sp.record(i = 1, j = 6), t = sp.record(i = 3, j = 6)), move_nb = 0).run(sender = player1)
        c1.play(move_data = play(f = sp.record(i = 6, j = 1), t = sp.record(i = 4, j = 1)), move_nb = 1).run(sender = player2)
        c1.play(move_data = play(f = sp.record(i = 3, j = 6), t = sp.record(i = 4, j = 6)), move_nb = 2).run(sender = player1)
        c1.play(move_data = play(f = sp.record(i = 4, j = 1), t = sp.record(i = 3, j = 1)), move_nb = 3).run(sender = player2)
        c1.play(move_data = play(f = sp.record(i = 4, j = 6), t = sp.record(i = 5, j = 6)), move_nb = 4).run(sender = player1)
        c1.play(move_data = play(f = sp.record(i = 3, j = 1), t = sp.record(i = 2, j = 1)), move_nb = 5).run(sender = player2)
        c1.play(move_data = play(f = sp.record(i = 5, j = 6), t = sp.record(i = 6, j = 7)), move_nb = 6).run(sender = player1)
        c1.play(move_data = play(f = sp.record(i = 2, j = 1), t = sp.record(i = 1, j = 0)), move_nb = 7).run(sender = player2)
        c1.play(move_data = play(f = sp.record(i = 6, j = 7), t = sp.record(i = 7, j = 6), promotion = sp.some(5)), move_nb = 8).run(sender = player1)
        c1.play(move_data = play(f = sp.record(i = 1, j = 0), t = sp.record(i = 0, j = 1), promotion = sp.some(3)), move_nb = 9).run(sender = player2)
        scenario.show(c1.build_fen())
        scenario.verify(c1.build_fen() == 'rnbqkbQr/p1ppppp1/8/8/8/8/1PPPPP1P/RnBQKBNR w KQkq - 0 6')