import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TBool, y = sp.TBool).layout(("x", "y")))
    self.init(x = False,
              y = False)

  @sp.entry_point
  def ep(self, params):
    sp.verify((params <= sp.address('KT1XvNYseNDJJ6Kw27qhSEDF8ys8JhDopzfG')) & (params >= sp.address('KT18amZmM5W7qDWVt2pH6uj7sCEd3kbzLrHT')), 'Not KT1')
    self.data.x = params < sp.address('KT1XvNYseNDJJ6Kw27qhSEDF8ys8JhDopzfG')
    self.data.y = params >= sp.address('KT18amZmM5W7qDWVt2pH6uj7sCEd3kbzLrHT')

  @sp.entry_point
  def test(self, params):
    sp.verify((sp.slice(sp.pack(sp.set_type_expr(sp.fst(params), sp.TAddress)), 6, 22)) == (sp.slice(sp.pack(sp.set_type_expr(sp.snd(params), sp.TAddress)), 6, 22)))

sp.add_compilation_target("test", Contract())