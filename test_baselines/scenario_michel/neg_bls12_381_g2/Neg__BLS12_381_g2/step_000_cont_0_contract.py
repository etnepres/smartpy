import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(g2 = sp.TOption(sp.TBls12_381_g2)).layout("g2"))
    self.init(g2 = sp.none)

  @sp.entry_point
  def negate(self, params):
    sp.set_type(params.g2, sp.TBls12_381_g2)
    self.data.g2 = sp.some(- params.g2)

sp.add_compilation_target("test", Contract())