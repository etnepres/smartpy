import smartpy as sp

tstorage = sp.TRecord(active = sp.TBool, admin = sp.TAddress, value = sp.TOption(sp.TInt)).layout(("active", ("admin", "value")))
tparameter = sp.TVariant(administrate = sp.TList(sp.TVariant(setActive = sp.TBool, setAdmin = sp.TAddress).layout(("setActive", "setAdmin"))), setValue = sp.TOption(sp.TInt)).layout(("administrate", "setValue"))
tprivates = { }
tviews = { }
