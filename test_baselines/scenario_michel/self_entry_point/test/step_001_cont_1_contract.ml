open SmartML

module Contract = struct
  let%entry_point ep3 params =
    set_type params bool;
    set_type (self_entry_point "ep4") (contract timestamp);
    transfer (self_entry_point "ep4") (tez 0) (open_some (contract (contract timestamp) data.a , entry_point='ep2'))

  let%entry_point ep4 params =
    set_type params timestamp

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {a = address}]
      ~storage:[%expr
                 {a = address "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1"}]
      [ep3; ep4]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())