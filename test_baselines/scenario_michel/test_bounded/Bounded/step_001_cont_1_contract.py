import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TBounded(['abc', 'def', 'ghi', 'jkl'], t=sp.TString)).layout("x"))
    self.init(x = 'abc')

  @sp.entry_point
  def ep(self):
    self.data.x = 'ghi'

  @sp.entry_point
  def ep2(self, params):
    sp.set_type(params, sp.TBounded(['abc', 'def', 'ghi', 'jkl'], t=sp.TString))
    self.data.x = params

sp.add_compilation_target("test", Contract())