open SmartML

module Contract = struct
  let%entry_point ep params =
    set_type params bounded(["abc", "def"], t=string)

  let%entry_point ep2 () =
    ()

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [ep; ep2]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())