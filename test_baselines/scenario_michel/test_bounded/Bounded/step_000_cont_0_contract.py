import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TBounded(['abc', 'def'], t=sp.TString)).layout("x"))
    self.init(x = 'abc')

  @sp.entry_point
  def ep(self):
    self.data.x = 'abc'

sp.add_compilation_target("test", Contract())