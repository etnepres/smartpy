import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(z = sp.TNat).layout("z"))
    self.init(z = 42)

sp.add_compilation_target("test", Contract())