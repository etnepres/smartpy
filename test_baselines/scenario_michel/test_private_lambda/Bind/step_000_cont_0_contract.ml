open SmartML

module Contract = struct

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x = intOrNat; y = intOrNat}]
      ~storage:[%expr
                 {x = 12;
                  y = 0}]
      []
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())