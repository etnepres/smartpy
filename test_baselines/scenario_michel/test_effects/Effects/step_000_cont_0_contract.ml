open SmartML

module Contract = struct
  let%entry_point sub_double () =
    data.a <- 2 * data.a

  let%entry_point sub_incr () =
    data.a <- data.a + 1

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {a = nat}]
      ~storage:[%expr
                 {a = 0}]
      [sub_double; sub_incr]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())