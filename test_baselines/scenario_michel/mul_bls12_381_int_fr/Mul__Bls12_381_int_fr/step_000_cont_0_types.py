import smartpy as sp

tstorage = sp.TRecord(fr = sp.TOption(sp.TBls12_381_fr)).layout("fr")
tparameter = sp.TVariant(mul_int_fr = sp.TPair(sp.TIntOrNat, sp.TBls12_381_fr), mul_nat_fr = sp.TPair(sp.TNat, sp.TBls12_381_fr)).layout(("mul_int_fr", "mul_nat_fr"))
tprivates = { }
tviews = { }
