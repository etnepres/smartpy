open SmartML

module Contract = struct
  let%entry_point toInt params =
    data.fr <- some (to_int params)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {fr = option int}]
      ~storage:[%expr
                 {fr = None}]
      [toInt]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())