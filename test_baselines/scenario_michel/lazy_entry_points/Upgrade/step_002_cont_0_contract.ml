open SmartML

module Contract = struct
  let%entry_point bounce params =
    set_type params address

  let%entry_point check_bounce () =
    verify (has_entry_point "bounce")

  let%entry_point modify_x params =
    data.x <- params

  let%entry_point take_ticket params =
    set_type params (ticket int)

  let%entry_point update_bounce params =
    set_entry_point("bounce", params);
    send sender (tez 1)

  let%entry_point update_modify_x params =
    set_entry_point("modify_x", params)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x = int; y = bool}]
      ~storage:[%expr
                 {x = 1;
                  y = true}]
      [bounce; check_bounce; modify_x; take_ticket; update_bounce; update_modify_x]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())