Table Of Contents

 Upgradable and Lazy Entry Points
# Use initial entry point
 Updating an entry point
# Switch it to incrementing
# Switch it to decrementing
# Switch it to decrementing with additional transfers
 Testing the presence of an entry point
# Entry point is still absent (lazy_no_code)
# Add entry point and call it
Comment...
 h1: Upgradable and Lazy Entry Points
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_002_cont_0_pre_michelson.michel 90
 -> (Pair (Pair 1 True) {Elt 1 { UNPAIR; IF_LEFT { PUSH int -1; FAILWITH } {}; UPDATE 1; NIL operation; PAIR }})
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_002_cont_0_storage.tz 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_002_cont_0_storage.json 21
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_002_cont_0_sizes.csv 2
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_002_cont_0_storage.py 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_002_cont_0_types.py 7
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_002_cont_0_contract.tz 107
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_002_cont_0_contract.json 180
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_002_cont_0_contract.py 34
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_002_cont_0_contract.ml 36
Comment...
 h2: Use initial entry point
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_004_cont_0_params.py 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_004_cont_0_params.tz 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_004_cont_0_params.json 1
Executing modify_x(1)...
 -> (Pair 1 True)
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_005_cont_0_params.py 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_005_cont_0_params.tz 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_005_cont_0_params.json 1
Executing modify_x(2)...
 -> (Pair 2 True)
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_006_cont_0_params.py 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_006_cont_0_params.tz 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_006_cont_0_params.json 1
Executing modify_x(3)...
 -> (Pair 3 True)
Verifying sp.contract_data(0).x == 3...
 OK
Comment...
 h1: Updating an entry point
Comment...
 h2: Switch it to incrementing
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_010_cont_0_params.py 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_010_cont_0_params.tz 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_010_cont_0_params.json 14
Executing update_modify_x(sp.build_lambda(f_x0))...
 -> (Pair 3 True)
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_011_cont_0_params.py 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_011_cont_0_params.tz 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_011_cont_0_params.json 1
Executing modify_x(1)...
 -> (Pair 4 True)
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_012_cont_0_params.py 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_012_cont_0_params.tz 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_012_cont_0_params.json 1
Executing modify_x(2)...
 -> (Pair 6 True)
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_013_cont_0_params.py 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_013_cont_0_params.tz 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_013_cont_0_params.json 1
Executing modify_x(3)...
 -> (Pair 9 True)
Verifying sp.contract_data(0).x == 9...
 OK
Comment...
 h2: Switch it to decrementing
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_016_cont_0_params.py 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_016_cont_0_params.tz 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_016_cont_0_params.json 15
Executing update_modify_x(sp.build_lambda(f_x1))...
 -> (Pair 9 True)
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_017_cont_0_params.py 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_017_cont_0_params.tz 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_017_cont_0_params.json 1
Executing modify_x(1)...
 -> (Pair 8 True)
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_018_cont_0_params.py 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_018_cont_0_params.tz 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_018_cont_0_params.json 1
Executing modify_x(2)...
 -> (Pair 6 True)
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_019_cont_0_params.py 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_019_cont_0_params.tz 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_019_cont_0_params.json 1
Executing modify_x(3)...
 -> (Pair 3 True)
Verifying sp.contract_data(0).x == 3...
 OK
Comment...
 h2: Switch it to decrementing with additional transfers
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_022_cont_0_params.py 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_022_cont_0_params.tz 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_022_cont_0_params.json 11
Executing update_modify_x(sp.build_lambda(f_x2))...
 -> (Pair 3 True)
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_023_cont_0_params.py 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_023_cont_0_params.tz 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_023_cont_0_params.json 1
Executing modify_x(1)...
 -> (Pair 2 True)
  + Transfer
     params: sp.unit
     amount: sp.tez(1)
     to:     sp.contract(sp.TUnit, sp.address('tz0Fakeme')).open_some()
  + Transfer
     params: sp.unit
     amount: sp.tez(2)
     to:     sp.contract(sp.TUnit, sp.address('tz0Fakeme')).open_some()
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_024_cont_0_params.py 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_024_cont_0_params.tz 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_024_cont_0_params.json 1
Executing modify_x(2)...
 -> (Pair 0 True)
  + Transfer
     params: sp.unit
     amount: sp.tez(1)
     to:     sp.contract(sp.TUnit, sp.address('tz0Fakeme')).open_some()
  + Transfer
     params: sp.unit
     amount: sp.tez(2)
     to:     sp.contract(sp.TUnit, sp.address('tz0Fakeme')).open_some()
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_025_cont_0_params.py 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_025_cont_0_params.tz 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_025_cont_0_params.json 1
Executing modify_x(3)...
 -> (Pair -3 True)
  + Transfer
     params: sp.unit
     amount: sp.tez(1)
     to:     sp.contract(sp.TUnit, sp.address('tz0Fakeme')).open_some()
  + Transfer
     params: sp.unit
     amount: sp.tez(2)
     to:     sp.contract(sp.TUnit, sp.address('tz0Fakeme')).open_some()
Verifying sp.contract_data(0).x == (-3)...
 OK
Comment...
 h1: Testing the presence of an entry point
Comment...
 h2: Entry point is still absent (lazy_no_code)
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_029_cont_0_params.py 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_029_cont_0_params.tz 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_029_cont_0_params.json 1
Executing check_bounce(sp.record())...
 -> --- Expected failure in transaction --- Wrong condition: (sp.has_entry_point("bounce") : sp.TBool) (python/templates/lazy_entry_points.py, line 31)
 (python/templates/lazy_entry_points.py, line 31)
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_030_cont_0_params.py 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_030_cont_0_params.tz 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_030_cont_0_params.json 1
Executing bounce(sp.resolve(sp.test_account("Alice").address))...
 -> --- Expected failure in transaction --- lazy entry point not found
Comment...
 h2: Add entry point and call it
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_032_cont_0_params.py 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_032_cont_0_params.tz 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_032_cont_0_params.json 15
Executing update_bounce(sp.build_lambda(f_x3))...
 -> (Pair -3 True)
  + Transfer
     params: sp.unit
     amount: sp.tez(1)
     to:     sp.contract(sp.TUnit, sp.address('tz0FakeBob')).open_some()
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_033_cont_0_params.py 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_033_cont_0_params.tz 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_033_cont_0_params.json 1
Executing bounce(sp.resolve(sp.test_account("Alice").address))...
 -> (Pair -3 True)
  + Transfer
     params: sp.unit
     amount: sp.mutez(2)
     to:     sp.contract(sp.TUnit, sp.address('tz0FakeAlice')).open_some()
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_034_cont_0_params.py 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_034_cont_0_params.tz 1
 => test_baselines/scenario_michel/lazy_entry_points/Upgrade/step_034_cont_0_params.json 1
Executing check_bounce(sp.record())...
 -> (Pair -3 True)
Table Of Contents

 Upgradable and Lazy Entry Points
# Use initial entry point
 Updating an entry point
# Switch it to incrementing
# Switch it to decrementing
# Switch it to decrementing with additional transfers
 Testing the presence of an entry point
# Entry point is still absent (lazy_no_code)
# Add entry point and call it
