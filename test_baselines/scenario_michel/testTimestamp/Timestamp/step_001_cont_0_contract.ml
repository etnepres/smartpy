open SmartML

module Contract = struct
  let%entry_point ep () =
    data.out <- now > (add_seconds now 1);
    verify (((add_seconds now 12) - now) = 12);
    verify ((now - (add_seconds now 12)) = (-12));
    verify ((now - (add_seconds now 12)) = (-12));
    data.next <- add_seconds now 86400

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {next = timestamp; out = bool}]
      ~storage:[%expr
                 {next = timestamp 0;
                  out = false}]
      [ep]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())