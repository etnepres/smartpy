import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)
    self.init()

  @sp.entry_point
  def pack_lambda_record(self):
    sp.verify(sp.unpack(sp.pack(sp.build_lambda(lambda _x6: sp.record(a = 1, b = _x6 + 2))), sp.TLambda(sp.TInt, sp.TRecord(a = sp.TInt, b = sp.TInt).layout(("a", "b")))).open_some()(100).a == 1)
    sp.verify(sp.unpack(sp.pack(sp.build_lambda(lambda _x7: sp.record(a = _x7 + 1))), sp.TLambda(sp.TInt, sp.TRecord(a = sp.TInt).layout("a"))).open_some()(100).a == 101)

  @sp.entry_point
  def pack_lambda_variant(self):
    sp.verify(sp.unpack(sp.pack(sp.build_lambda(lambda _x8: sp.set_type_expr(variant('a', _x8 + 1), sp.TVariant(a = sp.TInt, b = sp.TInt).layout(("a", "b"))))), sp.TLambda(sp.TInt, sp.TVariant(a = sp.TInt, b = sp.TInt).layout(("a", "b")))).open_some()(100).open_variant('a') == 101)
    sp.verify(sp.unpack(sp.pack(sp.build_lambda(lambda _x9: variant('a', _x9 + 1))), sp.TLambda(sp.TInt, sp.TVariant(a = sp.TInt).layout("a"))).open_some()(100).open_variant('a') == 101)

  @sp.entry_point
  def pack_lambdas(self):
    def f_x10(_x10):
      y = sp.local("y", 0)
      r = sp.local("r", 'A')
      sp.if _x10 == 0:
        sp.if _x10 == y.value:
          r.value = 'B'
      sp.result(r.value)
    f1 = sp.local("f1", sp.build_lambda(f_x10), sp.TLambda(sp.TInt, sp.TString))
    p1 = sp.local("p1", sp.pack(f1.value))
    f2 = sp.local("f2", sp.unpack(p1.value, sp.TLambda(sp.TInt, sp.TString)).open_some(), sp.TLambda(sp.TInt, sp.TString))
    p2 = sp.local("p2", sp.pack(f2.value))
    sp.verify(p1.value == p2.value)
    sp.verify(f1.value(0) == f2.value(0))
    sp.verify(f1.value(1) == f2.value(1))
    sp.verify(f1.value(2) == f2.value(2))

  @sp.entry_point
  def pack_lambdas2(self):
    f1 = sp.local("f1", sp.build_lambda(lambda _x11: _x11 > 0), sp.TLambda(sp.TInt, sp.TBool))
    p1 = sp.local("p1", sp.pack(f1.value))
    f2 = sp.local("f2", sp.unpack(p1.value, sp.TLambda(sp.TInt, sp.TBool)).open_some(), sp.TLambda(sp.TInt, sp.TBool))
    sp.verify(f2.value(1))

  @sp.entry_point
  def run(self):
    sp.verify(sp.pack(-3) == sp.bytes('0x050043'))
    sp.verify(sp.pack(sp.set_type_expr(-3, sp.TInt)) == sp.pack(sp.set_type_expr(-3, sp.TInt)))
    sp.verify(sp.pack(-10) == sp.bytes('0x05004a'))
    sp.verify(sp.pack(sp.set_type_expr(-10, sp.TInt)) == sp.pack(sp.set_type_expr(-10, sp.TInt)))
    sp.verify(sp.pack(-10000) == sp.bytes('0x0500d09c01'))
    sp.verify(sp.pack(sp.set_type_expr(-10000, sp.TInt)) == sp.pack(sp.set_type_expr(-10000, sp.TInt)))
    sp.verify(sp.pack(-1000000) == sp.bytes('0x0500c0897a'))
    sp.verify(sp.pack(sp.set_type_expr(-1000000, sp.TInt)) == sp.pack(sp.set_type_expr(-1000000, sp.TInt)))
    sp.verify(sp.pack(-1000000000000) == sp.bytes('0x0500c0c0a8ca9a3a'))
    sp.verify(sp.pack(sp.set_type_expr(-1000000000000, sp.TInt)) == sp.pack(sp.set_type_expr(-1000000000000, sp.TInt)))
    sp.verify(sp.pack('') == sp.bytes('0x050100000000'))
    sp.verify(sp.pack(sp.set_type_expr('', sp.TString)) == sp.pack(sp.set_type_expr('', sp.TString)))
    sp.verify(sp.pack('a') == sp.bytes('0x05010000000161'))
    sp.verify(sp.pack(sp.set_type_expr('a', sp.TString)) == sp.pack(sp.set_type_expr('a', sp.TString)))
    sp.verify(sp.pack('ab') == sp.bytes('0x0501000000026162'))
    sp.verify(sp.pack(sp.set_type_expr('ab', sp.TString)) == sp.pack(sp.set_type_expr('ab', sp.TString)))
    sp.verify(sp.pack('abc') == sp.bytes('0x050100000003616263'))
    sp.verify(sp.pack(sp.set_type_expr('abc', sp.TString)) == sp.pack(sp.set_type_expr('abc', sp.TString)))
    sp.verify(sp.pack(sp.bytes('0x')) == sp.bytes('0x050a00000000'))
    sp.verify(sp.pack(sp.set_type_expr(sp.bytes('0x'), sp.TBytes)) == sp.pack(sp.set_type_expr(sp.bytes('0x'), sp.TBytes)))
    sp.verify(sp.pack(sp.bytes('0x00')) == sp.bytes('0x050a0000000100'))
    sp.verify(sp.pack(sp.set_type_expr(sp.bytes('0x00'), sp.TBytes)) == sp.pack(sp.set_type_expr(sp.bytes('0x00'), sp.TBytes)))
    sp.verify(sp.pack(sp.bytes('0xab00')) == sp.bytes('0x050a00000002ab00'))
    sp.verify(sp.pack(sp.set_type_expr(sp.bytes('0xab00'), sp.TBytes)) == sp.pack(sp.set_type_expr(sp.bytes('0xab00'), sp.TBytes)))
    sp.verify(sp.pack((1, 2)) == sp.bytes('0x05070700010002'))
    sp.verify(sp.pack(sp.set_type_expr((1, 2), sp.TPair(sp.TIntOrNat, sp.TIntOrNat))) == sp.pack(sp.set_type_expr((1, 2), sp.TPair(sp.TIntOrNat, sp.TIntOrNat))))
    sp.verify(sp.pack(('a', True)) == sp.bytes('0x050707010000000161030a'))
    sp.verify(sp.pack(sp.set_type_expr(('a', True), sp.TPair(sp.TString, sp.TBool))) == sp.pack(sp.set_type_expr(('a', True), sp.TPair(sp.TString, sp.TBool))))
    sp.verify(sp.pack(sp.timestamp(123)) == sp.bytes('0x0500bb01'))
    sp.verify(sp.pack(sp.set_type_expr(sp.timestamp(123), sp.TTimestamp)) == sp.pack(sp.set_type_expr(sp.timestamp(123), sp.TTimestamp)))
    sp.verify(sp.pack(sp.signature('sigTBpkXw6tC72L2nJ2r2Jm5iB6uidTWqoMNd4oEawUbGBf5mHVfKawFYL8X8MJECpL73oBnfujyUZNLK2LQWD1FaCkYMP4j')) == sp.bytes('0x050a0000004027bcc456b12981bacfbf9755c589c16772da5200656386f34c46ee2a70a9c07be36268ce666894a5e8a0828027b6fa1a7a084a5ffa24208cc1dfec07d6f66e0f'))
    sp.verify(sp.pack(sp.set_type_expr(sp.signature('sigTBpkXw6tC72L2nJ2r2Jm5iB6uidTWqoMNd4oEawUbGBf5mHVfKawFYL8X8MJECpL73oBnfujyUZNLK2LQWD1FaCkYMP4j'), sp.TSignature)) == sp.pack(sp.set_type_expr(sp.signature('sigTBpkXw6tC72L2nJ2r2Jm5iB6uidTWqoMNd4oEawUbGBf5mHVfKawFYL8X8MJECpL73oBnfujyUZNLK2LQWD1FaCkYMP4j'), sp.TSignature)))
    sp.verify(sp.pack(sp.signature('spsig1PPUFZucuAQybs5wsqsNQ68QNgFaBnVKMFaoZZfi1BtNnuCAWnmL9wVy5HfHkR6AeodjVGxpBVVSYcJKyMURn6K1yknYLm')) == sp.bytes('0x050a00000040865b0ceaa2decd64fea1b7e0e97d6ae8035125d821ebe99d7d5c8a559f36fdd43388a570e475011f6d2bbef184ece9bafbf0ac12157f7257988e9575ae2cbca9'))
    sp.verify(sp.pack(sp.set_type_expr(sp.signature('spsig1PPUFZucuAQybs5wsqsNQ68QNgFaBnVKMFaoZZfi1BtNnuCAWnmL9wVy5HfHkR6AeodjVGxpBVVSYcJKyMURn6K1yknYLm'), sp.TSignature)) == sp.pack(sp.set_type_expr(sp.signature('sigfZnvWJNRpSqDh6BuhKDkBT2TrU17ircsgTUnmWQCHxfMqYMLDgxCp27Nv6Ch22Ja5takK7BkMLiTKbDTUnvrsmuTshpV7'), sp.TSignature)))
    sp.verify(sp.pack(sp.signature('p2sigsceCzcDw2AeYDzUonj4JT341WC9Px4wdhHBxbZcG1FhfqFVuG7f2fGCzrEHSAZgrsrQWpxduDPk9qZRgrpzwJnSHC3gZJ')) == sp.bytes('0x050a00000040e7b25662eb9bb1e92ef7cb20c2fcaca120eccbe2d6af7511c2e510eb586ca8612d9dfe26bc7882a33b29b8c9706a04124f98dd281f1622d8da50db7a5cd4ab43'))
    sp.verify(sp.pack(sp.set_type_expr(sp.signature('p2sigsceCzcDw2AeYDzUonj4JT341WC9Px4wdhHBxbZcG1FhfqFVuG7f2fGCzrEHSAZgrsrQWpxduDPk9qZRgrpzwJnSHC3gZJ'), sp.TSignature)) == sp.pack(sp.set_type_expr(sp.signature('sigtJRMuWfqZEc5HjiZ58XWwaGd7Xv8reXRFWLaC7AxnoR8Hyiat4ZoP5nhfHmCjN7YFptVkihZiSRnSsVxGNoW366MVvmvh'), sp.TSignature)))
    sp.verify(sp.pack(sp.signature('edsigthw6sSCfcZbjKCqGE9CZ9PoTsW1Kh5cgGu5SSU1AzcKyJ37oubeKVnDfY291minBiui7khzr8pzhoFVtF9ULs3hnUVKXGx')) == sp.bytes('0x050a000000404d6738931b59605ca0449b7b76a01d210ace9220051821ecf43382a7024ed99063c5bedb85c81b919d33a213c6d8fb47f2c9b4deaf6f68f56dee038ea739740f'))
    sp.verify(sp.pack(sp.set_type_expr(sp.signature('edsigthw6sSCfcZbjKCqGE9CZ9PoTsW1Kh5cgGu5SSU1AzcKyJ37oubeKVnDfY291minBiui7khzr8pzhoFVtF9ULs3hnUVKXGx'), sp.TSignature)) == sp.pack(sp.set_type_expr(sp.signature('sigY7dkNymLdQhG1qy67MNC6Wx6fvPiSk5uDKhi81BzGK1zDBDQhjEQiykVRXDgFbqHNEVqCDVJD2w4TTgb18NfzdgX96XzB'), sp.TSignature)))
    sp.verify(sp.pack(sp.address('tz1fextP23D6Ph2zeGTP8EwkP5Y8TufeFCHA')) == sp.bytes('0x050a000000160000db8fc132cf9624febe1675fbfd0c0fcdd7655f23'))
    sp.verify(sp.pack(sp.set_type_expr(sp.address('tz1fextP23D6Ph2zeGTP8EwkP5Y8TufeFCHA'), sp.TAddress)) == sp.pack(sp.set_type_expr(sp.address('tz1fextP23D6Ph2zeGTP8EwkP5Y8TufeFCHA'), sp.TAddress)))
    sp.verify(sp.pack(sp.address('tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93')) == sp.bytes('0x050a0000001600014b7c404fd4fbcf931cde0a8971caf76f53c8e5c0'))
    sp.verify(sp.pack(sp.set_type_expr(sp.address('tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93'), sp.TAddress)) == sp.pack(sp.set_type_expr(sp.address('tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93'), sp.TAddress)))
    sp.verify(sp.pack(sp.address('tz3WMqdzXqRWXwyvj5Hp2H7QEepaUuS7vd9K')) == sp.bytes('0x050a0000001600026e08178148d348cb59efffbdb403ca546b13304e'))
    sp.verify(sp.pack(sp.set_type_expr(sp.address('tz3WMqdzXqRWXwyvj5Hp2H7QEepaUuS7vd9K'), sp.TAddress)) == sp.pack(sp.set_type_expr(sp.address('tz3WMqdzXqRWXwyvj5Hp2H7QEepaUuS7vd9K'), sp.TAddress)))
    sp.verify(sp.pack(sp.key_hash('tz1fextP23D6Ph2zeGTP8EwkP5Y8TufeFCHA')) == sp.bytes('0x050a0000001500db8fc132cf9624febe1675fbfd0c0fcdd7655f23'))
    sp.verify(sp.pack(sp.set_type_expr(sp.key_hash('tz1fextP23D6Ph2zeGTP8EwkP5Y8TufeFCHA'), sp.TKeyHash)) == sp.pack(sp.set_type_expr(sp.key_hash('tz1fextP23D6Ph2zeGTP8EwkP5Y8TufeFCHA'), sp.TKeyHash)))
    sp.verify(sp.pack(sp.key_hash('tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93')) == sp.bytes('0x050a00000015014b7c404fd4fbcf931cde0a8971caf76f53c8e5c0'))
    sp.verify(sp.pack(sp.set_type_expr(sp.key_hash('tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93'), sp.TKeyHash)) == sp.pack(sp.set_type_expr(sp.key_hash('tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93'), sp.TKeyHash)))
    sp.verify(sp.pack(sp.key_hash('tz3WMqdzXqRWXwyvj5Hp2H7QEepaUuS7vd9K')) == sp.bytes('0x050a00000015026e08178148d348cb59efffbdb403ca546b13304e'))
    sp.verify(sp.pack(sp.set_type_expr(sp.key_hash('tz3WMqdzXqRWXwyvj5Hp2H7QEepaUuS7vd9K'), sp.TKeyHash)) == sp.pack(sp.set_type_expr(sp.key_hash('tz3WMqdzXqRWXwyvj5Hp2H7QEepaUuS7vd9K'), sp.TKeyHash)))
    sp.verify(sp.pack(sp.address('KT1Mpqi89gRyUuoXUPAWjHkqkk1F48eUKUVy')) == sp.bytes('0x050a00000016019142eb01374f37f03023eefc82f2ede96e8ae51500'))
    sp.verify(sp.pack(sp.set_type_expr(sp.address('KT1Mpqi89gRyUuoXUPAWjHkqkk1F48eUKUVy'), sp.TAddress)) == sp.pack(sp.set_type_expr(sp.address('KT1Mpqi89gRyUuoXUPAWjHkqkk1F48eUKUVy'), sp.TAddress)))
    sp.verify(sp.pack(sp.address('KT1Mpqi89gRyUuoXUPAWjHkqkk1F48eUKUVy%')) == sp.bytes('0x050a00000016019142eb01374f37f03023eefc82f2ede96e8ae51500'))
    sp.verify(sp.pack(sp.set_type_expr(sp.address('KT1Mpqi89gRyUuoXUPAWjHkqkk1F48eUKUVy%'), sp.TAddress)) == sp.pack(sp.set_type_expr(sp.address('KT1Mpqi89gRyUuoXUPAWjHkqkk1F48eUKUVy'), sp.TAddress)))
    sp.verify(sp.pack(sp.address('KT1Mpqi89gRyUuoXUPAWjHkqkk1F48eUKUVy%foo')) == sp.bytes('0x050a00000019019142eb01374f37f03023eefc82f2ede96e8ae51500666f6f'))
    sp.verify(sp.pack(sp.set_type_expr(sp.address('KT1Mpqi89gRyUuoXUPAWjHkqkk1F48eUKUVy%foo'), sp.TAddress)) == sp.pack(sp.set_type_expr(sp.address('KT1Mpqi89gRyUuoXUPAWjHkqkk1F48eUKUVy%foo'), sp.TAddress)))
    sp.verify(sp.pack(sp.list([0])) == sp.bytes('0x0502000000020000'))
    sp.verify(sp.pack(sp.set_type_expr(sp.list([0]), sp.TList(sp.TIntOrNat))) == sp.pack(sp.set_type_expr(sp.list([0]), sp.TList(sp.TIntOrNat))))
    sp.verify(sp.pack(sp.list([0, 1])) == sp.bytes('0x05020000000400000001'))
    sp.verify(sp.pack(sp.set_type_expr(sp.list([0, 1]), sp.TList(sp.TIntOrNat))) == sp.pack(sp.set_type_expr(sp.list([0, 1]), sp.TList(sp.TIntOrNat))))
    sp.verify(sp.pack(sp.list([0, 1, 2])) == sp.bytes('0x050200000006000000010002'))
    sp.verify(sp.pack(sp.set_type_expr(sp.list([0, 1, 2]), sp.TList(sp.TIntOrNat))) == sp.pack(sp.set_type_expr(sp.list([0, 1, 2]), sp.TList(sp.TIntOrNat))))
    sp.verify(sp.pack(sp.list([0, 1, 2, 3])) == sp.bytes('0x0502000000080000000100020003'))
    sp.verify(sp.pack(sp.set_type_expr(sp.list([0, 1, 2, 3]), sp.TList(sp.TIntOrNat))) == sp.pack(sp.set_type_expr(sp.list([0, 1, 2, 3]), sp.TList(sp.TIntOrNat))))
    sp.verify(sp.pack(sp.list([0, 1, 2, 3, 4])) == sp.bytes('0x05020000000a00000001000200030004'))
    sp.verify(sp.pack(sp.set_type_expr(sp.list([0, 1, 2, 3, 4]), sp.TList(sp.TIntOrNat))) == sp.pack(sp.set_type_expr(sp.list([0, 1, 2, 3, 4]), sp.TList(sp.TIntOrNat))))
    sp.verify(sp.pack(sp.list([0, 1, 2, 3, 4, 5])) == sp.bytes('0x05020000000c000000010002000300040005'))
    sp.verify(sp.pack(sp.set_type_expr(sp.list([0, 1, 2, 3, 4, 5]), sp.TList(sp.TIntOrNat))) == sp.pack(sp.set_type_expr(sp.list([0, 1, 2, 3, 4, 5]), sp.TList(sp.TIntOrNat))))
    sp.verify(sp.pack(sp.list([0, 1, 2, 3, 4, 5, 6])) == sp.bytes('0x05020000000e0000000100020003000400050006'))
    sp.verify(sp.pack(sp.set_type_expr(sp.list([0, 1, 2, 3, 4, 5, 6]), sp.TList(sp.TIntOrNat))) == sp.pack(sp.set_type_expr(sp.list([0, 1, 2, 3, 4, 5, 6]), sp.TList(sp.TIntOrNat))))
    sp.verify(sp.pack(sp.set([1, 2, 3, 4])) == sp.bytes('0x0502000000080001000200030004'))
    sp.verify(sp.pack(sp.set_type_expr(sp.set([1, 2, 3, 4]), sp.TSet(sp.TIntOrNat))) == sp.pack(sp.set_type_expr(sp.set([1, 2, 3, 4]), sp.TSet(sp.TIntOrNat))))
    sp.verify(sp.pack(sp.timestamp(123)) == sp.bytes('0x0500bb01'))
    sp.verify(sp.pack(sp.set_type_expr(sp.timestamp(123), sp.TTimestamp)) == sp.pack(sp.set_type_expr(sp.timestamp(123), sp.TTimestamp)))
    sp.verify(sp.pack(sp.address('tz1fextP23D6Ph2zeGTP8EwkP5Y8TufeFCHA')) == sp.bytes('0x050a000000160000db8fc132cf9624febe1675fbfd0c0fcdd7655f23'))
    sp.verify(sp.pack(sp.set_type_expr(sp.address('tz1fextP23D6Ph2zeGTP8EwkP5Y8TufeFCHA'), sp.TAddress)) == sp.pack(sp.set_type_expr(sp.address('tz1fextP23D6Ph2zeGTP8EwkP5Y8TufeFCHA'), sp.TAddress)))
    sp.verify(sp.pack(sp.build_lambda(lambda _x13: _x13 + 2)) == sp.bytes('0x0502000000080743035b00020312'))
    sp.verify(sp.pack(sp.set_type_expr(sp.build_lambda(lambda _x14: _x14 + 2), sp.TLambda(sp.TIntOrNat, sp.TIntOrNat))) == sp.pack(sp.set_type_expr(sp.lambda_michelson(" { PUSH int 2; ADD; }"), sp.TLambda(sp.TIntOrNat, sp.TIntOrNat))))
    def f_x16(_x16):
      sp.if _x16.a:
        sp.result(123)
      sp.else:
        sp.result(_x16.b)
    sp.verify(sp.pack(sp.build_lambda(f_x16)) == sp.bytes('0x05020000001b03210316072c020000000903200743035b00bb0102000000020317'))
    def f_x17(_x17):
      sp.if _x17.a:
        sp.result(123)
      sp.else:
        sp.result(_x17.b)
    sp.verify(sp.pack(sp.set_type_expr(sp.build_lambda(f_x17), sp.TLambda(sp.TRecord(a = sp.TBool, b = sp.TIntOrNat).layout(("a", "b")), sp.TIntOrNat))) == sp.pack(sp.set_type_expr(sp.lambda_michelson(" { DUP; CAR; IF { DROP; PUSH int 123; } { CDR; }; }"), sp.TLambda(sp.TRecord(a = sp.TBool, b = sp.TIntOrNat).layout(("a", "b")), sp.TIntOrNat))))
    def f_x19(_x19):
      sp.if _x19.a:
        sp.result(123)
      sp.else:
        sp.result(_x19.b + _x19.c)
    sp.verify(sp.pack(sp.build_lambda(f_x19)) == sp.bytes('0x05020000002703210316072c020000000903200743035b00bb01020000000e032105290004034c052900030312'))
    def f_x20(_x20):
      sp.if _x20.a:
        sp.result(123)
      sp.else:
        sp.result(_x20.b + _x20.c)
    sp.verify(sp.pack(sp.set_type_expr(sp.build_lambda(f_x20), sp.TLambda(sp.TRecord(a = sp.TBool, b = sp.TIntOrNat, c = sp.TIntOrNat).layout(("a", ("b", "c"))), sp.TIntOrNat))) == sp.pack(sp.set_type_expr(sp.lambda_michelson(" { DUP; CAR; IF { DROP; PUSH int 123; } { DUP; GET 4; SWAP; GET 3; ADD; }; }"), sp.TLambda(sp.TRecord(a = sp.TBool, b = sp.TIntOrNat, c = sp.TIntOrNat).layout(("a", ("b", "c"))), sp.TIntOrNat))))
    sp.verify(sp.pack(sp.lambda_michelson("DUP; PUSH int 42; ADD; MUL")) == sp.bytes('0x05020000000c03210743035b002a0312033a'))
    sp.verify(sp.pack(sp.set_type_expr(sp.lambda_michelson("DUP; PUSH int 42; ADD; MUL"), sp.TLambda(sp.TInt, sp.TInt))) == sp.pack(sp.set_type_expr(sp.lambda_michelson(" { DUP; PUSH int 42; ADD; MUL; }"), sp.TLambda(sp.TInt, sp.TInt))))

sp.add_compilation_target("test", Contract())