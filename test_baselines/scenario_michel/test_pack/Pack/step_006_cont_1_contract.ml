open SmartML

module Contract = struct
  let%entry_point pack_lambda_record () =
    verify ((100 (open_some (unpack (pack (fun _x6 -> result {a = 1; b = (_x6 + 2)})) lambda int {a = int; b = int}))).a = 1);
    verify ((100 (open_some (unpack (pack (fun _x7 -> result {a = (_x7 + 1)})) lambda int {a = int}))).a = 101)

  let%entry_point pack_lambda_variant () =
    verify ((open_variant (100 (open_some (unpack (pack (fun _x8 -> result (set_type_expr (variant a (_x8 + 1)) (`a int + `b int)))) lambda int (`a int + `b int)))) a) = 101);
    verify ((open_variant (100 (open_some (unpack (pack (fun _x9 -> result (variant a (_x9 + 1)))) lambda int (`a int)))) a) = 101)

  let%entry_point pack_lambdas () =
    let%mutable f1 = (set_type_expr (fun _x10 -> let%mutable y = 0 in ();
let%mutable r = "A" in ();
if _x10 = 0 then
  if _x10 = y then
    r <- "B";
result r) lambda int string) in ();
    let%mutable p1 = (pack f1) in ();
    let%mutable f2 = (set_type_expr (open_some (unpack p1 lambda int string)) lambda int string) in ();
    let%mutable p2 = (pack f2) in ();
    verify (p1 = p2);
    verify ((0 f1) = (0 f2));
    verify ((1 f1) = (1 f2));
    verify ((2 f1) = (2 f2))

  let%entry_point pack_lambdas2 () =
    let%mutable f1 = (set_type_expr (fun _x11 -> result (_x11 > 0)) lambda int bool) in ();
    let%mutable p1 = (pack f1) in ();
    let%mutable f2 = (set_type_expr (open_some (unpack p1 lambda int bool)) lambda int bool) in ();
    verify (1 f2)

  let%entry_point run () =
    verify ((pack (-3)) = (bytes "0x050043"));
    verify ((pack (set_type_expr (-3) int)) = (pack (set_type_expr (-3) int)));
    verify ((pack (-10)) = (bytes "0x05004a"));
    verify ((pack (set_type_expr (-10) int)) = (pack (set_type_expr (-10) int)));
    verify ((pack (-10000)) = (bytes "0x0500d09c01"));
    verify ((pack (set_type_expr (-10000) int)) = (pack (set_type_expr (-10000) int)));
    verify ((pack (-1000000)) = (bytes "0x0500c0897a"));
    verify ((pack (set_type_expr (-1000000) int)) = (pack (set_type_expr (-1000000) int)));
    verify ((pack (-1000000000000)) = (bytes "0x0500c0c0a8ca9a3a"));
    verify ((pack (set_type_expr (-1000000000000) int)) = (pack (set_type_expr (-1000000000000) int)));
    verify ((pack "") = (bytes "0x050100000000"));
    verify ((pack (set_type_expr "" string)) = (pack (set_type_expr "" string)));
    verify ((pack "a") = (bytes "0x05010000000161"));
    verify ((pack (set_type_expr "a" string)) = (pack (set_type_expr "a" string)));
    verify ((pack "ab") = (bytes "0x0501000000026162"));
    verify ((pack (set_type_expr "ab" string)) = (pack (set_type_expr "ab" string)));
    verify ((pack "abc") = (bytes "0x050100000003616263"));
    verify ((pack (set_type_expr "abc" string)) = (pack (set_type_expr "abc" string)));
    verify ((pack (bytes "0x")) = (bytes "0x050a00000000"));
    verify ((pack (set_type_expr (bytes "0x") bytes)) = (pack (set_type_expr (bytes "0x") bytes)));
    verify ((pack (bytes "0x00")) = (bytes "0x050a0000000100"));
    verify ((pack (set_type_expr (bytes "0x00") bytes)) = (pack (set_type_expr (bytes "0x00") bytes)));
    verify ((pack (bytes "0xab00")) = (bytes "0x050a00000002ab00"));
    verify ((pack (set_type_expr (bytes "0xab00") bytes)) = (pack (set_type_expr (bytes "0xab00") bytes)));
    verify ((pack (1, 2)) = (bytes "0x05070700010002"));
    verify ((pack (set_type_expr (1, 2) (pair intOrNat intOrNat))) = (pack (set_type_expr (1, 2) (pair intOrNat intOrNat))));
    verify ((pack ("a", true)) = (bytes "0x050707010000000161030a"));
    verify ((pack (set_type_expr ("a", true) (pair string bool))) = (pack (set_type_expr ("a", true) (pair string bool))));
    verify ((pack (timestamp 123)) = (bytes "0x0500bb01"));
    verify ((pack (set_type_expr (timestamp 123) timestamp)) = (pack (set_type_expr (timestamp 123) timestamp)));
    verify ((pack (signature "sigTBpkXw6tC72L2nJ2r2Jm5iB6uidTWqoMNd4oEawUbGBf5mHVfKawFYL8X8MJECpL73oBnfujyUZNLK2LQWD1FaCkYMP4j")) = (bytes "0x050a0000004027bcc456b12981bacfbf9755c589c16772da5200656386f34c46ee2a70a9c07be36268ce666894a5e8a0828027b6fa1a7a084a5ffa24208cc1dfec07d6f66e0f"));
    verify ((pack (set_type_expr (signature "sigTBpkXw6tC72L2nJ2r2Jm5iB6uidTWqoMNd4oEawUbGBf5mHVfKawFYL8X8MJECpL73oBnfujyUZNLK2LQWD1FaCkYMP4j") signature)) = (pack (set_type_expr (signature "sigTBpkXw6tC72L2nJ2r2Jm5iB6uidTWqoMNd4oEawUbGBf5mHVfKawFYL8X8MJECpL73oBnfujyUZNLK2LQWD1FaCkYMP4j") signature)));
    verify ((pack (signature "spsig1PPUFZucuAQybs5wsqsNQ68QNgFaBnVKMFaoZZfi1BtNnuCAWnmL9wVy5HfHkR6AeodjVGxpBVVSYcJKyMURn6K1yknYLm")) = (bytes "0x050a00000040865b0ceaa2decd64fea1b7e0e97d6ae8035125d821ebe99d7d5c8a559f36fdd43388a570e475011f6d2bbef184ece9bafbf0ac12157f7257988e9575ae2cbca9"));
    verify ((pack (set_type_expr (signature "spsig1PPUFZucuAQybs5wsqsNQ68QNgFaBnVKMFaoZZfi1BtNnuCAWnmL9wVy5HfHkR6AeodjVGxpBVVSYcJKyMURn6K1yknYLm") signature)) = (pack (set_type_expr (signature "sigfZnvWJNRpSqDh6BuhKDkBT2TrU17ircsgTUnmWQCHxfMqYMLDgxCp27Nv6Ch22Ja5takK7BkMLiTKbDTUnvrsmuTshpV7") signature)));
    verify ((pack (signature "p2sigsceCzcDw2AeYDzUonj4JT341WC9Px4wdhHBxbZcG1FhfqFVuG7f2fGCzrEHSAZgrsrQWpxduDPk9qZRgrpzwJnSHC3gZJ")) = (bytes "0x050a00000040e7b25662eb9bb1e92ef7cb20c2fcaca120eccbe2d6af7511c2e510eb586ca8612d9dfe26bc7882a33b29b8c9706a04124f98dd281f1622d8da50db7a5cd4ab43"));
    verify ((pack (set_type_expr (signature "p2sigsceCzcDw2AeYDzUonj4JT341WC9Px4wdhHBxbZcG1FhfqFVuG7f2fGCzrEHSAZgrsrQWpxduDPk9qZRgrpzwJnSHC3gZJ") signature)) = (pack (set_type_expr (signature "sigtJRMuWfqZEc5HjiZ58XWwaGd7Xv8reXRFWLaC7AxnoR8Hyiat4ZoP5nhfHmCjN7YFptVkihZiSRnSsVxGNoW366MVvmvh") signature)));
    verify ((pack (signature "edsigthw6sSCfcZbjKCqGE9CZ9PoTsW1Kh5cgGu5SSU1AzcKyJ37oubeKVnDfY291minBiui7khzr8pzhoFVtF9ULs3hnUVKXGx")) = (bytes "0x050a000000404d6738931b59605ca0449b7b76a01d210ace9220051821ecf43382a7024ed99063c5bedb85c81b919d33a213c6d8fb47f2c9b4deaf6f68f56dee038ea739740f"));
    verify ((pack (set_type_expr (signature "edsigthw6sSCfcZbjKCqGE9CZ9PoTsW1Kh5cgGu5SSU1AzcKyJ37oubeKVnDfY291minBiui7khzr8pzhoFVtF9ULs3hnUVKXGx") signature)) = (pack (set_type_expr (signature "sigY7dkNymLdQhG1qy67MNC6Wx6fvPiSk5uDKhi81BzGK1zDBDQhjEQiykVRXDgFbqHNEVqCDVJD2w4TTgb18NfzdgX96XzB") signature)));
    verify ((pack (address "tz1fextP23D6Ph2zeGTP8EwkP5Y8TufeFCHA")) = (bytes "0x050a000000160000db8fc132cf9624febe1675fbfd0c0fcdd7655f23"));
    verify ((pack (set_type_expr (address "tz1fextP23D6Ph2zeGTP8EwkP5Y8TufeFCHA") address)) = (pack (set_type_expr (address "tz1fextP23D6Ph2zeGTP8EwkP5Y8TufeFCHA") address)));
    verify ((pack (address "tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93")) = (bytes "0x050a0000001600014b7c404fd4fbcf931cde0a8971caf76f53c8e5c0"));
    verify ((pack (set_type_expr (address "tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93") address)) = (pack (set_type_expr (address "tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93") address)));
    verify ((pack (address "tz3WMqdzXqRWXwyvj5Hp2H7QEepaUuS7vd9K")) = (bytes "0x050a0000001600026e08178148d348cb59efffbdb403ca546b13304e"));
    verify ((pack (set_type_expr (address "tz3WMqdzXqRWXwyvj5Hp2H7QEepaUuS7vd9K") address)) = (pack (set_type_expr (address "tz3WMqdzXqRWXwyvj5Hp2H7QEepaUuS7vd9K") address)));
    verify ((pack (key_hash "tz1fextP23D6Ph2zeGTP8EwkP5Y8TufeFCHA")) = (bytes "0x050a0000001500db8fc132cf9624febe1675fbfd0c0fcdd7655f23"));
    verify ((pack (set_type_expr (key_hash "tz1fextP23D6Ph2zeGTP8EwkP5Y8TufeFCHA") key_hash)) = (pack (set_type_expr (key_hash "tz1fextP23D6Ph2zeGTP8EwkP5Y8TufeFCHA") key_hash)));
    verify ((pack (key_hash "tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93")) = (bytes "0x050a00000015014b7c404fd4fbcf931cde0a8971caf76f53c8e5c0"));
    verify ((pack (set_type_expr (key_hash "tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93") key_hash)) = (pack (set_type_expr (key_hash "tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93") key_hash)));
    verify ((pack (key_hash "tz3WMqdzXqRWXwyvj5Hp2H7QEepaUuS7vd9K")) = (bytes "0x050a00000015026e08178148d348cb59efffbdb403ca546b13304e"));
    verify ((pack (set_type_expr (key_hash "tz3WMqdzXqRWXwyvj5Hp2H7QEepaUuS7vd9K") key_hash)) = (pack (set_type_expr (key_hash "tz3WMqdzXqRWXwyvj5Hp2H7QEepaUuS7vd9K") key_hash)));
    verify ((pack (address "KT1Mpqi89gRyUuoXUPAWjHkqkk1F48eUKUVy")) = (bytes "0x050a00000016019142eb01374f37f03023eefc82f2ede96e8ae51500"));
    verify ((pack (set_type_expr (address "KT1Mpqi89gRyUuoXUPAWjHkqkk1F48eUKUVy") address)) = (pack (set_type_expr (address "KT1Mpqi89gRyUuoXUPAWjHkqkk1F48eUKUVy") address)));
    verify ((pack (address "KT1Mpqi89gRyUuoXUPAWjHkqkk1F48eUKUVy%")) = (bytes "0x050a00000016019142eb01374f37f03023eefc82f2ede96e8ae51500"));
    verify ((pack (set_type_expr (address "KT1Mpqi89gRyUuoXUPAWjHkqkk1F48eUKUVy%") address)) = (pack (set_type_expr (address "KT1Mpqi89gRyUuoXUPAWjHkqkk1F48eUKUVy") address)));
    verify ((pack (address "KT1Mpqi89gRyUuoXUPAWjHkqkk1F48eUKUVy%foo")) = (bytes "0x050a00000019019142eb01374f37f03023eefc82f2ede96e8ae51500666f6f"));
    verify ((pack (set_type_expr (address "KT1Mpqi89gRyUuoXUPAWjHkqkk1F48eUKUVy%foo") address)) = (pack (set_type_expr (address "KT1Mpqi89gRyUuoXUPAWjHkqkk1F48eUKUVy%foo") address)));
    verify ((pack [0]) = (bytes "0x0502000000020000"));
    verify ((pack (set_type_expr [0] (list intOrNat))) = (pack (set_type_expr [0] (list intOrNat))));
    verify ((pack [0; 1]) = (bytes "0x05020000000400000001"));
    verify ((pack (set_type_expr [0; 1] (list intOrNat))) = (pack (set_type_expr [0; 1] (list intOrNat))));
    verify ((pack [0; 1; 2]) = (bytes "0x050200000006000000010002"));
    verify ((pack (set_type_expr [0; 1; 2] (list intOrNat))) = (pack (set_type_expr [0; 1; 2] (list intOrNat))));
    verify ((pack [0; 1; 2; 3]) = (bytes "0x0502000000080000000100020003"));
    verify ((pack (set_type_expr [0; 1; 2; 3] (list intOrNat))) = (pack (set_type_expr [0; 1; 2; 3] (list intOrNat))));
    verify ((pack [0; 1; 2; 3; 4]) = (bytes "0x05020000000a00000001000200030004"));
    verify ((pack (set_type_expr [0; 1; 2; 3; 4] (list intOrNat))) = (pack (set_type_expr [0; 1; 2; 3; 4] (list intOrNat))));
    verify ((pack [0; 1; 2; 3; 4; 5]) = (bytes "0x05020000000c000000010002000300040005"));
    verify ((pack (set_type_expr [0; 1; 2; 3; 4; 5] (list intOrNat))) = (pack (set_type_expr [0; 1; 2; 3; 4; 5] (list intOrNat))));
    verify ((pack [0; 1; 2; 3; 4; 5; 6]) = (bytes "0x05020000000e0000000100020003000400050006"));
    verify ((pack (set_type_expr [0; 1; 2; 3; 4; 5; 6] (list intOrNat))) = (pack (set_type_expr [0; 1; 2; 3; 4; 5; 6] (list intOrNat))));
    verify ((pack (Set.make [1; 2; 3; 4])) = (bytes "0x0502000000080001000200030004"));
    verify ((pack (set_type_expr (Set.make [1; 2; 3; 4]) (set intOrNat))) = (pack (set_type_expr (Set.make [1; 2; 3; 4]) (set intOrNat))));
    verify ((pack (timestamp 123)) = (bytes "0x0500bb01"));
    verify ((pack (set_type_expr (timestamp 123) timestamp)) = (pack (set_type_expr (timestamp 123) timestamp)));
    verify ((pack (address "tz1fextP23D6Ph2zeGTP8EwkP5Y8TufeFCHA")) = (bytes "0x050a000000160000db8fc132cf9624febe1675fbfd0c0fcdd7655f23"));
    verify ((pack (set_type_expr (address "tz1fextP23D6Ph2zeGTP8EwkP5Y8TufeFCHA") address)) = (pack (set_type_expr (address "tz1fextP23D6Ph2zeGTP8EwkP5Y8TufeFCHA") address)));
    verify ((pack (fun _x13 -> result (_x13 + 2))) = (bytes "0x0502000000080743035b00020312"));
    verify ((pack (set_type_expr (fun _x14 -> result (_x14 + 2)) lambda intOrNat intOrNat)) = (pack (set_type_expr lambda_michelson(" { PUSH int 2; ADD; }") lambda intOrNat intOrNat)));
    verify ((pack (fun _x16 -> if _x16.a then
  result 123
else
  result _x16.b)) = (bytes "0x05020000001b03210316072c020000000903200743035b00bb0102000000020317"));
    verify ((pack (set_type_expr (fun _x17 -> if _x17.a then
  result 123
else
  result _x17.b) lambda {a = bool; b = intOrNat} intOrNat)) = (pack (set_type_expr lambda_michelson(" { DUP; CAR; IF { DROP; PUSH int 123; } { CDR; }; }") lambda {a = bool; b = intOrNat} intOrNat)));
    verify ((pack (fun _x19 -> if _x19.a then
  result 123
else
  result (_x19.b + _x19.c))) = (bytes "0x05020000002703210316072c020000000903200743035b00bb01020000000e032105290004034c052900030312"));
    verify ((pack (set_type_expr (fun _x20 -> if _x20.a then
  result 123
else
  result (_x20.b + _x20.c)) lambda {a = bool; b = intOrNat; c = intOrNat} intOrNat)) = (pack (set_type_expr lambda_michelson(" { DUP; CAR; IF { DROP; PUSH int 123; } { DUP; GET 4; SWAP; GET 3; ADD; }; }") lambda {a = bool; b = intOrNat; c = intOrNat} intOrNat)));
    verify ((pack lambda_michelson("DUP; PUSH int 42; ADD; MUL")) = (bytes "0x05020000000c03210743035b002a0312033a"));
    verify ((pack (set_type_expr lambda_michelson("DUP; PUSH int 42; ADD; MUL") lambda int int)) = (pack (set_type_expr lambda_michelson(" { DUP; PUSH int 42; ADD; MUL; }") lambda int int)))

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [pack_lambda_record; pack_lambda_variant; pack_lambdas; pack_lambdas2; run]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())