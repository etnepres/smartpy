import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(int = sp.TInt, nat = sp.TNat).layout(("int", "nat")))
    self.init(int = 0,
              nat = 0)

  @sp.entry_point
  def ep(self):
    self.data.int = sp.to_int(self.data.nat)
    self.data.nat = sp.as_nat(self.data.int)
    self.data.nat = abs(self.data.int)

sp.add_compilation_target("test", Contract())