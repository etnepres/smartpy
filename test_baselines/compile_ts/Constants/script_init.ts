class Constants {
    // The constant points to a lambda
    constant1 = Sp.constant<TLambda<TNat, TNat>>('expru54tk2k4E81xQy63P6x3RijnTz51s2m7BV7pr3fDQH8YDqiYvR');
    // The constant points to a nat
    constant2 = Sp.constant<TNat>('exprtX5EDHZJaBbSmnq14wSwcadxvXJDk5Du7eUpbmMgQFPeGsaV2M');

    constructor(public storage: TNat) {}

    @EntryPoint
    ep(x: TNat) {
        // Compute the new value from values stored as constants
        this.storage = this.constant1(x) + this.constant2;
    }
}

Dev.test({ name: 'Test constants' }, () => {
    // Define some lambda
    const someLambda = (x: TNat): TNat => {
        return x * 10;
    };
    // Create 2 constants from their values
    const constant1 = Scenario.prepareConstantValue(someLambda);
    const constant2 = Scenario.prepareConstantValue(100);
    // Print the constants
    Scenario.show(constant1);
    Scenario.show(constant2);

    // Create contract
    const contract = Scenario.originate(new Constants(0));

    // Call contract
    Scenario.transfer(contract.ep(10));
});

Dev.compileContract('constants_compilation', new Constants(0));
