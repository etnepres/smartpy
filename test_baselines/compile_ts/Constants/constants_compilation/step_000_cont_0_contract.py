import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TNat)
    self.init(0)

  @sp.entry_point
  def ep(self, params):
    sp.set_type(params, sp.TNat)
    self.data = sp.constant("expru54tk2k4E81xQy63P6x3RijnTz51s2m7BV7pr3fDQH8YDqiYvR", t = sp.TLambda(sp.TNat, sp.TNat))(params) + sp.constant("exprtX5EDHZJaBbSmnq14wSwcadxvXJDk5Du7eUpbmMgQFPeGsaV2M", t = sp.TNat)

sp.add_compilation_target("test", Contract())