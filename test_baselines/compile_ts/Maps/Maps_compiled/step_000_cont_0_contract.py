import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(entries = sp.TList(sp.TRecord(key = sp.TString, value = sp.TNat).layout(("key", "value"))), keys = sp.TList(sp.TString), map = sp.TMap(sp.TString, sp.TNat), value = sp.TNat, values = sp.TList(sp.TNat)).layout((("entries", "keys"), ("map", ("value", "values")))))
    self.init(entries = [],
              keys = [],
              map = {},
              value = 0,
              values = [])

  @sp.entry_point
  def set(self, params):
    sp.set_type(params, sp.TRecord(key = sp.TString, value = sp.TNat).layout(("key", "value")))
    self.data.map[params.key] = params.value
    self.data.keys = self.data.map.keys()
    self.data.values = self.data.map.values()
    self.data.entries = self.data.map.items()
    sp.for x in self.data.entries:
      self.data.value += x.value

  @sp.entry_point
  def remove(self, params):
    sp.set_type(params, sp.TString)
    value = sp.local("value", self.data.map[params])
    sp.if ((self.data.map.contains(params)) & (value.value == 1)) & (sp.len(self.data.map) == 3):
      del self.data.map[params]

sp.add_compilation_target("test", Contract())