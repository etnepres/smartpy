import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(elements = sp.TOption(sp.TList(sp.TString)), set = sp.TSet(sp.TString)).layout(("elements", "set")))
    self.init(elements = sp.none,
              set = sp.set([]))

  @sp.entry_point
  def add(self, params):
    sp.set_type(params, sp.TString)
    self.data.set.add(params)

  @sp.entry_point
  def contains(self, params):
    sp.set_type(params, sp.TString)
    sp.if (self.data.set.contains(params)) & (sp.len(self.data.set) == 3):
      self.data.elements = sp.some(self.data.set.elements())
      sp.for x in self.data.set.elements():
        self.data.set.remove(x)

sp.add_compilation_target("test", Contract())