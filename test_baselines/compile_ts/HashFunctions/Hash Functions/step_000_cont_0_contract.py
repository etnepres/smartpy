import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TOption(sp.TRecord(blake2b = sp.TBytes, keccak = sp.TBytes, sha256 = sp.TBytes, sha3 = sp.TBytes, sha512 = sp.TBytes).layout((("blake2b", "keccak"), ("sha256", ("sha3", "sha512"))))))
    self.init(sp.none)

  @sp.entry_point
  def ep(self, params):
    sp.set_type(params, sp.TString)
    bytes = sp.local("bytes", sp.pack(params))
    self.data = sp.some(sp.record(blake2b = sp.blake2b(bytes.value), keccak = sp.keccak(bytes.value), sha256 = sp.sha256(bytes.value), sha3 = sp.sha3(bytes.value), sha512 = sp.sha512(bytes.value)))

sp.add_compilation_target("test", Contract())