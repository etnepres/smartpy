import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(bossPublicKey = sp.TKey, counter = sp.TInt, currentValue = sp.TString).layout(("bossPublicKey", ("counter", "currentValue"))))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TPair(sp.TString, sp.TSignature))
    def ferror(error):
      sp.failwith('[Error: prim1: Pack]')
    sp.verify(sp.check_signature(self.data.bossPublicKey, sp.snd(params), sp.build_lambda(ferror)(sp.unit)), 'WrongCondition: sp.check_signature(self.data.bossPublicKey, params.userSignature, sp.pack(sp.record(c = self.data.counter, n = params.newValue, o = self.data.currentValue)))')
    x48 = sp.local("x48", (self.data.bossPublicKey, (self.data.counter, sp.fst(params))))
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., let [x61; x62] = Unpair(2, x48)
                            Pair
                              ( x61
                              , let [x63; x64] = Unpair(2, x62)
                                let _ = x63
                                Pair(Add(1, Car(Cdr(x48))), x64)
                              ))]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
