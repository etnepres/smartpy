import smartpy as sp

tstorage = sp.TRecord(x = sp.TIntOrNat, y = sp.TIntOrNat, z = sp.TString).layout(("x", ("y", "z")))
tparameter = sp.TVariant(ep2 = sp.TUnit).layout("ep2")
tprivates = { }
tviews = { }
