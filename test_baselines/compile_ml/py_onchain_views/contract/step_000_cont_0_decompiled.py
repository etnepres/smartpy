import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TInt, y = sp.TInt, z = sp.TString).layout(("x", ("y", "z"))))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TUnit)
    self.data = sp.record(x = self.data.x, y = self.data.y * 2, z = self.data.z)

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
