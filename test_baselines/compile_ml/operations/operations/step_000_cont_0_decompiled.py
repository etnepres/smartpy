import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TInt, y = sp.TInt).layout(("x", "y")))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TVariant(Left = sp.TPair(sp.TMutez, sp.TPair(sp.TOption(sp.TKeyHash), sp.TContract(sp.TInt))), Right = sp.TUnit).layout(("Left", "Right")), Right = sp.TVariant(Left = sp.TOption(sp.TKeyHash), Right = sp.TPair(sp.TMutez, sp.TContract(sp.TInt))).layout(("Left", "Right"))).layout(("Left", "Right")))
    with params.match_cases() as arg:
      with arg.match('Left') as l3:
        with l3.match_cases() as arg:
          with arg.match('Left') as l38:
    def ferror(error):
      sp.failwith('[Error: to_cmd: create_contract(parameter :  { x : int ; y : int } %myEntryPoint
                        storage   :  { a : int ; b : int }
                        ps121 ->
                        let x137 =
                          let [x214; x215] = Unpair(2, Cdr(ps121))
                          let _ = x214
                          Pair(Add(Car(Car(ps121)), Car(Cdr(ps121))), x215)
                        Pair
                          ( Nil<operation>
                          , let [x212; x213] = Unpair(2, x137)
                            Pair
                              ( x212
                              , let _ = x213
                                Add(Cdr(Car(ps121)), Cdr(x137))
                              )
                          ), None<key_hash>, 0#mutez, Pair(0, 0))]')
            sp.failwith(sp.build_lambda(ferror)(sp.unit))
          with arg.match('Right') as _r29:
    def ferror(error):
      sp.failwith('[Error: to_cmd: create_contract(parameter :  { x : int ; y : int } %myEntryPoint
                        storage   :  { a : int ; b : int }
                        ps46 ->
                        let x62 =
                          let [x210; x211] = Unpair(2, Cdr(ps46))
                          let _ = x210
                          Pair(Add(Car(Car(ps46)), Car(Cdr(ps46))), x211)
                        Pair
                          ( Nil<operation>
                          , let [x208; x209] = Unpair(2, x62)
                            Pair
                              ( x208
                              , let _ = x209
                                Add(Cdr(Car(ps46)), Cdr(x62))
                              )
                          ), None<key_hash>, 0#mutez, Pair(0, 0))]')
            sp.failwith(sp.build_lambda(ferror)(sp.unit))

      with arg.match('Right') as r4:
        with r4.match_cases() as arg:
          with arg.match('Left') as l5:
            x62 = sp.local("x62", (sp.cons(sp.set_delegate_operation(l5), sp.list([])), (self.data.x, self.data.y)))
            s170 = sp.local("s170", sp.fst(x62.value))
            s171 = sp.local("s171", sp.snd(x62.value))
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., s171)]')
            sp.failwith(sp.build_lambda(ferror)(sp.unit))
          with arg.match('Right') as r6:
            x62 = sp.local("x62", (sp.cons(sp.transfer_operation(42, sp.fst(r6), sp.snd(r6)), sp.list([])), (self.data.x, self.data.y)))
            s170 = sp.local("s170", sp.fst(x62.value))
            s171 = sp.local("s171", sp.snd(x62.value))
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., s171)]')
            sp.failwith(sp.build_lambda(ferror)(sp.unit))



sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
