open SmartML

module Contract = struct
  let%entry_point test_commands params =
    transfer 42 params.amount params.destination;
    set_delegate params.delegate;
    operations <- create contract ....operation :: operations

  let%entry_point test_transfer params =
    List.iter (fun _op ->
      operations <- _op :: operations
    ) [transfer_operation 42 params.amount params.destination]

  let%entry_point test_create_contract () =
    let create_op = create contract ... in ();
    List.iter (fun _op ->
      operations <- _op :: operations
    ) [create_op.operation]

  let%entry_point test_set_delegate params =
    List.iter (fun _op ->
      operations <- _op :: operations
    ) [set_delegate_operation params]

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x = intOrNat; y = intOrNat}]
      ~storage:[%expr
                 {x = 0;
                  y = 0}]
      [test_commands; test_transfer; test_create_contract; test_set_delegate]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())