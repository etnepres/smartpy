open SmartML

module MyContract = struct
  type t = {storedValue : int} (* [@@deriving smartml]*)

  let replace_params = [%typ {value = nat}]

  let divide_params = [%typ {divisor = nat}]

  let%entry_point replace params =
    set_type params [%e replace_params];
    set_type params {value = nat};
    set_type params.value [%e [%typ nat]];
    data.storedValue <- set_type_expr params.value nat

  let%entry_point sum params =
    set_type params {foo = nat; bar = {toto = nat}};
    set_type params.foo nat;
    set_type params.bar {toto = nat};
    set_type params.bar.toto nat;
    data.storedValue <- params.foo + params.bar.toto

  let%entry_point double () =
    data.storedValue <- data.storedValue * 2;
    ()

  let%entry_point divide params =
    set_type params [%e divide_params];
    set_type params {divisor = nat};
    verify (params.divisor > 5) ~msg:"params.divisor too small";
    verify (params.divisor > 6) ~msg:42;
    data.storedValue <- data.storedValue / params.divisor

  let%entry_point misc params =
    set_type params.divisor int;
    set_type params.bar (`A nat + `B nat + `c never);
    set_type params.bar2 (`A nat + `B (`c nat + `D never));
    set_type params.bar3 [nat; nat];
    verify (fst params.bar3 = snd params.bar3) ~msg:"not equal";
    verify (params.divisor > 5) ~msg:"params.divisor too small";
    verify (params.divisor > 6) ~msg:42;
    trace params.divisor;
    begin
      match params.bar with
      | `A x -> failwith (1 + x)
      | `B x -> data.storedValue <- x
      | `c n -> never n
    end;
    if 2 + params.foo < 42 then failwith params;
    if 1 + params.foo < 14
    then if 2 + params.foo < 42 then failwith params else failwith "abc";
    while data.storedValue < 15 do
      data.storedValue <- 2 * data.storedValue
    done;
    List.iter (fun x -> data.storedValue <- data.storedValue + x) params.l

  let another =
    Basics.build_entry_point
      ~name:"another"
      ~originate:false
      [%command data.storedValue <- data.storedValue * 42]

  let another_with_parameter =
    Basics.build_entry_point
      ~name:"another_with_parameter"
      ~originate:false
      ~tparameter:[%typ nat]
      [%command data.storedValue <- data.storedValue * params]

  let init value =
    let storage = [%expr {storedValue = [%e value]}] in
    Basics.build_contract
      ~storage
      [replace; sum; double; divide; misc; another; another_with_parameter]
end

let () =
  Target.register_test
    ~name:"two_contracts"
    [%actions
      register_contract (MyContract.init [%expr 123456]);
      register_contract (MyContract.init [%expr 1])]

let () =
  let cst = [%expr 15] in
  Target.register
    ~name:"my_test"
    ~kind:"test"
    [%actions
      let c1 = register_contract (MyContract.init [%expr 42]) in
      h1 "Store Value";
      h2 "Foo";
      verify (c1.data.storedValue = 42);
      call c1.replace {value = abs (11 + (12 / -6) - (11 % 2))};
      verify (c1.data.storedValue = 8);
      call c1.replace {value = 12};
      call c1.replace {value = 13};
      call c1.replace {value = [%e cst]};
      p "Some computation";
      show (-c1.data.storedValue * 12);
      call c1.replace {value = 25};
      call c1.double ();
      call c1.divide {divisor = 2} ~valid:false;
      verify (c1.data.storedValue = 50);
      call c1.divide {divisor = 7};
      verify (c1.data.storedValue <> 9)]
