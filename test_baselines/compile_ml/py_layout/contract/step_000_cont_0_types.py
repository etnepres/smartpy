import smartpy as sp

tstorage = sp.TRecord(a = sp.TIntOrNat, b = sp.TNat).layout(("a", "b"))
tparameter = sp.TVariant(run_record = sp.TRecord(a = sp.TInt, b = sp.TString, c = sp.TBool).layout(("a", ("b", "c"))), run_record_2 = sp.TRecord(a = sp.TInt, b = sp.TString, c = sp.TBool, d = sp.TNat).layout((("a", "b"), ("c", "d"))), run_type_record = sp.TRecord(a = sp.TInt, b = sp.TString).layout(("a", "b")), run_type_variant = sp.TVariant(d = sp.TString, e = sp.TInt).layout(("d", "e")), run_variant = sp.TVariant(a = sp.TInt, b = sp.TString, c = sp.TBool).layout(("a", ("b", "c")))).layout((("run_record", "run_record_2"), ("run_type_record", ("run_type_variant", "run_variant"))))
tprivates = { }
tviews = { }
