open SmartML

module Contract = struct
  let%entry_point build params =
    let%mutable rangeMap = (Map.make []) in ();
    List.iter (fun i ->
      Map.set rangeMap (i - 1) i
    ) (range 1 (params.size + 1) 1);
    Map.set data.games data.nbGames {bound = params.bound; claimed = false; deck = rangeMap; lastWins = false; nextPlayer = 1; size = params.size; winner = 0};
    data.nbGames <- data.nbGames + 1

  let%entry_point claim params =
    verify ((sum (Map.values (Map.get data.games params.gameId).deck)) = 0);
    (Map.get data.games params.gameId).claimed <- true;
    if (Map.get data.games params.gameId).lastWins then
      (Map.get data.games params.gameId).winner <- 3 - (Map.get data.games params.gameId).nextPlayer
    else
      (Map.get data.games params.gameId).winner <- (Map.get data.games params.gameId).nextPlayer

  let%entry_point remove params =
    verify (params.cell >= 0);
    verify (params.cell < (Map.get data.games params.gameId).size);
    verify (params.k >= 1);
    verify (params.k <= (Map.get data.games params.gameId).bound);
    verify (params.k <= (Map.get (Map.get data.games params.gameId).deck params.cell));
    Map.set (Map.get data.games params.gameId).deck params.cell ((Map.get (Map.get data.games params.gameId).deck params.cell) - params.k);
    (Map.get data.games params.gameId).nextPlayer <- 3 - (Map.get data.games params.gameId).nextPlayer

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {games = map intOrNat {bound = int; claimed = bool; deck = map int int; lastWins = bool; nextPlayer = int; size = int; winner = int}; nbGames = intOrNat}]
      ~storage:[%expr
                 {games = Map.make [];
                  nbGames = 0}]
      [build; claim; remove]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())