import smartpy as sp

tstorage = sp.TRecord(r = sp.TIntOrNat).layout("r")
tparameter = sp.TVariant(ep1 = sp.TUnit, ep1b = sp.TIntOrNat, ep2 = sp.TUnit, ep2b = sp.TIntOrNat).layout((("ep1", "ep1b"), ("ep2", "ep2b")))
tprivates = { }
tviews = { }
