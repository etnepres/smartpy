import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TInt)

  @sp.entry_point
  def ep1(self, params):
    sp.set_type(params, sp.TUnit)
    self.data = 1

  @sp.entry_point
  def ep1b(self, params):
    sp.set_type(params, sp.TInt)
    sp.if 2 <= params:
      self.data = 2
    sp.else:
      self.data = params

  @sp.entry_point
  def ep2(self, params):
    sp.set_type(params, sp.TUnit)
    self.data = 4

  @sp.entry_point
  def ep2b(self, params):
    sp.set_type(params, sp.TInt)
    sp.if 4 <= params:
      self.data = params
    sp.else:
      self.data = 4

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
