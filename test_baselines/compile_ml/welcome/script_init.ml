open SmartML

module MyContract = struct
  let%entry_point myEntryPoint params =
    verify (data.myParameter1 <= 123) ~msg:"myParameter1 too large";
    data.myParameter1 <- data.myParameter1 + params

  let init p1 p2 =
    let storage = [%expr {myParameter1 = [%e p1]; myParameter2 = [%e p2]}] in
    Basics.build_contract ~storage [myEntryPoint]
end

let () =
  Target.register_test
    ~name:"Welcome"
    [%actions
      h1 "Welcome";
      let c1 = register_contract (MyContract.init [%expr 12] [%expr 123]) in
      call c1.myEntryPoint 12;
      call c1.myEntryPoint 13;
      call c1.myEntryPoint 14;
      call c1.myEntryPoint 50;
      call c1.myEntryPoint 50;
      call c1.myEntryPoint 50 ~valid:false;
      verify (c1.data.myParameter1 = 151)]

(* let c2 =
     register_contract (MyContract.init [%expr 12] c1.data.myParameter2)
   in
   verify (c2.data.myParameter1 = 152)
*)
