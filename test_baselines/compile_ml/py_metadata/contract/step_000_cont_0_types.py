import smartpy as sp

tstorage = sp.TRecord(metadata = sp.TMap(sp.TString, sp.TBytes), x = sp.TIntOrNat).layout(("metadata", "x"))
tparameter = sp.TVariant(change_metadata = sp.TBytes, incr = sp.TUnit).layout(("change_metadata", "incr"))
tprivates = { }
tviews = { }
