open SmartML

module Contract = struct
  let%entry_point pop () =
    verify (data.fif.first < data.fif.last);
    Map.delete data.fif.saved data.fif.first;
    data.fif.first <- data.fif.first + 1

  let%entry_point push params =
    set_type params int;
    data.fif.last <- data.fif.last + 1;
    Map.set data.fif.saved data.fif.last params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {fif = {first = int; last = int; saved = map int int}}]
      ~storage:[%expr
                 {fif = {first = 0; last = (-1); saved = Map.make []}}]
      [pop; push]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())