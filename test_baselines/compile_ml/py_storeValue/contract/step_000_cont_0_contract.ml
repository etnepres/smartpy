open SmartML

module Contract = struct
  let%entry_point divide params =
    verify (params.divisor > 5);
    data.storedValue <- data.storedValue / params.divisor

  let%entry_point double () =
    data.storedValue <- data.storedValue * 2

  let%entry_point replace params =
    data.storedValue <- params.value

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {storedValue = nat}]
      ~storage:[%expr
                 {storedValue = 12}]
      [divide; double; replace]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())