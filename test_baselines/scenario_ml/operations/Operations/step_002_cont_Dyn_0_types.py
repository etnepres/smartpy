import smartpy as sp

tstorage = sp.TRecord(a = sp.TIntOrNat, b = sp.TIntOrNat).layout(("a", "b"))
tparameter = sp.TVariant(myEntryPoint = sp.TRecord(x = sp.TIntOrNat, y = sp.TIntOrNat).layout(("x", "y"))).layout("myEntryPoint")
tprivates = { }
tviews = { }
