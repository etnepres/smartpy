open SmartML

module Nim = struct
  let%entry_point claim params =
    verify (sum (Map.values data.deck) = 0);
    verify (not data.claimed);
    data.claimed <- true;
    data.winner <- data.nextPlayer;
    verify (params.winner = data.winner)

  let%entry_point remove params =
    verify (params.cell >= 0);
    verify (params.cell < data.size);
    verify (params.k >= 1);
    verify (params.k <= 2);
    verify (params.k <= Map.get data.deck params.cell);
    Map.set data.deck params.cell (Map.get data.deck params.cell - params.k);
    data.nextPlayer <- 3 - data.nextPlayer

  let init size =
    (* TODO meta-programming: bound, winnerIsLast *)
    Basics.build_contract
      ~storage:
        [%expr
          { claimed = false
          ; deck = Map.make [(0, 1); (1, 2); (2, 3); (3, 4); (4, 5)]
          ; nextPlayer = 1
          ; size = [%e size]
          ; winner = 0 }]
      [claim; remove]
end

let () =
  Target.register_test
    ~name:"Nim"
    [%actions
      h1 "Nim";
      let c = register_contract (Nim.init [%expr 5]) in
      h3 "A first move";
      call c.remove {cell = 2; k = 1};
      h3 "A second move";
      call c.remove {cell = 2; k = 2};
      h3 "An illegal move";
      call c.remove {cell = 2; k = 1} ~valid:false;
      h3 "Another illegal move";
      call c.claim {winner = 1} ~valid:false;
      h3 "A third move";
      call c.remove {cell = 1; k = 2};
      h3 "More moves";
      call c.remove {cell = 0; k = 1};
      call c.remove {cell = 3; k = 1};
      call c.remove {cell = 3; k = 1};
      call c.remove {cell = 3; k = 2};
      call c.remove {cell = 4; k = 1};
      call c.remove {cell = 4; k = 2};
      h3 "A failed attempt to claim";
      call c.claim {winner = 1} ~valid:false;
      h3 "A last removal";
      call c.remove {cell = 4; k = 2};
      h3 "And a final claim";
      call c.claim {winner = 1}]

(* TODO p( "The winner is" c.data.winner) *)
