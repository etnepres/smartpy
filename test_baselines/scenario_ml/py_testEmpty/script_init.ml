open SmartML

module Contract = struct
  let%entry_point ep1 () =
    data.m1 <- Map.make [("e", "f")];
    data.l1 <- ["g"];
    data.o1 <- some "h"

  let%entry_point ep2 () =
    data.m2 <- Map.make [];
    data.l2 <- []

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {l1 = list string; l2 = list string; m1 = map string string; m2 = map string string; o1 = option string}]
      ~storage:[%expr
                 {l1 = [];
                  l2 = ["c"];
                  m1 = Map.make [];
                  m2 = Map.make [("a", "b")];
                  o1 = None}]
      [ep1; ep2]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())