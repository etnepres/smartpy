import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(counter = sp.TIntOrNat, on_even = sp.TAddress, on_odd = sp.TAddress).layout(("counter", ("on_even", "on_odd"))))
    self.init(counter = 0,
              on_even = sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1'),
              on_odd = sp.address('KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF'))

  @sp.entry_point
  def run(self, params):
    sp.if params > 1:
      self.data.counter += 1
      p = sp.local("p", sp.record(k = sp.self_entry_point('run'), x = params))
      sp.if (params % 2) == 0:
        sp.transfer(p.value, sp.tez(0), sp.contract(sp.TRecord(k = sp.TContract(sp.TNat), x = sp.TNat).layout(("k", "x")), self.data.on_even).open_some())
      sp.else:
        sp.transfer(p.value, sp.tez(0), sp.contract(sp.TRecord(k = sp.TContract(sp.TNat), x = sp.TNat).layout(("k", "x")), self.data.on_odd).open_some())

  @sp.entry_point
  def reset(self):
    self.data.counter = 0

sp.add_compilation_target("test", Contract())