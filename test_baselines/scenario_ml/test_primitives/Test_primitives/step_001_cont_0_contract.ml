open SmartML

module Contract = struct
  let%entry_point test_type0 () =
    set_type () unit;
    set_type true bool;
    set_type 0 nat;
    set_type (tez 0) mutez;
    set_type "" string;
    set_type (bytes "0x") bytes;
    set_type (chain_id "0x") chain_id;
    set_type (address "tz1") address;
    set_type (key "edpkaaa") key;
    set_type (key_hash "tz1aaa") key_hash;
    set_type (signature "sigaaa") signature;
    set_type (bls12_381_g1 "0x") bls12_381_g1;
    set_type (bls12_381_g2 "0x") bls12_381_g2;
    set_type (bls12_381_fr "0x") bls12_381_fr

  let%entry_point test_type1 params =
    set_type (some ()) (option unit);
    set_type [()] (list unit);
    set_type (Set.make [()]) (set unit);
    set_type params.a (contract unit);
    set_type params.b (ticket unit)

  let%entry_point test_type2 () =
    set_type (fun x -> ()) lambda unit unit;
    set_type (Map.make []) (map unit unit);
    set_type ((), ()) (pair unit unit);
    set_type ((), ()) (pair unit unit)

  let%entry_point test_type () =
    set_type {x = 0; y = 0; z = 0} {x = int; y = nat; z = nat};
    set_type (variant A 0) (`A int + `B nat);
    set_type (1, 2, ()) [int; nat; unit]

  let%entry_point test_literal () =
    verify (() = ());
    verify (true <> false);
    verify (1 = 1);
    verify ((-1) = (-1));
    verify ((mutez 1) = (mutez 1));
    verify ("abc" = "abc");
    verify ((bytes "0xabcd") = (bytes "0xabcd"));
    verify ((chain_id "0xabcd") = (chain_id "0xabcd"));
    verify ((timestamp 42) = (timestamp 42));
    verify ((address "tz2aaa") = (address "tz2aaa"));
    verify ((key "edpkabcd") = (key "edpkabcd"));
    verify ((key_hash "tz1aaa") = (key_hash "tz1aaa"));
    verify ((signature "sigaaa") = (signature "sigaaa"));
    verify ((len [bls12_381_g1 "0xabcd"]) = 1);
    verify ((len [bls12_381_g2 "0xabcd"]) = 1);
    verify ((len [bls12_381_fr "0xabcd"]) = 1)

  let%entry_point test_mprim0 () =
    verify (amount = (tez 0));
    verify (balance = (tez 0));
    verify (chain_id = (chain_id "0x"));
    verify (level = 0);
    verify (now = now);
    verify (total_voting_power = total_voting_power)

  let%entry_point test_mprim1 () =
    verify ((abs 2) = 2);
    verify ((abs (-2)) = 2);
    verify ((not true) = false);
    verify ((not false) = true);
    verify ((is_nat 2) = (some 2));
    verify ((is_nat (-1)) = None);
    let p = (bytes "0x050707010000000161010000000162") in ();
    let r1 = (bytes "0x87b2e62f0bbc3002af9ebb095724a1787c7a824ee22b7fe5debf584a2ac76ecb") in ();
    verify ((blake2b p) = r1);
    let r2 = (bytes "0xa6c3b9db1b22687139376251744b2d62700897dd14fc75baae7dd1bbdd320ee3") in ();
    verify ((sha256 p) = r2);
    let r3 = (bytes "0x0dd9fbbd042a7ce815abad7f15279d9a80f2fb98c561f1e424343ed46b3d20e8da5da4eca6a6b96b71884893b8ab67ceff90cc96da78f8aa48a87874349c089a") in ();
    verify ((sha512 p) = r3);
    let r4 = (bytes "0x6b656363616ba5cf04378a792b1ff1a3bf318a59c191") in ();
    verify ((keccak p) = r4);
    let r5 = (bytes "0x13592317c972b6cbfb1804935dfdc4ecee477836e3a073172e860fca23b23952") in ();
    verify ((sha3 p) = r5)

  let%entry_point test_mprim2 () =
    verify ((5 << 2) = 20);
    verify ((23 >> 2) = 5)

  let%entry_point test_mprim3 () =
    ()

  let%entry_point test_prim0 () =
    ()

  let%entry_point test_prim1 params =
    verify ({x = 0}.x = 0);
    verify ((set_type_expr 0 int) = 0);
    verify ((fst (1, 2)) = 1);
    verify ((snd (1, 2)) = 2);
    verify ((sum [1; 2; 3; 4; 5]) = 15);
    let two = 2 in ();
    verify ((- two) = (-2));
    verify ((sign 2) = 1);
    verify ((sign 0) = 0);
    verify ((sign (-2)) = (-1));
    verify ((len [1; 2; 3]) = 3);
    verify ((to_int 2) = 2);
    verify ((pack ("a", "b")) = (bytes "0x050707010000000161010000000162"));
    verify ((some ("a", "b")) = (unpack (bytes "0x050707010000000161010000000162") (pair string string)));
    verify ((variant A 42) = (variant A 42));
    verify ((concat ["a"; "bc"; "d"]) = "abcd");
    verify ((concat [bytes "0xab"; bytes "0xcd01"; bytes "0x23"]) = (bytes "0xabcd0123"));
    verify (2 = 2);
    verify ((pack (List.rev [1; 2; 3])) = (pack [3; 2; 1]));
    let s = (Set.make [3; 1; 2]) in ();
    let m = (Map.make [(1, "a"); (2, "b"); (3, "c")]) in ();
    verify ((pack (Map.items m)) = (pack [(1, "a"); (2, "b"); (3, "c")]));
    verify ((pack (Map.keys m)) = (pack [1; 2; 3]));
    verify ((pack (Map.values m)) = (pack ["a"; "b"; "c"]));
    verify ((pack (Set.elements s)) = (pack [1; 2; 3]));
    let t = (ticket "foo" 42) in ();
    verify ((fst (read_ticket_raw t)) = (self_address, "foo", 42));
    let t1 = (ticket "foo" 2) in ();
    let t2 = (ticket "foo" 3) in ();
    match join_tickets_raw (t1, t2) with
      | `None _no_arg ->
        failwith ":("
      | `Some t12 ->
        verify ((fst (read_ticket_raw t12)) = (self_address, "foo", 5))
;
    set_type params.a (contract unit);
    verify ((to_address params.a) = params.b);
    verify ((to_address (implicit_account params.c)) = params.d);
    verify ((pairing_check params.e) = params.f);
    verify ((voting_power params.g) = params.h);
    verify (is_variant "A" (variant A 42));
    verify ((add_seconds params.i 60) = params.k)

  let%entry_point test_prim2 () =
    let f = (apply_lambda 1 (fun xy -> result ((10 * (fst xy)) + (snd xy)))) in ();
    verify ((2 f) = 12);
    verify (2 = 2);
    verify (2 <> 3);
    verify ((true || true) = true);
    verify ((true || false) = true);
    verify ((false || true) = true);
    verify ((false || false) = false);
    verify ((true && true) = true);
    verify ((true && false) = false);
    verify ((false && true) = false);
    verify ((false && false) = false);
    verify ((true ^ true) = false);
    verify ((true ^ false) = true);
    verify ((false ^ true) = true);
    verify ((false ^ false) = false);
    verify ((2 * 3) = 6);
    verify (2 < 3);
    verify (not (2 < 2));
    verify (3 > 2);
    verify (not (2 > 2));
    verify (2 <= 2);
    verify (2 <= 3);
    verify (2 >= 2);
    verify (3 >= 3);
    verify ((299 / 100) = 2);
    verify ((299 % 100) = 99);
    verify ((ediv 299 100) = (some (2, 99)));
    verify ((2 + 3) = 5);
    verify ((2 - 3) = (-1));
    verify ((mul 2 3) = 6);
    verify ((min 2 3) = 2);
    verify ((max 2 3) = 3);
    let s = (Set.make [3; 1; 2]) in ();
    let m = (Map.make [(1, "a"); (2, "b"); (3, "c")]) in ();
    verify (contains 2 s);
    verify ((get_opt m 1) = (some "a"));
    verify ((pack (1 :: [2; 3])) = (pack [1; 2; 3]));
    verify ((len [ticket "foo" 42]) = 1)

  let%entry_point test_prim3 params =
    verify ((pack (range 1 5 1)) = (pack [1; 2; 3; 4]));
    verify ((split_tokens params.a params.b params.c) = params.d);
    let m = (Map.make [(1, "a"); (2, "b"); (3, "c")]) in ();
    verify ((len [Map.update params.b2 params.c2 m]) = 1);
    verify ((len [get_and_update params.b4 params.c4 m]) = 1)

  let%entry_point test_expr () =
    let x = 0 in ();
    verify ((variant Left 2) <> (variant Right 2));
    verify (None <> (some "abc"));
    let s = (Set.make [3; 1; 2]) in ();
    verify ((len s) = 3);
    verify (contains 2 s);
    let m = (Map.make [(1, "a"); (2, "b"); (3, "c")]) in ();
    verify ((len m) = 3);
    verify ((get_opt m 1) = (some "a"));
    let t' = (ticket "foo" 50) in ();
    match split_ticket_raw t' (20, 30) with
      | `None _no_arg ->
        failwith ":("
      | `Some t12 ->
        verify ((fst (read_ticket_raw (fst t12))) = (self_address, "foo", 20))


  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [test_type0; test_type1; test_type2; test_type; test_literal; test_mprim0; test_mprim1; test_mprim2; test_mprim3; test_prim0; test_prim1; test_prim2; test_prim3; test_expr]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())