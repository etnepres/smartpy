open SmartML

module Contract = struct
  let%entry_point call_f params =
    let%var b = data.x + params in
    let%var r = 42 in
    data.x <- r

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x = intOrNat; y = intOrNat}]
      ~storage:[%expr
                 {x = 12;
                  y = 0}]
      [call_f]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())