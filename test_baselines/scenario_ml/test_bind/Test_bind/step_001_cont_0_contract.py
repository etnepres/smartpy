import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TIntOrNat, y = sp.TIntOrNat).layout(("x", "y")))
    self.init(x = 12,
              y = 0)

  @sp.entry_point
  def call_f(self, params):
    b = sp.local("b", self.data.x + params)
    r = sp.local("r", 42)
    self.data.x = r.value

sp.add_compilation_target("test", Contract())