import smartpy as sp

tstorage = sp.TRecord(storedValue = sp.TNat).layout("storedValue")
tparameter = sp.TVariant(divide = sp.TRecord(divisor = sp.TNat).layout("divisor"), double = sp.TUnit, misc = sp.TRecord(bar = sp.TVariant(A = sp.TNat, B = sp.TNat, c = sp.TNever).layout(("A", ("B", "c"))), bar2 = sp.TVariant(A = sp.TNat, B = sp.TVariant(D = sp.TNever, c = sp.TNat).layout(("D", "c"))).layout(("A", "B")), bar3 = sp.TPair(sp.TNat, sp.TNat), divisor = sp.TInt, foo = sp.TIntOrNat, l = sp.TList(sp.TNat)).layout(("bar", ("bar2", ("bar3", ("divisor", ("foo", "l")))))), replace = sp.TRecord(value = sp.TNat).layout("value"), sum = sp.TRecord(bar = sp.TRecord(toto = sp.TNat).layout("toto"), foo = sp.TNat).layout(("bar", "foo"))).layout((("divide", "double"), ("misc", ("replace", "sum"))))
tprivates = { }
tviews = { }
