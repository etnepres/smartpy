import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(storedValue = sp.TNat).layout("storedValue"))
    self.init(storedValue = 123456)

  @sp.entry_point
  def replace(self, params):
    sp.set_type(params, sp.TRecord(value = sp.TNat).layout("value"))
    sp.set_type(params, sp.TRecord(value = sp.TNat).layout("value"))
    sp.set_type(params.value, sp.TNat)
    self.data.storedValue = sp.set_type_expr(params.value, sp.TNat)

  @sp.entry_point
  def sum(self, params):
    sp.set_type(params, sp.TRecord(bar = sp.TRecord(toto = sp.TNat).layout("toto"), foo = sp.TNat).layout(("bar", "foo")))
    sp.set_type(params.foo, sp.TNat)
    sp.set_type(params.bar, sp.TRecord(toto = sp.TNat).layout("toto"))
    sp.set_type(params.bar.toto, sp.TNat)
    self.data.storedValue = params.foo + params.bar.toto

  @sp.entry_point
  def double(self):
    self.data.storedValue *= 2
    pass

  @sp.entry_point
  def divide(self, params):
    sp.set_type(params, sp.TRecord(divisor = sp.TNat).layout("divisor"))
    sp.set_type(params, sp.TRecord(divisor = sp.TNat).layout("divisor"))
    sp.verify(params.divisor > 5, 'params.divisor too small')
    sp.verify(params.divisor > 6, 42)
    self.data.storedValue //= params.divisor

  @sp.entry_point
  def misc(self, params):
    sp.set_type(params.divisor, sp.TInt)
    sp.set_type(params.bar, sp.TVariant(A = sp.TNat, B = sp.TNat, c = sp.TNever).layout(("A", ("B", "c"))))
    sp.set_type(params.bar2, sp.TVariant(A = sp.TNat, B = sp.TVariant(D = sp.TNever, c = sp.TNat).layout(("D", "c"))).layout(("A", "B")))
    sp.set_type(params.bar3, sp.TPair(sp.TNat, sp.TNat))
    sp.verify(sp.fst(params.bar3) == sp.snd(params.bar3), 'not equal')
    sp.verify(params.divisor > 5, 'params.divisor too small')
    sp.verify(params.divisor > 6, 42)
    sp.trace(params.divisor)
    with params.bar.match_cases() as arg:
      with arg.match('A') as x:
        sp.failwith(1 + x.value)
      with arg.match('B') as x:
        self.data.storedValue = x.value
      with arg.match('c') as n:
        sp.never(n.value)

    sp.if (2 + params.foo) < 42:
      sp.failwith(params)
    sp.if (1 + params.foo) < 14:
      sp.if (2 + params.foo) < 42:
        sp.failwith(params)
      sp.else:
        sp.failwith('abc')
    sp.while self.data.storedValue < 15:
      self.data.storedValue = 2 * self.data.storedValue
    sp.for x in params.l:
      self.data.storedValue += x.value

  @sp.entry_point(private = True)
  def another(self):
    self.data.storedValue *= 42

  @sp.entry_point(private = True)
  def another_with_parameter(self, params):
    self.data.storedValue *= params

sp.add_compilation_target("test", Contract())