open SmartML

module Contract = struct
  let%entry_point addTime () =
    data.timestamp <- add_seconds data.timestamp 1;
    data.timestamp <- add_seconds data.timestamp (1 * 60);
    data.timestamp <- add_seconds data.timestamp (1 * 3600);
    data.timestamp <- add_seconds data.timestamp (1 * 86400);
    verify ((now - data.timestamp) > 0) ~msg:"Sp.now.minus(this.storage.timestamp) > 0"

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {timestamp = timestamp}]
      ~storage:[%expr
                 {timestamp = timestamp 1627302929}]
      [addTime]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())