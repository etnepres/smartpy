open SmartML

module Contract = struct
  let%entry_point alive () =
    verify (sender = data.owner);
    data.timeout <- some (add_seconds now (((3600 * 24) * 365) * 2))

  let%entry_point withdraw params =
    set_type params.to_ address;
    set_type params.amount mutez;
    verify (sender = data.owner);
    verify (is_some data.timeout);
    let%mutable contract = (open_some (contract unit params.to_ , entry_point='default') ~message:"Invalid Interface") in ();
    transfer () params.amount contract;
    data.timeout <- some (add_seconds now (((3600 * 24) * 365) * 2))

  let%entry_point heirWithdraw params =
    set_type params.to_ address;
    set_type params.amount mutez;
    verify (sender = data.heir);
    verify ((not (is_some data.timeout)) || (now > (open_some data.timeout)));
    let%mutable contract = (open_some (contract unit params.to_ , entry_point='default') ~message:"Invalid Interface") in ();
    transfer () params.amount contract;
    data.timeout <- None

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {heir = address; owner = address; timeout = option timestamp}]
      ~storage:[%expr
                 {heir = address "tz1L3Pgin3YSeqZJb6UF9Fis9LBjp3L1YusM";
                  owner = address "tz1M43YV5vVt7HALGJeWgQ1GRQfiGExA1Jbf";
                  timeout = Some(timestamp 1672531200)}]
      [alive; withdraw; heirWithdraw]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())