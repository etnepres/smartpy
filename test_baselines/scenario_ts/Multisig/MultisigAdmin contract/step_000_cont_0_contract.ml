open SmartML

module Contract = struct
  let%entry_point proposal params =
    set_type params (`external (list {actions = bytes; target = address}) + `internal (list (`changeQuorum nat + `changeSigners (`added (list {address = address; publicKey = key}) + `removed (set address)))));
    verify (contains sender data.signers) ~msg:"Not_Signer";
    let%mutable signerLastProposalId = (Map.get data.signers sender).lastProposalId in ();
    if signerLastProposalId <> None then
      Set.remove data.activeProposals (open_some signerLastProposalId);
    data.lastProposalId <- data.lastProposalId + 1;
    let%mutable proposalId = data.lastProposalId in ();
    Set.add data.activeProposals proposalId;
    Map.set data.proposals proposalId {actions = params; endorsements = (Set.make [sender]); initiator = sender; startedAt = now};
    (Map.get data.signers sender).lastProposalId <- some proposalId;
    if data.quorum < 2 then
      let%mutable var_0 = (params self.onApproved) in ()

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {activeProposals = set nat; lastProposalId = nat; proposals = big_map nat {actions = `external (list {actions = bytes; target = address}) + `internal (list (`changeQuorum nat + `changeSigners (`added (list {address = address; publicKey = key}) + `removed (set address)))); endorsements = set address; initiator = address; startedAt = timestamp}; quorum = nat; signers = map address {lastProposalId = option nat; publicKey = key}}]
      ~storage:[%expr
                 {activeProposals = Set.make([]);
                  lastProposalId = 0;
                  proposals = Map.make [];
                  quorum = 3;
                  signers = Map.make [(address "tz1L3Pgin3YSeqZJb6UF9Fis9LBjp3L1YusM", {lastProposalId = None; publicKey = key "edpkub2iNvM3KnaccBaHujEz93Naj3ySBvAoEFLELjaLXk21otyTyT"}); (address "tz1M43YV5vVt7HALGJeWgQ1GRQfiGExA1Jbf", {lastProposalId = None; publicKey = key "edpkvF1HmWAdtpqPAc27pNRpNaYoF3vp9cgWXbhmC45RHN1JWh1f4z"})]}]
      [proposal]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())