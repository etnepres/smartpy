open SmartML

module Contract = struct
  let%entry_point set params =
    set_type params {key = string; value = nat};
    Map.set data.map params.key params.value;
    data.keys <- Map.keys data.map;
    data.values <- Map.values data.map;
    data.entries <- Map.items data.map;
    List.iter (fun x ->
      data.value <- data.value + x.value
    ) data.entries

  let%entry_point remove params =
    set_type params string;
    let%mutable value = (Map.get data.map params) in ();
    if ((contains params data.map) && (value = 1)) && ((len data.map) = 3) then
      Map.delete data.map params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {entries = list {key = string; value = nat}; keys = list string; map = map string nat; value = nat; values = list nat}]
      ~storage:[%expr
                 {entries = [];
                  keys = [];
                  map = Map.make [];
                  value = 0;
                  values = []}]
      [set; remove]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())