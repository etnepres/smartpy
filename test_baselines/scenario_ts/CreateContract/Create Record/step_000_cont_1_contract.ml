open SmartML

module Contract = struct
  let%entry_point create params =
    set_type params string;
    let%mutable var_0 = create contract ... in ();
    operations <- var_0.operation :: operations;
    let%mutable var_1 = create contract ... in ();
    operations <- var_1.operation :: operations;
    data.value <- some var_1.address

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {value = option address}]
      ~storage:[%expr
                 {value = None}]
      [create]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())