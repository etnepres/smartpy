open SmartML

module Contract = struct
  let%entry_point updateValue params =
    set_type params string;
    data.value <- params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {value = string}]
      ~storage:[%expr
                 {value = "Hello World"}]
      [updateValue]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())