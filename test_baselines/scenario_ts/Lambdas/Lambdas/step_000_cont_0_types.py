import smartpy as sp

tstorage = sp.TRecord(decrement = sp.TLambda(sp.TNat, sp.TInt), increment = sp.TLambda(sp.TNat, sp.TNat), storedValue = sp.TNat, transformer = sp.TLambda(sp.TNat, sp.TNat)).layout(("decrement", ("increment", ("storedValue", "transformer"))))
tparameter = sp.TVariant(default = sp.TUnit, store = sp.TRecord(transformer2 = sp.TLambda(sp.TNat, sp.TNat), value = sp.TNat).layout(("transformer2", "value"))).layout(("default", "store"))
tprivates = { "transformer4": sp.TLambda(sp.TNat, sp.TNat), "transformer5": sp.TLambda(sp.TNat, sp.TNat, with_storage="read-only"), "callback": sp.TLambda(sp.TUnit, sp.TUnit, with_operations=True) }
tviews = { }
