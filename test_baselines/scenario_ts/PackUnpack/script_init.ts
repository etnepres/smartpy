type Subject = {
    value: TNat;
};

interface TStorage {
    packed: TOption<TBytes>;
    unpacked: TOption<Subject>;
}

@Contract
export class PackUnpack {
    storage: TStorage = {
        packed: Sp.none,
        unpacked: Sp.none,
    };

    @EntryPoint
    pack(subject: Subject): void {
        this.storage.packed = Sp.some(Sp.pack(subject));
    }

    @EntryPoint
    unpack(): void {
        if (this.storage.packed.isSome()) {
            this.storage.unpacked = Sp.unpack<Subject>(this.storage.packed.openSome('this.storage.packed is None'));
        }
    }
}

Dev.test({ name: 'PackUnpack' }, () => {
    const c1 = Scenario.originate(new PackUnpack());
    Scenario.transfer(c1.pack({ value: 1 }));
    Scenario.transfer(c1.unpack());
});

Dev.compileContract('compile_contract', new PackUnpack());
