open SmartML

module Contract = struct
  let%entry_point replace params =
    set_type params nat;
    data.storedValue <- params

  let%entry_point add params =
    set_type params.value nat;
    set_type params.value2 nat;
    data.storedValue <- params.value + params.value2

  let%entry_point fail () =
    failwith "FAILED"

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {storedValue = nat}]
      ~storage:[%expr
                 {storedValue = 1}]
      [replace; add; fail]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())