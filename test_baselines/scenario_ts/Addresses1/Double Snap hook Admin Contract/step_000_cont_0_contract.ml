open SmartML

module Contract = struct
  let%entry_point add params =
    set_type params (set address);
    verify (contains sender data.admins);
    data.admins <- params;
    List.iter (fun admin ->
      Set.add data.admins admin
    ) (Set.elements params)

  let%entry_point remove params =
    set_type params (set address);
    verify (contains sender data.admins);
    List.iter (fun admin ->
      verify (admin = self_address);
      Set.remove data.admins admin
    ) (Set.elements params)

  let%entry_point changeAdmins params =
    set_type params.remove (option (set address));
    set_type params.add (option (set address));
    verify (contains sender data.admins);
    if is_some params.remove then
      List.iter (fun admin ->
        verify (admin = self_address);
        Set.remove data.admins admin
      ) (Set.elements (open_some params.remove));
    if is_some params.add then
      List.iter (fun admin ->
        Set.add data.admins admin
      ) (Set.elements (open_some params.add))

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {admins = set address}]
      ~storage:[%expr
                 {admins = Set.make([address "tz1aTgF2c3vyrk2Mko1yzkJQGAnqUeDapxxm"])}]
      [add; remove; changeAdmins]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())