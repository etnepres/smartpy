open SmartML

module Contract = struct
  let%entry_point testDelegate params =
    set_type params key_hash;
    if (some params) <> data.currentDelegate then
      (
        set_delegate (some params);
        set_delegate data.currentDelegate
      )

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {currentDelegate = option key_hash; nominatedDelegates = set key_hash}]
      ~storage:[%expr
                 {currentDelegate = Some(key_hash "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w");
                  nominatedDelegates = Set.make([])}]
      [testDelegate]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())