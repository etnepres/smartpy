open SmartML

module Contract = struct
  let%entry_point for () =
    let%mutable i = 0 in ();
    while i < 10 do
      i <- i + 1;
      i <- i + 1
    done

  let%entry_point while () =
    let%mutable i = 1 in ();
    while i = 1 do
      i <- i + 1
    done

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [for; while]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())