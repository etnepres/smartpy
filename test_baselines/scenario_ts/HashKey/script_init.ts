@Contract
export class HashKey {
    @EntryPoint
    ep(key: TKey): void {
        Sp.verify(('tz1Xv78KMT7cHyVDLi9RmQP1KuWULHDafZdK' as TKey_hash) === Sp.hashKey(key));
    }
}

Dev.test({ name: 'HashKey' }, () => {
    const c1 = Scenario.originate(new HashKey());
    Scenario.transfer(c1.ep('edpktx799pgw7M4z8551URER52VcENNCSZwE9f9cst4v6h5vCrQmJE'));
});

Dev.compileContract('HashKey', new HashKey());
