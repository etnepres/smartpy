import * as LocalModule from './test_ImportsNested';
import * as RemoteModule from 'https://smartpy.io/releases/20211106-51985c20a6a77e31b6de0d3b0400cccce74e38ad/typescript/templates/Minimal.ts';

class LocalImport extends LocalModule.Template {
    storage: LocalModule.StorageType = LocalModule.InitialStorage;
}

class RemoteImport extends RemoteModule.Minimal {
    @EntryPoint
    setValue(value: TNat) {
        this.storage.value = value;
    }
}

Dev.test({ name: 'LocalImport' }, () => {
    const c1 = Scenario.originate(new LocalModule.Template());
    Scenario.transfer(c1.ep(1));
    const c2 = Scenario.originate(new LocalImport());
    Scenario.transfer(c2.ep(1));
});

Dev.test({ name: 'RemoteImport' }, () => {
    const c1 = Scenario.originate(new RemoteImport());
    Scenario.transfer(c1.setValue(1));
});

Dev.compileContract('compile_LocalModule.Template', new LocalModule.Template());
Dev.compileContract('compile_LocalImport', new LocalImport());
Dev.compileContract('compile_RemoteImport', new RemoteImport());
