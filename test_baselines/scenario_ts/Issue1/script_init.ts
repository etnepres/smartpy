export class C1 {}

export class C2 {
    constructor(public storage: TAddress) {}

    @EntryPoint
    unpack(): void {
        this.storage = Sp.unpack<TAddress>(Sp.pack('tz1' as TAddress)).openSome('Could not extract unpacked data.');
    }
}

Dev.test({ name: 'Issue1' }, () => {
    const c1 = Scenario.originate(new C1(), { initialBalance: 1000000 });
    Scenario.originate(new C2(c1.address));
});

Dev.compileContract('Issue1', new C2('tz1'));
