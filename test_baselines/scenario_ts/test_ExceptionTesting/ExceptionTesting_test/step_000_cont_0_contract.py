import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TInt)
    self.init(1)

  @sp.entry_point
  def ep(self, params):
    sp.set_type(params, sp.TInt)
    sp.verify(params < 5, 'This is false: param > 5')

sp.add_compilation_target("test", Contract())