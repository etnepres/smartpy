open SmartML

module Contract = struct
  let%entry_point ep params =
    set_type params int;
    verify (params < 5) ~msg:"This is false: param > 5"

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ int]
      ~storage:[%expr 1]
      [ep]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())