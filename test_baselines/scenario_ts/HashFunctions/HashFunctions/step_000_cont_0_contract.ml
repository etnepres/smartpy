open SmartML

module Contract = struct
  let%entry_point ep params =
    set_type params string;
    let%mutable bytes = (pack params) in ();
    data <- some {blake2b = (blake2b bytes); keccak = (keccak bytes); sha256 = (sha256 bytes); sha3 = (sha3 bytes); sha512 = (sha512 bytes)}

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ option {blake2b = bytes; keccak = bytes; sha256 = bytes; sha3 = bytes; sha512 = bytes}]
      ~storage:[%expr None]
      [ep]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())