import smartpy as sp

tstorage = sp.TRecord(map = sp.TBigMap(sp.TString, sp.TNat)).layout("map")
tparameter = sp.TVariant(remove = sp.TString, set = sp.TRecord(key = sp.TString, value = sp.TNat).layout(("key", "value"))).layout(("remove", "set"))
tprivates = { }
tviews = { }
