open SmartML

module Contract = struct
  let%entry_point changeAdmins params =
    set_type params.remove (option (set address));
    set_type params.add (option (set address));
    verify (contains sender data.admins) ~msg:"onlyAdmin";
    if is_some params.remove then
      List.iter (fun admin ->
        verify (admin <> sender) ~msg:"SelfRemoving";
        Set.remove data.admins admin
      ) (Set.elements (open_some params.remove));
    if is_some params.add then
      List.iter (fun admin ->
        Set.add data.admins admin
      ) (Set.elements (open_some params.add))

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {admins = set address}]
      ~storage:[%expr
                 {admins = Set.make([address "tz1aTgF2c3vyrk2Mko1yzkJQGAnqUeDapxxm"])}]
      [changeAdmins]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())