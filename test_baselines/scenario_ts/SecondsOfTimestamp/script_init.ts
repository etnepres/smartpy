interface StorageSpec {
    value: TNat;
}

@Contract
export class SecondsOfTypestamp {
    storage: StorageSpec = {
        value: 1,
    };

    @EntryPoint
    ep(value: TTimestamp): void {
        this.storage.value = (110 as TTimestamp).toSeconds();
        this.storage.value = value.toSeconds();
    }
}

Dev.test({ name: 'SecondsOfTypestamp' }, () => {
    const c1 = Scenario.originate(new SecondsOfTypestamp());
    Scenario.transfer(c1.ep(1));
    Scenario.transfer(c1.ep('2021-09-02T16:35:09.936Z'));
});

Dev.compileContract('toSeconds', new SecondsOfTypestamp());
