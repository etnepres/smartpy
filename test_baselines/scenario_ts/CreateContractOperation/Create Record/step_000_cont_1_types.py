import smartpy as sp

tstorage = sp.TRecord(value = sp.TOption(sp.TAddress)).layout("value")
tparameter = sp.TVariant(createWithoutOrigination = sp.TString).layout("createWithoutOrigination")
tprivates = { }
tviews = { }
