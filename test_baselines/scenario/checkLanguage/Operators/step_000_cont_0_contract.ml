open SmartML

module Contract = struct
  let%entry_point run () =
    List.iter (fun test ->
      name, loc_op, loc_tests = match_tuple(test, "name", "loc_op", "loc_tests")
      List.iter (fun test_ ->
        x, y, res = match_tuple(test_, "x", "y", "res")
        let%mutable z = ((x, y) loc_op) in ();
        verify (z = res) ~msg:(name, (x, (y, z, res)))
      ) loc_tests
    ) [("+", fun _x0 -> result ((fst _x0) + (snd _x0)), [(11, 22, 33); (11, 28, 39); (11, -47, -36); (11, -2, 9); (123, 22, 145); (123, 28, 151); (123, -47, 76); (123, -2, 121); (-15, 22, 7); (-15, 28, 13); (-15, -47, -62); (-15, -2, -17)]); ("-", fun _x1 -> result ((0 + (fst _x1)) - (snd _x1)), [(11, 22, -11); (11, 28, -17); (11, -47, 58); (11, -2, 13); (123, 22, 101); (123, 28, 95); (123, -47, 170); (123, -2, 125); (-15, 22, -37); (-15, 28, -43); (-15, -47, 32); (-15, -2, -13)]); ("*", fun _x2 -> result ((fst _x2) * (snd _x2)), [(11, 22, 242); (11, 28, 308); (11, -47, -517); (11, -2, -22); (123, 22, 2706); (123, 28, 3444); (123, -47, -5781); (123, -2, -246); (-15, 22, -330); (-15, 28, -420); (-15, -47, 705); (-15, -2, 30)])];
    List.iter (fun test ->
      name, loc_op, loc_tests = match_tuple(test, "name", "loc_op", "loc_tests")
      List.iter (fun test_ ->
        x, y, res = match_tuple(test_, "x", "y", "res")
        let%mutable z = ((x, y) loc_op) in ();
        verify (z = res) ~msg:(name, (x, (y, z, res)))
      ) loc_tests
    ) [("%", fun _x3 -> result ((fst _x3) % (snd _x3)), [(11, 22, 11); (11, 28, 11); (123, 22, 13); (123, 28, 11)]); ("//", fun _x4 -> result ((fst _x4) / (snd _x4)), [(11, 22, 0); (11, 28, 0); (123, 22, 5); (123, 28, 4)])];
    List.iter (fun test ->
      name, loc_op, loc_tests = match_tuple(test, "name", "loc_op", "loc_tests")
      List.iter (fun test_ ->
        x, y, res = match_tuple(test_, "x", "y", "res")
        let%mutable z = ((x, y) loc_op) in ();
        verify (z = res) ~msg:(name, (x, (y, z, res)))
      ) loc_tests
    ) [("&", fun _x5 -> result ((fst _x5) && (snd _x5)), [(true, true, true); (true, false, false); (false, true, false); (false, false, false)]); ("|", fun _x6 -> result ((fst _x6) || (snd _x6)), [(true, true, true); (true, false, true); (false, true, true); (false, false, false)])]

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [run]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())