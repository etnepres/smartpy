open SmartML

module Contract = struct
  let%entry_point ep () =
    data.x <- "abc"

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x = bounded(["abc", "def"], t=string)}]
      ~storage:[%expr
                 {x = "abc"}]
      [ep]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())