open SmartML

module Contract = struct
  let%entry_point put_int params =
    set_type params int

  let%entry_point test_expr_other () =
    let%mutable compute_test_eval_order_312 = (open_some (("A", some 1) self.f_option) ~message:(("B", 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_314 = (open_some (some 1) ~message:(("B", 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_317 = (Map.get (("A", Map.make [(1, 2)]) self.f_map) (("B", 1) self.f_int)) in ();
    verify (data.x = "B");
    let%mutable compute_test_eval_order_319 = (("A", 1) self.f_int, ("B", 1) self.f_int) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_321 = {a = (("A", 1) self.f_int); b = (("B", 1) self.f_int)} in ();
    let%mutable compute_test_eval_order_323 = [("A", 1) self.f_int; ("B", 1) self.f_int] in ();
    let%mutable compute_test_eval_order_325 = (Set.make [("A", 1) self.f_int; ("B", 1) self.f_int]) in ();
    let%mutable compute_test_eval_order_327 = (Map.make [(("A", 1) self.f_int, ("B", 1) self.f_int); (("C", 1) self.f_int, ("D", 1) self.f_int)]) in ();
    let%mutable compute_test_eval_order_329 = (Map.make [(1, ("B", 1) self.f_int); (("C", 1) self.f_int, ("D", 1) self.f_int)]) in ();
    let%mutable compute_test_eval_order_332 = (map (("B", fun _x22 -> result _x22) self.f_lambda) (("A", []) self.f_list)) in ();
    verify (data.x = "A");
    let%mutable create_contract_test_eval_order_336 = create contract ... in ();
    let%mutable compute_test_eval_order_336 = create_contract_test_eval_order_336 in ();
    verify (data.x = "A");
    let%mutable create_contract_test_eval_order_338 = create contract ... in ();
    let%mutable compute_test_eval_order_338 = create_contract_test_eval_order_338 in ();
    verify (data.x = "B");
    let%mutable compute_test_eval_order_342 = (slice (("A", "abc") self.f_string) (("B", 1) self.f_nat) (("C", 1) self.f_nat)) in ();
    verify (data.x = "B");
    let%mutable compute_test_eval_order_344 = (slice (("A", "abc") self.f_string) 0 (("C", 1) self.f_nat)) in ();
    verify (data.x = "C");
    let%mutable compute_test_eval_order_347 = (transfer_operation (("A", 1) self.f_int) (("B", mutez 1) self.f_mutez) (("C", self_entry_point "put_int") self.f_contract)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_349 = (transfer_operation 0 (("B", mutez 1) self.f_mutez) (("C", self_entry_point "put_int") self.f_contract)) in ();
    verify (data.x = "B")

  let%entry_point test_mprim2 () =
    let%mutable compute_test_eval_order_258 = ((("A", 1) self.f_nat) << (("B", 1) self.f_nat)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_261 = ((("A", 1) self.f_nat) >> (("B", 1) self.f_nat)) in ();
    verify (data.x = "A")

  let%entry_point test_mprim3 params =
    set_type params.chest_key chest_key;
    let%mutable compute_test_eval_order_277 = (open_chest (("A", params.chest_key) self.f_chest_key) (("B", chest "0xbcf89e8a80d99cd49690dda59accc4d381c1a3facbacc1a3f9aab5e7ecdc8db6b0928eed84a7e5d9958e96b5b1f3c8ddd9b98dd394f9bafaafad85a2d7cbffeec1e280d5c6ccbab3caddb5ced3c1d39c82cea0cc99b7f788bbca85eeb1badd91fdc8e1e1e9a6cddca7bdef8faaf6acf2aeef8ccbd3d987f6d0ecf9c8c9818c92eb8a81e385fff9e8c4ad96d6e7bad6a480ec83f3cb84bedeac82baafa1b88bfd90968b9ee7b2d8aabef9d8e0d9a9c8a5a8859a8db5e6d496de95cfd2cfc4dcfae3edf9f1d5faee998694ed8aada2a7d4b88aecc8acda98d6f69cfb8df3a5ca80b48d91aa9bbbf68f918998ddc5d3cc82d3f0ce81869fd9edb8b0989dcfa5e9b59db18dd099c7b8bb848586afc7c8888f86d2c6988eeee5e5fbfc9de8a7f19e84ebc2b687049390ebd5cecb86eecfe2bef994f9a7c3eabeabdda0b4b7b59381a8a4c3d7c1b382bcf6e1adb2e3c6ce9ca5e0c0a0beb999f4eacc89cf858fb282b294d99ec2daccd9cdcc98a98b8cb5b0bbf9ed80b0f6ccd2a994f3a1faafbbacf7eebbec9399bfdfd1f0da9ee4aa9dcb8b9c85fcab87bd86b98fd7cc9a8cb2e3cc93ef86df83e5ce9bbda8c7d4cad0e4b893fde8e18b9cf283f9829f9ab8f48da8b0f1b5c8f3a1d2a1a6e5c1fec0abb09eb9b0c2c3ae9f9df6c6a7bad396f7b3b1fc90e490a4f2a5f599fedbc8b3b297a99db3b285a3f0acefe0c8d4cec08195af8098d0f4b8d488bdacd7e8effea5cdaf8dd1ccdbe4fee59eb7e1cebff2eb839b8f81d598abc798d5bdcea59bfcbaf89fbdbbcb8182a3eea1f4f5dccafcb4ddd4ff83d3879edef5d3ca06d2f1146cf4c25faf0e432bf540b4de74fae3cc68e0bc57c700000021cf744e778f4d5e220d4be7310077e6735795b6c90bbe2bdcdd9b686e3c71f833f2") self.f_chest) (("C", 1) self.f_nat)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_279 = (open_chest params.chest_key (("B", chest "0xbcf89e8a80d99cd49690dda59accc4d381c1a3facbacc1a3f9aab5e7ecdc8db6b0928eed84a7e5d9958e96b5b1f3c8ddd9b98dd394f9bafaafad85a2d7cbffeec1e280d5c6ccbab3caddb5ced3c1d39c82cea0cc99b7f788bbca85eeb1badd91fdc8e1e1e9a6cddca7bdef8faaf6acf2aeef8ccbd3d987f6d0ecf9c8c9818c92eb8a81e385fff9e8c4ad96d6e7bad6a480ec83f3cb84bedeac82baafa1b88bfd90968b9ee7b2d8aabef9d8e0d9a9c8a5a8859a8db5e6d496de95cfd2cfc4dcfae3edf9f1d5faee998694ed8aada2a7d4b88aecc8acda98d6f69cfb8df3a5ca80b48d91aa9bbbf68f918998ddc5d3cc82d3f0ce81869fd9edb8b0989dcfa5e9b59db18dd099c7b8bb848586afc7c8888f86d2c6988eeee5e5fbfc9de8a7f19e84ebc2b687049390ebd5cecb86eecfe2bef994f9a7c3eabeabdda0b4b7b59381a8a4c3d7c1b382bcf6e1adb2e3c6ce9ca5e0c0a0beb999f4eacc89cf858fb282b294d99ec2daccd9cdcc98a98b8cb5b0bbf9ed80b0f6ccd2a994f3a1faafbbacf7eebbec9399bfdfd1f0da9ee4aa9dcb8b9c85fcab87bd86b98fd7cc9a8cb2e3cc93ef86df83e5ce9bbda8c7d4cad0e4b893fde8e18b9cf283f9829f9ab8f48da8b0f1b5c8f3a1d2a1a6e5c1fec0abb09eb9b0c2c3ae9f9df6c6a7bad396f7b3b1fc90e490a4f2a5f599fedbc8b3b297a99db3b285a3f0acefe0c8d4cec08195af8098d0f4b8d488bdacd7e8effea5cdaf8dd1ccdbe4fee59eb7e1cebff2eb839b8f81d598abc798d5bdcea59bfcbaf89fbdbbcb8182a3eea1f4f5dccafcb4ddd4ff83d3879edef5d3ca06d2f1146cf4c25faf0e432bf540b4de74fae3cc68e0bc57c700000021cf744e778f4d5e220d4be7310077e6735795b6c90bbe2bdcdd9b686e3c71f833f2") self.f_chest) (("C", 1) self.f_nat)) in ();
    verify (data.x = "B");
    verify (check_signature (("A", params.key) self.f_key) (("B", params.signature) self.f_signature) (("C", bytes "0x00") self.f_bytes));
    verify (data.x = "A");
    verify (check_signature params.key (("B", params.signature) self.f_signature) (("C", bytes "0x00") self.f_bytes));
    verify (data.x = "B")

  let%entry_point test_prim2 () =
    let%mutable ticket_136 = (ticket 1 1) in ();
    let%mutable compute_test_eval_order_144 = (get_opt (("A", Map.make []) self.f_map) (("B", 1) self.f_int)) in ();
    verify (data.x = "B");
    let%mutable compute_test_eval_order_146 = (contains (("B", 1) self.f_int) (("A", Map.make []) self.f_map)) in ();
    verify (data.x = "B");
    let%mutable compute_test_eval_order_148 = ((("B", 1) self.f_int) (("A", fun _x23 -> result _x23) self.f_lambda)) in ();
    verify (data.x = "B");
    let%mutable compute_test_eval_order_150 = (apply_lambda (("B", 1) self.f_int) (("A", fun _x24 -> result 0) self.f_lambda2)) in ();
    verify (data.x = "B");
    let%mutable compute_test_eval_order_152 = ((("A", 1) self.f_int) :: (("B", []) self.f_list)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_154 = (add_seconds (("A", timestamp 1) self.f_timestamp) (("B", 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable ticket_156 = (ticket (("A", 1) self.f_int) (("B", 1) self.f_nat)) in ();
    let%mutable compute_test_eval_order_156 = ticket_156 in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_161 = ((("A", 1) self.f_int) + (("B", 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_163 = ((("A", 1) self.f_int) * (("B", 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_165 = ((("A", 1) self.f_int) - (("B", 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_167 = ((("A", 1) self.f_int) + (("B", 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_169 = ((("A", 1) self.f_nat) / (("B", 1) self.f_nat)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_171 = ((("A", 1) self.f_int) = (("B", 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_173 = ((("A", 1) self.f_int) <> (("B", 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_175 = ((("A", 1) self.f_int) < (("B", 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_177 = ((("A", 1) self.f_int) <= (("B", 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_179 = ((("A", 1) self.f_int) > (("B", 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_181 = ((("A", 1) self.f_int) >= (("B", 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_183 = ((("A", 1) self.f_int) % (("B", 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_185 = ((("A", 1) self.f_nat) ^ (("B", 1) self.f_nat)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_187 = (min (("A", 1) self.f_int) (("B", 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_189 = (max (("A", 1) self.f_int) (("B", 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_191 = (ediv (("A", 1) self.f_int) (("B", 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_194 = ((("A", false) self.f_bool) || (("B", false) self.f_bool)) in ();
    verify (data.x = "B");
    let%mutable compute_test_eval_order_196 = ((("A", false) self.f_bool) || (("B", true) self.f_bool)) in ();
    verify (data.x = "B");
    let%mutable compute_test_eval_order_198 = ((("A", true) self.f_bool) || (("B", false) self.f_bool)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_200 = ((("A", true) self.f_bool) || (("B", true) self.f_bool)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_203 = ((("A", false) self.f_bool) && (("B", false) self.f_bool)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_205 = ((("A", false) self.f_bool) && (("B", true) self.f_bool)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_207 = ((("A", true) self.f_bool) && (("B", false) self.f_bool)) in ();
    verify (data.x = "B");
    let%mutable compute_test_eval_order_209 = ((("A", true) self.f_bool) && (("B", true) self.f_bool)) in ();
    verify (data.x = "B")

  let%entry_point test_prim3 () =
    let%mutable compute_test_eval_order_223 = (split_tokens (("A", mutez 1) self.f_mutez) (("B", 1) self.f_nat) (("C", 1) self.f_nat)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_225 = (split_tokens (tez 0) (("B", 1) self.f_nat) (("C", 1) self.f_nat)) in ();
    verify (data.x = "B");
    let%mutable compute_test_eval_order_228 = (range (("A", 1) self.f_nat) (("B", 1) self.f_nat) (("C", 1) self.f_nat)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_232 = (Map.update (("B", 1) self.f_int) (("C", some 1) self.f_option) (("A", Map.make []) self.f_map)) in ();
    verify (data.x = "B");
    let%mutable compute_test_eval_order_234 = (Map.update 0 (("C", some 1) self.f_option) (("A", Map.make []) self.f_map)) in ();
    verify (data.x = "C");
    match_pair_test_eval_order_238_fst, match_pair_test_eval_order_238_snd = match_tuple(get_and_update (("B", 1) self.f_int) (("C", some 1) self.f_option) (("A", Map.make []) self.f_map), "match_pair_test_eval_order_238_fst", "match_pair_test_eval_order_238_snd")
    let%mutable compute_test_eval_order_238 = (match_pair_test_eval_order_238_fst, match_pair_test_eval_order_238_snd) in ();
    verify (data.x = "B");
    match_pair_test_eval_order_240_fst, match_pair_test_eval_order_240_snd = match_tuple(get_and_update 0 (("C", some 1) self.f_option) (("A", Map.make []) self.f_map), "match_pair_test_eval_order_240_fst", "match_pair_test_eval_order_240_snd")
    let%mutable compute_test_eval_order_240 = (match_pair_test_eval_order_240_fst, match_pair_test_eval_order_240_snd) in ();
    verify (data.x = "C");
    let%mutable compute_test_eval_order_243 = (eif (("A", true) self.f_bool) (("B", 1) self.f_int) (("C", 1) self.f_int)) in ();
    verify (data.x = "B");
    let%mutable compute_test_eval_order_245 = (eif (("A", true) self.f_bool) 0 (("C", 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_248 = (eif (("A", false) self.f_bool) (("B", 1) self.f_int) (("C", 1) self.f_int)) in ();
    verify (data.x = "C");
    let%mutable compute_test_eval_order_250 = (eif (("A", false) self.f_bool) (("B", 1) self.f_int) 0) in ();
    verify (data.x = "A")

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {sub = address; x = string}]
      ~storage:[%expr
                 {sub = address "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1";
                  x = ""}]
      [put_int; test_expr_other; test_mprim2; test_mprim3; test_prim2; test_prim3]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())