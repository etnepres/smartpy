import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(sub = sp.TAddress, x = sp.TString).layout(("sub", "x")))
    self.init(sub = sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1'),
              x = '')

  @sp.entry_point
  def put_int(self, params):
    sp.set_type(params, sp.TInt)

  @sp.entry_point
  def test_expr_other(self):
    compute_test_eval_order_312 = sp.local("compute_test_eval_order_312", self.f_option(('A', sp.some(1))).open_some(message = self.f_int(('B', 1))))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_314 = sp.local("compute_test_eval_order_314", sp.some(1).open_some(message = self.f_int(('B', 1))))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_317 = sp.local("compute_test_eval_order_317", self.f_map(('A', {1 : 2}))[self.f_int(('B', 1))])
    sp.verify(self.data.x == 'B')
    compute_test_eval_order_319 = sp.local("compute_test_eval_order_319", (self.f_int(('A', 1)), self.f_int(('B', 1))))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_321 = sp.local("compute_test_eval_order_321", sp.record(a = self.f_int(('A', 1)), b = self.f_int(('B', 1))))
    compute_test_eval_order_323 = sp.local("compute_test_eval_order_323", sp.list([self.f_int(('A', 1)), self.f_int(('B', 1))]))
    compute_test_eval_order_325 = sp.local("compute_test_eval_order_325", sp.set([self.f_int(('A', 1)), self.f_int(('B', 1))]))
    compute_test_eval_order_327 = sp.local("compute_test_eval_order_327", {self.f_int(('A', 1)) : self.f_int(('B', 1)), self.f_int(('C', 1)) : self.f_int(('D', 1))})
    compute_test_eval_order_329 = sp.local("compute_test_eval_order_329", {1 : self.f_int(('B', 1)), self.f_int(('C', 1)) : self.f_int(('D', 1))})
    compute_test_eval_order_332 = sp.local("compute_test_eval_order_332", self.f_list(('A', sp.list([]))).map(self.f_lambda(('B', sp.build_lambda(lambda _x22: _x22)))))
    sp.verify(self.data.x == 'A')
    create_contract_test_eval_order_336 = sp.local("create_contract_test_eval_order_336", create contract ...)
    compute_test_eval_order_336 = sp.local("compute_test_eval_order_336", create_contract_test_eval_order_336.value)
    sp.verify(self.data.x == 'A')
    create_contract_test_eval_order_338 = sp.local("create_contract_test_eval_order_338", create contract ...)
    compute_test_eval_order_338 = sp.local("compute_test_eval_order_338", create_contract_test_eval_order_338.value)
    sp.verify(self.data.x == 'B')
    compute_test_eval_order_342 = sp.local("compute_test_eval_order_342", sp.slice(self.f_string(('A', 'abc')), self.f_nat(('B', 1)), self.f_nat(('C', 1))))
    sp.verify(self.data.x == 'B')
    compute_test_eval_order_344 = sp.local("compute_test_eval_order_344", sp.slice(self.f_string(('A', 'abc')), 0, self.f_nat(('C', 1))))
    sp.verify(self.data.x == 'C')
    compute_test_eval_order_347 = sp.local("compute_test_eval_order_347", sp.transfer_operation(self.f_int(('A', 1)), self.f_mutez(('B', sp.mutez(1))), self.f_contract(('C', sp.self_entry_point('put_int')))))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_349 = sp.local("compute_test_eval_order_349", sp.transfer_operation(0, self.f_mutez(('B', sp.mutez(1))), self.f_contract(('C', sp.self_entry_point('put_int')))))
    sp.verify(self.data.x == 'B')

  @sp.entry_point
  def test_mprim2(self):
    compute_test_eval_order_258 = sp.local("compute_test_eval_order_258", self.f_nat(('A', 1)) << self.f_nat(('B', 1)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_261 = sp.local("compute_test_eval_order_261", self.f_nat(('A', 1)) >> self.f_nat(('B', 1)))
    sp.verify(self.data.x == 'A')

  @sp.entry_point
  def test_mprim3(self, params):
    sp.set_type(params.chest_key, sp.TChest_key)
    compute_test_eval_order_277 = sp.local("compute_test_eval_order_277", sp.open_chest(self.f_chest_key(('A', params.chest_key)), self.f_chest(('B', sp.chest('0xbcf89e8a80d99cd49690dda59accc4d381c1a3facbacc1a3f9aab5e7ecdc8db6b0928eed84a7e5d9958e96b5b1f3c8ddd9b98dd394f9bafaafad85a2d7cbffeec1e280d5c6ccbab3caddb5ced3c1d39c82cea0cc99b7f788bbca85eeb1badd91fdc8e1e1e9a6cddca7bdef8faaf6acf2aeef8ccbd3d987f6d0ecf9c8c9818c92eb8a81e385fff9e8c4ad96d6e7bad6a480ec83f3cb84bedeac82baafa1b88bfd90968b9ee7b2d8aabef9d8e0d9a9c8a5a8859a8db5e6d496de95cfd2cfc4dcfae3edf9f1d5faee998694ed8aada2a7d4b88aecc8acda98d6f69cfb8df3a5ca80b48d91aa9bbbf68f918998ddc5d3cc82d3f0ce81869fd9edb8b0989dcfa5e9b59db18dd099c7b8bb848586afc7c8888f86d2c6988eeee5e5fbfc9de8a7f19e84ebc2b687049390ebd5cecb86eecfe2bef994f9a7c3eabeabdda0b4b7b59381a8a4c3d7c1b382bcf6e1adb2e3c6ce9ca5e0c0a0beb999f4eacc89cf858fb282b294d99ec2daccd9cdcc98a98b8cb5b0bbf9ed80b0f6ccd2a994f3a1faafbbacf7eebbec9399bfdfd1f0da9ee4aa9dcb8b9c85fcab87bd86b98fd7cc9a8cb2e3cc93ef86df83e5ce9bbda8c7d4cad0e4b893fde8e18b9cf283f9829f9ab8f48da8b0f1b5c8f3a1d2a1a6e5c1fec0abb09eb9b0c2c3ae9f9df6c6a7bad396f7b3b1fc90e490a4f2a5f599fedbc8b3b297a99db3b285a3f0acefe0c8d4cec08195af8098d0f4b8d488bdacd7e8effea5cdaf8dd1ccdbe4fee59eb7e1cebff2eb839b8f81d598abc798d5bdcea59bfcbaf89fbdbbcb8182a3eea1f4f5dccafcb4ddd4ff83d3879edef5d3ca06d2f1146cf4c25faf0e432bf540b4de74fae3cc68e0bc57c700000021cf744e778f4d5e220d4be7310077e6735795b6c90bbe2bdcdd9b686e3c71f833f2'))), self.f_nat(('C', 1))))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_279 = sp.local("compute_test_eval_order_279", sp.open_chest(params.chest_key, self.f_chest(('B', sp.chest('0xbcf89e8a80d99cd49690dda59accc4d381c1a3facbacc1a3f9aab5e7ecdc8db6b0928eed84a7e5d9958e96b5b1f3c8ddd9b98dd394f9bafaafad85a2d7cbffeec1e280d5c6ccbab3caddb5ced3c1d39c82cea0cc99b7f788bbca85eeb1badd91fdc8e1e1e9a6cddca7bdef8faaf6acf2aeef8ccbd3d987f6d0ecf9c8c9818c92eb8a81e385fff9e8c4ad96d6e7bad6a480ec83f3cb84bedeac82baafa1b88bfd90968b9ee7b2d8aabef9d8e0d9a9c8a5a8859a8db5e6d496de95cfd2cfc4dcfae3edf9f1d5faee998694ed8aada2a7d4b88aecc8acda98d6f69cfb8df3a5ca80b48d91aa9bbbf68f918998ddc5d3cc82d3f0ce81869fd9edb8b0989dcfa5e9b59db18dd099c7b8bb848586afc7c8888f86d2c6988eeee5e5fbfc9de8a7f19e84ebc2b687049390ebd5cecb86eecfe2bef994f9a7c3eabeabdda0b4b7b59381a8a4c3d7c1b382bcf6e1adb2e3c6ce9ca5e0c0a0beb999f4eacc89cf858fb282b294d99ec2daccd9cdcc98a98b8cb5b0bbf9ed80b0f6ccd2a994f3a1faafbbacf7eebbec9399bfdfd1f0da9ee4aa9dcb8b9c85fcab87bd86b98fd7cc9a8cb2e3cc93ef86df83e5ce9bbda8c7d4cad0e4b893fde8e18b9cf283f9829f9ab8f48da8b0f1b5c8f3a1d2a1a6e5c1fec0abb09eb9b0c2c3ae9f9df6c6a7bad396f7b3b1fc90e490a4f2a5f599fedbc8b3b297a99db3b285a3f0acefe0c8d4cec08195af8098d0f4b8d488bdacd7e8effea5cdaf8dd1ccdbe4fee59eb7e1cebff2eb839b8f81d598abc798d5bdcea59bfcbaf89fbdbbcb8182a3eea1f4f5dccafcb4ddd4ff83d3879edef5d3ca06d2f1146cf4c25faf0e432bf540b4de74fae3cc68e0bc57c700000021cf744e778f4d5e220d4be7310077e6735795b6c90bbe2bdcdd9b686e3c71f833f2'))), self.f_nat(('C', 1))))
    sp.verify(self.data.x == 'B')
    sp.verify(sp.check_signature(self.f_key(('A', params.key)), self.f_signature(('B', params.signature)), self.f_bytes(('C', sp.bytes('0x00')))))
    sp.verify(self.data.x == 'A')
    sp.verify(sp.check_signature(params.key, self.f_signature(('B', params.signature)), self.f_bytes(('C', sp.bytes('0x00')))))
    sp.verify(self.data.x == 'B')

  @sp.entry_point
  def test_prim2(self):
    ticket_136 = sp.local("ticket_136", sp.ticket(1, 1))
    compute_test_eval_order_144 = sp.local("compute_test_eval_order_144", self.f_map(('A', {})).get_opt(self.f_int(('B', 1))))
    sp.verify(self.data.x == 'B')
    compute_test_eval_order_146 = sp.local("compute_test_eval_order_146", self.f_map(('A', {})).contains(self.f_int(('B', 1))))
    sp.verify(self.data.x == 'B')
    compute_test_eval_order_148 = sp.local("compute_test_eval_order_148", self.f_lambda(('A', sp.build_lambda(lambda _x23: _x23)))(self.f_int(('B', 1))))
    sp.verify(self.data.x == 'B')
    compute_test_eval_order_150 = sp.local("compute_test_eval_order_150", self.f_lambda2(('A', sp.build_lambda(lambda _x24: 0))).apply(self.f_int(('B', 1))))
    sp.verify(self.data.x == 'B')
    compute_test_eval_order_152 = sp.local("compute_test_eval_order_152", sp.cons(self.f_int(('A', 1)), self.f_list(('B', sp.list([])))))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_154 = sp.local("compute_test_eval_order_154", sp.add_seconds(self.f_timestamp(('A', sp.timestamp(1))), self.f_int(('B', 1))))
    sp.verify(self.data.x == 'A')
    ticket_156 = sp.local("ticket_156", sp.ticket(self.f_int(('A', 1)), self.f_nat(('B', 1))))
    compute_test_eval_order_156 = sp.local("compute_test_eval_order_156", ticket_156.value)
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_161 = sp.local("compute_test_eval_order_161", self.f_int(('A', 1)) + self.f_int(('B', 1)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_163 = sp.local("compute_test_eval_order_163", self.f_int(('A', 1)) * self.f_int(('B', 1)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_165 = sp.local("compute_test_eval_order_165", self.f_int(('A', 1)) - self.f_int(('B', 1)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_167 = sp.local("compute_test_eval_order_167", self.f_int(('A', 1)) + self.f_int(('B', 1)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_169 = sp.local("compute_test_eval_order_169", self.f_nat(('A', 1)) // self.f_nat(('B', 1)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_171 = sp.local("compute_test_eval_order_171", self.f_int(('A', 1)) == self.f_int(('B', 1)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_173 = sp.local("compute_test_eval_order_173", self.f_int(('A', 1)) != self.f_int(('B', 1)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_175 = sp.local("compute_test_eval_order_175", self.f_int(('A', 1)) < self.f_int(('B', 1)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_177 = sp.local("compute_test_eval_order_177", self.f_int(('A', 1)) <= self.f_int(('B', 1)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_179 = sp.local("compute_test_eval_order_179", self.f_int(('A', 1)) > self.f_int(('B', 1)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_181 = sp.local("compute_test_eval_order_181", self.f_int(('A', 1)) >= self.f_int(('B', 1)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_183 = sp.local("compute_test_eval_order_183", self.f_int(('A', 1)) % self.f_int(('B', 1)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_185 = sp.local("compute_test_eval_order_185", self.f_nat(('A', 1)) ^ self.f_nat(('B', 1)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_187 = sp.local("compute_test_eval_order_187", sp.min(self.f_int(('A', 1)), self.f_int(('B', 1))))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_189 = sp.local("compute_test_eval_order_189", sp.max(self.f_int(('A', 1)), self.f_int(('B', 1))))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_191 = sp.local("compute_test_eval_order_191", sp.ediv(self.f_int(('A', 1)), self.f_int(('B', 1))))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_194 = sp.local("compute_test_eval_order_194", self.f_bool(('A', False)) | self.f_bool(('B', False)))
    sp.verify(self.data.x == 'B')
    compute_test_eval_order_196 = sp.local("compute_test_eval_order_196", self.f_bool(('A', False)) | self.f_bool(('B', True)))
    sp.verify(self.data.x == 'B')
    compute_test_eval_order_198 = sp.local("compute_test_eval_order_198", self.f_bool(('A', True)) | self.f_bool(('B', False)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_200 = sp.local("compute_test_eval_order_200", self.f_bool(('A', True)) | self.f_bool(('B', True)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_203 = sp.local("compute_test_eval_order_203", self.f_bool(('A', False)) & self.f_bool(('B', False)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_205 = sp.local("compute_test_eval_order_205", self.f_bool(('A', False)) & self.f_bool(('B', True)))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_207 = sp.local("compute_test_eval_order_207", self.f_bool(('A', True)) & self.f_bool(('B', False)))
    sp.verify(self.data.x == 'B')
    compute_test_eval_order_209 = sp.local("compute_test_eval_order_209", self.f_bool(('A', True)) & self.f_bool(('B', True)))
    sp.verify(self.data.x == 'B')

  @sp.entry_point
  def test_prim3(self):
    compute_test_eval_order_223 = sp.local("compute_test_eval_order_223", sp.split_tokens(self.f_mutez(('A', sp.mutez(1))), self.f_nat(('B', 1)), self.f_nat(('C', 1))))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_225 = sp.local("compute_test_eval_order_225", sp.split_tokens(sp.tez(0), self.f_nat(('B', 1)), self.f_nat(('C', 1))))
    sp.verify(self.data.x == 'B')
    compute_test_eval_order_228 = sp.local("compute_test_eval_order_228", sp.range(self.f_nat(('A', 1)), self.f_nat(('B', 1)), self.f_nat(('C', 1))))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_232 = sp.local("compute_test_eval_order_232", sp.update_map(self.f_map(('A', {})), self.f_int(('B', 1)), self.f_option(('C', sp.some(1)))))
    sp.verify(self.data.x == 'B')
    compute_test_eval_order_234 = sp.local("compute_test_eval_order_234", sp.update_map(self.f_map(('A', {})), 0, self.f_option(('C', sp.some(1)))))
    sp.verify(self.data.x == 'C')
    match_pair_test_eval_order_238_fst, match_pair_test_eval_order_238_snd = sp.match_tuple(sp.get_and_update(self.f_map(('A', {})), self.f_int(('B', 1)), self.f_option(('C', sp.some(1)))), "match_pair_test_eval_order_238_fst", "match_pair_test_eval_order_238_snd")
    compute_test_eval_order_238 = sp.local("compute_test_eval_order_238", (match_pair_test_eval_order_238_fst, match_pair_test_eval_order_238_snd))
    sp.verify(self.data.x == 'B')
    match_pair_test_eval_order_240_fst, match_pair_test_eval_order_240_snd = sp.match_tuple(sp.get_and_update(self.f_map(('A', {})), 0, self.f_option(('C', sp.some(1)))), "match_pair_test_eval_order_240_fst", "match_pair_test_eval_order_240_snd")
    compute_test_eval_order_240 = sp.local("compute_test_eval_order_240", (match_pair_test_eval_order_240_fst, match_pair_test_eval_order_240_snd))
    sp.verify(self.data.x == 'C')
    compute_test_eval_order_243 = sp.local("compute_test_eval_order_243", sp.eif(self.f_bool(('A', True)), self.f_int(('B', 1)), self.f_int(('C', 1))))
    sp.verify(self.data.x == 'B')
    compute_test_eval_order_245 = sp.local("compute_test_eval_order_245", sp.eif(self.f_bool(('A', True)), 0, self.f_int(('C', 1))))
    sp.verify(self.data.x == 'A')
    compute_test_eval_order_248 = sp.local("compute_test_eval_order_248", sp.eif(self.f_bool(('A', False)), self.f_int(('B', 1)), self.f_int(('C', 1))))
    sp.verify(self.data.x == 'C')
    compute_test_eval_order_250 = sp.local("compute_test_eval_order_250", sp.eif(self.f_bool(('A', False)), self.f_int(('B', 1)), 0))
    sp.verify(self.data.x == 'A')

  @sp.private_lambda()
  def f_address(_x0):
    self.data.x = sp.fst(_x0)
    sp.result(sp.set_type_expr(sp.snd(_x0), sp.TAddress))

  @sp.private_lambda()
  def f_bool(_x1):
    self.data.x = sp.fst(_x1)
    sp.result(sp.set_type_expr(sp.snd(_x1), sp.TBool))

  @sp.private_lambda()
  def f_bytes(_x2):
    self.data.x = sp.fst(_x2)
    sp.result(sp.set_type_expr(sp.snd(_x2), sp.TBytes))

  @sp.private_lambda()
  def f_chest(_x3):
    self.data.x = sp.fst(_x3)
    sp.result(sp.set_type_expr(sp.snd(_x3), sp.TChest))

  @sp.private_lambda()
  def f_chest_key(_x4):
    self.data.x = sp.fst(_x4)
    sp.result(sp.set_type_expr(sp.snd(_x4), sp.TChest_key))

  @sp.private_lambda()
  def f_contract(_x5):
    self.data.x = sp.fst(_x5)
    sp.result(sp.set_type_expr(sp.snd(_x5), sp.TContract(sp.TInt)))

  @sp.private_lambda()
  def f_int(_x6):
    self.data.x = sp.fst(_x6)
    sp.result(sp.set_type_expr(sp.snd(_x6), sp.TInt))

  @sp.private_lambda()
  def f_key(_x7):
    x1, x2 = sp.match_tuple(_x7, "x1", "x2")
    self.data.x = x1
    sp.result(sp.set_type_expr(x2, sp.TKey))

  @sp.private_lambda()
  def f_lambda(_x8):
    self.data.x = sp.fst(_x8)
    sp.result(sp.set_type_expr(sp.snd(_x8), sp.TLambda(sp.TInt, sp.TInt)))

  @sp.private_lambda()
  def f_lambda2(_x9):
    self.data.x = sp.fst(_x9)
    sp.result(sp.set_type_expr(sp.snd(_x9), sp.TLambda(sp.TPair(sp.TInt, sp.TInt), sp.TInt)))

  @sp.private_lambda()
  def f_list(_x10):
    self.data.x = sp.fst(_x10)
    sp.result(sp.set_type_expr(sp.snd(_x10), sp.TList(sp.TInt)))

  @sp.private_lambda()
  def f_map(_x11):
    self.data.x = sp.fst(_x11)
    sp.result(sp.set_type_expr(sp.snd(_x11), sp.TMap(sp.TInt, sp.TInt)))

  @sp.private_lambda()
  def f_mutez(_x12):
    self.data.x = sp.fst(_x12)
    sp.result(sp.set_type_expr(sp.snd(_x12), sp.TMutez))

  @sp.private_lambda()
  def f_nat(_x13):
    self.data.x = sp.fst(_x13)
    sp.result(sp.set_type_expr(sp.snd(_x13), sp.TNat))

  @sp.private_lambda()
  def f_option(_x14):
    self.data.x = sp.fst(_x14)
    sp.result(sp.set_type_expr(sp.snd(_x14), sp.TOption(sp.TInt)))

  @sp.private_lambda()
  def f_option_key_hash(_x15):
    self.data.x = sp.fst(_x15)
    sp.result(sp.set_type_expr(sp.snd(_x15), sp.TOption(sp.TKeyHash)))

  @sp.private_lambda()
  def f_pair(_x16):
    self.data.x = sp.fst(_x16)
    sp.result(sp.set_type_expr(sp.snd(_x16), sp.TPair(sp.TNat, sp.TNat)))

  @sp.private_lambda()
  def f_signature(_x17):
    x1, x2 = sp.match_tuple(_x17, "x1", "x2")
    self.data.x = x1
    sp.result(sp.set_type_expr(x2, sp.TSignature))

  @sp.private_lambda()
  def f_string(_x18):
    self.data.x = sp.fst(_x18)
    sp.result(sp.set_type_expr(sp.snd(_x18), sp.TString))

  @sp.private_lambda()
  def f_ticket(_x19):
    x1, x2 = sp.match_tuple(_x19, "x1", "x2")
    self.data.x = x1
    sp.result(sp.set_type_expr(x2, sp.TTicket(sp.TInt)))

  @sp.private_lambda()
  def f_timestamp(_x20):
    self.data.x = sp.fst(_x20)
    sp.result(sp.set_type_expr(sp.snd(_x20), sp.TTimestamp))

  @sp.private_lambda()
  def f_unit(_x21):
    self.data.x = _x21
    pass

sp.add_compilation_target("test", Contract())