open SmartML

module Contract = struct
  let%entry_point ep () =
    let%mutable k = "abc" in ();
    with match_record(Map.get data.m k, "data") as data:
      k <- "xyz" + k;
    data.out <- k

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {m = map string {a = intOrNat; b = intOrNat}; out = string}]
      ~storage:[%expr
                 {m = Map.make [("abc", {a = 10; b = 20})];
                  out = "z"}]
      [ep]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())