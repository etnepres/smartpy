Verifying sp.view("a_view", sp.address('KT1'), 0, sp.TNat) == sp.none...
 OK
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> 0
 => test_baselines/scenario/test_onchain_views_exceptions/Test_view_no_exception/step_002_cont_0_storage.tz 1
 => test_baselines/scenario/test_onchain_views_exceptions/Test_view_no_exception/step_002_cont_0_storage.json 1
 => test_baselines/scenario/test_onchain_views_exceptions/Test_view_no_exception/step_002_cont_0_sizes.csv 2
 => test_baselines/scenario/test_onchain_views_exceptions/Test_view_no_exception/step_002_cont_0_storage.py 1
 => test_baselines/scenario/test_onchain_views_exceptions/Test_view_no_exception/step_002_cont_0_types.py 7
 => test_baselines/scenario/test_onchain_views_exceptions/Test_view_no_exception/step_002_cont_0_contract.tz 8
 => test_baselines/scenario/test_onchain_views_exceptions/Test_view_no_exception/step_002_cont_0_contract.json 5
 => test_baselines/scenario/test_onchain_views_exceptions/Test_view_no_exception/step_002_cont_0_contract.py 8
 => test_baselines/scenario/test_onchain_views_exceptions/Test_view_no_exception/step_002_cont_0_contract.ml 17
Verifying sp.contract_view(0, "other", 42).open_some(message = 'View other is invalid!') == 43...
 OK
Verifying sp.view("other", sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1'), 1, sp.TIntOrNat) == sp.some(2)...
 OK
Verifying sp.view("other", sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1'), 1, sp.TIntOrNat) == sp.some(2)...
 OK
Verifying sp.view("missing_view", sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1'), 1, sp.TUnknown()) == sp.none...
 OK
Verifying sp.view("other", sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1'), 1, sp.TInt) == sp.none...
 OK
