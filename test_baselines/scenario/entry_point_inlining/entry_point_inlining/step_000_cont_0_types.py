import smartpy as sp

tstorage = sp.TRecord(x = sp.TIntOrNat).layout("x")
tparameter = sp.TVariant(entry_point_1 = sp.TUnit, g = sp.TIntOrNat).layout(("entry_point_1", "g"))
tprivates = { }
tviews = { }
