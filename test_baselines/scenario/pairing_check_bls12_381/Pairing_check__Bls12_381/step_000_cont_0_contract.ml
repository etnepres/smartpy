open SmartML

module Contract = struct
  let%entry_point pairing_check params =
    data.check <- some (pairing_check params)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {check = option bool}]
      ~storage:[%expr
                 {check = None}]
      [pairing_check]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())