open SmartML

module Contract = struct
  let%entry_point f params =
    data.a <- 2 * params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {a = intOrNat; b = bool}]
      ~storage:[%expr
                 {a = 12;
                  b = true}]
      [f]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())