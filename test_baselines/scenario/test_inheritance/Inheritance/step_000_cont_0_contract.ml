open SmartML

module Contract = struct
  let%entry_point myEntryPoint params =
    verify (data.x <= 100);
    data.x <- data.x + params;
    data.y <- 12345;
    data.y <- data.y + (params + 2);
    data.x <- data.x + 10

  let%entry_point myEntryPoint2 () =
    ()

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x = intOrNat; y = intOrNat; z = intOrNat}]
      ~storage:[%expr
                 {x = 1;
                  y = 2;
                  z = 42}]
      [myEntryPoint; myEntryPoint2]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())