open SmartML

module Contract = struct
  let%entry_point bar () =
    data.board <- self.zero;
    List.iter (fun k ->
      Map.set (Map.get data.board 5) k 1
    ) [0; 1; 2; 3; 4; 5; 6; 7; 8; 9]

  let%entry_point glider () =
    data.board <- self.zero;
    Map.set (Map.get data.board 3) 3 1;
    Map.set (Map.get data.board 4) 4 1;
    Map.set (Map.get data.board 5) 2 1;
    Map.set (Map.get data.board 5) 3 1;
    Map.set (Map.get data.board 5) 4 1

  let%entry_point reset () =
    data.board <- self.zero

  let%entry_point run () =
    let%mutable next = self.zero in ();
    List.iter (fun i ->
      List.iter (fun j ->
        let%mutable sum = 0 in ();
        List.iter (fun k ->
          if ((i + k) >= 0) && ((i + k) < 10) then
            List.iter (fun l ->
              if ((j + l) >= 0) && ((j + l) < 10) then
                if (k <> 0) || (l <> 0) then
                  sum <- sum + (Map.get (Map.get data.board (i + k)) (j + l))
            ) (range (-1) 2 1)
        ) (range (-1) 2 1);
        if (Map.get (Map.get data.board i) j) = 0 then
          if sum = 3 then
            Map.set (Map.get next i) j 1
        else
          if (sum >= 2) && (sum <= 3) then
            Map.set (Map.get next i) j 1
      ) [0; 1; 2; 3; 4; 5; 6; 7; 8; 9]
    ) [0; 1; 2; 3; 4; 5; 6; 7; 8; 9];
    data.board <- next

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {board = map int (map int intOrNat)}]
      ~storage:[%expr
                 {board = Map.make []}]
      [bar; glider; reset; run]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())