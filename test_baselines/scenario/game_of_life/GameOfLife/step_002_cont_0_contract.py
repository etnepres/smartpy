import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(board = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TIntOrNat))).layout("board"))
    self.init(board = {})

  @sp.entry_point
  def bar(self):
    self.data.board = self.zero
    sp.for k in sp.list([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]):
      self.data.board[5][k] = 1

  @sp.entry_point
  def glider(self):
    self.data.board = self.zero
    self.data.board[3][3] = 1
    self.data.board[4][4] = 1
    self.data.board[5][2] = 1
    self.data.board[5][3] = 1
    self.data.board[5][4] = 1

  @sp.entry_point
  def reset(self):
    self.data.board = self.zero

  @sp.entry_point
  def run(self):
    next = sp.local("next", self.zero)
    sp.for i in sp.list([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]):
      sp.for j in sp.list([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]):
        sum = sp.local("sum", 0)
        sp.for k in sp.range(-1, 2):
          sp.if ((i + k) >= 0) & ((i + k) < 10):
            sp.for l in sp.range(-1, 2):
              sp.if ((j + l) >= 0) & ((j + l) < 10):
                sp.if (k != 0) | (l != 0):
                  sum.value += self.data.board[i + k][j + l]
        sp.if self.data.board[i][j] == 0:
          sp.if sum.value == 3:
            next.value[i][j] = 1
        sp.else:
          sp.if (sum.value >= 2) & (sum.value <= 3):
            next.value[i][j] = 1
    self.data.board = next.value

sp.add_compilation_target("test", Contract())