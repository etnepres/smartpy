open SmartML

module Contract = struct
  let%entry_point myEntryPoint params =
    verify (params.x <= 123) ~msg:((data, 0) self.get_error);
    verify (params.y <= 123) ~msg:((data, 1) self.get_error);
    verify (params.z <= 123) ~msg:((data, 2) self.get_error);
    data.x <- data.x + ((params.x + params.y) + params.z)

  let%entry_point myEntryPoint2 () =
    data.s <- ((data, 0) self.get_error) + ((data, 3) self.get_error)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {bb = big_map bool int; msg = big_map nat string; s = string; x = intOrNat}]
      ~storage:[%expr
                 {bb = Map.make [];
                  msg = Map.make [(0, "Bad compare"); (1, "Bad compare2"); (2, "Bad compare3"); (3, "abcdefg")];
                  s = "";
                  x = 0}]
      [myEntryPoint; myEntryPoint2]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())