open SmartML

module Contract = struct
  let%entry_point decrease params =
    data.result <- some ((open_some data.result) - params)

  let%entry_point store params =
    data.result <- some params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {result = option mutez}]
      ~storage:[%expr
                 {result = None}]
      [decrease; store]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())