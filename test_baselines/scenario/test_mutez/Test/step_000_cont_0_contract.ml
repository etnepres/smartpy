open SmartML

module Contract = struct
  let%entry_point test params =
    verify ((fst (open_some (ediv amount (mutez 1)) ~message:())) = params);
    verify ((mul params (mutez 1)) = amount)

  let%entry_point test_diff params =
    let%mutable compute_test_mutez_11 = (params.x - params.y) in ()

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [test; test_diff]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())