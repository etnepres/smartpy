import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TRecord(a = sp.TInt, b = sp.TInt).layout(("a", "b")), y = sp.TVariant(A = sp.TInt, B = sp.TInt).layout(("A", "B"))).layout(("x", "y")))
    self.init(x = sp.record(a = 1, b = 2),
              y = A(42))

  @sp.entry_point
  def test_pair(self):
    self.data.x = sp.convert((5, 6))
    sp.verify(self.data.x == sp.record(a = 5, b = 6))

  @sp.entry_point
  def test_record(self):
    self.data.x = sp.convert(sp.set_type_expr(sp.record(c = 3, d = 4), sp.TRecord(c = sp.TInt, d = sp.TInt).layout(("c", "d"))))
    sp.verify(self.data.x == sp.record(a = 3, b = 4))

  @sp.entry_point
  def test_variant(self):
    self.data.y = sp.convert(sp.set_type_expr(variant('C', 43), sp.TVariant(C = sp.TInt, D = sp.TInt).layout(("C", "D"))))
    sp.verify(self.data.y == variant('A', 43))

sp.add_compilation_target("test", Contract())