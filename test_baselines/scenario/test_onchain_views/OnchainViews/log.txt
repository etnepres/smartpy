Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> 42
 => test_baselines/scenario/test_onchain_views/OnchainViews/step_000_cont_0_storage.tz 1
 => test_baselines/scenario/test_onchain_views/OnchainViews/step_000_cont_0_storage.json 1
 => test_baselines/scenario/test_onchain_views/OnchainViews/step_000_cont_0_sizes.csv 2
 => test_baselines/scenario/test_onchain_views/OnchainViews/step_000_cont_0_storage.py 1
 => test_baselines/scenario/test_onchain_views/OnchainViews/step_000_cont_0_types.py 7
 => test_baselines/scenario/test_onchain_views/OnchainViews/step_000_cont_0_contract.tz 16
 => test_baselines/scenario/test_onchain_views/OnchainViews/step_000_cont_0_contract.json 14
 => test_baselines/scenario/test_onchain_views/OnchainViews/step_000_cont_0_contract.py 8
 => test_baselines/scenario/test_onchain_views/OnchainViews/step_000_cont_0_contract.ml 17
Creating contract KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF
 -> (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1" (Pair None (Pair None (Pair None (Pair None None)))))
 => test_baselines/scenario/test_onchain_views/OnchainViews/step_001_cont_1_storage.tz 1
 => test_baselines/scenario/test_onchain_views/OnchainViews/step_001_cont_1_storage.json 16
 => test_baselines/scenario/test_onchain_views/OnchainViews/step_001_cont_1_sizes.csv 2
 => test_baselines/scenario/test_onchain_views/OnchainViews/step_001_cont_1_storage.py 1
 => test_baselines/scenario/test_onchain_views/OnchainViews/step_001_cont_1_types.py 7
 => test_baselines/scenario/test_onchain_views/OnchainViews/step_001_cont_1_contract.tz 116
 => test_baselines/scenario/test_onchain_views/OnchainViews/step_001_cont_1_contract.json 293
 => test_baselines/scenario/test_onchain_views/OnchainViews/step_001_cont_1_contract.py 32
 => test_baselines/scenario/test_onchain_views/OnchainViews/step_001_cont_1_contract.ml 37
 => test_baselines/scenario/test_onchain_views/OnchainViews/step_002_cont_1_params.py 1
 => test_baselines/scenario/test_onchain_views/OnchainViews/step_002_cont_1_params.tz 1
 => test_baselines/scenario/test_onchain_views/OnchainViews/step_002_cont_1_params.json 1
Executing store(sp.record())...
 -> (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1" (Pair (Some 42) (Pair (Some (Pair 0 (Pair 1334 (Pair 0x9caecab9 (Pair 101 (Pair "1970-01-01T00:00:01Z" (Pair "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF" (Pair "tz0Faketester" 10)))))))) (Pair (Some "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF") (Pair (Some "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1") (Some "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF"))))))
Computing sp.contract_data(1).result2.open_some()...
 => sp.record(amount = sp.tez(0), balance = sp.mutez(1334), chain_id = sp.chain_id('0x9caecab9'), level = 101, now = sp.timestamp(1), sender = sp.address('KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF'), source = sp.address('tz0Faketester'), total_voting_power = 10)
Verifying sp.contract_data(1).result2.open_some() == sp.record(amount = sp.tez(0), balance = sp.mutez(1334), chain_id = sp.chain_id('0x9caecab9'), level = 101, now = sp.timestamp(1), sender = sp.address('KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF'), source = sp.resolve(sp.test_account("tester").address), total_voting_power = 10)...
 OK
 => test_baselines/scenario/test_onchain_views/OnchainViews/step_005_cont_1_params.py 1
 => test_baselines/scenario/test_onchain_views/OnchainViews/step_005_cont_1_params.tz 1
 => test_baselines/scenario/test_onchain_views/OnchainViews/step_005_cont_1_params.json 1
Executing store(sp.record())...
 -> (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1" (Pair (Some 42) (Pair (Some (Pair 0 (Pair 1484 (Pair 0x9caecab9 (Pair 102 (Pair "1970-01-01T00:00:02Z" (Pair "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF" (Pair "tz0Faketester" 10)))))))) (Pair (Some "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF") (Pair (Some "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1") (Some "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF"))))))
Verifying sp.contract_data(1).result2.open_some() == sp.record(amount = sp.tez(0), balance = sp.mutez(1484), chain_id = sp.chain_id('0x9caecab9'), level = 102, now = sp.timestamp(2), sender = sp.address('KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF'), source = sp.resolve(sp.test_account("tester").address), total_voting_power = 10)...
 OK
 => test_baselines/scenario/test_onchain_views/OnchainViews/step_007_cont_1_params.py 1
 => test_baselines/scenario/test_onchain_views/OnchainViews/step_007_cont_1_params.tz 1
 => test_baselines/scenario/test_onchain_views/OnchainViews/step_007_cont_1_params.json 1
Executing failing_entry_point(sp.record())...
 -> --- Expected failure in transaction --- Failure: 'This is a failure'
 (python/templates/test_onchain_views.py, line 74)
 => test_baselines/scenario/test_onchain_views/OnchainViews/step_008_cont_1_params.py 1
 => test_baselines/scenario/test_onchain_views/OnchainViews/step_008_cont_1_params.tz 1
 => test_baselines/scenario/test_onchain_views/OnchainViews/step_008_cont_1_params.json 1
Executing store(sp.record())...
 -> (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1" (Pair (Some 42) (Pair (Some (Pair 0 (Pair 1684 (Pair 0x9caecab9 (Pair 102 (Pair "1970-01-01T00:00:02Z" (Pair "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF" (Pair "tz0Faketester" 10)))))))) (Pair (Some "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF") (Pair (Some "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1") (Some "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF"))))))
Verifying sp.contract_data(1).result2.open_some() == sp.record(amount = sp.tez(0), balance = sp.mutez(1684), chain_id = sp.chain_id('0x9caecab9'), level = 102, now = sp.timestamp(2), sender = sp.address('KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF'), source = sp.resolve(sp.test_account("tester").address), total_voting_power = 10)...
 OK
