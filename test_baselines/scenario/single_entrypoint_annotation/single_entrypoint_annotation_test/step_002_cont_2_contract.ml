open SmartML

module Contract = struct
  let%entry_point ep1 params =
    transfer 1 (tez 0) (open_some (contract nat params.address1 ) ~message:"WRONG_INTERFACE_Contract1");
    transfer 1 (tez 0) (open_some (contract nat params.address2 , entry_point='ep') ~message:"WRONG_INTERFACE_Contract2")

  let%entry_point ep2 params =
    transfer 1 (tez 0) (open_some (contract nat params.address1 ) ~message:"WRONG_INTERFACE_Contract1");
    transfer 1 (tez 0) (open_some (contract nat params.address2 , entry_point='ep') ~message:"WRONG_INTERFACE_Contract2")

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [ep1; ep2]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())