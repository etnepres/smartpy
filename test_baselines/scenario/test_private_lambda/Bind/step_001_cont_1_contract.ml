open SmartML

module Contract = struct
  let%entry_point check params =
    verify ((params.params self.f) = params.result)

  let%entry_point test params =
    data.x <- some (params self.f)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x = option intOrNat}]
      ~storage:[%expr
                 {x = None}]
      [check; test]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())