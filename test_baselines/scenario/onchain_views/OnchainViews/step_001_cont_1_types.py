import smartpy as sp

tstorage = sp.TRecord(x = sp.TIntOrNat, y = sp.TIntOrNat, z = sp.TString).layout(("x", ("y", "z")))
tparameter = sp.TVariant(ep2 = sp.TUnit, ep3 = sp.TUnit).layout(("ep2", "ep3"))
tprivates = { }
tviews = { "view1": ((), sp.TString), "view2": (sp.TIntOrNat, sp.TIntOrNat) }
