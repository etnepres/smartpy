import smartpy as sp

class Mul__Bls12_381_int_fr(sp.Contract):
    def __init__(self):
        self.init(fr = sp.none)

    """
    MUL: Multiply a curve point or field element by a scalar field element. Fr
    elements can be built from naturals by multiplying by the unit of Fr using PUSH bls12_381_fr 1; MUL. Note
    that the multiplication will be computed using the natural modulo the order
    of Fr.

    :: int : bls12_381_fr : 'S -> bls12_381_fr : 'S
    """
    @sp.entry_point
    def mul_int_fr(self, p):
        i, fr = sp.match_pair(p)
        self.data.fr = sp.some(sp.mul(i, fr))

    """
    MUL: Multiply a curve point or field element by a scalar field element. Fr
    elements can be built from naturals by multiplying by the unit of Fr using PUSH bls12_381_fr 1; MUL. Note
    that the multiplication will be computed using the natural modulo the order
    of Fr.

    :: nat : bls12_381_fr : 'S -> bls12_381_fr : 'S
    """
    @sp.entry_point
    def mul_nat_fr(self, p):
        n, fr = sp.match_pair(p)
        self.data.fr = sp.some(sp.mul(n, fr))

@sp.add_test(name = "Mul__Bls12_381_int_fr")
def test():
    c1 = Mul__Bls12_381_int_fr();

    scenario = sp.test_scenario()
    scenario += c1

    c1.mul_int_fr(
        sp.pair(
            1,
            sp.bls12_381_fr("0x2ef123703093cbbbd124e15f2054fa5781ed0b8d092ec3c6e5d76b4ca918a221")
        )
    );

    c1.mul_nat_fr(
        sp.pair(
            sp.nat(2),
            sp.bls12_381_fr("0x2ef123703093cbbbd124e15f2054fa5781ed0b8d092ec3c6e5d76b4ca918a221")
        )
    );
