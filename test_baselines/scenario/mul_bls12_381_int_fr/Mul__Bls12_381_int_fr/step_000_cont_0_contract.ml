open SmartML

module Contract = struct
  let%entry_point mul_int_fr params =
    match_pair_mul_bls12_381_int_fr_17_fst, match_pair_mul_bls12_381_int_fr_17_snd = match_tuple(params, "match_pair_mul_bls12_381_int_fr_17_fst", "match_pair_mul_bls12_381_int_fr_17_snd")
    data.fr <- some (mul match_pair_mul_bls12_381_int_fr_17_fst match_pair_mul_bls12_381_int_fr_17_snd)

  let%entry_point mul_nat_fr params =
    match_pair_mul_bls12_381_int_fr_30_fst, match_pair_mul_bls12_381_int_fr_30_snd = match_tuple(params, "match_pair_mul_bls12_381_int_fr_30_fst", "match_pair_mul_bls12_381_int_fr_30_snd")
    data.fr <- some (mul match_pair_mul_bls12_381_int_fr_30_fst match_pair_mul_bls12_381_int_fr_30_snd)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {fr = option bls12_381_fr}]
      ~storage:[%expr
                 {fr = None}]
      [mul_int_fr; mul_nat_fr]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())