import smartpy as sp

tstorage = sp.TRecord(x = sp.TIntOrNat).layout("x")
tparameter = sp.TUnit
tprivates = { "sub": sp.TLambda(sp.TUnknown(), sp.TUnit, with_storage="read-write") }
tviews = { }
