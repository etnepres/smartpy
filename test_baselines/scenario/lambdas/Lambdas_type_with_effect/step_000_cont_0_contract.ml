open SmartML

module Contract = struct
  let%entry_point exec_lambda params =
    data.result <- some params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {result = option lambda unit unit ~with_operations:true}]
      ~storage:[%expr
                 {result = None}]
      [exec_lambda]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())