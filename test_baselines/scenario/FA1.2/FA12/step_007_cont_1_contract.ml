open SmartML

module Contract = struct
  let%entry_point compute params =
    set_type params.params nat;
    let%var __s5 = Map.get params.data.token_metadata params.params in
    data.result <- some __s5

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {result = option {token_id = nat; token_info = map string bytes}}]
      ~storage:[%expr
                 {result = None}]
      [compute]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())