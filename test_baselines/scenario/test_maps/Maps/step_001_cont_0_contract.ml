open SmartML

module Contract = struct
  let%entry_point test_get_and_update () =
    match_pair_test_maps_46_fst, match_pair_test_maps_46_snd = match_tuple(get_and_update 1 (some "one") data.m, "match_pair_test_maps_46_fst", "match_pair_test_maps_46_snd")
    data.z <- match_pair_test_maps_46_fst

  let%entry_point test_map_get params =
    set_type params (map int string);
    data.x <- Map.get params 12

  let%entry_point test_map_get2 params =
    set_type params (map int string);
    data.x <- Map.get params 12

  let%entry_point test_map_get_default_values params =
    set_type params (map int string);
    data.x <- get ~default_value:%s params 12 "abc"

  let%entry_point test_map_get_missing_value params =
    set_type params (map int string);
    data.x <- get ~message:%s params 12 "missing 12"

  let%entry_point test_map_get_missing_value2 params =
    set_type params (map int string);
    data.x <- get ~message:%s params 12 1234

  let%entry_point test_map_get_opt params =
    set_type params (map int string);
    data.y <- get_opt params 12

  let%entry_point test_update_map () =
    Map.set data.m 1 "one"

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {m = map intOrNat string; x = string; y = option string; z = option string}]
      ~storage:[%expr
                 {m = Map.make [];
                  x = "a";
                  y = None;
                  z = Some("na")}]
      [test_get_and_update; test_map_get; test_map_get2; test_map_get_default_values; test_map_get_missing_value; test_map_get_missing_value2; test_map_get_opt; test_update_map]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())