open SmartML

module Contract = struct
  let%entry_point append_message params =
    if (len data.message) = 0 then
      data.message <- params.message
    else
      data.message <- concat [data.message; params.separator; params.message]

  let%entry_point set_message params =
    data.message <- params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {message = string}]
      ~storage:[%expr
                 {message = ""}]
      [append_message; set_message]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())