import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(message = sp.TString).layout("message"))
    self.init(message = '')

  @sp.entry_point
  def append_message(self, params):
    sp.if sp.len(self.data.message) == 0:
      self.data.message = params.message
    sp.else:
      self.data.message = sp.concat(sp.list([self.data.message, params.separator, params.message]))

  @sp.entry_point
  def set_message(self, params):
    self.data.message = params

sp.add_compilation_target("test", Contract())