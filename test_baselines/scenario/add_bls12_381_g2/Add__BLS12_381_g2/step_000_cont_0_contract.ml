open SmartML

module Contract = struct
  let%entry_point add params =
    data.g2 <- some ((fst params) + (snd params))

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {g2 = option bls12_381_g2}]
      ~storage:[%expr
                 {g2 = None}]
      [add]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())