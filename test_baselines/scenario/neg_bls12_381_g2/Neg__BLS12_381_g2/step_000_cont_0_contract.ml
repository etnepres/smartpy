open SmartML

module Contract = struct
  let%entry_point negate params =
    set_type params.g2 bls12_381_g2;
    data.g2 <- some (- params.g2)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {g2 = option bls12_381_g2}]
      ~storage:[%expr
                 {g2 = None}]
      [negate]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())