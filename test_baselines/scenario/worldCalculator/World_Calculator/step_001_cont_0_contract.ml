open SmartML

module Contract = struct
  let%entry_point compute params =
    let%mutable bin_ops = (Map.make [("+", fun _x3 -> result ((fst _x3) + (snd _x3))); ("*", fun _x4 -> result ((fst _x4) * (snd _x4))); ("-", fun _x5 -> result ((snd _x5) - (fst _x5))); ("^", fun _x6 -> let%mutable result = 1 in ();
List.iter (fun i ->
  result <- result * (snd _x6)
) (range 0 (fst _x6) 1);
result result)]) in ();
    let%mutable elements = ({s = params; sep = " "} self.string_split) in ();
    let%mutable stack = (set_type_expr [] (list int)) in ();
    let%mutable formulas = [] in ();
    List.iter (fun element ->
      if element <> "" then
        if contains element bin_ops then
          (
            with match_cons(stack) as match_cons_67:
              with match_cons(match_cons_67.tail) as match_cons_68:
                stack <- match_cons_68.tail;
                stack <- ((match_cons_67.head, match_cons_68.head) (Map.get bin_ops element)) :: stack
              else:
                failwith "Bad formula: too many operators"
            else:
              failwith "Bad formula: too many operators";
            with match_cons(formulas) as match_cons_75:
              with match_cons(match_cons_75.tail) as match_cons_76:
                formulas <- match_cons_76.tail;
                formulas <- (concat ["("; match_cons_76.head; element; match_cons_75.head; ")"]) :: formulas
              else:
                ()
            else:
              ()
          )
        else
          (
            stack <- (element self.nat_of_string) :: stack;
            formulas <- element :: formulas
          )
    ) elements;
    data.formula <- params;
    data.operations <- elements;
    let%mutable length = (len stack) in ();
    verify (length = 1) ~msg:("Bad stack at the end of the computation. Length = " + (length self.string_of_nat), stack);
    with match_cons(stack) as match_cons_86:
      with match_cons(formulas) as match_cons_87:
        data.result <- match_cons_86.head;
        if match_cons_86.head >= 0 then
          data.summary <- (match_cons_87.head + " = ") + ((open_some (is_nat match_cons_86.head)) self.string_of_nat)
        else
          data.summary <- (match_cons_87.head + " = -") + ((open_some (is_nat (- match_cons_86.head))) self.string_of_nat)
      else:
        ()
    else:
      ()

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {Help = list string; formula = string; operations = list string; result = int; summary = string}]
      ~storage:[%expr
                 {Help = ["SmartPy Reverse Polish Calculator Template"; "Operations: +, -, *, ^"; "Example: 2 3 4 * - 3 ^"];
                  formula = "";
                  operations = [];
                  result = 0;
                  summary = ""}]
      [compute]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())