open SmartML

module Contract = struct
  let%entry_point test params =
    data.a <- some {l = params.l; lr = (List.rev params.l); mi = (Map.items params.m); mir = (Map.rev_items params.m); mk = (Map.keys params.m); mkr = (Map.rev_keys params.m); mv = (Map.values params.m); mvr = (Map.rev_values params.m); s = (Set.elements params.s); sr = (Set.rev_elements params.s)};
    data.b <- sum params.l;
    data.c <- concat (Map.keys params.m);
    data.d <- sum (Set.rev_elements params.s);
    data.e <- "";
    List.iter (fun x ->
      if snd x then
        data.e <- data.e + (fst x)
    ) (Map.values params.m);
    List.iter (fun i ->
      data.f <- (i * i) :: data.f
    ) (range 0 5 1);
    data.g <- range 1 12 1

  let%entry_point test_match params =
    with match_cons(params) as match_cons_52:
      data.head <- match_cons_52.head;
      data.tail <- match_cons_52.tail
    else:
      data.head <- "abc"

  let%entry_point test_match2 params =
    with match_cons(params) as match_cons_60:
      with match_cons(match_cons_60.tail) as match_cons_61:
        data.head <- match_cons_60.head + match_cons_61.head;
        data.tail <- match_cons_61.tail
      else:
        ()
    else:
      data.head <- "abc"

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {a = option {l = list intOrNat; lr = list intOrNat; mi = list {key = string; value = pair string bool}; mir = list {key = string; value = pair string bool}; mk = list string; mkr = list string; mv = list (pair string bool); mvr = list (pair string bool); s = list intOrNat; sr = list intOrNat}; b = intOrNat; c = string; d = intOrNat; e = string; f = list intOrNat; g = list intOrNat; head = string; tail = list string}]
      ~storage:[%expr
                 {a = None;
                  b = 0;
                  c = "";
                  d = 0;
                  e = "";
                  f = [];
                  g = [];
                  head = "no head";
                  tail = ["no tail"]}]
      [test; test_match; test_match2]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())