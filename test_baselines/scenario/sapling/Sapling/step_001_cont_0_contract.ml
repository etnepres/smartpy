open SmartML

module Contract = struct
  let%entry_point entry_point_1 params =
    set_type params.s (sapling_state 15);
    data.y <- some (sapling_verify_update params.s params.t)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {h = lambda {a = sapling_state 12; b = sapling_transaction 12} (option (pair int (sapling_state 12))); x = intOrNat; y = option (option (pair int (sapling_state 15))); z = lambda intOrNat (pair intOrNat (sapling_state 8))}]
      ~storage:[%expr
                 {h = lambda(lambda {a = sapling_state 12; b = sapling_transaction 12} (option (pair int (sapling_state 12))));
                  x = 12;
                  y = None;
                  z = lambda(lambda intOrNat (pair intOrNat (sapling_state 8)))}]
      [entry_point_1]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())