import smartpy as sp

tstorage = sp.TRecord(counter = sp.TIntOrNat, onEven = sp.TAddress, onOdd = sp.TAddress).layout(("counter", ("onEven", "onOdd")))
tparameter = sp.TVariant(reset = sp.TUnit, run = sp.TNat).layout(("reset", "run"))
tprivates = { }
tviews = { }
