open SmartML

module Contract = struct
  let%entry_point f () =
    let%mutable compute_sub_entry_point_23 = (5 self.a) in ();
    let%mutable compute_sub_entry_point_23i = (10 self.a) in ();
    data.z <- compute_sub_entry_point_23 + compute_sub_entry_point_23i

  let%entry_point g () =
    let%mutable compute_sub_entry_point_27 = (6 self.a) in ();
    data.z <- compute_sub_entry_point_27

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x = nat; y = string; z = nat}]
      ~storage:[%expr
                 {x = 2;
                  y = "aaa";
                  z = 0}]
      [f; g]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())