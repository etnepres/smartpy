open SmartML

module Contract = struct
  let%entry_point administrate params =
    verify (sender = data.admin) ~msg:"Aggregator_NotAdmin";
    set_type params (list (`changeActive bool + `changeAdmin address + `changeOracles {added = list (pair address {adminAddress = address; endingRound = option nat; startingRound = nat}); removed = list address} + `updateFutureRounds {maxSubmissions = nat; minSubmissions = nat; oraclePayment = nat; restartDelay = nat; timeout = nat}));
    List.iter (fun action ->
      match action with
        | `changeActive changeActive ->
          data.active <- changeActive
        | `changeAdmin changeAdmin ->
          data.admin <- changeAdmin
        | `updateFutureRounds updateFutureRounds ->
          verify (sender = data.admin) ~msg:"Aggregator_NotAdmin";
          let%mutable compute_price_feed_672 = (len data.oracles) in ();
          verify (updateFutureRounds.maxSubmissions >= updateFutureRounds.minSubmissions) ~msg:"Aggregator_MaxInferiorToMin";
          verify (compute_price_feed_672 >= updateFutureRounds.maxSubmissions) ~msg:"Aggregator_MaxExceedActive";
          verify ((compute_price_feed_672 = 0) || (compute_price_feed_672 > updateFutureRounds.restartDelay)) ~msg:"Aggregator_DelayExceedTotal";
          verify ((compute_price_feed_672 = 0) || (updateFutureRounds.minSubmissions > 0)) ~msg:"Aggregator_MinSubmissionsTooLow";
          verify (data.recordedFunds.available >= ((updateFutureRounds.oraclePayment * compute_price_feed_672) * 2)) ~msg:"Aggregator_InsufficientFundsForPayment";
          data.restartDelay <- updateFutureRounds.restartDelay;
          data.minSubmissions <- updateFutureRounds.minSubmissions;
          data.maxSubmissions <- updateFutureRounds.maxSubmissions;
          data.timeout <- updateFutureRounds.timeout;
          data.oraclePayment <- updateFutureRounds.oraclePayment
        | `changeOracles changeOracles ->
          verify (sender = data.admin) ~msg:"Aggregator_NotAdmin";
          List.iter (fun oracleAddress ->
            Map.delete data.oracles oracleAddress
          ) changeOracles.removed;
          List.iter (fun oracle ->
            match_pair_price_feed_696_fst, match_pair_price_feed_696_snd = match_tuple(oracle, "match_pair_price_feed_696_fst", "match_pair_price_feed_696_snd")
            set_type match_pair_price_feed_696_snd.endingRound (option nat);
            let%mutable endingRound = 4294967295 in ();
            if is_some match_pair_price_feed_696_snd.endingRound then
              endingRound <- open_some match_pair_price_feed_696_snd.endingRound;
            Map.set data.oracles match_pair_price_feed_696_fst {adminAddress = match_pair_price_feed_696_snd.adminAddress; endingRound = endingRound; lastStartedRound = 0; startingRound = match_pair_price_feed_696_snd.startingRound; withdrawable = 0};
            if (data.reportingRoundId <> 0) && (match_pair_price_feed_696_snd.startingRound <= data.reportingRoundId) then
              Set.add data.reportingRoundDetails.activeOracles match_pair_price_feed_696_fst
          ) changeOracles.added

    ) params

  let%entry_point decimals params =
    transfer data.decimals (tez 0) params

  let%entry_point forceBalanceUpdate () =
    transfer {requests = [{owner = self_address; token_id = 0}]; callback = (self_entry_point "updateAvailableFunds")} (tez 0) (open_some (contract {callback = contract (list {balance = nat; request = {owner = address; token_id = nat}}); requests = list {owner = address; token_id = nat}} data.linkToken , entry_point='balance_of') ~message:"Aggregator_InvalidTokenkInterface")

  let%entry_point latestRoundData params =
    transfer (Map.get data.rounds data.latestRoundId) (tez 0) params

  let%entry_point submit params =
    match_pair_price_feed_622_fst, match_pair_price_feed_622_snd = match_tuple(params, "match_pair_price_feed_622_fst", "match_pair_price_feed_622_snd")
    verify data.active;
    verify (contains sender data.oracles) ~msg:"Aggregator_NotOracle";
    verify ((Map.get data.oracles sender).startingRound <= match_pair_price_feed_622_fst) ~msg:"Aggregator_NotYetEnabledOracle";
    verify ((Map.get data.oracles sender).endingRound > match_pair_price_feed_622_fst) ~msg:"Aggregator_NotLongerAllowedOracle";
    verify ((((match_pair_price_feed_622_fst + 1) = data.reportingRoundId) || (match_pair_price_feed_622_fst = data.reportingRoundId)) || (match_pair_price_feed_622_fst = (data.reportingRoundId + 1))) ~msg:"Aggregator_InvalidRound";
    if (match_pair_price_feed_622_fst + 1) = data.reportingRoundId then
      (
        verify (not (contains sender data.previousRoundDetails.submissions)) ~msg:"Aggregator_SubmittedInCurrent";
        verify ((len data.previousRoundDetails.submissions) < data.previousRoundDetails.maxSubmissions) ~msg:"Aggregator_RoundMaxSubmissionExceed";
        Map.set data.previousRoundDetails.submissions sender match_pair_price_feed_622_snd;
        if (len data.previousRoundDetails.submissions) >= data.previousRoundDetails.minSubmissions then
          (
            (Map.get data.rounds (open_some (is_nat (data.reportingRoundId - 1)))).answer <- (Map.values data.previousRoundDetails.submissions) self.median;
            (Map.get data.rounds (open_some (is_nat (data.reportingRoundId - 1)))).updatedAt <- now;
            (Map.get data.rounds (open_some (is_nat (data.reportingRoundId - 1)))).answeredInRound <- data.reportingRoundId
          )
      )
    else
      if match_pair_price_feed_622_fst = data.reportingRoundId then
        (
          verify (not (contains sender data.reportingRoundDetails.submissions)) ~msg:"Aggregator_AlreadySubmittedForThisRound";
          verify ((len data.reportingRoundDetails.submissions) < data.reportingRoundDetails.maxSubmissions) ~msg:"Aggregator_RoundMaxSubmissionExceed";
          Map.set data.reportingRoundDetails.submissions sender match_pair_price_feed_622_snd;
          if (len data.reportingRoundDetails.submissions) >= data.reportingRoundDetails.minSubmissions then
            (
              (Map.get data.rounds data.reportingRoundId).answer <- (Map.values data.reportingRoundDetails.submissions) self.median;
              (Map.get data.rounds data.reportingRoundId).updatedAt <- now;
              (Map.get data.rounds data.reportingRoundId).answeredInRound <- data.reportingRoundId;
              data.latestRoundId <- data.reportingRoundId
            )
        )
      else
        (
          if data.reportingRoundId > 0 then
            (
              verify (((Map.get data.oracles sender).lastStartedRound = 0) || ((data.reportingRoundId + 1) > ((Map.get data.oracles sender).lastStartedRound + data.restartDelay))) ~msg:"Aggregator_WaitBeforeInit";
              verify ((now > (add_seconds (Map.get data.rounds data.reportingRoundId).startedAt ((to_int data.timeout) * 60))) || ((Map.get data.rounds data.reportingRoundId).answeredInRound = data.reportingRoundId)) ~msg:"Aggregator_PreviousRoundNotOver"
            );
          let%mutable answer = 0 in ();
          let%mutable answeredInRound = 0 in ();
          if data.minSubmissions = 1 then
            (
              answer <- match_pair_price_feed_622_snd;
              answeredInRound <- data.reportingRoundId + 1
            );
          Map.set data.rounds (data.reportingRoundId + 1) {answer = answer; answeredInRound = answeredInRound; roundId = (data.reportingRoundId + 1); startedAt = now; updatedAt = now};
          data.previousRoundDetails <- data.reportingRoundDetails;
          let%mutable compute_price_feed_588 = ((data.reportingRoundId + 1) self.getActiveOracles) in ();
          data.reportingRoundDetails <- {activeOracles = compute_price_feed_588; maxSubmissions = data.maxSubmissions; minSubmissions = data.minSubmissions; submissions = (Map.make [(sender, match_pair_price_feed_622_snd)]); timeout = data.timeout};
          (Map.get data.oracles sender).lastStartedRound <- data.reportingRoundId + 1;
          data.reportingRoundId <- data.reportingRoundId + 1
        );
    data.recordedFunds.available <- open_some (is_nat (data.recordedFunds.available - data.oraclePayment)) ~message:"Aggregator_OraclePaymentUnderflow";
    data.recordedFunds.allocated <- data.recordedFunds.allocated + data.oraclePayment;
    (Map.get data.oracles sender).withdrawable <- (Map.get data.oracles sender).withdrawable + data.oraclePayment

  let%entry_point updateAvailableFunds params =
    set_type params (list {balance = nat; request = {owner = address; token_id = nat}});
    verify (sender = data.linkToken) ~msg:"Aggregator_NotLinkToken";
    let%mutable balance = 0 in ();
    List.iter (fun resp ->
      verify (resp.request.owner = self_address);
      balance <- resp.balance
    ) params;
    if balance <> data.recordedFunds.available then
      data.recordedFunds.available <- balance

  let%entry_point withdrawPayment params =
    verify ((Map.get data.oracles params.oracleAddress).adminAddress = sender) ~msg:"Aggregator_NotOracleAdmin";
    verify ((Map.get data.oracles params.oracleAddress).withdrawable >= params.amount) ~msg:"Aggregator_InsufficientWithdrawableFunds";
    (Map.get data.oracles params.oracleAddress).withdrawable <- open_some (is_nat ((Map.get data.oracles params.oracleAddress).withdrawable - params.amount));
    data.recordedFunds.allocated <- open_some (is_nat (data.recordedFunds.allocated - params.amount));
    transfer [{from_ = self_address; txs = [{to_ = params.recipientAddress; token_id = 0; amount = params.amount}]}] (tez 0) (open_some (contract (list {from_ = address; txs = list {amount = nat; to_ = address; token_id = nat}}) data.linkToken , entry_point='transfer') ~message:"Aggregator_InvalidTokenkInterface");
    transfer {requests = [{owner = self_address; token_id = 0}]; callback = (self_entry_point "updateAvailableFunds")} (tez 0) (open_some (contract {callback = contract (list {balance = nat; request = {owner = address; token_id = nat}}); requests = list {owner = address; token_id = nat}} data.linkToken , entry_point='balance_of') ~message:"Aggregator_InvalidTokenkInterface")

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {active = bool; admin = address; decimals = nat; latestRoundId = nat; linkToken = address; maxSubmissions = nat; metadata = big_map string bytes; minSubmissions = nat; oraclePayment = nat; oracles = map address {adminAddress = address; endingRound = nat; lastStartedRound = nat; startingRound = nat; withdrawable = nat}; previousRoundDetails = {activeOracles = set address; maxSubmissions = nat; minSubmissions = nat; submissions = map address nat; timeout = nat}; recordedFunds = {allocated = nat; available = nat}; reportingRoundDetails = {activeOracles = set address; maxSubmissions = nat; minSubmissions = nat; submissions = map address nat; timeout = nat}; reportingRoundId = nat; restartDelay = nat; rounds = big_map nat {answer = nat; answeredInRound = nat; roundId = nat; startedAt = timestamp; updatedAt = timestamp}; timeout = nat}]
      ~storage:[%expr
                 {active = true;
                  admin = address "tz1evBmfWZoPDN38avoRGbjJaLBJUP8AZz6a";
                  decimals = 8;
                  latestRoundId = 0;
                  linkToken = address "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1";
                  maxSubmissions = 6;
                  metadata = Map.make [("", bytes "0x3c55524c3e")];
                  minSubmissions = 3;
                  oraclePayment = 1;
                  oracles = Map.make [(address "KT1LLTzYhdhxTqKu7ByJz8KaShF6qPTdx5os", {adminAddress = address "tz1evBmfWZoPDN38avoRGbjJaLBJUP8AZz6a"; endingRound = 4294967295; lastStartedRound = 0; startingRound = 0; withdrawable = 0}); (address "KT1LhTzYhdhxTqKu7ByJz8KaShF6qPTdx5os", {adminAddress = address "tz1evBmfWZoPDN38avoRGbjJaLBJUP8AZz6a"; endingRound = 4294967295; lastStartedRound = 0; startingRound = 0; withdrawable = 0}); (address "KT1P7oeoKWHx5SXt73qpEanzkr8yeEKABqko", {adminAddress = address "tz1evBmfWZoPDN38avoRGbjJaLBJUP8AZz6a"; endingRound = 4294967295; lastStartedRound = 0; startingRound = 0; withdrawable = 0}); (address "KT1SCkxmTqTkmc7zoAP5uMYT9rp9iqVVRgdt", {adminAddress = address "tz1evBmfWZoPDN38avoRGbjJaLBJUP8AZz6a"; endingRound = 4294967295; lastStartedRound = 0; startingRound = 0; withdrawable = 0})];
                  previousRoundDetails = {activeOracles = Set.make([]); maxSubmissions = 0; minSubmissions = 0; submissions = Map.make []; timeout = 0};
                  recordedFunds = {allocated = 0; available = 0};
                  reportingRoundDetails = {activeOracles = Set.make([]); maxSubmissions = 0; minSubmissions = 0; submissions = Map.make []; timeout = 0};
                  reportingRoundId = 0;
                  restartDelay = 2;
                  rounds = Map.make [];
                  timeout = 10}]
      [administrate; decimals; forceBalanceUpdate; latestRoundData; submit; updateAvailableFunds; withdrawPayment]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())