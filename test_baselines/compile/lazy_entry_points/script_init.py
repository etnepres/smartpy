import smartpy as sp

class C(sp.Contract):
    def __init__(self):
        self.set_initial_balance(sp.tez(100))
        self.init(x = 1, y = True)

    @sp.entry_point(lazify = True)
    def modify_x(self, n):
        self.data.x = n

    @sp.entry_point(lazify = False)
    def update_modify_x(self, ep):
        sp.set_entry_point("modify_x", ep)

    @sp.entry_point(lazify = True, lazy_no_code = True)
    def bounce(self, p):
        sp.set_type(p, sp.TAddress)

    @sp.entry_point(lazify = False)
    def update_bounce(self, ep):
        sp.set_entry_point("bounce", ep)
        sp.send(sp.sender, sp.tez(1))

    @sp.entry_point(lazify = False)
    def take_ticket(self, p):
        sp.set_type(p, sp.TTicket(sp.TInt))

    @sp.entry_point
    def check_bounce(self):
        sp.verify(sp.has_entry_point("bounce"))

def ep_incr(self, params):
    self.data.x += params

def ep_decr(self, params):
    self.data.x -= params

def ep_decr_transfers(self, params):
    sp.send(sp.test_account("me").address, sp.tez(1))
    sp.send(sp.test_account("me").address, sp.tez(2))
    self.data.x -= params

def ep_bounce(self, params):
    sp.send(params, sp.mutez(2))

@sp.add_test(name = "Upgrade")
def test():
    s = sp.test_scenario()

    s.table_of_contents()

    s.h1("Upgradable and Lazy Entry Points")

    alice = sp.test_account("Alice")
    bob = sp.test_account("Bob")

    c = C()
    s += c

    s.h2("Use initial entry point")
    c.modify_x(1)
    c.modify_x(2)
    c.modify_x(3)
    s.verify(c.data.x == 3)

    s.h1("Updating an entry point")

    s.h2("Switch it to incrementing")
    c.update_modify_x(sp.utils.wrap_entry_point("modify_x", ep_incr))
    c.modify_x(1)
    c.modify_x(2)
    c.modify_x(3)
    s.verify(c.data.x == 9)

    s.h2("Switch it to decrementing")
    c.update_modify_x(sp.utils.wrap_entry_point("modify_x", ep_decr))
    c.modify_x(1)
    c.modify_x(2)
    c.modify_x(3)
    s.verify(c.data.x == 3)

    s.h2("Switch it to decrementing with additional transfers")
    c.update_modify_x(sp.utils.wrap_entry_point("modify_x", ep_decr_transfers))
    c.modify_x(1)
    c.modify_x(2)
    c.modify_x(3)
    s.verify(c.data.x == -3)

    s.h1("Testing the presence of an entry point")

    s.h2("Entry point is still absent (lazy_no_code)")
    c.check_bounce().run(valid = False)
    c.bounce(alice.address).run(valid = False)

    s.h2("Add entry point and call it")
    c.update_bounce(sp.utils.wrap_entry_point("bounce", ep_bounce)).run(sender = bob)
    c.bounce(alice.address)
    c.check_bounce()

    s.table_of_contents()
