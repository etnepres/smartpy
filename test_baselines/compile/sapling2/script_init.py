import smartpy as sp

class MyContract(sp.Contract):
    def __init__(self):
        self.init_type(sp.TRecord(ledger = sp.TSaplingState()))

    @sp.entry_point
    def handle(self, operations):
        sp.for operation in operations:
            result = sp.local('result', sp.sapling_verify_update(self.data.ledger, operation.transaction).open_some())
            self.data.ledger = sp.snd(result.value)
            amount = sp.local('amount', sp.fst(result.value))
            amount_tez = sp.local('amount_tez', sp.split_tokens(sp.mutez(1), abs(amount.value), 1))
            sp.if amount.value > 0:
                sp.transfer(sp.unit, amount_tez.value, sp.implicit_account(operation.key.open_some()))
            sp.else:
                sp.verify(~operation.key.is_some())
                sp.verify(sp.amount == amount_tez.value)

@sp.add_test(name = "Sapling2")
def test():
    scenario = sp.test_scenario()
    alice = sp.test_account("alice")

    scenario.h1("Sapling2")

    c1 = MyContract()
    c1.init_storage(sp.record(ledger = sp.sapling_empty_state(8)))

    scenario += c1

    c1.handle([sp.record(key = sp.none,
                                      transaction = sp.sapling_test_transaction("", "alice", 12000000, 8))
                           ]).run(valid = False)

    c1.handle([sp.record(key = sp.none,
                                      transaction = sp.sapling_test_transaction("", "alice", 12000000, 8))
                           ]).run(amount = sp.tez(12))

    c1.handle([sp.record(key = sp.none,
                                      transaction = sp.sapling_test_transaction("alice", "bob", 2000000, 8))
                           ]).run(amount = sp.tez(2), valid = False)

    c1.handle([sp.record(key = sp.none,
                                      transaction = sp.sapling_test_transaction("alice", "bob", 2000000, 8))
                           ]).run()

    c1.handle([sp.record(key = sp.some(alice.public_key_hash),
                                      transaction = sp.sapling_test_transaction("alice", "", 2000000, 8))
                           ]).run()

    c1.handle([sp.record(key = sp.some(alice.public_key_hash),
                                      transaction = sp.sapling_test_transaction("alice", "", 8000000, 8))
                           ]).run()

    c1.handle([sp.record(key = sp.some(alice.public_key_hash),
                                      transaction = sp.sapling_test_transaction("alice", "", 1, 8))
                           ]).run(valid = False)
