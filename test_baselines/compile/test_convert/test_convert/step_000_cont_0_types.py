import smartpy as sp

tstorage = sp.TRecord(x = sp.TRecord(a = sp.TInt, b = sp.TInt).layout(("a", "b")), y = sp.TVariant(A = sp.TInt, B = sp.TInt).layout(("A", "B"))).layout(("x", "y"))
tparameter = sp.TVariant(test_pair = sp.TUnit, test_record = sp.TUnit, test_variant = sp.TUnit).layout(("test_pair", ("test_record", "test_variant")))
tprivates = { }
tviews = { }
