import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(result = sp.TOption(sp.TNat)).layout("result"))
    self.init(result = sp.none)

  @sp.entry_point
  def ep(self, params):
    sp.set_type(params, sp.TNat)
    self.data.result = sp.some(params)

sp.add_compilation_target("test", Contract())