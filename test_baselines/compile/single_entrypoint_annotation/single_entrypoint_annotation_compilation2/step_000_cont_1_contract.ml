open SmartML

module Contract = struct
  let%entry_point ep params =
    set_type params nat;
    data.result <- some params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {result = option nat}]
      ~storage:[%expr
                 {result = None}]
      [ep]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())