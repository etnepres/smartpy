import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(board_state = sp.TPair(sp.TPair(sp.TPair(sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TBool)), sp.TBool), sp.TPair(sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt)), sp.TOption(sp.TPair(sp.TInt, sp.TInt)))), sp.TPair(sp.TPair(sp.TNat, sp.TNat), sp.TPair(sp.TMap(sp.TInt, sp.TPair(sp.TInt, sp.TInt)), sp.TPair(sp.TInt, sp.TMap(sp.TPair(sp.TNat, sp.TInt), sp.TBytes))))), draw_offer = sp.TSet(sp.TAddress), metadata = sp.TBigMap(sp.TString, sp.TBytes), players = sp.TSet(sp.TAddress), players_map = sp.TMap(sp.TInt, sp.TAddress), status = sp.TVariant(Left = sp.TVariant(Left = sp.TUnit, Right = sp.TString).layout(("Left", "Right")), Right = sp.TVariant(Left = sp.TUnit, Right = sp.TUnit).layout(("Left", "Right"))).layout(("Left", "Right"))).layout((("board_state", ("draw_offer", "metadata")), ("players", ("players_map", "status")))))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TVariant(Left = sp.TVariant(Left = sp.TUnit, Right = sp.TPair(sp.TPair(sp.TOption(sp.TPair(sp.TNat, sp.TNat)), sp.TPair(sp.TInt, sp.TInt)), sp.TPair(sp.TOption(sp.TNat), sp.TPair(sp.TInt, sp.TInt)))).layout(("Left", "Right")), Right = sp.TVariant(Left = sp.TUnit, Right = sp.TUnit).layout(("Left", "Right"))).layout(("Left", "Right")), Right = sp.TVariant(Left = sp.TVariant(Left = sp.TUnit, Right = sp.TUnit).layout(("Left", "Right")), Right = sp.TVariant(Left = sp.TPair(sp.TPair(sp.TOption(sp.TPair(sp.TNat, sp.TNat)), sp.TPair(sp.TInt, sp.TInt)), sp.TPair(sp.TOption(sp.TNat), sp.TPair(sp.TInt, sp.TInt))), Right = sp.TPair(sp.TNat, sp.TNat)).layout(("Left", "Right"))).layout(("Left", "Right"))).layout(("Left", "Right")))
    def ferror(error):
      sp.failwith('[Error: to_cmd: (fun (x1 : { (map(int, map(int, int)) ; int) ; (int ; (bool ; int)) }) : list({ int ; int }) ->
let [s447; s448; s449; s450; s451; s452] =
  if Car(Cdr(Cdr(x1)))
  then
    let x288 = Mul(1, Cdr(Cdr(Cdr(x1))))
    let [s298; s299; s300; s301; s302; s303; s304; s305] =
      match Get(Add(Cdr(Car(x1)), Cdr(Cdr(Cdr(x1)))), Car(Car(x1))) with
      | None _ ->
          [ Empty_set<int,int>
          ; x288
          ; Nil<{ int ; int }>
          ; Cdr(Cdr(Cdr(x1)))
          ; Car(Cdr(x1))
          ; Car(Car(x1))
          ; Cdr(Car(x1))
          ; x1
          ]
      | Some s296 ->
          [ s296
          ; x288
          ; Nil<{ int ; int }>
          ; Cdr(Cdr(Cdr(x1)))
          ; Car(Cdr(x1))
          ; Car(Car(x1))
          ; Cdr(Car(x1))
          ; x1
          ]
      end
    let [s318; s319; s320; s321; s322; s323; s324; s325] =
      match Get(Sub(s302, 1), s298) with
      | None _ -> [ 0; s299; s300; s301; s302; s303; s304; s305 ]
      | Some s316 -> [ s316; s299; s300; s301; s302; s303; s304; s305 ]
      end
    let [s350; s351; s352; s353; s354; s355] =
      if Eq(Compare(s318, s319))
      then
        [ Cons(Pair(Add(s324, s321), Sub(s322, 1)), s320)
        ; s321
        ; s322
        ; s323
        ; s324
        ; s325
        ]
      else
        [ s320; s321; s322; s323; s324; s325 ]
    let x379 = Mul(1, s351)
    let [s389; s390; s391; s392; s393; s394; s395; s396] =
      match Get(Add(s354, s351), s353) with
      | None _ ->
          [ Empty_set<int,int>; x379; s350; s351; s352; s353; s354; s355 ]
      | Some s387 -> [ s387; x379; s350; s351; s352; s353; s354; s355 ]
      end
    let [s409; s410; s411; s412; s413; s414; s415; s416] =
      match Get(Add(s393, 1), s389) with
      | None _ -> [ 0; s390; s391; s392; s393; s394; s395; s396 ]
      | Some s407 -> [ s407; s390; s391; s392; s393; s394; s395; s396 ]
      end
    let [s441; s442; s443; s444; s445; s446] =
      if Eq(Compare(s409, s410))
      then
        [ Cons(Pair(Add(s415, s412), Add(s413, 1)), s411)
        ; s412
        ; s413
        ; s414
        ; s415
        ; s416
        ]
      else
        [ s411; s412; s413; s414; s415; s416 ]
    [ s441; s442; s443; s444; s445; s446 ]
  else
    let x48 = Mul(1, Cdr(Cdr(Cdr(x1))))
    let [s58; s59; s60; s61; s62; s63; s64; s65] =
      match Get(Sub(Cdr(Car(x1)), Cdr(Cdr(Cdr(x1)))), Car(Car(x1))) with
      | None _ ->
          [ Empty_set<int,int>
          ; x48
          ; Nil<{ int ; int }>
          ; Cdr(Cdr(Cdr(x1)))
          ; Car(Cdr(x1))
          ; Car(Car(x1))
          ; Cdr(Car(x1))
          ; x1
          ]
      | Some s56 ->
          [ s56
          ; x48
          ; Nil<{ int ; int }>
          ; Cdr(Cdr(Cdr(x1)))
          ; Car(Cdr(x1))
          ; Car(Car(x1))
          ; Cdr(Car(x1))
          ; x1
          ]
      end
    let [s75; s76; s77; s78; s79; s80; s81; s82] =
      match Get(s62, s58) with
      | None _ -> [ 0; s59; s60; s61; s62; s63; s64; s65 ]
      | Some s73 -> [ s73; s59; s60; s61; s62; s63; s64; s65 ]
      end
    let [s104; s105; s106; s107; s108; s109] =
      if Eq(Compare(s75, s76))
      then
        [ Cons(Pair(Sub(s81, s78), s79), s77); s78; s79; s80; s81; s82 ]
      else
        [ s77; s78; s79; s80; s81; s82 ]
    let [s128; s129; s130; s131; s132; s133; s134] =
      if Eq(Compare(s105, 1))
      then
        [ Eq(Compare(s108, 3)); s104; s105; s106; s107; s108; s109 ]
      else
        [ False; s104; s105; s106; s107; s108; s109 ]
    let [s161; s162; s163; s164; s165; s166; s167] =
      if s128
      then
        [ True; s129; s130; s131; s132; s133; s134 ]
      else
        let [s153; s154; s155; s156; s157; s158; s159] =
          if Eq(Compare(s130, -1))
          then
            [ Eq(Compare(s133, 4)); s129; s130; s131; s132; s133; s134 ]
          else
            [ False; s129; s130; s131; s132; s133; s134 ]
        [ s153; s154; s155; s156; s157; s158; s159 ]
    let [s259; s260; s261; s262; s263; s264] =
      if s161
      then
        let x194 = Mul(1, s163)
        let [s204; s205; s206; s207; s208; s209; s210; s211] =
          match Get(Add(s166, Mul(s163, 2)), s165) with
          | None _ ->
              [ Empty_set<int,int>
              ; x194
              ; s162
              ; s163
              ; s164
              ; s165
              ; s166
              ; s167
              ]
          | Some s202 -> [ s202; x194; s162; s163; s164; s165; s166; s167 ]
          end
        let [s221; s222; s223; s224; s225; s226; s227; s228] =
          match Get(s208, s204) with
          | None _ -> [ 0; s205; s206; s207; s208; s209; s210; s211 ]
          | Some s219 -> [ s219; s205; s206; s207; s208; s209; s210; s211 ]
          end
        let [s253; s254; s255; s256; s257; s258] =
          if Eq(Compare(s221, s222))
          then
            [ Cons(Pair(Add(s227, Mul(s224, 2)), s225), s223)
            ; s224
            ; s225
            ; s226
            ; s227
            ; s228
            ]
          else
            [ s223; s224; s225; s226; s227; s228 ]
        [ s253; s254; s255; s256; s257; s258 ]
      else
        [ s162; s163; s164; s165; s166; s167 ]
    [ s259; s260; s261; s262; s263; s264 ]
let x472 = Mul(3, s448)
let [s482; s483; s484; s485; s486; s487; s488; s489] =
  match Get(Add(s451, 2), s450) with
  | None _ ->
      [ Empty_set<int,int>; x472; s447; s448; s449; s450; s451; s452 ]
  | Some s480 -> [ s480; x472; s447; s448; s449; s450; s451; s452 ]
  end
let [s502; s503; s504; s505; s506; s507; s508; s509] =
  match Get(Sub(s486, 1), s482) with
  | None _ -> [ 0; s483; s484; s485; s486; s487; s488; s489 ]
  | Some s500 -> [ s500; s483; s484; s485; s486; s487; s488; s489 ]
  end
let [s531; s532; s533; s534; s535; s536] =
  if Eq(Compare(s502, s503))
  then
    [ Cons(Pair(Add(s508, 2), Sub(s506, 1)), s504)
    ; s505
    ; s506
    ; s507
    ; s508
    ; s509
    ]
  else
    [ s504; s505; s506; s507; s508; s509 ]
let x556 = Mul(3, s532)
let [s566; s567; s568; s569; s570; s571; s572; s573] =
  match Get(Add(s535, 2), s534) with
  | None _ ->
      [ Empty_set<int,int>; x556; s531; s532; s533; s534; s535; s536 ]
  | Some s564 -> [ s564; x556; s531; s532; s533; s534; s535; s536 ]
  end
let [s586; s587; s588; s589; s590; s591; s592; s593] =
  match Get(Add(s570, 1), s566) with
  | None _ -> [ 0; s567; s568; s569; s570; s571; s572; s573 ]
  | Some s584 -> [ s584; s567; s568; s569; s570; s571; s572; s573 ]
  end
let [s615; s616; s617; s618; s619; s620] =
  if Eq(Compare(s586, s587))
  then
    [ Cons(Pair(Add(s592, 2), Add(s590, 1)), s588)
    ; s589
    ; s590
    ; s591
    ; s592
    ; s593
    ]
  else
    [ s588; s589; s590; s591; s592; s593 ]
let x640 = Mul(3, s616)
let [s650; s651; s652; s653; s654; s655; s656; s657] =
  match Get(Add(s619, 1), s618) with
  | None _ ->
      [ Empty_set<int,int>; x640; s615; s616; s617; s618; s619; s620 ]
  | Some s648 -> [ s648; x640; s615; s616; s617; s618; s619; s620 ]
  end
let [s670; s671; s672; s673; s674; s675; s676; s677] =
  match Get(Sub(s654, 2), s650) with
  | None _ -> [ 0; s651; s652; s653; s654; s655; s656; s657 ]
  | Some s668 -> [ s668; s651; s652; s653; s654; s655; s656; s657 ]
  end
let [s699; s700; s701; s702; s703; s704] =
  if Eq(Compare(s670, s671))
  then
    [ Cons(Pair(Add(s676, 1), Sub(s674, 2)), s672)
    ; s673
    ; s674
    ; s675
    ; s676
    ; s677
    ]
  else
    [ s672; s673; s674; s675; s676; s677 ]
let x724 = Mul(3, s700)
let [s734; s735; s736; s737; s738; s739; s740; s741] =
  match Get(Add(s703, 1), s702) with
  | None _ ->
      [ Empty_set<int,int>; x724; s699; s700; s701; s702; s703; s704 ]
  | Some s732 -> [ s732; x724; s699; s700; s701; s702; s703; s704 ]
  end
let [s754; s755; s756; s757; s758; s759; s760; s761] =
  match Get(Add(s738, 2), s734) with
  | None _ -> [ 0; s735; s736; s737; s738; s739; s740; s741 ]
  | Some s752 -> [ s752; s735; s736; s737; s738; s739; s740; s741 ]
  end
let [s783; s784; s785; s786; s787; s788] =
  if Eq(Compare(s754, s755))
  then
    [ Cons(Pair(Add(s760, 1), Add(s758, 2)), s756)
    ; s757
    ; s758
    ; s759
    ; s760
    ; s761
    ]
  else
    [ s756; s757; s758; s759; s760; s761 ]
let x808 = Mul(3, s784)
let [s818; s819; s820; s821; s822; s823; s824; s825] =
  match Get(Sub(s787, 2), s786) with
  | None _ ->
      [ Empty_set<int,int>; x808; s783; s784; s785; s786; s787; s788 ]
  | Some s816 -> [ s816; x808; s783; s784; s785; s786; s787; s788 ]
  end
let [s838; s839; s840; s841; s842; s843; s844; s845] =
  match Get(Sub(s822, 1), s818) with
  | None _ -> [ 0; s819; s820; s821; s822; s823; s824; s825 ]
  | Some s836 -> [ s836; s819; s820; s821; s822; s823; s824; s825 ]
  end
let [s867; s868; s869; s870; s871; s872] =
  if Eq(Compare(s838, s839))
  then
    [ Cons(Pair(Sub(s844, 2), Sub(s842, 1)), s840)
    ; s841
    ; s842
    ; s843
    ; s844
    ; s845
    ]
  else
    [ s840; s841; s842; s843; s844; s845 ]
let x892 = Mul(3, s868)
let [s902; s903; s904; s905; s906; s907; s908; s909] =
  match Get(Sub(s871, 2), s870) with
  | None _ ->
      [ Empty_set<int,int>; x892; s867; s868; s869; s870; s871; s872 ]
  | Some s900 -> [ s900; x892; s867; s868; s869; s870; s871; s872 ]
  end
let [s922; s923; s924; s925; s926; s927; s928; s929] =
  match Get(Add(s906, 1), s902) with
  | None _ -> [ 0; s903; s904; s905; s906; s907; s908; s909 ]
  | Some s920 -> [ s920; s903; s904; s905; s906; s907; s908; s909 ]
  end
let [s951; s952; s953; s954; s955; s956] =
  if Eq(Compare(s922, s923))
  then
    [ Cons(Pair(Sub(s928, 2), Add(s926, 1)), s924)
    ; s925
    ; s926
    ; s927
    ; s928
    ; s929
    ]
  else
    [ s924; s925; s926; s927; s928; s929 ]
let x976 = Mul(3, s952)
let [s986; s987; s988; s989; s990; s991; s992; s993] =
  match Get(Sub(s955, 1), s954) with
  | None _ ->
      [ Empty_set<int,int>; x976; s951; s952; s953; s954; s955; s956 ]
  | Some s984 -> [ s984; x976; s951; s952; s953; s954; s955; s956 ]
  end
let [s1006; s1007; s1008; s1009; s1010; s1011; s1012; s1013] =
  match Get(Sub(s990, 2), s986) with
  | None _ -> [ 0; s987; s988; s989; s990; s991; s992; s993 ]
  | Some s1004 -> [ s1004; s987; s988; s989; s990; s991; s992; s993 ]
  end
let [s1035; s1036; s1037; s1038; s1039; s1040] =
  if Eq(Compare(s1006, s1007))
  then
    [ Cons(Pair(Sub(s1012, 1), Sub(s1010, 2)), s1008)
    ; s1009
    ; s1010
    ; s1011
    ; s1012
    ; s1013
    ]
  else
    [ s1008; s1009; s1010; s1011; s1012; s1013 ]
let x1060 = Mul(3, s1036)
let [s1070; s1071; s1072; s1073; s1074; s1075; s1076; s1077] =
  match Get(Sub(s1039, 1), s1038) with
  | None _ ->
      [ Empty_set<int,int>; x1060; s1035; s1036; s1037; s1038; s1039; s1040 ]
  | Some s1068 -> [ s1068; x1060; s1035; s1036; s1037; s1038; s1039; s1040 ]
  end
let [s1090; s1091; s1092; s1093; s1094; s1095; s1096; s1097] =
  match Get(Add(s1074, 2), s1070) with
  | None _ -> [ 0; s1071; s1072; s1073; s1074; s1075; s1076; s1077 ]
  | Some s1088 -> [ s1088; s1071; s1072; s1073; s1074; s1075; s1076; s1077 ]
  end
let [s1119; s1120; s1121; s1122; s1123; s1124] =
  if Eq(Compare(s1090, s1091))
  then
    [ Cons(Pair(Sub(s1096, 1), Add(s1094, 2)), s1092)
    ; s1093
    ; s1094
    ; s1095
    ; s1096
    ; s1097
    ]
  else
    [ s1092; s1093; s1094; s1095; s1096; s1097 ]
let x1141 = Mul(Mul(6, s1120), -1)
let [s1150; s1151; s1152; s1153; s1154; s1155; s1156; s1157] =
  match Get(s1123, s1122) with
  | None _ ->
      [ Empty_set<int,int>; x1141; s1119; s1120; s1121; s1122; s1123; s1124 ]
  | Some s1148 -> [ s1148; x1141; s1119; s1120; s1121; s1122; s1123; s1124 ]
  end
let [s1167; s1168; s1169; s1170; s1171; s1172; s1173; s1174] =
  match Get(s1154, s1150) with
  | None _ -> [ 0; s1151; s1152; s1153; s1154; s1155; s1156; s1157 ]
  | Some s1165 -> [ s1165; s1151; s1152; s1153; s1154; s1155; s1156; s1157 ]
  end
let [s1849; s1850; s1851; s1852; s1853; s1854] =
  if Neq(Compare(s1167, s1168))
  then
    let x1196 = Mul(6, s1170)
    let [s1206; s1207; s1208; s1209; s1210; s1211; s1212; s1213] =
      match Get(Add(s1173, 1), s1172) with
      | None _ ->
          [ Empty_set<int,int>
          ; x1196
          ; s1169
          ; s1170
          ; s1171
          ; s1172
          ; s1173
          ; s1174
          ]
      | Some s1204 ->
          [ s1204; x1196; s1169; s1170; s1171; s1172; s1173; s1174 ]
      end
    let [s1226; s1227; s1228; s1229; s1230; s1231; s1232; s1233] =
      match Get(Add(s1210, 1), s1206) with
      | None _ -> [ 0; s1207; s1208; s1209; s1210; s1211; s1212; s1213 ]
      | Some s1224 ->
          [ s1224; s1207; s1208; s1209; s1210; s1211; s1212; s1213 ]
      end
    let [s1255; s1256; s1257; s1258; s1259; s1260] =
      if Eq(Compare(s1226, s1227))
      then
        [ Cons(Pair(Add(s1232, 1), Add(s1230, 1)), s1228)
        ; s1229
        ; s1230
        ; s1231
        ; s1232
        ; s1233
        ]
      else
        [ s1228; s1229; s1230; s1231; s1232; s1233 ]
    let x1280 = Mul(6, s1256)
    let [s1290; s1291; s1292; s1293; s1294; s1295; s1296; s1297] =
      match Get(Sub(s1259, 1), s1258) with
      | None _ ->
          [ Empty_set<int,int>
          ; x1280
          ; s1255
          ; s1256
          ; s1257
          ; s1258
          ; s1259
          ; s1260
          ]
      | Some s1288 ->
          [ s1288; x1280; s1255; s1256; s1257; s1258; s1259; s1260 ]
      end
    let [s1310; s1311; s1312; s1313; s1314; s1315; s1316; s1317] =
      match Get(Sub(s1294, 1), s1290) with
      | None _ -> [ 0; s1291; s1292; s1293; s1294; s1295; s1296; s1297 ]
      | Some s1308 ->
          [ s1308; s1291; s1292; s1293; s1294; s1295; s1296; s1297 ]
      end
    let [s1339; s1340; s1341; s1342; s1343; s1344] =
      if Eq(Compare(s1310, s1311))
      then
        [ Cons(Pair(Sub(s1316, 1), Sub(s1314, 1)), s1312)
        ; s1313
        ; s1314
        ; s1315
        ; s1316
        ; s1317
        ]
      else
        [ s1312; s1313; s1314; s1315; s1316; s1317 ]
    let x1364 = Mul(6, s1340)
    let [s1374; s1375; s1376; s1377; s1378; s1379; s1380; s1381] =
      match Get(Add(s1343, 1), s1342) with
      | None _ ->
          [ Empty_set<int,int>
          ; x1364
          ; s1339
          ; s1340
          ; s1341
          ; s1342
          ; s1343
          ; s1344
          ]
      | Some s1372 ->
          [ s1372; x1364; s1339; s1340; s1341; s1342; s1343; s1344 ]
      end
    let [s1394; s1395; s1396; s1397; s1398; s1399; s1400; s1401] =
      match Get(Sub(s1378, 1), s1374) with
      | None _ -> [ 0; s1375; s1376; s1377; s1378; s1379; s1380; s1381 ]
      | Some s1392 ->
          [ s1392; s1375; s1376; s1377; s1378; s1379; s1380; s1381 ]
      end
    let [s1423; s1424; s1425; s1426; s1427; s1428] =
      if Eq(Compare(s1394, s1395))
      then
        [ Cons(Pair(Add(s1400, 1), Sub(s1398, 1)), s1396)
        ; s1397
        ; s1398
        ; s1399
        ; s1400
        ; s1401
        ]
      else
        [ s1396; s1397; s1398; s1399; s1400; s1401 ]
    let x1448 = Mul(6, s1424)
    let [s1458; s1459; s1460; s1461; s1462; s1463; s1464; s1465] =
      match Get(Sub(s1427, 1), s1426) with
      | None _ ->
          [ Empty_set<int,int>
          ; x1448
          ; s1423
          ; s1424
          ; s1425
          ; s1426
          ; s1427
          ; s1428
          ]
      | Some s1456 ->
          [ s1456; x1448; s1423; s1424; s1425; s1426; s1427; s1428 ]
      end
    let [s1478; s1479; s1480; s1481; s1482; s1483; s1484; s1485] =
      match Get(Add(s1462, 1), s1458) with
      | None _ -> [ 0; s1459; s1460; s1461; s1462; s1463; s1464; s1465 ]
      | Some s1476 ->
          [ s1476; s1459; s1460; s1461; s1462; s1463; s1464; s1465 ]
      end
    let [s1507; s1508; s1509; s1510; s1511; s1512] =
      if Eq(Compare(s1478, s1479))
      then
        [ Cons(Pair(Sub(s1484, 1), Add(s1482, 1)), s1480)
        ; s1481
        ; s1482
        ; s1483
        ; s1484
        ; s1485
        ]
      else
        [ s1480; s1481; s1482; s1483; s1484; s1485 ]
    let x1532 = Mul(6, s1508)
    let [s1542; s1543; s1544; s1545; s1546; s1547; s1548; s1549] =
      match Get(Add(s1511, 0), s1510) with
      | None _ ->
          [ Empty_set<int,int>
          ; x1532
          ; s1507
          ; s1508
          ; s1509
          ; s1510
          ; s1511
          ; s1512
          ]
      | Some s1540 ->
          [ s1540; x1532; s1507; s1508; s1509; s1510; s1511; s1512 ]
      end
    let [s1562; s1563; s1564; s1565; s1566; s1567; s1568; s1569] =
      match Get(Add(s1546, 1), s1542) with
      | None _ -> [ 0; s1543; s1544; s1545; s1546; s1547; s1548; s1549 ]
      | Some s1560 ->
          [ s1560; s1543; s1544; s1545; s1546; s1547; s1548; s1549 ]
      end
    let [s1591; s1592; s1593; s1594; s1595; s1596] =
      if Eq(Compare(s1562, s1563))
      then
        [ Cons(Pair(Add(s1568, 0), Add(s1566, 1)), s1564)
        ; s1565
        ; s1566
        ; s1567
        ; s1568
        ; s1569
        ]
      else
        [ s1564; s1565; s1566; s1567; s1568; s1569 ]
    let x1616 = Mul(6, s1592)
    let [s1626; s1627; s1628; s1629; s1630; s1631; s1632; s1633] =
      match Get(Add(s1595, 1), s1594) with
      | None _ ->
          [ Empty_set<int,int>
          ; x1616
          ; s1591
          ; s1592
          ; s1593
          ; s1594
          ; s1595
          ; s1596
          ]
      | Some s1624 ->
          [ s1624; x1616; s1591; s1592; s1593; s1594; s1595; s1596 ]
      end
    let [s1646; s1647; s1648; s1649; s1650; s1651; s1652; s1653] =
      match Get(Add(s1630, 0), s1626) with
      | None _ -> [ 0; s1627; s1628; s1629; s1630; s1631; s1632; s1633 ]
      | Some s1644 ->
          [ s1644; s1627; s1628; s1629; s1630; s1631; s1632; s1633 ]
      end
    let [s1675; s1676; s1677; s1678; s1679; s1680] =
      if Eq(Compare(s1646, s1647))
      then
        [ Cons(Pair(Add(s1652, 1), Add(s1650, 0)), s1648)
        ; s1649
        ; s1650
        ; s1651
        ; s1652
        ; s1653
        ]
      else
        [ s1648; s1649; s1650; s1651; s1652; s1653 ]
    let x1700 = Mul(6, s1676)
    let [s1710; s1711; s1712; s1713; s1714; s1715; s1716; s1717] =
      match Get(Add(s1679, 0), s1678) with
      | None _ ->
          [ Empty_set<int,int>
          ; x1700
          ; s1675
          ; s1676
          ; s1677
          ; s1678
          ; s1679
          ; s1680
          ]
      | Some s1708 ->
          [ s1708; x1700; s1675; s1676; s1677; s1678; s1679; s1680 ]
      end
    let [s1730; s1731; s1732; s1733; s1734; s1735; s1736; s1737] =
      match Get(Sub(s1714, 1), s1710) with
      | None _ -> [ 0; s1711; s1712; s1713; s1714; s1715; s1716; s1717 ]
      | Some s1728 ->
          [ s1728; s1711; s1712; s1713; s1714; s1715; s1716; s1717 ]
      end
    let [s1759; s1760; s1761; s1762; s1763; s1764] =
      if Eq(Compare(s1730, s1731))
      then
        [ Cons(Pair(Add(s1736, 0), Sub(s1734, 1)), s1732)
        ; s1733
        ; s1734
        ; s1735
        ; s1736
        ; s1737
        ]
      else
        [ s1732; s1733; s1734; s1735; s1736; s1737 ]
    let x1784 = Mul(6, s1760)
    let [s1794; s1795; s1796; s1797; s1798; s1799; s1800; s1801] =
      match Get(Sub(s1763, 1), s1762) with
      | None _ ->
          [ Empty_set<int,int>
          ; x1784
          ; s1759
          ; s1760
          ; s1761
          ; s1762
          ; s1763
          ; s1764
          ]
      | Some s1792 ->
          [ s1792; x1784; s1759; s1760; s1761; s1762; s1763; s1764 ]
      end
    let [s1814; s1815; s1816; s1817; s1818; s1819; s1820; s1821] =
      match Get(Add(s1798, 0), s1794) with
      | None _ -> [ 0; s1795; s1796; s1797; s1798; s1799; s1800; s1801 ]
      | Some s1812 ->
          [ s1812; s1795; s1796; s1797; s1798; s1799; s1800; s1801 ]
      end
    let [s1843; s1844; s1845; s1846; s1847; s1848] =
      if Eq(Compare(s1814, s1815))
      then
        [ Cons(Pair(Sub(s1820, 1), Add(s1818, 0)), s1816)
        ; s1817
        ; s1818
        ; s1819
        ; s1820
        ; s1821
        ]
      else
        [ s1816; s1817; s1818; s1819; s1820; s1821 ]
    [ s1843; s1844; s1845; s1846; s1847; s1848 ]
  else
    [ s1169; s1170; s1171; s1172; s1173; s1174 ]
let [_; _; _; r2156; r2157; r2158; r2159; r2160; r2161] =
  loop [ Gt(Compare(s1851, 1))
       ; 1
       ; s1851
       ; True
       ; s1849
       ; s1850
       ; s1851
       ; s1852
       ; s1853
       ; s1854
       ]
  step s1871; s1872; s1873; s1874; s1875; s1876; s1877; s1878; s1879 ->
    let [s2134; s2135; s2136; s2137; s2138; s2139; s2140; s2141; s2142] =
      if s1873
      then
        let [s1913; s1914; s1915; s1916; s1917; s1918; s1919; s1920; s1921;
              s1922] =
          match Get(Add(s1878, Mul(s1871, 0)), s1877) with
          | None _ ->
              [ Empty_set<int,int>
              ; s1871
              ; s1872
              ; s1873
              ; s1874
              ; s1875
              ; s1876
              ; s1877
              ; s1878
              ; s1879
              ]
          | Some s1911 ->
              [ s1911
              ; s1871
              ; s1872
              ; s1873
              ; s1874
              ; s1875
              ; s1876
              ; s1877
              ; s1878
              ; s1879
              ]
          end
        let [s1942; s1943; s1944; s1945; s1946; s1947; s1948; s1949; s1950;
              s1951] =
          match Get(Add(s1919, Mul(s1914, -1)), s1913) with
          | None _ ->
              [ 0
              ; s1914
              ; s1915
              ; s1916
              ; s1917
              ; s1918
              ; s1919
              ; s1920
              ; s1921
              ; s1922
              ]
          | Some s1940 ->
              [ s1940
              ; s1914
              ; s1915
              ; s1916
              ; s1917
              ; s1918
              ; s1919
              ; s1920
              ; s1921
              ; s1922
              ]
          end
        let [s1979; s1980; s1981; s1982; s1983; s1984; s1985; s1986; s1987;
              s1988; s1989] =
          if Neq(Compare(0, s1942))
          then
            [ Neq(Compare(s1942, Mul(Mul(6, s1947), -1)))
            ; s1942
            ; s1943
            ; s1944
            ; s1945
            ; s1946
            ; s1947
            ; s1948
            ; s1949
            ; s1950
            ; s1951
            ]
          else
            [ False
            ; s1942
            ; s1943
            ; s1944
            ; s1945
            ; s1946
            ; s1947
            ; s1948
            ; s1949
            ; s1950
            ; s1951
            ]
        let [s2125; s2126; s2127; s2128; s2129; s2130; s2131; s2132; s2133] =
          if s1979
          then
            let [s2036; _; s2038; s2039; s2040; s2041; s2042; s2043; s2044;
                  s2045; s2046] =
              if Eq(Compare(s1980, Mul(5, s1985)))
              then
                [ True
                ; s1980
                ; s1981
                ; s1982
                ; s1983
                ; s1984
                ; s1985
                ; s1986
                ; s1987
                ; s1988
                ; s1989
                ]
              else
                [ Eq(Compare(s1980, Mul(2, s1985)))
                ; s1980
                ; s1981
                ; s1982
                ; s1983
                ; s1984
                ; s1985
                ; s1986
                ; s1987
                ; s1988
                ; s1989
                ]
            let [s2116; s2117; s2118; s2119; s2120; s2121; s2122; s2123;
                  s2124] =
              if s2036
              then
                [ s2038
                ; s2039
                ; s2040
                ; Cons
                    ( Pair
                        ( Add(s2045, Mul(s2038, 0))
                        , Add(s2043, Mul(s2038, -1))
                        )
                    , s2041
                    )
                ; s2042
                ; s2043
                ; s2044
                ; s2045
                ; s2046
                ]
              else
                [ s2038
                ; s2039
                ; False
                ; s2041
                ; s2042
                ; s2043
                ; s2044
                ; s2045
                ; s2046
                ]
            [ s2116; s2117; s2118; s2119; s2120; s2121; s2122; s2123; s2124 ]
          else
            [ s1981; s1982; s1983; s1984; s1985; s1986; s1987; s1988; s1989 ]
        [ s2125; s2126; s2127; s2128; s2129; s2130; s2131; s2132; s2133 ]
      else
        [ s1871; s1872; s1873; s1874; s1875; s1876; s1877; s1878; s1879 ]
    let x2146 = Add(1, s2134)
    [ Gt(Compare(s2135, x2146))
    ; x2146
    ; s2135
    ; s2136
    ; s2137
    ; s2138
    ; s2139
    ; s2140
    ; s2141
    ; s2142
    ]
let x2182 = Sub(8, r2158)
let [_; _; _; r2471; r2472; r2473; r2474; r2475; r2476] =
  loop [ Gt(Compare(x2182, 1))
       ; 1
       ; x2182
       ; True
       ; r2156
       ; r2157
       ; r2158
       ; r2159
       ; r2160
       ; r2161
       ]
  step s2186; s2187; s2188; s2189; s2190; s2191; s2192; s2193; s2194 ->
    let [s2449; s2450; s2451; s2452; s2453; s2454; s2455; s2456; s2457] =
      if s2188
      then
        let [s2228; s2229; s2230; s2231; s2232; s2233; s2234; s2235; s2236;
              s2237] =
          match Get(Add(s2193, Mul(s2186, 0)), s2192) with
          | None _ ->
              [ Empty_set<int,int>
              ; s2186
              ; s2187
              ; s2188
              ; s2189
              ; s2190
              ; s2191
              ; s2192
              ; s2193
              ; s2194
              ]
          | Some s2226 ->
              [ s2226
              ; s2186
              ; s2187
              ; s2188
              ; s2189
              ; s2190
              ; s2191
              ; s2192
              ; s2193
              ; s2194
              ]
          end
        let [s2257; s2258; s2259; s2260; s2261; s2262; s2263; s2264; s2265;
              s2266] =
          match Get(Add(s2234, Mul(s2229, 1)), s2228) with
          | None _ ->
              [ 0
              ; s2229
              ; s2230
              ; s2231
              ; s2232
              ; s2233
              ; s2234
              ; s2235
              ; s2236
              ; s2237
              ]
          | Some s2255 ->
              [ s2255
              ; s2229
              ; s2230
              ; s2231
              ; s2232
              ; s2233
              ; s2234
              ; s2235
              ; s2236
              ; s2237
              ]
          end
        let [s2294; s2295; s2296; s2297; s2298; s2299; s2300; s2301; s2302;
              s2303; s2304] =
          if Neq(Compare(0, s2257))
          then
            [ Neq(Compare(s2257, Mul(Mul(6, s2262), -1)))
            ; s2257
            ; s2258
            ; s2259
            ; s2260
            ; s2261
            ; s2262
            ; s2263
            ; s2264
            ; s2265
            ; s2266
            ]
          else
            [ False
            ; s2257
            ; s2258
            ; s2259
            ; s2260
            ; s2261
            ; s2262
            ; s2263
            ; s2264
            ; s2265
            ; s2266
            ]
        let [s2440; s2441; s2442; s2443; s2444; s2445; s2446; s2447; s2448] =
          if s2294
          then
            let [s2351; _; s2353; s2354; s2355; s2356; s2357; s2358; s2359;
                  s2360; s2361] =
              if Eq(Compare(s2295, Mul(5, s2300)))
              then
                [ True
                ; s2295
                ; s2296
                ; s2297
                ; s2298
                ; s2299
                ; s2300
                ; s2301
                ; s2302
                ; s2303
                ; s2304
                ]
              else
                [ Eq(Compare(s2295, Mul(2, s2300)))
                ; s2295
                ; s2296
                ; s2297
                ; s2298
                ; s2299
                ; s2300
                ; s2301
                ; s2302
                ; s2303
                ; s2304
                ]
            let [s2431; s2432; s2433; s2434; s2435; s2436; s2437; s2438;
                  s2439] =
              if s2351
              then
                [ s2353
                ; s2354
                ; s2355
                ; Cons
                    ( Pair
                        ( Add(s2360, Mul(s2353, 0))
                        , Add(s2358, Mul(s2353, 1))
                        )
                    , s2356
                    )
                ; s2357
                ; s2358
                ; s2359
                ; s2360
                ; s2361
                ]
              else
                [ s2353
                ; s2354
                ; False
                ; s2356
                ; s2357
                ; s2358
                ; s2359
                ; s2360
                ; s2361
                ]
            [ s2431; s2432; s2433; s2434; s2435; s2436; s2437; s2438; s2439 ]
          else
            [ s2296; s2297; s2298; s2299; s2300; s2301; s2302; s2303; s2304 ]
        [ s2440; s2441; s2442; s2443; s2444; s2445; s2446; s2447; s2448 ]
      else
        [ s2186; s2187; s2188; s2189; s2190; s2191; s2192; s2193; s2194 ]
    let x2461 = Add(1, s2449)
    [ Gt(Compare(s2450, x2461))
    ; x2461
    ; s2450
    ; s2451
    ; s2452
    ; s2453
    ; s2454
    ; s2455
    ; s2456
    ; s2457
    ]
let [_; _; _; r2786; r2787; r2788; r2789; r2790; r2791] =
  loop [ Gt(Compare(r2475, 1))
       ; 1
       ; r2475
       ; True
       ; r2471
       ; r2472
       ; r2473
       ; r2474
       ; r2475
       ; r2476
       ]
  step s2501; s2502; s2503; s2504; s2505; s2506; s2507; s2508; s2509 ->
    let [s2764; s2765; s2766; s2767; s2768; s2769; s2770; s2771; s2772] =
      if s2503
      then
        let [s2543; s2544; s2545; s2546; s2547; s2548; s2549; s2550; s2551;
              s2552] =
          match Get(Add(s2508, Mul(s2501, -1)), s2507) with
          | None _ ->
              [ Empty_set<int,int>
              ; s2501
              ; s2502
              ; s2503
              ; s2504
              ; s2505
              ; s2506
              ; s2507
              ; s2508
              ; s2509
              ]
          | Some s2541 ->
              [ s2541
              ; s2501
              ; s2502
              ; s2503
              ; s2504
              ; s2505
              ; s2506
              ; s2507
              ; s2508
              ; s2509
              ]
          end
        let [s2572; s2573; s2574; s2575; s2576; s2577; s2578; s2579; s2580;
              s2581] =
          match Get(Add(s2549, Mul(s2544, 0)), s2543) with
          | None _ ->
              [ 0
              ; s2544
              ; s2545
              ; s2546
              ; s2547
              ; s2548
              ; s2549
              ; s2550
              ; s2551
              ; s2552
              ]
          | Some s2570 ->
              [ s2570
              ; s2544
              ; s2545
              ; s2546
              ; s2547
              ; s2548
              ; s2549
              ; s2550
              ; s2551
              ; s2552
              ]
          end
        let [s2609; s2610; s2611; s2612; s2613; s2614; s2615; s2616; s2617;
              s2618; s2619] =
          if Neq(Compare(0, s2572))
          then
            [ Neq(Compare(s2572, Mul(Mul(6, s2577), -1)))
            ; s2572
            ; s2573
            ; s2574
            ; s2575
            ; s2576
            ; s2577
            ; s2578
            ; s2579
            ; s2580
            ; s2581
            ]
          else
            [ False
            ; s2572
            ; s2573
            ; s2574
            ; s2575
            ; s2576
            ; s2577
            ; s2578
            ; s2579
            ; s2580
            ; s2581
            ]
        let [s2755; s2756; s2757; s2758; s2759; s2760; s2761; s2762; s2763] =
          if s2609
          then
            let [s2666; _; s2668; s2669; s2670; s2671; s2672; s2673; s2674;
                  s2675; s2676] =
              if Eq(Compare(s2610, Mul(5, s2615)))
              then
                [ True
                ; s2610
                ; s2611
                ; s2612
                ; s2613
                ; s2614
                ; s2615
                ; s2616
                ; s2617
                ; s2618
                ; s2619
                ]
              else
                [ Eq(Compare(s2610, Mul(2, s2615)))
                ; s2610
                ; s2611
                ; s2612
                ; s2613
                ; s2614
                ; s2615
                ; s2616
                ; s2617
                ; s2618
                ; s2619
                ]
            let [s2746; s2747; s2748; s2749; s2750; s2751; s2752; s2753;
                  s2754] =
              if s2666
              then
                [ s2668
                ; s2669
                ; s2670
                ; Cons
                    ( Pair
                        ( Add(s2675, Mul(s2668, -1))
                        , Add(s2673, Mul(s2668, 0))
                        )
                    , s2671
                    )
                ; s2672
                ; s2673
                ; s2674
                ; s2675
                ; s2676
                ]
              else
                [ s2668
                ; s2669
                ; False
                ; s2671
                ; s2672
                ; s2673
                ; s2674
                ; s2675
                ; s2676
                ]
            [ s2746; s2747; s2748; s2749; s2750; s2751; s2752; s2753; s2754 ]
          else
            [ s2611; s2612; s2613; s2614; s2615; s2616; s2617; s2618; s2619 ]
        [ s2755; s2756; s2757; s2758; s2759; s2760; s2761; s2762; s2763 ]
      else
        [ s2501; s2502; s2503; s2504; s2505; s2506; s2507; s2508; s2509 ]
    let x2776 = Add(1, s2764)
    [ Gt(Compare(s2765, x2776))
    ; x2776
    ; s2765
    ; s2766
    ; s2767
    ; s2768
    ; s2769
    ; s2770
    ; s2771
    ; s2772
    ]
let x2814 = Sub(8, r2790)
let [_; _; _; r3103; r3104; r3105; r3106; r3107; r3108] =
  loop [ Gt(Compare(x2814, 1))
       ; 1
       ; x2814
       ; True
       ; r2786
       ; r2787
       ; r2788
       ; r2789
       ; r2790
       ; r2791
       ]
  step s2818; s2819; s2820; s2821; s2822; s2823; s2824; s2825; s2826 ->
    let [s3081; s3082; s3083; s3084; s3085; s3086; s3087; s3088; s3089] =
      if s2820
      then
        let [s2860; s2861; s2862; s2863; s2864; s2865; s2866; s2867; s2868;
              s2869] =
          match Get(Add(s2825, Mul(s2818, 1)), s2824) with
          | None _ ->
              [ Empty_set<int,int>
              ; s2818
              ; s2819
              ; s2820
              ; s2821
              ; s2822
              ; s2823
              ; s2824
              ; s2825
              ; s2826
              ]
          | Some s2858 ->
              [ s2858
              ; s2818
              ; s2819
              ; s2820
              ; s2821
              ; s2822
              ; s2823
              ; s2824
              ; s2825
              ; s2826
              ]
          end
        let [s2889; s2890; s2891; s2892; s2893; s2894; s2895; s2896; s2897;
              s2898] =
          match Get(Add(s2866, Mul(s2861, 0)), s2860) with
          | None _ ->
              [ 0
              ; s2861
              ; s2862
              ; s2863
              ; s2864
              ; s2865
              ; s2866
              ; s2867
              ; s2868
              ; s2869
              ]
          | Some s2887 ->
              [ s2887
              ; s2861
              ; s2862
              ; s2863
              ; s2864
              ; s2865
              ; s2866
              ; s2867
              ; s2868
              ; s2869
              ]
          end
        let [s2926; s2927; s2928; s2929; s2930; s2931; s2932; s2933; s2934;
              s2935; s2936] =
          if Neq(Compare(0, s2889))
          then
            [ Neq(Compare(s2889, Mul(Mul(6, s2894), -1)))
            ; s2889
            ; s2890
            ; s2891
            ; s2892
            ; s2893
            ; s2894
            ; s2895
            ; s2896
            ; s2897
            ; s2898
            ]
          else
            [ False
            ; s2889
            ; s2890
            ; s2891
            ; s2892
            ; s2893
            ; s2894
            ; s2895
            ; s2896
            ; s2897
            ; s2898
            ]
        let [s3072; s3073; s3074; s3075; s3076; s3077; s3078; s3079; s3080] =
          if s2926
          then
            let [s2983; _; s2985; s2986; s2987; s2988; s2989; s2990; s2991;
                  s2992; s2993] =
              if Eq(Compare(s2927, Mul(5, s2932)))
              then
                [ True
                ; s2927
                ; s2928
                ; s2929
                ; s2930
                ; s2931
                ; s2932
                ; s2933
                ; s2934
                ; s2935
                ; s2936
                ]
              else
                [ Eq(Compare(s2927, Mul(2, s2932)))
                ; s2927
                ; s2928
                ; s2929
                ; s2930
                ; s2931
                ; s2932
                ; s2933
                ; s2934
                ; s2935
                ; s2936
                ]
            let [s3063; s3064; s3065; s3066; s3067; s3068; s3069; s3070;
                  s3071] =
              if s2983
              then
                [ s2985
                ; s2986
                ; s2987
                ; Cons
                    ( Pair
                        ( Add(s2992, Mul(s2985, 1))
                        , Add(s2990, Mul(s2985, 0))
                        )
                    , s2988
                    )
                ; s2989
                ; s2990
                ; s2991
                ; s2992
                ; s2993
                ]
              else
                [ s2985
                ; s2986
                ; False
                ; s2988
                ; s2989
                ; s2990
                ; s2991
                ; s2992
                ; s2993
                ]
            [ s3063; s3064; s3065; s3066; s3067; s3068; s3069; s3070; s3071 ]
          else
            [ s2928; s2929; s2930; s2931; s2932; s2933; s2934; s2935; s2936 ]
        [ s3072; s3073; s3074; s3075; s3076; s3077; s3078; s3079; s3080 ]
      else
        [ s2818; s2819; s2820; s2821; s2822; s2823; s2824; s2825; s2826 ]
    let x3093 = Add(1, s3081)
    [ Gt(Compare(s3082, x3093))
    ; x3093
    ; s3082
    ; s3083
    ; s3084
    ; s3085
    ; s3086
    ; s3087
    ; s3088
    ; s3089
    ]
let [s3155; s3156; s3157; s3158; s3159; s3160; s3161; s3162] =
  if Le(Compare(r3105, r3107))
  then
    [ r3105; True; r3103; r3104; r3105; r3106; r3107; r3108 ]
  else
    [ r3107; True; r3103; r3104; r3105; r3106; r3107; r3108 ]
let [_; _; _; r3514; r3515; r3516; r3517; r3518; r3519] =
  loop [ Gt(Compare(s3155, 1))
       ; 1
       ; s3155
       ; s3156
       ; s3157
       ; s3158
       ; s3159
       ; s3160
       ; s3161
       ; s3162
       ]
  step s3173; s3174; s3175; s3176; s3177; s3178; s3179; s3180; s3181 ->
    if Lt(Compare(Add(s3178, Mul(s3173, -1)), 9))
    then
      let [s3492; s3493; s3494; s3495; s3496; s3497; s3498; s3499; s3500] =
        if s3175
        then
          let [s3271; s3272; s3273; s3274; s3275; s3276; s3277; s3278; s3279;
                s3280] =
            match Get(Add(s3180, Mul(s3173, -1)), s3179) with
            | None _ ->
                [ Empty_set<int,int>
                ; s3173
                ; s3174
                ; s3175
                ; s3176
                ; s3177
                ; s3178
                ; s3179
                ; s3180
                ; s3181
                ]
            | Some s3269 ->
                [ s3269
                ; s3173
                ; s3174
                ; s3175
                ; s3176
                ; s3177
                ; s3178
                ; s3179
                ; s3180
                ; s3181
                ]
            end
          let [s3300; s3301; s3302; s3303; s3304; s3305; s3306; s3307; s3308;
                s3309] =
            match Get(Add(s3277, Mul(s3272, -1)), s3271) with
            | None _ ->
                [ 0
                ; s3272
                ; s3273
                ; s3274
                ; s3275
                ; s3276
                ; s3277
                ; s3278
                ; s3279
                ; s3280
                ]
            | Some s3298 ->
                [ s3298
                ; s3272
                ; s3273
                ; s3274
                ; s3275
                ; s3276
                ; s3277
                ; s3278
                ; s3279
                ; s3280
                ]
            end
          let [s3337; s3338; s3339; s3340; s3341; s3342; s3343; s3344; s3345;
                s3346; s3347] =
            if Neq(Compare(0, s3300))
            then
              [ Neq(Compare(s3300, Mul(Mul(6, s3305), -1)))
              ; s3300
              ; s3301
              ; s3302
              ; s3303
              ; s3304
              ; s3305
              ; s3306
              ; s3307
              ; s3308
              ; s3309
              ]
            else
              [ False
              ; s3300
              ; s3301
              ; s3302
              ; s3303
              ; s3304
              ; s3305
              ; s3306
              ; s3307
              ; s3308
              ; s3309
              ]
          let [s3483; s3484; s3485; s3486; s3487; s3488; s3489; s3490; s3491] =
            if s3337
            then
              let [s3394; _; s3396; s3397; s3398; s3399; s3400; s3401; s3402;
                    s3403; s3404] =
                if Eq(Compare(s3338, Mul(5, s3343)))
                then
                  [ True
                  ; s3338
                  ; s3339
                  ; s3340
                  ; s3341
                  ; s3342
                  ; s3343
                  ; s3344
                  ; s3345
                  ; s3346
                  ; s3347
                  ]
                else
                  [ Eq(Compare(s3338, Mul(4, s3343)))
                  ; s3338
                  ; s3339
                  ; s3340
                  ; s3341
                  ; s3342
                  ; s3343
                  ; s3344
                  ; s3345
                  ; s3346
                  ; s3347
                  ]
              let [s3474; s3475; s3476; s3477; s3478; s3479; s3480; s3481;
                    s3482] =
                if s3394
                then
                  [ s3396
                  ; s3397
                  ; s3398
                  ; Cons
                      ( Pair
                          ( Add(s3403, Mul(s3396, -1))
                          , Add(s3401, Mul(s3396, -1))
                          )
                      , s3399
                      )
                  ; s3400
                  ; s3401
                  ; s3402
                  ; s3403
                  ; s3404
                  ]
                else
                  [ s3396
                  ; s3397
                  ; False
                  ; s3399
                  ; s3400
                  ; s3401
                  ; s3402
                  ; s3403
                  ; s3404
                  ]
              [ s3474
              ; s3475
              ; s3476
              ; s3477
              ; s3478
              ; s3479
              ; s3480
              ; s3481
              ; s3482
              ]
            else
              [ s3339
              ; s3340
              ; s3341
              ; s3342
              ; s3343
              ; s3344
              ; s3345
              ; s3346
              ; s3347
              ]
          [ s3483; s3484; s3485; s3486; s3487; s3488; s3489; s3490; s3491 ]
        else
          [ s3173; s3174; s3175; s3176; s3177; s3178; s3179; s3180; s3181 ]
      let x3504 = Add(1, s3492)
      [ Gt(Compare(s3493, x3504))
      ; x3504
      ; s3493
      ; s3494
      ; s3495
      ; s3496
      ; s3497
      ; s3498
      ; s3499
      ; s3500
      ]
    else
      Failwith(Pair(Pair(-1, s3180), Pair(s3178, Pair(s3173, 0))))
let x3545 = Sub(8, r3518)
let x3549 = Sub(8, r3516)
let [s3570; s3571; s3572; s3573; s3574; s3575; s3576; s3577] =
  if Le(Compare(x3549, x3545))
  then
    [ x3549; True; r3514; r3515; r3516; r3517; r3518; r3519 ]
  else
    [ x3545; True; r3514; r3515; r3516; r3517; r3518; r3519 ]
let [_; _; _; r3929; r3930; r3931; r3932; r3933; r3934] =
  loop [ Gt(Compare(s3570, 1))
       ; 1
       ; s3570
       ; s3571
       ; s3572
       ; s3573
       ; s3574
       ; s3575
       ; s3576
       ; s3577
       ]
  step s3588; s3589; s3590; s3591; s3592; s3593; s3594; s3595; s3596 ->
    if Lt(Compare(Add(s3593, Mul(s3588, 1)), 9))
    then
      let [s3907; s3908; s3909; s3910; s3911; s3912; s3913; s3914; s3915] =
        if s3590
        then
          let [s3686; s3687; s3688; s3689; s3690; s3691; s3692; s3693; s3694;
                s3695] =
            match Get(Add(s3595, Mul(s3588, 1)), s3594) with
            | None _ ->
                [ Empty_set<int,int>
                ; s3588
                ; s3589
                ; s3590
                ; s3591
                ; s3592
                ; s3593
                ; s3594
                ; s3595
                ; s3596
                ]
            | Some s3684 ->
                [ s3684
                ; s3588
                ; s3589
                ; s3590
                ; s3591
                ; s3592
                ; s3593
                ; s3594
                ; s3595
                ; s3596
                ]
            end
          let [s3715; s3716; s3717; s3718; s3719; s3720; s3721; s3722; s3723;
                s3724] =
            match Get(Add(s3692, Mul(s3687, 1)), s3686) with
            | None _ ->
                [ 0
                ; s3687
                ; s3688
                ; s3689
                ; s3690
                ; s3691
                ; s3692
                ; s3693
                ; s3694
                ; s3695
                ]
            | Some s3713 ->
                [ s3713
                ; s3687
                ; s3688
                ; s3689
                ; s3690
                ; s3691
                ; s3692
                ; s3693
                ; s3694
                ; s3695
                ]
            end
          let [s3752; s3753; s3754; s3755; s3756; s3757; s3758; s3759; s3760;
                s3761; s3762] =
            if Neq(Compare(0, s3715))
            then
              [ Neq(Compare(s3715, Mul(Mul(6, s3720), -1)))
              ; s3715
              ; s3716
              ; s3717
              ; s3718
              ; s3719
              ; s3720
              ; s3721
              ; s3722
              ; s3723
              ; s3724
              ]
            else
              [ False
              ; s3715
              ; s3716
              ; s3717
              ; s3718
              ; s3719
              ; s3720
              ; s3721
              ; s3722
              ; s3723
              ; s3724
              ]
          let [s3898; s3899; s3900; s3901; s3902; s3903; s3904; s3905; s3906] =
            if s3752
            then
              let [s3809; _; s3811; s3812; s3813; s3814; s3815; s3816; s3817;
                    s3818; s3819] =
                if Eq(Compare(s3753, Mul(5, s3758)))
                then
                  [ True
                  ; s3753
                  ; s3754
                  ; s3755
                  ; s3756
                  ; s3757
                  ; s3758
                  ; s3759
                  ; s3760
                  ; s3761
                  ; s3762
                  ]
                else
                  [ Eq(Compare(s3753, Mul(4, s3758)))
                  ; s3753
                  ; s3754
                  ; s3755
                  ; s3756
                  ; s3757
                  ; s3758
                  ; s3759
                  ; s3760
                  ; s3761
                  ; s3762
                  ]
              let [s3889; s3890; s3891; s3892; s3893; s3894; s3895; s3896;
                    s3897] =
                if s3809
                then
                  [ s3811
                  ; s3812
                  ; s3813
                  ; Cons
                      ( Pair
                          ( Add(s3818, Mul(s3811, 1))
                          , Add(s3816, Mul(s3811, 1))
                          )
                      , s3814
                      )
                  ; s3815
                  ; s3816
                  ; s3817
                  ; s3818
                  ; s3819
                  ]
                else
                  [ s3811
                  ; s3812
                  ; False
                  ; s3814
                  ; s3815
                  ; s3816
                  ; s3817
                  ; s3818
                  ; s3819
                  ]
              [ s3889
              ; s3890
              ; s3891
              ; s3892
              ; s3893
              ; s3894
              ; s3895
              ; s3896
              ; s3897
              ]
            else
              [ s3754
              ; s3755
              ; s3756
              ; s3757
              ; s3758
              ; s3759
              ; s3760
              ; s3761
              ; s3762
              ]
          [ s3898; s3899; s3900; s3901; s3902; s3903; s3904; s3905; s3906 ]
        else
          [ s3588; s3589; s3590; s3591; s3592; s3593; s3594; s3595; s3596 ]
      let x3919 = Add(1, s3907)
      [ Gt(Compare(s3908, x3919))
      ; x3919
      ; s3908
      ; s3909
      ; s3910
      ; s3911
      ; s3912
      ; s3913
      ; s3914
      ; s3915
      ]
    else
      Failwith(Pair(Pair(1, s3595), Pair(s3593, Pair(s3588, 1))))
let x3962 = Sub(8, r3931)
let [s3983; s3984; s3985; s3986; s3987; s3988; s3989; s3990] =
  if Le(Compare(x3962, r3933))
  then
    [ x3962; True; r3929; r3930; r3931; r3932; r3933; r3934 ]
  else
    [ r3933; True; r3929; r3930; r3931; r3932; r3933; r3934 ]
let [_; _; _; r4342; r4343; r4344; r4345; r4346; r4347] =
  loop [ Gt(Compare(s3983, 1))
       ; 1
       ; s3983
       ; s3984
       ; s3985
       ; s3986
       ; s3987
       ; s3988
       ; s3989
       ; s3990
       ]
  step s4001; s4002; s4003; s4004; s4005; s4006; s4007; s4008; s4009 ->
    if Lt(Compare(Add(s4006, Mul(s4001, 1)), 9))
    then
      let [s4320; s4321; s4322; s4323; s4324; s4325; s4326; s4327; s4328] =
        if s4003
        then
          let [s4099; s4100; s4101; s4102; s4103; s4104; s4105; s4106; s4107;
                s4108] =
            match Get(Add(s4008, Mul(s4001, -1)), s4007) with
            | None _ ->
                [ Empty_set<int,int>
                ; s4001
                ; s4002
                ; s4003
                ; s4004
                ; s4005
                ; s4006
                ; s4007
                ; s4008
                ; s4009
                ]
            | Some s4097 ->
                [ s4097
                ; s4001
                ; s4002
                ; s4003
                ; s4004
                ; s4005
                ; s4006
                ; s4007
                ; s4008
                ; s4009
                ]
            end
          let [s4128; s4129; s4130; s4131; s4132; s4133; s4134; s4135; s4136;
                s4137] =
            match Get(Add(s4105, Mul(s4100, 1)), s4099) with
            | None _ ->
                [ 0
                ; s4100
                ; s4101
                ; s4102
                ; s4103
                ; s4104
                ; s4105
                ; s4106
                ; s4107
                ; s4108
                ]
            | Some s4126 ->
                [ s4126
                ; s4100
                ; s4101
                ; s4102
                ; s4103
                ; s4104
                ; s4105
                ; s4106
                ; s4107
                ; s4108
                ]
            end
          let [s4165; s4166; s4167; s4168; s4169; s4170; s4171; s4172; s4173;
                s4174; s4175] =
            if Neq(Compare(0, s4128))
            then
              [ Neq(Compare(s4128, Mul(Mul(6, s4133), -1)))
              ; s4128
              ; s4129
              ; s4130
              ; s4131
              ; s4132
              ; s4133
              ; s4134
              ; s4135
              ; s4136
              ; s4137
              ]
            else
              [ False
              ; s4128
              ; s4129
              ; s4130
              ; s4131
              ; s4132
              ; s4133
              ; s4134
              ; s4135
              ; s4136
              ; s4137
              ]
          let [s4311; s4312; s4313; s4314; s4315; s4316; s4317; s4318; s4319] =
            if s4165
            then
              let [s4222; _; s4224; s4225; s4226; s4227; s4228; s4229; s4230;
                    s4231; s4232] =
                if Eq(Compare(s4166, Mul(5, s4171)))
                then
                  [ True
                  ; s4166
                  ; s4167
                  ; s4168
                  ; s4169
                  ; s4170
                  ; s4171
                  ; s4172
                  ; s4173
                  ; s4174
                  ; s4175
                  ]
                else
                  [ Eq(Compare(s4166, Mul(4, s4171)))
                  ; s4166
                  ; s4167
                  ; s4168
                  ; s4169
                  ; s4170
                  ; s4171
                  ; s4172
                  ; s4173
                  ; s4174
                  ; s4175
                  ]
              let [s4302; s4303; s4304; s4305; s4306; s4307; s4308; s4309;
                    s4310] =
                if s4222
                then
                  [ s4224
                  ; s4225
                  ; s4226
                  ; Cons
                      ( Pair
                          ( Add(s4231, Mul(s4224, -1))
                          , Add(s4229, Mul(s4224, 1))
                          )
                      , s4227
                      )
                  ; s4228
                  ; s4229
                  ; s4230
                  ; s4231
                  ; s4232
                  ]
                else
                  [ s4224
                  ; s4225
                  ; False
                  ; s4227
                  ; s4228
                  ; s4229
                  ; s4230
                  ; s4231
                  ; s4232
                  ]
              [ s4302
              ; s4303
              ; s4304
              ; s4305
              ; s4306
              ; s4307
              ; s4308
              ; s4309
              ; s4310
              ]
            else
              [ s4167
              ; s4168
              ; s4169
              ; s4170
              ; s4171
              ; s4172
              ; s4173
              ; s4174
              ; s4175
              ]
          [ s4311; s4312; s4313; s4314; s4315; s4316; s4317; s4318; s4319 ]
        else
          [ s4001; s4002; s4003; s4004; s4005; s4006; s4007; s4008; s4009 ]
      let x4332 = Add(1, s4320)
      [ Gt(Compare(s4321, x4332))
      ; x4332
      ; s4321
      ; s4322
      ; s4323
      ; s4324
      ; s4325
      ; s4326
      ; s4327
      ; s4328
      ]
    else
      Failwith(Pair(Pair(1, s4008), Pair(s4006, Pair(s4001, 2))))
let x4371 = Sub(8, r4346)
let [s4396; s4397; s4398; s4399; s4400; s4401; s4402; s4403] =
  if Le(Compare(r4344, x4371))
  then
    [ r4344; True; r4342; r4343; r4344; r4345; r4346; r4347 ]
  else
    [ x4371; True; r4342; r4343; r4344; r4345; r4346; r4347 ]
let [_; _; _; r4755; _; _; _; _; _] =
  loop [ Gt(Compare(s4396, 1))
       ; 1
       ; s4396
       ; s4397
       ; s4398
       ; s4399
       ; s4400
       ; s4401
       ; s4402
       ; s4403
       ]
  step s4414; s4415; s4416; s4417; s4418; s4419; s4420; s4421; s4422 ->
    if Lt(Compare(Add(s4419, Mul(s4414, -1)), 9))
    then
      let [s4733; s4734; s4735; s4736; s4737; s4738; s4739; s4740; s4741] =
        if s4416
        then
          let [s4512; s4513; s4514; s4515; s4516; s4517; s4518; s4519; s4520;
                s4521] =
            match Get(Add(s4421, Mul(s4414, 1)), s4420) with
            | None _ ->
                [ Empty_set<int,int>
                ; s4414
                ; s4415
                ; s4416
                ; s4417
                ; s4418
                ; s4419
                ; s4420
                ; s4421
                ; s4422
                ]
            | Some s4510 ->
                [ s4510
                ; s4414
                ; s4415
                ; s4416
                ; s4417
                ; s4418
                ; s4419
                ; s4420
                ; s4421
                ; s4422
                ]
            end
          let [s4541; s4542; s4543; s4544; s4545; s4546; s4547; s4548; s4549;
                s4550] =
            match Get(Add(s4518, Mul(s4513, -1)), s4512) with
            | None _ ->
                [ 0
                ; s4513
                ; s4514
                ; s4515
                ; s4516
                ; s4517
                ; s4518
                ; s4519
                ; s4520
                ; s4521
                ]
            | Some s4539 ->
                [ s4539
                ; s4513
                ; s4514
                ; s4515
                ; s4516
                ; s4517
                ; s4518
                ; s4519
                ; s4520
                ; s4521
                ]
            end
          let [s4578; s4579; s4580; s4581; s4582; s4583; s4584; s4585; s4586;
                s4587; s4588] =
            if Neq(Compare(0, s4541))
            then
              [ Neq(Compare(s4541, Mul(Mul(6, s4546), -1)))
              ; s4541
              ; s4542
              ; s4543
              ; s4544
              ; s4545
              ; s4546
              ; s4547
              ; s4548
              ; s4549
              ; s4550
              ]
            else
              [ False
              ; s4541
              ; s4542
              ; s4543
              ; s4544
              ; s4545
              ; s4546
              ; s4547
              ; s4548
              ; s4549
              ; s4550
              ]
          let [s4724; s4725; s4726; s4727; s4728; s4729; s4730; s4731; s4732] =
            if s4578
            then
              let [s4635; _; s4637; s4638; s4639; s4640; s4641; s4642; s4643;
                    s4644; s4645] =
                if Eq(Compare(s4579, Mul(5, s4584)))
                then
                  [ True
                  ; s4579
                  ; s4580
                  ; s4581
                  ; s4582
                  ; s4583
                  ; s4584
                  ; s4585
                  ; s4586
                  ; s4587
                  ; s4588
                  ]
                else
                  [ Eq(Compare(s4579, Mul(4, s4584)))
                  ; s4579
                  ; s4580
                  ; s4581
                  ; s4582
                  ; s4583
                  ; s4584
                  ; s4585
                  ; s4586
                  ; s4587
                  ; s4588
                  ]
              let [s4715; s4716; s4717; s4718; s4719; s4720; s4721; s4722;
                    s4723] =
                if s4635
                then
                  [ s4637
                  ; s4638
                  ; s4639
                  ; Cons
                      ( Pair
                          ( Add(s4644, Mul(s4637, 1))
                          , Add(s4642, Mul(s4637, -1))
                          )
                      , s4640
                      )
                  ; s4641
                  ; s4642
                  ; s4643
                  ; s4644
                  ; s4645
                  ]
                else
                  [ s4637
                  ; s4638
                  ; False
                  ; s4640
                  ; s4641
                  ; s4642
                  ; s4643
                  ; s4644
                  ; s4645
                  ]
              [ s4715
              ; s4716
              ; s4717
              ; s4718
              ; s4719
              ; s4720
              ; s4721
              ; s4722
              ; s4723
              ]
            else
              [ s4580
              ; s4581
              ; s4582
              ; s4583
              ; s4584
              ; s4585
              ; s4586
              ; s4587
              ; s4588
              ]
          [ s4724; s4725; s4726; s4727; s4728; s4729; s4730; s4731; s4732 ]
        else
          [ s4414; s4415; s4416; s4417; s4418; s4419; s4420; s4421; s4422 ]
      let x4745 = Add(1, s4733)
      [ Gt(Compare(s4734, x4745))
      ; x4745
      ; s4734
      ; s4735
      ; s4736
      ; s4737
      ; s4738
      ; s4739
      ; s4740
      ; s4741
      ]
    else
      Failwith(Pair(Pair(-1, s4421), Pair(s4419, Pair(s4414, 3))))
r4755)]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
