import smartpy as sp

tstorage = sp.TRecord(b2b = sp.TBytes, keccak = sp.TBytes, s256 = sp.TBytes, s512 = sp.TBytes, sha3 = sp.TBytes, tz1 = sp.TKeyHash, v = sp.TBytes).layout((("b2b", ("keccak", "s256")), (("s512", "sha3"), ("tz1", "v"))))
tparameter = sp.TVariant(new_key = sp.TKey, new_value = sp.TBytes).layout(("new_key", "new_value"))
tprivates = { }
tviews = { }
