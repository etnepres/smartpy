import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(a = sp.TInt, b = sp.TBool).layout(("a", "b")))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TInt)
    self.data.a = params + self.data.a

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
