open SmartML

module Contract = struct
  let%entry_point test_and params =
    data.a <- data.a && params

  let%entry_point test_exclusive_or params =
    data.a <- data.a ^ params

  let%entry_point test_or params =
    data.a <- data.a || params

  let%entry_point test_shift_left params =
    data.a <- data.a << params

  let%entry_point test_shift_right params =
    data.a <- data.a >> params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {a = nat}]
      ~storage:[%expr
                 {a = 1234567}]
      [test_and; test_exclusive_or; test_or; test_shift_left; test_shift_right]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())