import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TNat)

  @sp.entry_point
  def test_and(self, params):
    sp.set_type(params, sp.TNat)
    self.data = params & self.data

  @sp.entry_point
  def test_exclusive_or(self, params):
    sp.set_type(params, sp.TNat)
    self.data = params ^ self.data

  @sp.entry_point
  def test_or(self, params):
    sp.set_type(params, sp.TNat)
    self.data = params | self.data

  @sp.entry_point
  def test_shift_left(self, params):
    sp.set_type(params, sp.TNat)
    self.data = self.data << params

  @sp.entry_point
  def test_shift_right(self, params):
    sp.set_type(params, sp.TNat)
    self.data = self.data >> params

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
