import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TIntOrNat, y = sp.TIntOrNat, z = sp.TString).layout(("x", ("y", "z"))))
    self.init(x = 2,
              y = 1,
              z = 'ABC')

  @sp.entry_point
  def ep2(self):
    self.data.y *= 2

sp.add_compilation_target("test", Contract())