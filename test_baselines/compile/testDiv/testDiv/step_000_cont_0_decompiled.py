import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(a = sp.TOption(sp.TPair(sp.TNat, sp.TNat)), b = sp.TOption(sp.TPair(sp.TInt, sp.TNat)), c = sp.TOption(sp.TPair(sp.TNat, sp.TNat)), d = sp.TOption(sp.TPair(sp.TInt, sp.TNat)), e = sp.TOption(sp.TPair(sp.TInt, sp.TNat)), f = sp.TOption(sp.TPair(sp.TNat, sp.TNat)), g = sp.TOption(sp.TPair(sp.TInt, sp.TNat)), h = sp.TOption(sp.TPair(sp.TInt, sp.TNat)), i = sp.TOption(sp.TPair(sp.TNat, sp.TMutez)), j = sp.TOption(sp.TPair(sp.TNat, sp.TMutez)), k = sp.TOption(sp.TPair(sp.TNat, sp.TMutez)), l = sp.TOption(sp.TPair(sp.TMutez, sp.TMutez)), m = sp.TOption(sp.TPair(sp.TMutez, sp.TMutez))).layout(((("a", ("b", "c")), ("d", ("e", "f"))), (("g", ("h", "i")), (("j", "k"), ("l", "m"))))))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TNat)
    self.data = sp.record(a = sp.ediv(1, 0), b = sp.ediv(-1, 0), c = sp.ediv(1, 12), d = sp.ediv(-1, 12), e = sp.ediv(-1, -12), f = sp.ediv(15, 12), g = sp.ediv(-15, 12), h = sp.ediv(-15, -12), i = sp.ediv(sp.tez(2), sp.mutez(100)), j = sp.ediv(sp.tez(2), sp.mutez(101)), k = sp.ediv(sp.tez(2), sp.tez(100)), l = sp.ediv(sp.tez(2), 15), m = sp.ediv(sp.amount, params))

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
