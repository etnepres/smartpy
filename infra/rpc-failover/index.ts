
import * as fs from 'fs';
import { prepare, runTask } from './service';

const {
    TASK_DELAY,
    FAILOVER_ON,            // Disable node after <FAILOVER_ON> failed requests,
    ALLOWED_BLOCK_AGE,
    CONFIG_LOCATION,
    RELOAD_NGINX_CMD,
    NODES                   // List of nodes being monitored
} = JSON.parse(fs.readFileSync(process.env.FAILOVER_CONFIG_FILE || "config.json", { encoding: "utf-8" }));

prepare({
    FAILOVER_ON,
    ALLOWED_BLOCK_AGE,
    CONFIG_LOCATION,
    RELOAD_NGINX_CMD,
    NODES
});

setInterval(runTask, TASK_DELAY);
