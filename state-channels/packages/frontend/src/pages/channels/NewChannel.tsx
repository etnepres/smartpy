import React from 'react';

import { createStyles, makeStyles } from '@mui/styles';
import { Theme } from '@mui/material/styles';
import { Typography, Alert, Grid, Box, Container } from '@mui/material';
import { Comparison, Constants, EntryPoints, Hashers } from 'state-channels-common';

import Button from '../../components/base/Button';
import http from '../../services/http';
import wallet from '../../services/wallet';
import TextField from '../../components/base/TextField';
import Dialog from '../../components/base/Dialog';
import { copyToClipboard } from '../../utils/clipboard';
import CopyButton from '../../components/base/CopyButton';
import Logger from '../../services/logger';
import { randomNonce } from '../../utils/random';
import NonceTextField from '../../components/base/NonceTextField';
import useWalletContext from 'src/hooks/useWalletContext';
import useAppContext from 'src/hooks/useAppContext';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            padding: 20,
            borderRadius: 10,
            boxShadow: '0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22)',
            display: 'flex',
            flexDirection: 'column',
            border: '1px solid #FFF',
        },
        button: {
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
            marginTop: theme.spacing(2),
        },
        wrap: {
            overflowWrap: 'anywhere',
        },
        divider: {
            margin: theme.spacing(1),
        },
        alertMessage: {
            textAlign: 'center',
            flex: 1,
        },
    }),
);

const CreateChannel: React.FC = () => {
    const classes = useStyles();
    const { pkh } = useWalletContext();
    const [error, setError] = React.useState<string>();
    const [success, setSuccess] = React.useState<string>();
    const [nonce, setNonce] = React.useState<string>(randomNonce());
    const [withdrawDelay, setWithdrawDelay] = React.useState<string>();
    const [players, setPlayers] = React.useState<Record<string, { address: string; publicKey: string }>>();
    const { platform } = useAppContext();

    const channelID = React.useMemo(() => {
        if (nonce && players && Object.keys(players).length === 2) {
            try {
                return Hashers.Channel.hashID({
                    nonce,
                    players: Object.values(players).sort(({ address: a1 }, { address: a2 }) =>
                        Comparison.compare(a1, a2),
                    ),
                    platformAddress: platform.id,
                });
            } catch (e) {
                Logger.debug(e);
            }
        }
    }, [nonce, players]);

    const handleWithdrawDelayChange = React.useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
        setWithdrawDelay(e.target.value);
    }, []);

    const handlePlayerChange = React.useCallback(async (e: React.ChangeEvent<HTMLInputElement>) => {
        const address = e.target.value;
        const id = e.target.id;
        try {
            const publicKey = (
                await http.get(`${Constants.rpc}/chains/main/blocks/head/context/contracts/${address}/manager_key`)
            ).data;

            setPlayers((players) => ({
                ...players,
                [id]: {
                    address,
                    publicKey,
                },
            }));
        } catch {
            setPlayers((players) => {
                players?.[id] && delete players[id];
                return { ...players };
            });
        }
    }, []);

    const injectOperation = React.useCallback(async () => {
        try {
            if (!nonce) {
                return setError('You must provide a nonce.');
            }
            if (!withdrawDelay) {
                return setError('You must provide a withdraw delay.');
            }
            if (!players || Object.keys(players).length !== 2) {
                return setError('You must provide 2 participants.');
            }

            const withdrawDelayInSeconds = Number(withdrawDelay) * 60;
            const parameters = EntryPoints.Channel.newChannel(nonce, withdrawDelayInSeconds, Object.values(players));
            await wallet.Beacon.transfer(platform.id, parameters);

            setSuccess(channelID);
        } catch (e) {
            setError(e.message);
        }
    }, [players, nonce, withdrawDelay, channelID]);

    const onNonceChange: React.ChangeEventHandler<HTMLInputElement> = (e) => {
        setNonce(e.target.value);
    };

    const clipboard = React.useCallback(() => {
        success && copyToClipboard(success);
    }, [success]);

    return (
        <>
            <Container maxWidth="sm">
                <Box
                    component="form"
                    className={classes.container}
                    sx={{ border: '1px solid', borderColor: 'primary.main' }}
                    autoComplete="off"
                    onSubmit={injectOperation}
                >
                    <Typography variant="h4" textAlign="center">
                        New Channel
                    </Typography>
                    <div className={classes.divider} />
                    {error && (
                        <Alert variant="outlined" severity="error" onClose={() => setError('')}>
                            <Typography className={classes.wrap}>{error}</Typography>
                        </Alert>
                    )}
                    <div className={classes.divider} />
                    <NonceTextField
                        id="nonce"
                        value={nonce}
                        label="Nonce"
                        variant="outlined"
                        margin="normal"
                        onChange={onNonceChange}
                        onClick={() => setNonce(randomNonce())}
                    />
                    <TextField
                        id="withdraw_delay"
                        required
                        type="number"
                        inputProps={{
                            min: 0,
                        }}
                        label="Withdraw delay (in minutes)"
                        value={withdrawDelay}
                        onChange={handleWithdrawDelayChange}
                        margin="normal"
                        variant="outlined"
                    />
                    <Typography variant="overline" textAlign="center">
                        Participants
                    </Typography>
                    <TextField
                        id="player1"
                        inputProps={{
                            list: 'known_addresses',
                        }}
                        required
                        color={players?.['player1']?.publicKey ? 'success' : 'error'}
                        focused={!!players?.['player1']?.address}
                        label="Address (Player 1)"
                        onChange={handlePlayerChange}
                        margin="normal"
                        variant="outlined"
                    />
                    <TextField
                        id="player2"
                        inputProps={{
                            list: 'known_addresses',
                        }}
                        required
                        color={players?.['player2']?.publicKey ? 'success' : 'error'}
                        focused={!!players?.['player2']?.address}
                        label="Address (Player 2)"
                        onChange={handlePlayerChange}
                        variant="outlined"
                    />
                    <datalist id="known_addresses">
                        <option value={pkh} />
                    </datalist>
                    <div className={classes.divider} />
                    <Button className={classes.button} onClick={injectOperation}>
                        Inject Operation
                    </Button>
                </Box>
            </Container>
            <Dialog open={!!success} onClose={() => setSuccess('')}>
                <Grid container direction="column" justifyContent="center">
                    <Alert
                        variant="outlined"
                        severity="success"
                        icon={false}
                        classes={{ message: classes.alertMessage }}
                    >
                        Channel was successfully created!
                    </Alert>
                    <div className={classes.divider} />
                    <CopyButton
                        sx={{ width: '100%' }}
                        color="success"
                        label={`Channel ID: ${success}`}
                        onClick={clipboard}
                    />
                </Grid>
            </Dialog>
        </>
    );
};

export default CreateChannel;
