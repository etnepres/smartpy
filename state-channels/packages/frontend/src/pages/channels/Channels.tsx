import React from 'react';
import { Container, Grid } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';

import Table from './components/Table';
import RouterFab from '../../components/base/RouterFab';

const Channels: React.FC = () => {
    return (
        <Container maxWidth="md" sx={{ margin: 5 }}>
            <Grid container justifyContent="center" alignItems="center" direction="column" spacing={5}>
                <Grid item>
                    <RouterFab variant="extended" to="/new-channel" color="primary">
                        <AddIcon sx={{ mr: 1 }} />
                        New channel
                    </RouterFab>
                </Grid>
                <Grid item>
                    <Table />
                </Grid>
            </Grid>
        </Container>
    );
};

export default Channels;
