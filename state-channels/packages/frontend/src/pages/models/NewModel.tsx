import React from 'react';

import { createStyles, makeStyles } from '@mui/styles';
import { Theme } from '@mui/material/styles';
import { Typography, Alert, Grid, Box, Container } from '@mui/material';
import { EntryPoints, Hashers } from 'state-channels-common';

import Button from '../../components/base/Button';
import wallet from '../../services/wallet';
import TextField from '../../components/base/TextField';
import Dialog from '../../components/base/Dialog';
import { copyToClipboard } from '../../utils/clipboard';
import CopyButton from '../../components/base/CopyButton';
import Logger from '../../services/logger';
import useAppContext from 'src/hooks/useAppContext';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            padding: 20,
            borderRadius: 10,
            boxShadow: '0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22)',
            display: 'flex',
            flexDirection: 'column',
            border: '1px solid #FFF',
        },
        button: {
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
            marginTop: theme.spacing(2),
        },
        wrap: {
            overflowWrap: 'anywhere',
        },
        divider: {
            margin: theme.spacing(1),
        },
        alertMessage: {
            textAlign: 'center',
            flex: 1,
        },
    }),
);

const CreateModel: React.FC = () => {
    const classes = useStyles();
    const { platform } = useAppContext();
    const [error, setError] = React.useState<string>();
    const [success, setSuccess] = React.useState<string>();
    const [modelBytes, setModelBytes] = React.useState<string>();
    const [metadata, setMetadata] = React.useState<Record<string, string>>({});

    const modelID = React.useMemo(() => {
        if (modelBytes) {
            try {
                return Hashers.Model.hashID(modelBytes);
            } catch (e) {
                Logger.debug(e);
            }
        }
    }, [modelBytes]);

    const handleChange: React.ChangeEventHandler<HTMLInputElement | HTMLTextAreaElement> = React.useCallback((e) => {
        switch (e.target.id) {
            case 'name':
            case 'init_type':
                return setMetadata((metadata) => ({
                    ...metadata,
                    [e.target.id]: e.target.value?.trim() || '',
                }));
            case 'model_bytes':
                return setModelBytes(e.target.value?.trim() || '');
        }
    }, []);

    const injectOperation = React.useCallback(async () => {
        try {
            if (!modelBytes) {
                return setError('You must provide the model bytes.');
            }
            if (!metadata.name) {
                return setError('You must provide a name.');
            }
            if (!metadata.init_type) {
                return setError('You must provide the init type.');
            }
            const parameters = EntryPoints.Model.newModel({ metadata, modelBytes });
            await wallet.Beacon.transfer(platform.id, parameters);

            setSuccess(modelID);
        } catch (e) {
            Logger.debug(e);
            setError(e.message);
        }
    }, [modelBytes, modelID, metadata]);

    const clipboard = React.useCallback(() => {
        success && copyToClipboard(success);
    }, [success]);

    return (
        <>
            <Container maxWidth="sm" sx={{ margin: 5 }}>
                <Box
                    component="form"
                    className={classes.container}
                    sx={{ border: '1px solid', borderColor: 'primary.main' }}
                    autoComplete="off"
                    onSubmit={injectOperation}
                >
                    <Typography variant="h4" textAlign="center">
                        New Model
                    </Typography>
                    <div className={classes.divider} />
                    {error && (
                        <Alert variant="outlined" severity="error" onClose={() => setError('')}>
                            <Typography className={classes.wrap}>{error}</Typography>
                        </Alert>
                    )}
                    <div className={classes.divider} />
                    <TextField
                        id="name"
                        required
                        label="Name (Hex)"
                        value={metadata['name']}
                        onChange={handleChange}
                        variant="outlined"
                    />
                    <div className={classes.divider} />
                    <TextField
                        id="init_type"
                        required
                        label="Init Type (Hex)"
                        value={metadata['init_type']}
                        onChange={handleChange}
                        margin="normal"
                        variant="outlined"
                    />
                    <div className={classes.divider} />
                    <TextField
                        id="model_bytes"
                        required
                        label="Packed Model"
                        value={modelBytes}
                        onChange={handleChange}
                        variant="outlined"
                        multiline
                        rows={10}
                    />
                    <div className={classes.divider} />
                    <Button className={classes.button} onClick={injectOperation}>
                        Inject Operation
                    </Button>
                </Box>
            </Container>
            <Dialog open={!!success} onClose={() => setSuccess('')}>
                <Grid container direction="column" justifyContent="center">
                    <Alert
                        variant="outlined"
                        severity="success"
                        icon={false}
                        classes={{ message: classes.alertMessage }}
                    >
                        Model was successfully created!
                    </Alert>
                    <div className={classes.divider} />
                    <CopyButton
                        sx={{ width: '100%' }}
                        color="success"
                        label={`Model ID: ${success}`}
                        onClick={clipboard}
                    />
                </Grid>
            </Dialog>
        </>
    );
};

export default CreateModel;
