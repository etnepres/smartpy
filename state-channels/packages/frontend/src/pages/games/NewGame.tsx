import React from 'react';
import { useParams } from 'react-router-dom';
import { useQuery } from '@apollo/client';
import { SigningType } from '@airgap/beacon-sdk';

import { createStyles, makeStyles } from '@mui/styles';
import { Theme } from '@mui/material/styles';
import {
    Typography,
    Alert,
    Grid,
    Box,
    Container,
    MenuItem,
    Select,
    SelectChangeEvent,
    FormControl,
    InputLabel,
    Chip,
    Divider,
} from '@mui/material';

import {
    SC_Channel,
    SC_Settlement,
    GameConstants,
    Packers,
    Api,
    Hashers,
    SC_SettlementKind,
} from 'state-channels-common';

import Button from 'src/components/base/Button';
import TextField from 'src/components/base/TextField';
import Dialog from 'src/components/base/Dialog';
import { Query } from 'src/services/graphql';
import useWalletContext from 'src/hooks/useWalletContext';
import { randomNonce } from 'src/utils/random';
import NonceTextField from 'src/components/base/NonceTextField';
import AddSettlements from './components/AddSettlements';
import Wallet from 'src/services/wallet';
import Settlements from './components/Settlements';
import RouterFab from 'src/components/base/RouterFab';
import useModels from 'src/hooks/useModels';
import { Encoders } from 'state-channels-common';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            padding: 20,
            borderRadius: 10,
            boxShadow: '0 5px 20px rgba(0,0,0,0.30), 0 10px 10px rgba(0,0,0,0.22)',
            display: 'flex',
            flexDirection: 'column',
        },
        button: {
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
            marginTop: theme.spacing(2),
        },
        wrap: {
            overflowWrap: 'anywhere',
        },
        divider: {
            margin: theme.spacing(1),
        },
        alertMessage: {
            textAlign: 'center',
            flex: 1,
        },
    }),
);

const NewGame: React.FC = () => {
    const { id } = useParams<{ id: string }>();
    const { pkh } = useWalletContext();
    const {
        loading,
        error: errors,
        data,
    } = useQuery<{ channels: SC_Channel[] }>(Query.Channel.CHANNEL, {
        variables: { id },
    });
    const classes = useStyles();
    const [error, setError] = React.useState<string>();
    const [success, setSuccess] = React.useState<string>();
    const [modelKind, setModelKind] = React.useState<string>();
    const [nonce, setNonce] = React.useState(randomNonce());
    const [player1, setPlayer1] = React.useState<string>();
    const [player2, setPlayer2] = React.useState<string>();
    const [playDelay, setPlayDelay] = React.useState<number>(0);
    const [settlements, setSettlements] = React.useState<SC_Settlement[]>([]);
    const [initialArgs, setInitialArgs] = React.useState<Record<string, string>>({});
    const models = useModels();

    const [modelInitType, modelInitArgs] = React.useMemo(() => {
        if (modelKind && models[modelKind]?.metadata['init_type']) {
            const type = JSON.parse(models[modelKind].metadata['init_type']);
            return [type, Encoders.Michelson.extractArgumentsFromType(type)];
        }
        return [];
    }, [modelKind]);

    const handleInitialArgChange: React.ChangeEventHandler<HTMLInputElement> = React.useCallback((e) => {
        setInitialArgs((args) => ({
            ...args,
            [e.target.id]: e.target.value,
        }));
    }, []);

    const players = React.useMemo(
        () =>
            data?.channels.reduce<string[]>((acc, { participants }) => {
                participants.forEach(({ publicKeyHash }) => {
                    acc.push(publicKeyHash);
                });
                return acc;
            }, []),
        [data?.channels],
    );

    const outcomes = React.useMemo(() => (modelKind ? models[modelKind].outcomes : []), [models, modelKind]);

    const gameID = React.useMemo(() => {
        if (id && modelKind && nonce) {
            return Hashers.Game.hashID({
                channelID: id,
                gameNonce: nonce,
                modelID: models[modelKind].id,
            });
        }
    }, [id, nonce, modelKind]);

    const handleSelection = React.useCallback((e: SelectChangeEvent<string>) => {
        switch (e.target.name) {
            case 'player1_select':
                setPlayer1(e.target.value);
                break;
            case 'player2_select':
                setPlayer2(e.target.value);
                break;
            case 'model_select':
                // Reset args
                setInitialArgs({});
                // Set model kind
                setModelKind(e.target.value);
                break;
        }
    }, []);

    const handleChange: React.ChangeEventHandler<HTMLInputElement> = React.useCallback((e) => {
        switch (e.target.id) {
            case 'play_delay':
                setPlayDelay(Number(e.target.value));
                break;
        }
    }, []);

    const handleAddSettlement = (settlement: SC_Settlement) => {
        setSettlements((settlements) => {
            for (const i in settlements) {
                if (
                    settlements[i].kind === settlement.kind &&
                    settlements[i].value === settlement.value &&
                    settlements[i].sender === settlement.sender &&
                    settlements[i].recipient === settlement.recipient
                ) {
                    Object.entries(settlement.bonds).forEach(([tokenID, amount]) => {
                        settlements[i].bonds[Number(tokenID)] = amount;
                    });
                    return [...settlements];
                }
            }
            return [...settlements, settlement];
        });
    };

    const handleRemoveSettlement = (settlement: SC_Settlement) => {
        setSettlements((settlements) =>
            settlements.filter(
                (st) =>
                    !(
                        st.kind === settlement.kind &&
                        st.value === settlement.value &&
                        st.sender === settlement.sender &&
                        st.recipient === settlement.recipient
                    ),
            ),
        );
    };

    const onNonceChange: React.ChangeEventHandler<HTMLInputElement> = (e) => {
        setNonce(e.target.value);
    };

    const injectOperation = React.useCallback(async () => {
        setError('');
        setSuccess('');
        try {
            if (!modelKind) {
                return setError('You must select a model.');
            }
            if (!playDelay) {
                return setError('You must provide a play delay and it shall be higher than 0.');
            }
            if (!player1 || !player2) {
                return setError('You must provide both players.');
            }

            const playDelayInSeconds = Number(playDelay) * 60;
            const publicKey = await Wallet.Beacon.signer.publicKey();
            const constants: GameConstants = {
                channelID: id,
                gameNonce: nonce,
                modelID: models[modelKind].id,
                playDelay: playDelayInSeconds,
                settlements: [
                    ...settlements,
                    {
                        kind: SC_SettlementKind.GameAborted,
                        bonds: {},
                    },
                ],
                bonds: settlements.reduce<Record<number, { [bond: number]: number }>>(
                    (acc, settlement) => {
                        Object.entries(settlement.bonds).forEach(([tokenID, amount]) => {
                            if (settlement.sender) {
                                acc[settlement.sender][Number(tokenID)] =
                                    (acc[settlement.sender][Number(tokenID)] || 0) + amount;
                            }
                        });

                        return acc;
                    },
                    {
                        1: {},
                        2: {},
                    },
                ),
                players: {
                    [player1]: 1,
                    [player2]: 2,
                },
            };
            const initParams = Packers.Game.packInitParams(modelInitType, initialArgs);
            const bytes = Packers.Game.packNewGameAction(initParams, constants);
            const signature = await Wallet.Beacon.signer.sign(bytes, undefined, SigningType.MICHELINE);

            // Store new game
            const result = await Api.Game.newGame({
                constants,
                initParams,
                signature: {
                    publicKey,
                    signature: signature.prefixSig,
                },
            });
            setSuccess(result.data?.result);
        } catch (e) {
            console.trace(e);
            setError(e.message);
        }
    }, [id, modelKind, nonce, playDelay, player1, player2, settlements]);

    if (loading || !pkh) {
        return (
            <Container maxWidth="md" sx={{ margin: 5 }}>
                <Typography variant="h3" color="text.secondary" gutterBottom>
                    Loading...
                </Typography>
            </Container>
        );
    }
    if (errors || !players) {
        return (
            <Container maxWidth="md" sx={{ margin: 5 }}>
                <Typography variant="h3" color="text.secondary" gutterBottom>
                    {errors?.message || 'Something went wrong'}
                </Typography>
            </Container>
        );
    }

    if (!data) {
        return (
            <Container maxWidth="md" sx={{ margin: 5 }}>
                <Typography variant="h3" color="text.secondary" gutterBottom>
                    The channel does not exist.
                </Typography>
            </Container>
        );
    }
    return (
        <>
            <Container maxWidth="md" sx={{ margin: 5 }}>
                <Box
                    component="form"
                    className={classes.container}
                    sx={{ border: '1px solid', borderColor: 'primary.main' }}
                    autoComplete="off"
                    onSubmit={injectOperation}
                >
                    <Typography variant="h4" textAlign="center" gutterBottom>
                        New Game
                    </Typography>
                    <div className={classes.divider} />
                    {error && (
                        <Alert variant="outlined" severity="error" onClose={() => setError('')}>
                            <Typography className={classes.wrap}>{error}</Typography>
                        </Alert>
                    )}
                    <div className={classes.divider} />
                    <FormControl fullWidth margin="normal">
                        <InputLabel id="game-model">Game Model</InputLabel>
                        <Select
                            name="model_select"
                            labelId="game-model"
                            label="Game Model"
                            value={modelKind || ''}
                            onChange={handleSelection}
                        >
                            {Object.values(models || {})
                                .filter(({ metadata }) => !!metadata.name)
                                .map(({ metadata }) => (
                                    <MenuItem value={metadata.name} key={metadata.name}>
                                        {metadata.name?.replaceAll(/[_-]/g, ' ').toUpperCase()}
                                    </MenuItem>
                                ))}
                        </Select>
                    </FormControl>
                    <NonceTextField
                        id="nonce"
                        value={nonce}
                        label="Game nonce"
                        variant="outlined"
                        margin="normal"
                        onChange={onNonceChange}
                        onClick={() => setNonce(randomNonce())}
                    />
                    <TextField
                        id="play_delay"
                        value={playDelay}
                        type="number"
                        inputProps={{
                            min: 0,
                        }}
                        onChange={handleChange}
                        label="Play Delay (Minutes a player has to play on-chain when the other player calls starving)"
                        margin="normal"
                        variant="outlined"
                    />
                    <div className={classes.divider} />
                    <Typography variant="h6">Initialization Parameters</Typography>
                    <Divider flexItem />
                    {Object.entries(modelInitArgs || {}).map(([field, type], i) => (
                        <TextField
                            key={field}
                            id={field}
                            onChange={handleInitialArgChange}
                            value={initialArgs[field] || ''}
                            label={field}
                            placeholder={type}
                            margin={i % 2 === 0 ? 'normal' : undefined}
                            variant="outlined"
                        />
                    ))}
                    <div className={classes.divider} />
                    <Typography variant="h6">Players</Typography>
                    <Divider flexItem />
                    <div className={classes.divider} />
                    <FormControl fullWidth error={player1 === player2}>
                        <InputLabel id="player1">Player 1 Address</InputLabel>
                        <Select
                            name="player1_select"
                            labelId="player1"
                            label="Player 1 Address"
                            value={player1 || ''}
                            onChange={handleSelection}
                        >
                            {players?.map((player) => (
                                <MenuItem value={player} key={player}>
                                    <Typography margin={1} variant="overline">
                                        {player}
                                    </Typography>
                                    <Chip label={pkh === player ? 'You' : 'Opponent'} />
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                    <div className={classes.divider} />
                    <FormControl fullWidth error={player1 === player2}>
                        <InputLabel id="player2">Player 2 Address</InputLabel>
                        <Select
                            name="player2_select"
                            labelId="player2"
                            label="Player 2 Address"
                            value={player2 || ''}
                            onChange={handleSelection}
                        >
                            {players?.map((player) => (
                                <MenuItem value={player} key={player}>
                                    <Typography margin={1} variant="overline">
                                        {player}
                                    </Typography>
                                    <Chip label={pkh === player ? 'You' : 'Opponent'} />
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                    <div className={classes.divider} />
                    <Typography variant="h6">Settlements</Typography>
                    <Divider flexItem />
                    <div className={classes.divider} />
                    <Settlements
                        settlements={settlements}
                        yourPlayerNumber={player1 === pkh ? 1 : 2}
                        handleRemoveSettlement={handleRemoveSettlement}
                    />
                    <div className={classes.divider} />
                    <Divider flexItem />
                    <AddSettlements
                        player1={player1}
                        player2={player2}
                        onAdd={handleAddSettlement}
                        outcomes={outcomes}
                    />
                    <div className={classes.divider} />
                    <Button className={classes.button} onClick={injectOperation}>
                        Send game request
                    </Button>
                </Box>
            </Container>
            <Dialog open={!!success} onClose={() => setSuccess('')}>
                <Grid container direction="column" justifyContent="center">
                    <Alert
                        variant="outlined"
                        severity="success"
                        icon={false}
                        classes={{ message: classes.alertMessage }}
                    >
                        {success}
                    </Alert>
                    <div className={classes.divider} />
                    <RouterFab to={`/channels/${id}/games/${gameID}`}>Open Game</RouterFab>
                </Grid>
            </Dialog>
        </>
    );
};

export default NewGame;
