import React from 'react';
import { useSubscription } from '@apollo/client';
import { useParams } from 'react-router-dom';

import { createStyles, makeStyles } from '@mui/styles';
import { Theme } from '@mui/material/styles';
import {
    Typography,
    Alert,
    Grid,
    Container,
    Divider,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    Box,
    Chip,
} from '@mui/material';

import {
    GameConstants,
    Packers,
    Api,
    SC_Game,
    Cryptography,
    EntryPoints,
    SC_GameMoveAction,
    Time,
    OffchainViews,
} from 'state-channels-common';
import { SigningType } from '@airgap/beacon-sdk';

import Dialog from 'src/components/base/Dialog';
import { Subscription } from 'src/services/graphql';
import useWalletContext from 'src/hooks/useWalletContext';
import Wallet from 'src/services/wallet';
import Fab from 'src/components/base/Fab';
import TicTacToe from './models/TicTacToe';
import Logger from 'src/services/logger';
import Table from 'src/components/base/Table';
import GameInfo from './components/GameInfo';
import CountDownTime from 'src/components/base/CountDownTime';
import Button from 'src/components/base/Button';
import HeadTail from './models/HeadTail';
import { ModelKind } from './models/enums';
import useAppContext from 'src/hooks/useAppContext';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        divider: {
            margin: theme.spacing(1),
        },
        alertMessage: {
            textAlign: 'center',
            flex: 1,
        },
    }),
);

const Game: React.FC = () => {
    const { channelID, gameID } = useParams<{ channelID: string; gameID: string }>();
    const { platform } = useAppContext();
    const { pkh, publicKey } = useWalletContext();
    const { loading, error, data } = useSubscription<{ games: SC_Game[] }, { id: string }>(Subscription.Game.GAME, {
        variables: { id: gameID },
    });
    const classes = useStyles();
    const [err, setError] = React.useState<string>();
    const [success, setSuccess] = React.useState<string>();
    const [showInfo, setShowInfo] = React.useState(false);

    const game = React.useMemo(
        () => (typeof data !== 'undefined' && data?.games.length ? data.games[0] : undefined),
        [data],
    );

    const latestMove: SC_GameMoveAction | undefined = React.useMemo(
        () => (game?.latestMove?.length ? game.latestMove[0] : undefined),
        [game],
    );

    const gameAccepted = React.useMemo(() => game?.signatures.some(({ publicKey: pk }) => pk === publicKey), [game]);

    const acceptGame = React.useCallback(async () => {
        setError('');
        setSuccess('');
        try {
            const publicKey = await Wallet.Beacon.signer.publicKey();
            if (game) {
                if (game.signatures.length !== 1) {
                    return setError(`The oponnent did not signed the game yet!`);
                }
                const opponentSignature = game.signatures[0];

                // Prepare new game bytes
                const constants: GameConstants = {
                    channelID: channelID,
                    gameNonce: game?.nonce,
                    modelID: game?.modelID,
                    playDelay: game?.playDelay,
                    settlements: game?.settlements,
                    bonds: game?.bonds,
                    players: game?.players,
                };
                const bytes = Packers.Game.packNewGameAction(game.initParams, constants);

                // Validate opponent signature
                const publicKeyHash = await Cryptography.hashPublicKey(opponentSignature.publicKey);
                // Fail if public key is not owned by one of the players
                if (!Object.keys(game.players).includes(publicKeyHash)) {
                    return setError('This new game was not signed by the opponent.');
                }
                const isValid = await Cryptography.verifySignature(
                    bytes,
                    opponentSignature.signature,
                    opponentSignature.publicKey,
                );
                // Fail if the signature is invalid
                if (!isValid) {
                    return setError('The contents signed by the opponent are not the same you are signing.');
                }

                // Sign the new game confirmation
                const signature = await Wallet.Beacon.signer.sign(bytes, undefined, SigningType.MICHELINE);

                // Validate new game action before acepting the game (This will throw if something is invalid)
                await OffchainViews.offchain_new_game(platform, constants, game.initParams, [
                    ...game.signatures,
                    {
                        publicKey,
                        signature: signature.prefixSig,
                    },
                ]);

                await Api.Game.acceptGame(game.id, {
                    publicKey,
                    signature: signature.prefixSig,
                });
            }
        } catch (e) {
            Logger.debug(e);
            setError(e.message);
        }
    }, [channelID, game]);

    const ModelView = React.useCallback(() => {
        switch (game?.model.metadata['name']) {
            case ModelKind.tictactoe:
                return <TicTacToe platform={platform} game={game} onError={setError} onSuccess={setSuccess} />;
            case ModelKind.head_tail:
                return <HeadTail platform={platform} game={game} onError={setError} onSuccess={setSuccess} />;
        }
        return (
            <Typography variant="overline" color="text.secondary" gutterBottom>
                Model <Chip label={game?.model.metadata['name']} /> doesn't have any view implemented.
            </Typography>
        );
    }, [game]);

    const starving = React.useCallback(
        async (flag: boolean) => {
            setError('');
            setSuccess('');
            if (game) {
                try {
                    // Call "starving" entrypoint
                    await Wallet.Beacon.transfer(platform.id, EntryPoints.Game.starving(game.id, flag));
                } catch (e) {
                    Logger.debug(e);
                    setError(e.message);
                }
            }
        },
        [game],
    );

    const starved = React.useCallback(
        async (playerID: number) => {
            setError('');
            setSuccess('');
            if (game) {
                try {
                    // Call "starved" entrypoint
                    await Wallet.Beacon.transfer(platform.id, EntryPoints.Game.starved(game.id, playerID));
                } catch (e) {
                    Logger.debug(e);
                    setError(e.message);
                }
            }
        },
        [game],
    );

    if (loading || !pkh) {
        return (
            <Container maxWidth="md" sx={{ margin: 5 }}>
                <Typography variant="h3" color="text.secondary" gutterBottom>
                    Loading...
                </Typography>
            </Container>
        );
    }

    if (error) {
        return (
            <Container maxWidth="md" sx={{ margin: 5 }}>
                <Typography variant="h3" color="text.secondary" gutterBottom>
                    {error?.message || 'Something went wrong'}
                </Typography>
            </Container>
        );
    }
    if (!game) {
        return (
            <Container maxWidth="md" sx={{ margin: 5 }}>
                <Typography variant="h3" color="text.secondary" gutterBottom>
                    The game does not exist.
                </Typography>
            </Container>
        );
    }
    if (!Object.keys(game.players).includes(pkh)) {
        return (
            <Container maxWidth="md" sx={{ margin: 5 }}>
                <Typography variant="h3" color="text.secondary" gutterBottom>
                    You are not a player in this game.
                </Typography>
            </Container>
        );
    }

    if (
        !game.onChain &&
        game.signatures.some(({ publicKey: pk }) => pk === publicKey) &&
        game.signatures.length !== Object.keys(game.players).length
    ) {
        return (
            <Container maxWidth="md" sx={{ margin: 5 }}>
                <Grid container direction="column" justifyContent="center" spacing={2}>
                    <Grid item display="flex" justifyContent="center">
                        <Typography variant="overline" textAlign="center" color="text.secondary" gutterBottom>
                            The opponent has not accepted the game yet.
                        </Typography>
                    </Grid>
                </Grid>
            </Container>
        );
    }

    if ((!game.onChain && !gameAccepted) || showInfo) {
        return (
            <Container maxWidth="md" sx={{ margin: 5 }}>
                <Grid container direction="column" justifyContent="center">
                    <Grid item display="flex" justifyContent="center">
                        <GameInfo game={game} gameAccepted={gameAccepted || showInfo} />
                    </Grid>
                    <Grid item display="flex" justifyContent="center" sx={{ marginTop: 5 }}>
                        {showInfo ? (
                            <Fab variant="extended" color="primary" onClick={() => setShowInfo(false)}>
                                Go back to game
                            </Fab>
                        ) : (
                            <Fab variant="extended" color="primary" onClick={acceptGame}>
                                Accept game
                            </Fab>
                        )}
                    </Grid>
                </Grid>

                <Dialog open={!!err} onClose={() => setError('')}>
                    <Alert variant="outlined" severity="error" icon={false} classes={{ message: classes.alertMessage }}>
                        {err}
                    </Alert>
                </Dialog>
            </Container>
        );
    }

    return (
        <>
            <Container maxWidth="md" sx={{ margin: 5 }}>
                <Grid container justifyContent="center" alignItems="center">
                    <Grid
                        item
                        xs={12}
                        display="flex"
                        justifyContent="center"
                        alignItems="center"
                        sx={{ marginBottom: 5 }}
                    >
                        <Fab onClick={() => setShowInfo(true)}>Show Game Info</Fab>
                    </Grid>
                    <Grid item xs={12} display="flex" flexDirection="column" sx={{ marginBottom: 5 }}>
                        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                            <Typography variant="overline">Timeouts</Typography>
                            {game?.outcome || latestMove?.outcome ? null : game.timeouts.filter(
                                  (t) => t.playerID !== game.players[pkh],
                              ).length ? (
                                <Button onClick={() => starving(false)}>Stop starving</Button>
                            ) : (
                                <Button onClick={() => starving(true)}>Start starving</Button>
                            )}
                        </div>
                        <Divider flexItem sx={{ marginBottom: 1, marginTop: 0.2 }} />
                        <Table
                            header={
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Player</TableCell>
                                        <TableCell align="right">Timeout</TableCell>
                                        <TableCell align="right">Timeout in</TableCell>
                                        <TableCell align="right">Last Update</TableCell>
                                        {game?.outcome || latestMove?.outcome ? null : (
                                            <TableCell align="right"></TableCell>
                                        )}
                                    </TableRow>
                                </TableHead>
                            }
                            body={
                                <TableBody>
                                    {game.timeouts ? (
                                        game.timeouts.map((timeout) => (
                                            <TableRow key={timeout.playerID}>
                                                <TableCell>
                                                    {timeout.playerID === game.players[pkh] ? 'Opponent' : 'You'}
                                                </TableCell>
                                                <TableCell align="right">
                                                    {Time.prettifyTimestamp(timeout.timeout)}
                                                </TableCell>
                                                <TableCell align="right">
                                                    <CountDownTime targetTime={timeout.timeout} />
                                                </TableCell>
                                                <TableCell align="right">
                                                    {Time.prettifyTimestamp(timeout.updatedAt)}
                                                </TableCell>

                                                {game?.outcome || latestMove?.outcome ? null : (
                                                    <TableCell align="right">
                                                        <Fab
                                                            variant="extended"
                                                            color="primary"
                                                            onClick={() => starved(timeout.playerID)}
                                                        >
                                                            Starved
                                                        </Fab>
                                                    </TableCell>
                                                )}
                                            </TableRow>
                                        ))
                                    ) : (
                                        <TableRow>
                                            <TableCell colSpan={6}>
                                                <Box
                                                    sx={{
                                                        display: 'flex',
                                                        justifyContent: 'center',
                                                        alignItems: 'center',
                                                    }}
                                                >
                                                    <Typography variant="overline" textAlign="center">
                                                        No active timeouts
                                                    </Typography>
                                                </Box>
                                            </TableCell>
                                        </TableRow>
                                    )}
                                </TableBody>
                            }
                        />
                    </Grid>

                    <Grid
                        item
                        md={12}
                        display="flex"
                        justifyContent="center"
                        alignItems="center"
                        flexDirection="column"
                    >
                        <ModelView />
                    </Grid>
                </Grid>
            </Container>

            <Dialog open={!!success} onClose={() => setSuccess('')}>
                <Grid container direction="column" justifyContent="center">
                    <Alert
                        variant="outlined"
                        severity="success"
                        icon={false}
                        classes={{ message: classes.alertMessage }}
                    >
                        Channel was successfully created!
                    </Alert>
                    <div className={classes.divider} />
                </Grid>
            </Dialog>

            <Dialog open={!!err} onClose={() => setError('')}>
                <Alert variant="outlined" severity="error" icon={false} classes={{ message: classes.alertMessage }}>
                    {err}
                </Alert>
            </Dialog>
        </>
    );
};

export default Game;
