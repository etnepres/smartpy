import { Encoders, Packers } from 'state-channels-common';

/**
 * @description Pack game state
 * @param {number[][]} state Game state
 * @returns {string} Packed game state
 */
export function packState(state?: number[][]): string {
    // Default state (Necessary at initialization)
    state = state || [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0],
    ];
    return Packers.packData(
        state.map(
            (row, rowIndex) => ({
                prim: 'Elt',
                args: [
                    {
                        int: String(rowIndex),
                    },
                    row.map((value, colIndex) => ({
                        prim: 'Elt',
                        args: [
                            {
                                int: String(colIndex),
                            },
                            {
                                int: String(value),
                            },
                        ],
                    })),
                ],
            }),
            {
                prim: 'map',
                args: [
                    { prim: 'int' },
                    {
                        prim: 'map',
                        args: [{ prim: 'int' }, { prim: 'int' }],
                    },
                ],
            },
        ),
    );
}

/**
 * @description Unpack game state
 * @param {string} state Packed game state
 * @returns {number[][]} Unpacked game state
 */
export function unpackState(state: string): number[][] {
    const type: any = {
        prim: 'map',
        args: [
            {
                prim: 'nat',
            },
            {
                prim: 'map',
                args: [
                    {
                        prim: 'nat',
                    },
                    {
                        prim: 'nat',
                    },
                ],
            },
        ],
    };

    const boardMap: any = Encoders.Michelson.translateMichelson(type, state);

    // build board from map
    const board: number[][] = [];
    for (const [key, value] of boardMap.entries()) {
        const rowIndex = Number(key);
        board[rowIndex] = [];
        for (const [col, player] of value.entries()) {
            board[rowIndex][Number(col)] = player.toNumber();
        }
    }
    return board;
}

const TicTacToeUtils = {
    packState,
    unpackState,
};

export default TicTacToeUtils;
