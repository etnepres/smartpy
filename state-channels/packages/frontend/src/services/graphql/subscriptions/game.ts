import { gql } from '@apollo/client';

export const GAMES = gql`
    subscription Games($channelID: String!) {
        games: game(order_by: { created_at: desc }, where: { channel_id: { _eq: $channelID } }, limit: 30) {
            id
            bonds
            channelID: channel_id
            nonce: game_nonce
            modelID: model_id
            playDelay: play_delay
            settlements
            players
            currentPlayer: current_player
            initParams: init_params
            metadata
            moveNumber: move_nb
            onChain: on_chain
            outcome
            settled
            state
            createdAt: created_at
            updatedAt: updated_at
            timeouts {
                playerID: player_id
                timeout
                createdAt: created_at
                updatedAt: updated_at
            }
            signatures {
                publicKey: public_key
                signature
            }
            model {
                metadata
                outcomes
            }
            moves {
                id
                move_number
                outcome
                signatures {
                    public_key
                    signature
                }
            }
        }
    }
`;

export const GAME = gql`
    subscription Game($id: String!) {
        games: game(where: { id: { _eq: $id } }, limit: 1) {
            id
            bonds
            channelID: channel_id
            nonce: game_nonce
            modelID: model_id
            playDelay: play_delay
            settlements
            players
            currentPlayer: current_player
            initParams: init_params
            metadata
            moveNumber: move_nb
            onChain: on_chain
            outcome
            settled
            state
            createdAt: created_at
            updatedAt: updated_at
            timeouts {
                playerID: player_id
                timeout
                createdAt: created_at
                updatedAt: updated_at
            }
            signatures {
                publicKey: public_key
                signature
            }
            model {
                metadata
                outcomes
            }
            latestMove: moves(
                where: { total_game_move_signatures: { _gte: 2 } }
                order_by: { move_number: desc }
                limit: 1
            ) {
                id
                moveNumber: move_number
                playerNumber: player_number
                outcome
                state
                move: move_data
                signatures {
                    publicKey: public_key
                    signature
                }
            }
            pendingMove: moves(
                where: { total_game_move_signatures: { _lt: 2 } }
                order_by: { move_number: desc }
                limit: 1
            ) {
                id
                moveNumber: move_number
                playerNumber: player_number
                outcome
                state
                move: move_data
                signatures {
                    publicKey: public_key
                    signature
                }
            }
        }
    }
`;
