import { gql } from '@apollo/client';

export const MODELS = gql`
    subscription Models($platform: String!) {
        models: model(where: { platform: { id: { _eq: $platform } } }, order_by: { created_at: desc }, limit: 30) {
            id
            metadata
            outcomes
            createdAt: created_at
            updatedAt: updated_at
        }
    }
`;
