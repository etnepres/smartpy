import { gql } from '@apollo/client';

export const TOKENS = gql`
    subscription Tokens {
        tokens: token(order_by: { id: asc }) {
            id
            metadata
        }
    }
`;
