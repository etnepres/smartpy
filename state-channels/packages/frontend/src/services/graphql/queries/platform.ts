import { gql } from '@apollo/client';

export const Platforms = gql`
    query Platforms {
        platforms: platform {
            id
            ledger
            metadata
        }
    }
`;
