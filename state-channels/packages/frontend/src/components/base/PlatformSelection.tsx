import React from 'react';
import { FormControl, FormControlProps, InputLabel, MenuItem, Select, SelectChangeEvent } from '@mui/material';
import usePlatforms from 'src/hooks/usePlatforms';
import useAppContext from 'src/hooks/useAppContext';

const PlatformSelection: React.FC<FormControlProps> = (props) => {
    const { platform, changePlatform } = useAppContext();
    const platforms = usePlatforms();

    const handleChange = (event: SelectChangeEvent<string>) => {
        const platform = platforms.find(({ id }) => id === event.target.value);
        platform && changePlatform(platform);
    };

    if (platforms.length) {
        return (
            <FormControl margin="dense" {...props}>
                <InputLabel id="platform-selection">Platform</InputLabel>
                <Select
                    name="platform_select"
                    labelId="platform-selection"
                    label="Platform"
                    value={platform.id}
                    onChange={handleChange}
                >
                    {Object.values(platforms).map(({ id }) => (
                        <MenuItem value={id} key={id}>
                            {id}
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
        );
    }

    return null;
};

export default PlatformSelection;
