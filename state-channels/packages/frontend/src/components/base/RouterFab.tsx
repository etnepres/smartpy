import React from 'react';
import { Link } from 'react-router-dom';
import { LocationDescriptor } from 'history';
import { Fab, FabProps } from '@mui/material';

interface OwnProps extends FabProps {
    to: LocationDescriptor;
}

const RouterFab: React.FC<OwnProps> = ({ to, ...props }) => (
    <Fab
        variant="extended"
        {...props}
        component={React.forwardRef<HTMLAnchorElement, any>((linkProps, ref) => (
            <Link {...linkProps} to={to} ref={ref} />
        ))}
    />
);

export default RouterFab;
