import React from 'react';

import * as jdenticon from 'jdenticon';

interface OwnProps {
    value: string;
}

const Avatar: React.FC<OwnProps> = ({ value }) => (
    <div dangerouslySetInnerHTML={{ __html: jdenticon.toSvg(value, 32) }} style={{ display: 'flex' }} />
);

export default Avatar;
