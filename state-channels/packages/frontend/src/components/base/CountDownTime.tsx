import React from 'react';

import { Time } from 'state-channels-common';

interface OwnProps {
    targetTime: string | number;
}

const remaining = (targetTime: string | number) =>
    Time.getDuration(Time.getCurrentInstant().until(Time.instantFrom(targetTime)).seconds * Time.SECOND);

const CountDownTime: React.FC<OwnProps> = ({ targetTime }) => {
    const [remainingTime, setRemainingTime] = React.useState(remaining(targetTime));

    React.useEffect(() => {
        const timeout = setTimeout(() => {
            setRemainingTime(remaining(targetTime));
        }, Time.SECOND);
        return () => {
            clearInterval(timeout);
        };
    });

    return <React.Fragment>{remainingTime}</React.Fragment>;
};

export default CountDownTime;
