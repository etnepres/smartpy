import React from 'react';
import { TextField, TextFieldProps, IconButton } from '@mui/material';

import RefreshIcon from '@mui/icons-material/Autorenew';

const NonceTextField: React.FC<TextFieldProps> = ({ onClick, ...props }) => {
    return (
        <TextField
            {...props}
            disabled={!props.onChange}
            InputProps={{
                endAdornment: (
                    <IconButton onClick={onClick as () => void}>
                        <RefreshIcon />
                    </IconButton>
                ),
            }}
        />
    );
};

export default NonceTextField;
