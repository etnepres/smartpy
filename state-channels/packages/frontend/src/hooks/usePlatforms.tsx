import { useSubscription } from '@apollo/client';

import { Query } from 'src/services/graphql';
import Logger from 'src/services/logger';
import { SC_Platform } from 'state-channels-common';

function usePlatforms(): SC_Platform[] {
    const { error, data } = useSubscription<{ platforms: SC_Platform[] }>(Query.Platform.Platforms);

    if (error) {
        Logger.debug(error);
    }

    return data?.platforms || [];
}

export default usePlatforms;
