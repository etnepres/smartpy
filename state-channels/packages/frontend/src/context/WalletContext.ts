import { createContext } from 'react';

export interface IWalletContext {
    pkh?: string;
    publicKey?: string;
    connectWallet: () => Promise<void>;
}

const contextStub = {
    connectWallet: async () => {
        // stub
    },
};

const WalletContext = createContext<IWalletContext>(contextStub);

export default WalletContext;
