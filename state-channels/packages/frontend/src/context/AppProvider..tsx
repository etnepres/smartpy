import React from 'react';
import { Box, Typography } from '@mui/material';
import usePlatforms from 'src/hooks/usePlatforms';
import WalletProvider from './WalletProvider';
import AppContext from './AppContext';
import { SC_Platform } from '../../../common/src';

interface BaseInfoStorage {
    platform?: SC_Platform;
}

const STORAGE_KEY = '@SC/BaseInfo';
const storage: BaseInfoStorage = JSON.parse(localStorage.getItem(STORAGE_KEY) || '{}');

function updateStorage(storage: BaseInfoStorage) {
    localStorage.setItem(STORAGE_KEY, JSON.stringify(storage, null, 4));
}

const AppProvider: React.FC = ({ children }) => {
    const platforms = usePlatforms();
    const [platform, setPlatform] = React.useState<SC_Platform>();

    const changePlatform = React.useCallback((plat: SC_Platform) => {
        storage.platform = plat;
        setPlatform(plat);
        updateStorage(storage);
    }, []);

    React.useEffect(() => {
        const platform = platforms.find(({ id }) => id === storage?.platform?.id);

        if (platform) {
            return setPlatform(platform);
        }

        platforms.length && setPlatform(platforms[0]);
    }, [platforms]);

    if (!platform) {
        return (
            <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                <Typography variant="overline">SYNCHRONIZING...</Typography>
            </Box>
        );
    }

    return (
        <AppContext.Provider value={{ platform, changePlatform }}>
            <WalletProvider>{children}</WalletProvider>
        </AppContext.Provider>
    );
};

export default AppProvider;
