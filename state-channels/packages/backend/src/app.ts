import express from 'express';
import dotenv from 'dotenv';
import morgan from 'morgan';
import cors from 'cors';

import { createWriteStream } from 'fs';
import { format } from 'util';
import path from 'path';

import { GameService } from './api/services';
import { Api } from 'state-channels-common';

// Load ENVIRONMENT Variables
dotenv.config();

const app = express();

// Body parser
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// CORS middleware
app.use(cors());

const access = createWriteStream(path.join(__dirname, './output.log'), { flags: 'w' });

console.log = (...d: any) => {
    access.write(`[INFO] - ${format(d)} \n`);
};

console.debug = (...d: any) => {
    access.write(`[DEBUG] - ${format(d)} \n`);
};

const logFile = createWriteStream(path.join(__dirname, './access.log'), { flags: 'a' });
app.use(morgan('combined', { stream: logFile }));

// Game routes
app.post(Api.Routes.PATH.Games, GameService.newGame);
app.post(Api.Routes.PATH.AcceptGame, GameService.acceptGame);
app.post(Api.Routes.PATH.GameMove, GameService.gameMove);
app.post(Api.Routes.PATH.AcceptGameMove, GameService.acceptGameMove);

const port = parseInt(process.env.PORT || '3000', 10);
app.listen(port, () => console.info(`Listening on port ${port}!`));
