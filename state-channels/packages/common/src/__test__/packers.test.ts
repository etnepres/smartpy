import { MichelsonType } from '@taquito/michel-codec';
import { SC_SettlementKind } from '../@types';
import { Game } from '../utils/packers';

describe('Testing signatures', () => {
    it('new game', () => {
        const packedData = Game.packNewGameAction('05030b', {
            channelID: '81d6e2fae5bf2d18dee5e6a17d1ee5b39a8be604726b9b6c868a7ae250799d48',
            gameNonce: '20723165754',
            modelID: '4261e0508a06644ceb9339d4b85bde0c96f6e14fb0c5c01044d37f477e70b8e3',
            playDelay: 10,
            bonds: {
                1: {
                    0: 10,
                },
                2: {
                    0: 11,
                },
            },
            settlements: [
                {
                    kind: SC_SettlementKind.GameFinished,
                    value: 'player_1_won',
                    bonds: {
                        0: 10,
                    },
                    sender: 1,
                    recipient: 2,
                },
                {
                    kind: SC_SettlementKind.GameFinished,
                    value: 'player_2_won',
                    bonds: {
                        0: 11,
                    },
                    sender: 1,
                    recipient: 2,
                },
            ],
            players: {
                tz1NHco4L3SqhC8RboiSqXPZCRZsEgym3mff: 1,
                tz1THWgwHiDfCBk2cjipYPLE3DA6hWqmgEBX: 2,
            },
        });
        expect(packedData).toEqual(
            '05070701000000084e65772047616d6507070707020000001e07040001020000000607040000000a07040002020000000607040000000b07070a0000002081d6e2fae5bf2d18dee5e6a17d1ee5b39a8be604726b9b6c868a7ae250799d480707010000000b323037323331363537353407070a000000204261e0508a06644ceb9339d4b85bde0c96f6e14fb0c5c01044d37f477e70b8e30707000a0707020000003e070400010a0000001600001d0c1c01b2dfec703b53d3315102300746c1a648070400020a00000016000053dfa44489438013dec3de6e96b47cf6b550c9f8020000005a07040505010000000c706c617965725f315f776f6e02000000130707020000000607040000000a07070001000207040505010000000c706c617965725f325f776f6e02000000130707020000000607040000000b0707000100020a0000000305030b',
        );
    });
    it('packInitParams', () => {
        const type: MichelsonType = {
            prim: 'pair',
            args: [
                { prim: 'bytes', annots: ['%hash1'] },
                { prim: 'bytes', annots: ['%hash2'] },
            ],
        };
        const args = {
            hash1: '00',
            hash2: '01',
        };
        expect(Game.packInitParams(type, args)).toEqual('0507070a00000001000a0000000101');
    });
});
