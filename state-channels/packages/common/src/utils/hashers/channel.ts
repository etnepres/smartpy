import { blake2bHex } from 'blakejs';
import { ChannelID } from '../../@types/channel';
import { Packers } from '..';

export function hashID(data: ChannelID): string {
    return blake2bHex(Buffer.from(Packers.Channel.packID(data), 'hex'), undefined, 32);
}
