import { MichelsonData, MichelsonType, packDataBytes } from '@taquito/michel-codec';
import { Comparison } from '..';

import { ChannelID } from '../../@types/channel';

const ChannelIdType: MichelsonType = {
    prim: 'pair',
    args: [{ prim: 'address' }, { prim: 'map', args: [{ prim: 'address' }, { prim: 'key' }] }, { prim: 'string' }],
};

const buildID = (data: ChannelID): MichelsonData => ({
    prim: 'Pair',
    args: [
        { string: data.platformAddress },
        data.players
            .sort(({ address: address1 }, { address: address2 }) => Comparison.compare(address1, address2))
            .map(({ address, publicKey }) => ({
                prim: 'Elt',
                args: [{ string: address }, { string: publicKey }],
            })),
        { string: data.nonce },
    ],
});

export function packID(data: ChannelID): string {
    return packDataBytes(buildID(data), ChannelIdType).bytes;
}
