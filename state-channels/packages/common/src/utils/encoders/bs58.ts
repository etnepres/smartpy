import * as bs58check from 'bs58check';

export enum PREFIX_STRING {
    sig = 'sig',
    edsig = 'edsig',
    edpk = 'edpk',

    // public key hash
    tz1 = 'tz1',
}

export const PREFIX_BYTES: Record<PREFIX_STRING, Buffer> = {
    [PREFIX_STRING.sig]: Buffer.from([4, 13, 43]),
    [PREFIX_STRING.edsig]: Buffer.from([9, 245, 205, 134, 18]),
    [PREFIX_STRING.edpk]: Buffer.from([13, 15, 37, 217]),

    // public key hash
    [PREFIX_STRING.tz1]: Buffer.from([6, 161, 159]),
};

export const decodeCheck = (hex: string, prefix: PREFIX_STRING): Buffer => {
    return bs58check.decode(hex).slice(PREFIX_BYTES[prefix].length);
};

export const encodeCheck = (buffer: Buffer, prefix: PREFIX_STRING): string => {
    return bs58check.encode(Buffer.concat([PREFIX_BYTES[prefix], buffer]));
};
