import {
    MichelsonType as TaquitoMichelsonType,
    BytesLiteral,
    decodeAddressBytes,
    unpackDataBytes,
} from '@taquito/michel-codec';
import { Schema } from '@taquito/michelson-encoder';
import { Encoders } from '..';
import { PREFIX_STRING } from './bs58';

export interface MichelsonType {
    prim: 'pair' | 'string' | 'address' | 'bytes' | 'nat' | 'int' | 'option' | 'map' | 'bool' | 'or' | 'unit' | 'list';
    args?: this[];
    annots?: string[];
}

export interface MichelsonValue {
    int?: string;
    string?: string;
    bytes?: string;
    prim?: 'Pair' | 'None' | 'Some' | 'Elt' | 'True' | 'False';
    args?: this[];
}

export function extractFromMichelson(type: MichelsonType, value: MichelsonValue | MichelsonValue[]): any {
    if (Array.isArray(value)) {
        switch (type.prim) {
            case 'map':
                return value.reduce<{ key: unknown; value: unknown }[]>((acc, v) => {
                    if (type.args && v.args) {
                        return [
                            ...acc,
                            {
                                key: extractFromMichelson(type.args?.[0], v.args[0]),
                                value: extractFromMichelson(type.args?.[1], v.args[1]),
                            },
                        ];
                    }
                    throw new Error(`Invalid michelson (${type.prim})`);
                }, []);
        }
        throw new Error(`Invalid michelson type (${type.prim})`);
    }

    switch (type.prim) {
        case 'or':
            console.error(type, value);
            // return {
            //     kind: extractAnnot(type.annots),
            //     value: extractFromMichelson(type.args?.[0], v)
            // }
            break;
        case 'bytes':
            return value.bytes;
        case 'string':
            return value.string;
        case 'address':
            if (value.bytes) {
                return Encoders.Bs58.encodeCheck(
                    Buffer.from(decodeAddressBytes(value as BytesLiteral).hash),
                    PREFIX_STRING.tz1,
                );
            }
            throw new Error(`Invalid Michelson (${type.prim})`);
        case 'nat':
        case 'int':
            if (value.int) {
                return BigInt(value.int);
            }
            throw new Error(`Invalid Michelson (${type.prim})`);
        case 'option':
            switch (value.prim) {
                case 'None':
                    return null;
                case 'Some':
                    if (type.args?.[0] && value.args?.[0]) {
                        return extractFromMichelson(type.args[0], value.args[0]);
                    }
            }
            throw new Error(`Invalid Michelson (${type.prim})`);
        case 'bool':
            switch (value.prim) {
                case 'False':
                    return false;
                case 'True':
                    return true;
            }
            throw new Error(`Invalid Michelson (${type.prim})`);
        case 'pair':
            return value.args?.reduce((acc, v, i) => {
                if (type.args?.[i]) {
                    if (type.args?.[i].annots) {
                        return {
                            ...acc,
                            [extractMichelsonAnnotation(type.args?.[i].annots)]: extractFromMichelson(
                                type.args?.[i],
                                v,
                            ),
                        };
                    }
                    return {
                        ...acc,
                        ...extractFromMichelson(type.args?.[i], v),
                    };
                }
                throw new Error(`Invalid michelson type (${type.prim})`);
            }, {});
    }
}

export function extractMichelsonAnnotation(annots?: string[]): string {
    return annots?.[0].replace('%', '') || '';
}

export function extractArgumentsFromType(type: MichelsonType): Record<string, string> {
    const annotation = extractMichelsonAnnotation(type.annots);
    switch (type.prim) {
        case 'pair':
            return (
                type.args?.reduce((acc, arg) => {
                    return {
                        ...acc,
                        ...extractArgumentsFromType(arg),
                    };
                }, {}) || {}
            );
        default:
            if (!annotation) {
                return {};
            }
            return {
                [annotation]: type.prim,
            };
    }
}

/**
 * @description Translate packed bytes into a readable values
 * @param type Michelson type
 * @param bytes Packed bytes
 * @returns {T} Readable values
 */
export function translateMichelson<T>(type: TaquitoMichelsonType, bytes: string): T {
    const schema = new Schema(type);
    return schema.Execute(unpackDataBytes({ bytes }, type));
}
