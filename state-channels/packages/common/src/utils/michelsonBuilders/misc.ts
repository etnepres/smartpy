import { MichelsonData } from '@taquito/michel-codec';
import { extractMichelsonAnnotation, MichelsonType } from '../encoders/michelson';

/**
 * @description Build michelson value from its type
 * @param type Michelson type
 * @param args Values mapping to each annotation
 * @returns
 */
export function buildDataFromType(type: MichelsonType, args: Record<string, unknown>): MichelsonData {
    const annotation = extractMichelsonAnnotation(type.annots);
    switch (type.prim) {
        case 'unit':
            return {
                prim: 'Unit',
            };
        case 'pair':
            return {
                prim: 'Pair',
                args: type.args?.map((arg) => buildDataFromType(arg, args)) || [],
            };
        case 'string':
        case 'address':
            return {
                string: String(args[annotation]),
            };
        case 'bytes':
            return {
                bytes: String(args[annotation]),
            };
        case 'int':
        case 'nat':
            return {
                int: String(args[annotation]),
            };
    }
    throw new Error('Invalid michelson type.');
}
