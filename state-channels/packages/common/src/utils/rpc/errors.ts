/**
 * @description Interpret Tezos RPC error
 */
export function parseError(error: any): string {
    if (error?.isAxiosError) {
        if (Array.isArray(error.response.data)) {
            return JSON.stringify([...error.response.data].reverse()[0]?.with);
        }
    }

    return error.message;
}
