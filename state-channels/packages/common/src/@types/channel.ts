import { SC_Game } from './game';

export interface SC_ChannelParticipantBond {
    amount: number;
    id: number;
}

export interface SC_ChannelParticipantWithdraw {
    challenge?: string[];
    challengeTokens: Record<string, number>;
    timeout: number;
    tokens: Record<string, number>;
}

export interface SC_ChannelParticipant {
    publicKeyHash: string;
    publicKey: string;
    withdrawID: string;
    withdraw: SC_ChannelParticipantWithdraw;
    bonds: SC_ChannelParticipantBond[];
}

export interface SC_Channel {
    id: string;
    closed: boolean;
    nonce: string;
    withdrawDelay: number;
    participants: SC_ChannelParticipant[];
    games: SC_Game[];
    model: {
        name: string;
        outcomes: string[];
    };
    updatedAt: string;
    createdAt: string;
}

export interface ChannelID {
    platformAddress: string;
    players: {
        address: string;
        publicKey: string;
    }[];
    nonce: string;
}
