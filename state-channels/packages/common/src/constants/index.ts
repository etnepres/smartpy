export * from './tokens';

export const Constants =
    process.env.NODE_ENV === 'development'
        ? {
              network: {
                  type: 'granadanet',
              },
              rpc: 'https://granadanet.smartpy.io',
              api: 'http://localhost:3005',
              graphQlHttp: 'http://localhost:8081/v1/graphql',
              graphQlWs: 'ws://localhost:8081/v1/graphql',
          }
        : {
              network: {
                  type: 'granadanet',
              },
              rpc: 'https://granadanet.smartpy.io',
              api: 'http://florencenet.smartpy.io:3005',
              graphQlHttp: 'http://florencenet.smartpy.io:8081/v1/graphql',
              graphQlWs: 'ws://florencenet.smartpy.io:8081/v1/graphql',
          };
