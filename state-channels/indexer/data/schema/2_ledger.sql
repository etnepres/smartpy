-- //////////////////
--
-- Ledger Information
--
-- //////////////////

--
-- Name: ledger; Type: TABLE;
--
CREATE TABLE public.ledger (
    id TEXT PRIMARY KEY
);
--
-- Name: TABLE ledger; Type: COMMENT;
--
COMMENT ON TABLE public.ledger IS 'Ledger information';

--
-- Name: token; Type: TABLE;
--
CREATE TABLE public.token (
    id BIGINT NOT NULL,
    ledger_id TEXT NOT NULL,
    metadata JSONB NOT NULL,
    updated_at TIMESTAMP NOT NULL DEFAULT NOW(),
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    PRIMARY KEY (id, ledger_id)
);
--
-- Name: TABLE token; Type: COMMENT;
--
COMMENT ON TABLE public.game_move IS 'Token Information';
--
-- Name: fk_token__ledger_id; Type: FOREIGN KEY;
--
ALTER TABLE token
ADD CONSTRAINT fk_token__ledger_id
FOREIGN KEY (ledger_id)
REFERENCES ledger (id);
