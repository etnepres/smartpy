-- /////////////////
--
-- Chain information
--
-- /////////////////

--
-- Name: state; Type: TABLE;
--
CREATE TABLE public.state (
    chain_id TEXT NOT NULL,
    l_block_level BIGINT,
    l_block_hash TEXT
);
--
-- Name: TABLE state; Type: COMMENT;
--
COMMENT ON TABLE public.state IS 'Chain information';
