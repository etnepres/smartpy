package crawler

import (
	"database/sql"
	"encoding/json"
	"time"

	"github.com/pkg/errors"
)

type channelPlayerBond struct {
	TokenID int64
	Amount  int64
}

type channelPlayerWithdraw struct {
	Challenge       []string        `json:"challenge"`
	ChallengeTokens map[int64]int64 `json:"challengeTokens"`
	Timeout         int64           `json:"timeout"`
	Tokens          map[int64]int64 `json:"tokens"`
}

type channelPlayer struct {
	Bonds         []channelPlayerBond
	PublicKeyHash string
	PublicKey     string
	Withdraw      *channelPlayerWithdraw
	WithdrawID    int64
}

type channel struct {
	ID            string
	Closed        bool
	Nonce         string
	WithdrawDelay int64
	Players       map[string]channelPlayer
}

type bigMapChannelDiff struct {
	Key   string `json:"Key"`
	Value struct {
		Closed        bool   `json:"closed"`
		Nonce         string `json:"nonce"`
		WithdrawDelay int64  `json:"withdraw_delay"`
		Players       []struct {
			Key   string `json:"Key"`
			Value struct {
				WithdrawID int64  `json:"withdraw_id"`
				PublicKey  string `json:"pk"`
				Bonds      []struct {
					Key   int64 `json:"Key"`
					Value int64 `json:"Value"`
				} `json:"bonds"`
				Withdraw *struct {
					Challenge       []string `json:"challenge"`
					Timeout         int64    `json:"timeout"`
					ChallengeTokens []struct {
						Key   int64 `json:"Key"`
						Value int64 `json:"Value"`
					} `json:"challenge_tokens"`
					Tokens []struct {
						Key   int64 `json:"Key"`
						Value int64 `json:"Value"`
					} `json:"tokens"`
				} `json:"withdraw,omitempty"`
			} `json:"Value"`
		} `json:"players"`
	} `json:"Value"`
}

func (c *Crawler) updateChannelParticipantBonds(channelPlayerID int64, bonds []channelPlayerBond) error {
	for _, bond := range bonds {
		var exists bool
		err := c.DB.QueryRow("SELECT EXISTS(SELECT 1 FROM channel_participant_bond WHERE channel_participant_id = $1 AND bond_id = $2)", channelPlayerID, bond.TokenID).Scan(&exists)
		if err != nil {
			return errors.Wrap(err, "Could not query table (channel_participant_bond).")
		}

		if exists {
			// Preparing channel_participant_bond update
			updateChannelParticipantBond, err := c.DB.Prepare(`UPDATE channel_participant_bond SET bond_id = $1, amount = $2 WHERE channel_participant_id = $3`)
			if err != nil {
				return errors.Wrap(err, "Could not prepare update statement for table (channel_participant_bond).")
			}

			_, err = updateChannelParticipantBond.Exec(
				bond.TokenID,
				bond.Amount,
				channelPlayerID,
			)
			if err != nil {
				return err
			}
		} else {
			err = c.insertChannelParticipantBond(channelPlayerID, bond)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (c *Crawler) insertChannelParticipantBond(channelPlayerID int64, bond channelPlayerBond) error {
	// Preparing channel_participant_bond insertion
	insertChannelParticipantBond, err := c.DB.Prepare(
		`INSERT INTO channel_participant_bond(
		channel_participant_id,
		bond_id,
		amount
	) VALUES($1, $2, $3)`)
	if err != nil {
		return errors.Wrap(err, "Could not prepare insert statement for table (channel_participant_bond).")
	}

	_, err = insertChannelParticipantBond.Exec(
		channelPlayerID,
		bond.TokenID,
		bond.Amount,
	)
	if err != nil {
		return errors.Wrap(err, "Could not insert on table (channel_participant_bond).")
	}

	return nil
}

func (c *Crawler) updateChannelParticipants(channelID string, participants map[string]channelPlayer) error {
	for _, participant := range participants {
		var id int64
		err := c.DB.QueryRow("SELECT id FROM channel_participant WHERE channel_id = $1 AND public_key_hash = $2", channelID, participant.PublicKeyHash).Scan(&id)
		if err != nil && err != sql.ErrNoRows {
			return errors.Wrap(err, "Could not query table (channel_participant).")
		}

		// Add channel if it doesn't exist
		if id == 0 {
			id, err = c.insertChannelParticipant(channelID, participant)
			if err != nil {
				return errors.Wrap(err, "Could not insert on table (channel_participant).")
			}
		} else {
			// Preparing channel_participant update
			updateParticipantChannel, err := c.DB.Prepare(`
				UPDATE channel_participant
				SET withdraw_id = $1, withdraw = $2
				WHERE id = $3
			`)
			if err != nil {
				return errors.Wrap(err, "Could not prepare update statement for table (channel_participant).")
			}

			withdraw, err := json.MarshalIndent(participant.Withdraw, "", "")
			if err != nil {
				return err
			}

			_, err = updateParticipantChannel.Exec(
				participant.WithdrawID,
				withdraw,
				id,
			)
			if err != nil {
				return err
			}
		}

		err = c.updateChannelParticipantBonds(id, participant.Bonds)
		if err != nil {
			return errors.Wrap(err, "Could not update table (channel_participant_bond).")
		}
	}

	return nil
}

func (c *Crawler) insertChannelParticipant(channelID string, participant channelPlayer) (int64, error) {
	// Preparing channel_participant insertion
	insertChannelParticipant, err := c.DB.Prepare(
		`INSERT INTO channel_participant(
			channel_id,
			public_key_hash,
			public_key,
			withdraw_id
		) VALUES($1, $2, $3, $4) RETURNING id`)
	if err != nil {
		return 0, errors.Wrap(err, "Could not prepare insert statement for table (channel_participant).")
	}

	var id int64
	err = insertChannelParticipant.QueryRow(
		channelID,
		participant.PublicKeyHash,
		participant.PublicKey,
		participant.WithdrawID,
	).Scan(&id)

	return id, err
}

func (c *Crawler) updateChannel(channel channel, timestamp time.Time, platformAddress string) error {
	var exists bool
	err := c.DB.QueryRow("SELECT EXISTS(SELECT 1 FROM channel WHERE id = $1)", channel.ID).Scan(&exists)
	if err != nil {
		return errors.Wrap(err, "Could not query table (channel).")
	}

	if exists {
		// Preparing channel update
		updateChannel, err := c.DB.Prepare(`UPDATE channel SET closed = $1, updated_at = $2 WHERE id = $3`)
		if err != nil {
			return errors.Wrap(err, "Could not prepare update statement for table (channel).")
		}

		_, err = updateChannel.Exec(
			channel.Closed,
			timestamp,
			channel.ID,
		)
		if err != nil {
			return errors.Wrap(err, "Could not update table (channel).")
		}
	} else {

		// Preparing channel insertion
		insertChannel, err := c.DB.Prepare(`INSERT INTO channel(
			id,
			platform_id,
			closed,
			nonce,
			withdraw_delay,
			updated_at,
			created_at
		) VALUES($1, $2, $3, $4, $5, $6, $7)`)
		if err != nil {
			return errors.Wrap(err, "Could not prepare insert statement for table (channel).")
		}

		_, err = insertChannel.Exec(
			channel.ID,
			platformAddress,
			channel.Closed,
			channel.Nonce,
			channel.WithdrawDelay,
			timestamp,
			timestamp,
		)
		if err != nil {
			return errors.Wrap(err, "Could not insert on table (channel).")
		}
	}

	return c.updateChannelParticipants(channel.ID, channel.Players)
}

func parseChannel(entry michelsonMapValue) (channel, error) {
	channel := channel{}

	diff, err := unmarshalChannelDiff(entry)
	if err != nil {
		return channel, errors.Errorf("Could not parse channel big_map difference.")
	}
	channel.ID = diff.Key
	channel.Closed = diff.Value.Closed
	channel.Nonce = diff.Value.Nonce
	channel.WithdrawDelay = diff.Value.WithdrawDelay
	channel.Players = make(map[string]channelPlayer)

	for _, playerMap := range diff.Value.Players {
		playerBonds := make([]channelPlayerBond, len(playerMap.Value.Bonds))
		for _, bondMap := range playerMap.Value.Bonds {
			playerBonds = append(playerBonds, channelPlayerBond{
				TokenID: bondMap.Key,
				Amount:  bondMap.Value,
			})
		}

		var withdraw *channelPlayerWithdraw
		if playerMap.Value.Withdraw != nil {
			withdrawChallengeTokens := make(map[int64]int64)
			for _, challengeToken := range playerMap.Value.Withdraw.ChallengeTokens {
				withdrawChallengeTokens[challengeToken.Key] = challengeToken.Value
			}

			withdrawTokens := make(map[int64]int64)
			for _, token := range playerMap.Value.Withdraw.Tokens {
				withdrawTokens[token.Key] = token.Value
			}

			withdraw = &channelPlayerWithdraw{
				Challenge:       playerMap.Value.Withdraw.Challenge,
				ChallengeTokens: withdrawChallengeTokens,
				Timeout:         playerMap.Value.Withdraw.Timeout,
				Tokens:          withdrawTokens,
			}
		}

		channel.Players[playerMap.Key] = channelPlayer{
			Withdraw:      withdraw,
			WithdrawID:    playerMap.Value.WithdrawID,
			PublicKey:     playerMap.Value.PublicKey,
			PublicKeyHash: playerMap.Key,
			Bonds:         playerBonds,
		}
	}

	return channel, nil
}

func unmarshalChannelDiff(entry michelsonMapValue) (bigMapChannelDiff, error) {
	var diff bigMapChannelDiff
	bytes, err := json.Marshal(entry)
	if err != nil {
		return diff, err
	}
	err = json.Unmarshal(bytes, &diff)
	if err != nil {
		return diff, err
	}

	return diff, nil
}
