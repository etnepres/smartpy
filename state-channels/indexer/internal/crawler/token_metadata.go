package crawler

import (
	"encoding/json"
	"time"

	"github.com/pkg/errors"
)

type metadata_type struct {
	ID     int64
	Values map[string]string
}

type bigMapMetadataDiff struct {
	Key   int64 `json:"Key"`
	Value struct {
		TokenID   int64 `json:"token_id"`
		TokenInfo []struct {
			Key   string `json:"Key"`
			Value string `json:"Value"`
		} `json:"token_info"`
	} `json:"Value"`
}

func (c *Crawler) updateMetadata(metadata metadata_type, timestamp time.Time, ledgerAddress string) error {
	var exists bool
	err := c.DB.QueryRow("SELECT EXISTS(SELECT 1 FROM token WHERE id = $1)", metadata.ID).Scan(&exists)
	if err != nil {
		return errors.Wrap(err, "Could not query table (token).")
	}

	values, err := json.MarshalIndent(metadata.Values, "", "")
	if err != nil {
		return err
	}

	if exists {
		// Preparing metadata update
		updateToken, err := c.DB.Prepare(`UPDATE token SET metadata = $1 WHERE id = $2`)
		if err != nil {
			return errors.Wrap(err, "Could not prepare update metadata for table (token).")
		}

		_, err = updateToken.Exec(
			values,
			metadata.ID,
		)
		if err != nil {
			return errors.Wrap(err, "Could not update table (metadata).")
		}
	} else {

		// Preparing metadata insertion
		insertMetadata, err := c.DB.Prepare(`INSERT INTO token(
			id,
			ledger_id,
			metadata,
			updated_at,
			created_at
		) VALUES($1, $2, $3, $4, $5)`)
		if err != nil {
			return errors.Wrap(err, "Could not prepare insert statement for table (metadata).")
		}

		_, err = insertMetadata.Exec(
			metadata.ID,
			ledgerAddress,
			values,
			timestamp,
			timestamp,
		)
		if err != nil {
			return errors.Wrap(err, "Could not insert on table (metadata).")
		}
	}

	return nil
}

func parseMetadata(entry michelsonMapValue) (metadata_type, error) {
	metadata := metadata_type{
		Values: make(map[string]string),
	}
	bigMapDiff, err := unmarshalTokenMetadataDiff(entry)
	if err != nil {
		return metadata, err
	}

	// Get metadata information
	metadata.ID = bigMapDiff.Value.TokenID
	for _, entry := range bigMapDiff.Value.TokenInfo {
		metadata.Values[entry.Key] = entry.Value
	}

	return metadata, nil
}

func unmarshalTokenMetadataDiff(entry michelsonMapValue) (bigMapMetadataDiff, error) {
	var metadataDiff bigMapMetadataDiff
	bytes, err := json.Marshal(entry)
	if err != nil {
		return metadataDiff, err
	}
	err = json.Unmarshal(bytes, &metadataDiff)
	if err != nil {
		return metadataDiff, err
	}

	return metadataDiff, nil
}
