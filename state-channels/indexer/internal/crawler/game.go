package crawler

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/pkg/errors"

	LOG "github.com/romarq/tezplorer/internal/logger"
	Utils "github.com/romarq/tezplorer/pkg/utils"
)

type gameSettlement struct {
	Kind      string          `json:"kind"`
	Value     string          `json:"value"`
	Bonds     map[int64]int64 `json:"bonds"`
	Sender    int64           `json:"sender"`
	Recipient int64           `json:"recipient"`
}

type game_type struct {
	ID         string
	InitParams string
	Settled    bool
	State      string
	Metadata   map[string]string
	Timeouts   map[int64]int64
	// current
	Outcome       json.RawMessage
	MoveNumber    int64
	CurrentPlayer int64
	// constants
	Players     map[string]int64
	Bonds       map[int64]map[int64]int64
	ChannelID   string
	Nonce       string
	ModelID     string
	PlayDelay   int64
	Settlements []gameSettlement
}

type bigMapGameDiff struct {
	Key   string `json:"Key"`
	Value struct {
		Players []struct {
			Key   string `json:"Key"`
			Value int64  `json:"Value"`
		} `json:"addr_players"`
		Constants struct {
			Bonds []struct {
				Key   int64 `json:"Key"`
				Value []struct {
					Key   int64 `json:"Key"`
					Value int64 `json:"Value"`
				} `json:"Value"`
			} `json:"bonds"`
			ChannelID string `json:"channel_id"`
			GameNonce string `json:"game_nonce"`
			ModelID   string `json:"model_id"`
			PlayDelay int64  `json:"play_delay"`
			Players   []struct {
				Key   int64  `json:"Key"`
				Value string `json:"Value"`
			} `json:"players_addr"`
			Settlements []struct {
				Key   map[string]interface{} `json:"Key"`
				Value []struct {
					Bonds []struct {
						Key   int64 `json:"Key"`
						Value int64 `json:"Value"`
					} `json:"bonds"`
					Receiver int64 `json:"receiver"`
					Sender   int64 `json:"sender"`
				} `json:"Value"`
			} `json:"settlements"`
		} `json:"constants"`
		Current struct {
			MoveNumber int64           `json:"move_nb"`
			Outcome    json.RawMessage `json:"outcome,omitempty"`
			Player     int64           `json:"player"`
		} `json:"current"`
		InitParams string `json:"init_input"`
		Metadata   []struct {
			Key   string `json:"Key"`
			Value string `json:"Value"`
		} `json:"metadata,omitempty"`
		Settled  bool   `json:"settled"`
		State    string `json:"state,omitempty"`
		Timeouts []struct {
			Key   int64 `json:"Key"`
			Value int64 `json:"Value"`
		} `json:"timeouts,omitempty"`
	} `json:"Value"`
}

func (c *Crawler) updateGameTimeouts(gameID string, timeouts map[int64]int64, timestamp time.Time) error {
	for id, timeout := range timeouts {
		timeoutTime := time.Unix(timeout, 0).UTC()
		var exists bool
		err := c.DB.QueryRow("SELECT EXISTS(SELECT 1 FROM game_timeout WHERE player_id = $1 and game_id = $2)", id, gameID).Scan(&exists)
		if err != nil {
			return errors.Wrap(err, "Could not query table (game_timeout).")
		}

		LOG.Debug("Processing timeout %d with value %d. (%v)", id, timeout, exists)

		if exists {
			if timeout == 0 {
				if _, err := c.DB.Exec("DELETE FROM game_timeout WHERE player_id = $1 and game_id = $2", id, gameID); err != nil {
					return errors.Wrap(err, "Could not delete row from table (game_timeout).")
				}
			} else {
				// Preparing game update
				updateGame, err := c.DB.Prepare(`
					UPDATE game_timeout SET timeout = $1, updated_at = $2 WHERE player_id = $3 and game_id = $4
				`)
				if err != nil {
					return errors.Wrap(err, "Could not prepare update statement for table (game_timeout).")
				}
				_, err = updateGame.Exec(
					timeoutTime,
					timestamp,
					id,
					gameID,
				)
				if err != nil {
					return errors.Wrap(err, "Could not update table (game_timeout).")
				}
			}
		} else if timeout != 0 {
			// Preparing game insertion
			insertGame, err := c.DB.Prepare(`INSERT INTO game_timeout(
					player_id,
					game_id,
					timeout,
					updated_at,
					created_at
				) VALUES($1, $2, $3, $4, $5)`)
			if err != nil {
				return errors.Wrap(err, "Could not prepare insert statement for table (game_timeout).")
			}

			_, err = insertGame.Exec(
				id,
				gameID,
				timeoutTime,
				timestamp,
				timestamp,
			)
			if err != nil {
				return errors.Wrap(err, "Could not insert on table (game).")
			}
		}
	}

	return nil
}

func (c *Crawler) updateGame(game game_type, timestamp time.Time) error {
	var exists bool
	err := c.DB.QueryRow("SELECT EXISTS(SELECT 1 FROM game WHERE id = $1)", game.ID).Scan(&exists)
	if err != nil {
		return errors.Wrap(err, "Could not query table (game).")
	}

	metadata, err := json.MarshalIndent(game.Metadata, "", "")
	if err != nil {
		return err
	}

	players, err := json.MarshalIndent(game.Players, "", "")
	if err != nil {
		return err
	}

	bonds, err := json.MarshalIndent(game.Bonds, "", "")
	if err != nil {
		return err
	}

	settlements, err := json.MarshalIndent(game.Settlements, "", "")
	if err != nil {
		return err
	}

	if exists {
		// Preparing game update
		updateGame, err := c.DB.Prepare(`
			UPDATE game
			SET settled = $1,
				state = $2,
				metadata = $3,
				updated_at = $4,
				outcome = $5,
				move_nb = $6,
				current_player = $7,
				on_chain = $8,
				players = $9,
				bonds = $10,
				settlements = $11
			WHERE id = $12
		`)
		if err != nil {
			return errors.Wrap(err, "Could not prepare update statement for table (game).")
		}

		_, err = updateGame.Exec(
			game.Settled,
			game.State,
			metadata,
			timestamp,
			game.Outcome,
			game.MoveNumber,
			game.CurrentPlayer,
			true,
			players,
			bonds,
			settlements,
			game.ID,
		)
		if err != nil {
			return errors.Wrap(err, "Could not update table (game).")
		}
	} else {
		// Preparing game insertion
		insertGame, err := c.DB.Prepare(`INSERT INTO game(
			id,
			on_chain,
			init_params,
			settled,
			state,
			metadata,
			updated_at,
			created_at,
			outcome,
			move_nb,
			current_player,
			players,
			bonds,
			channel_id,
			game_nonce,
			model_id,
			play_delay,
			settlements
		) VALUES(
			$1, $2, $3, $4, $5, $6, $7, $8, $9, $10,
			$11, $12, $13, $14, $15, $16, $17, $18
		)`)
		if err != nil {
			return errors.Wrap(err, "Could not prepare insert statement for table (game).")
		}

		_, err = insertGame.Exec(
			game.ID,
			true,
			game.InitParams,
			game.Settled,
			game.State,
			metadata,
			timestamp,
			timestamp,
			game.Outcome,
			game.MoveNumber,
			game.CurrentPlayer,
			players,
			bonds,
			game.ChannelID,
			game.Nonce,
			game.ModelID,
			game.PlayDelay,
			settlements,
		)
		if err != nil {
			return errors.Wrap(err, "Could not insert on table (game).")
		}
	}

	if err = c.updateGameTimeouts(game.ID, game.Timeouts, timestamp); err != nil {
		return errors.Wrap(err, "Could not update game timeouts.")
	}

	return nil
}

func parseGame(entry michelsonMapValue) (game_type, error) {
	game := game_type{
		Metadata: make(map[string]string),
		Players:  make(map[string]int64),
		Bonds:    make(map[int64]map[int64]int64),
		Timeouts: make(map[int64]int64),
	}
	bigMapGameDiff, err := unmarshalGameDiff(entry)
	if err != nil {
		return game, err
	}

	game.ID = bigMapGameDiff.Key
	game.InitParams = bigMapGameDiff.Value.InitParams
	game.Settled = bigMapGameDiff.Value.Settled
	game.State = bigMapGameDiff.Value.State
	for _, entry := range bigMapGameDiff.Value.Metadata {
		game.Metadata[entry.Key] = entry.Value
	}
	// Constants
	for _, entry := range bigMapGameDiff.Value.Constants.Players {
		game.Players[entry.Value] = entry.Key
		game.Timeouts[entry.Key] = 0
	}
	for _, entry := range bigMapGameDiff.Value.Timeouts {
		game.Timeouts[entry.Key] = entry.Value
	}
	for _, entry := range bigMapGameDiff.Value.Constants.Bonds {
		game.Bonds[entry.Key] = make(map[int64]int64)
		for _, value := range entry.Value {
			game.Bonds[entry.Key][value.Key] = value.Value
		}
	}
	// Convert settlement representation
	game.Settlements = make([]gameSettlement, len(bigMapGameDiff.Value.Constants.Settlements))
	for i, entry := range bigMapGameDiff.Value.Constants.Settlements {
		settlement := gameSettlement{}
		LOG.Debug("Settlement (%d): %v", i, Utils.PrettifyJSON(bigMapGameDiff.Value.Constants.Settlements))
		// Extract settlement kind and value
		for k, v := range entry.Key {
			value := ""
			switch x := v.(type) {
			case string:
				value = x
			case int64:
			case int:
			case float64:
				value = fmt.Sprint(x)
			default:
				LOG.Debug("Did not expect settlement value of type %T", x)
			}
			settlement.Kind = k
			settlement.Value = value
		}

		// Extract settlement sender, receiver and bonds
		for _, record := range entry.Value {
			settlement.Sender = record.Sender
			settlement.Recipient = record.Receiver
			// Extract bonds
			settlement.Bonds = make(map[int64]int64)
			for _, v := range record.Bonds {
				settlement.Bonds[v.Key] = v.Value
			}
		}

		game.Settlements[i] = settlement
	}
	game.Nonce = bigMapGameDiff.Value.Constants.GameNonce
	game.ChannelID = bigMapGameDiff.Value.Constants.ChannelID
	game.ModelID = bigMapGameDiff.Value.Constants.ModelID
	game.PlayDelay = bigMapGameDiff.Value.Constants.PlayDelay
	// Current
	game.CurrentPlayer = bigMapGameDiff.Value.Current.Player
	game.MoveNumber = bigMapGameDiff.Value.Current.MoveNumber
	game.Outcome = bigMapGameDiff.Value.Current.Outcome

	return game, nil
}

func unmarshalGameDiff(entry michelsonMapValue) (bigMapGameDiff, error) {
	var gameDiff bigMapGameDiff
	bytes, err := json.Marshal(entry)
	if err != nil {
		return gameDiff, err
	}
	err = json.Unmarshal(bytes, &gameDiff)
	if err != nil {
		return gameDiff, err
	}

	return gameDiff, nil
}
