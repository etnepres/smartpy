package crawler

import (
	"encoding/json"
	"time"

	"github.com/lib/pq"
	"github.com/pkg/errors"
)

type model_type struct {
	ID       string
	Metadata map[string]string
	Outcomes []string
}

type bigMapModelDiff struct {
	Key   string `json:"Key"`
	Value struct {
		Outcomes []string `json:"outcomes"`
	} `json:"Value"`
}

func (c *Crawler) updateModel(model model_type, timestamp time.Time, platformAddress string) error {
	var exists bool
	err := c.DB.QueryRow("SELECT EXISTS(SELECT 1 FROM model WHERE id = $1)", model.ID).Scan(&exists)
	if err != nil {
		return errors.Wrap(err, "Could not query table (model).")
	}

	if exists {
		// Preparing model update
		updateModel, err := c.DB.Prepare(`UPDATE model SET updated_at = $1 WHERE id = $2`)
		if err != nil {
			return errors.Wrap(err, "Could not prepare update statement for table (model).")
		}

		_, err = updateModel.Exec(
			timestamp,
			model.ID,
		)
		if err != nil {
			return errors.Wrap(err, "Could not update table (model).")
		}
	} else {

		// Preparing model insertion
		insertModel, err := c.DB.Prepare(`INSERT INTO model(
			id,
			platform_id,
			outcomes,
			updated_at,
			created_at
		) VALUES($1, $2, $3, $4, $5)`)
		if err != nil {
			return errors.Wrap(err, "Could not prepare insert statement for table (model).")
		}

		_, err = insertModel.Exec(
			model.ID,
			platformAddress,
			pq.Array(model.Outcomes),
			timestamp,
			timestamp,
		)
		if err != nil {
			return errors.Wrap(err, "Could not insert on table (model).")
		}
	}

	return nil
}

func (c *Crawler) updateModelMetadata(diff michelsonMapValue, timestamp time.Time) error {
	metadata, err := parseModelMetadata(diff)
	if err != nil {
		return err
	}

	for key, value := range metadata {
		var exists bool
		err := c.DB.QueryRow("SELECT EXISTS(SELECT 1 FROM model WHERE id = $1)", key).Scan(&exists)
		if err != nil {
			return errors.Wrap(err, "Could not query table (model).")
		}

		if exists {
			// Preparing model update
			updateModel, err := c.DB.Prepare(`UPDATE model SET metadata = $1, updated_at = $2 WHERE id = $3`)
			if err != nil {
				return errors.Wrap(err, "Could not prepare update statement for table (model).")
			}

			metadataJSON, err := json.MarshalIndent(value, "", "")
			if err != nil {
				return err
			}

			_, err = updateModel.Exec(
				metadataJSON,
				timestamp,
				key,
			)
			if err != nil {
				return errors.Wrap(err, "Could not update table (model).")
			}
		}
	}

	return nil
}

func parseModelMetadata(diff michelsonMapValue) (map[string]map[string]string, error) {
	metadata := make(map[string]map[string]string)

	var metadataDiff struct {
		Key   string `json:"Key"`
		Value []struct {
			Key   string `json:"Key"`
			Value string `json:"Value"`
		}
	}
	bytes, err := json.Marshal(diff)
	if err != nil {
		return metadata, err
	}
	err = json.Unmarshal(bytes, &metadataDiff)
	if err != nil {
		return metadata, err
	}

	metadata[metadataDiff.Key] = make(map[string]string)

	for _, entry := range metadataDiff.Value {
		metadata[metadataDiff.Key][entry.Key] = entry.Value
	}

	return metadata, nil
}

func parseModel(entry michelsonMapValue) (model_type, error) {
	model := model_type{}
	bigMapModelDiff, err := unmarshalModelDiff(entry)
	if err != nil {
		return model, err
	}

	// Get model information
	model.ID = bigMapModelDiff.Key
	model.Outcomes = bigMapModelDiff.Value.Outcomes
	return model, nil
}

func unmarshalModelDiff(entry michelsonMapValue) (bigMapModelDiff, error) {
	var modelDiff bigMapModelDiff
	bytes, err := json.Marshal(entry)
	if err != nil {
		return modelDiff, err
	}
	err = json.Unmarshal(bytes, &modelDiff)
	if err != nil {
		return modelDiff, err
	}

	return modelDiff, nil
}
