package RPC

import (
	"context"
	"fmt"

	"blockwatch.cc/tzgo/micheline"
	"blockwatch.cc/tzgo/rpc"
	"blockwatch.cc/tzgo/tezos"
)

type ChainIdHash = tezos.ChainIdHash
type BlockHash = tezos.BlockHash
type Block = rpc.Block
type BlockHeader = rpc.BlockHeader

type Origination = rpc.OriginationOp
type Transaction = rpc.TransactionOp
type InternalResult = rpc.InternalResult

var ParseHash = tezos.ParseHash
var OpTypeTransaction = tezos.OpTypeTransaction
var OpTypeOrigination = tezos.OpTypeOrigination

// RPC struct
type RPC struct {
	rpc *rpc.Client
	ctx context.Context
}

// New Instance
func New(rpcURL string) (*RPC, error) {
	rpc, err := rpc.NewClient(rpcURL, nil)
	if err != nil {
		return nil, err
	}

	return &RPC{
		rpc: rpc,
		ctx: context.TODO(),
	}, nil
}

func (r *RPC) GetHeadBlock() (*rpc.Block, error) {
	var head Block
	u := fmt.Sprintf("chains/%s/blocks/head", r.rpc.ChainID)
	if err := r.rpc.Get(r.ctx, u, &head); err != nil {
		return nil, err
	}
	return &head, nil
}

func (r *RPC) GetHeadBlockHeader() (*rpc.BlockHeader, error) {
	return r.rpc.GetTipHeader(r.ctx)
}

func (r *RPC) GetBlockByLevel(level int64) (*rpc.Block, error) {
	return r.rpc.GetBlockHeight(r.ctx, level)
}

func (r *RPC) GetBlockHeaderByLevel(level int64) (*rpc.BlockHeader, error) {
	return r.rpc.GetBlockHeader(r.ctx, level)
}

func (r *RPC) GetContractScriptAtLevel(address string, level int64) (*micheline.Script, error) {
	addr := tezos.MustParseAddress(address)
	u := fmt.Sprintf("chains/%s/blocks/%d/context/contracts/%s/script", r.rpc.ChainID, level, addr)
	s := micheline.NewScript()
	err := r.rpc.Get(r.ctx, u, s)
	if err != nil {
		return nil, err
	}
	return s, nil
}

func (r *RPC) GetBigmapInfo(id int64) (*rpc.BigmapInfo, error) {
	return r.rpc.GetBigmapInfo(r.ctx, id)
}
