#!/bin/sh

BASE_DIR=$(realpath $(dirname $0))

DB_NAME=tezplorer
DB_SCHEMA_DIR=$BASE_DIR/../data/schema

# Clear old schema
psql -h localhost -d $DB_NAME -U postgres -p 5432 <<EOF
DROP SCHEMA public CASCADE;
CREATE SCHEMA public;
EOF

for file in $(ls $DB_SCHEMA_DIR); do
    path="$DB_SCHEMA_DIR/$file"
    if [[ -f $path ]]; then
        # Create schema
        psql -h localhost -d $DB_NAME -U postgres -p 5432 -a -q -f $path 1>/dev/null
    fi
done;
